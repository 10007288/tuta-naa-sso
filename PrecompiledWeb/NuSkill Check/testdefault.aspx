<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site.master" autoeventwireup="true" inherits="testdefault, App_Web_i3xfoyb2" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <asp:Panel ID="pnlUpdate" runat="server">
                        <asp:UpdateProgress ID="udpProgress" runat="server">
                            <ProgressTemplate>
                                <asp:Panel ID="pnlBuffer" runat="server">
                                    <table>
                                        <tr>
                                            <td align="center" valign="middle">
                                                <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:Panel>
                </td>
            </tr>
            <tr style="height: 80px; vertical-align: middle">
                <td align="center" style="vertical-align: middle; font-size: large; 
                    padding: 0px 10px 0px 10px">
                    <asp:Label ID="lblExamName" runat="server" CssClass="testQuestion" />
                </td>
            </tr>
            <tr>
                <td align="center" >
                    <asp:Label ID="lblInstructions" runat="server" Text="Instructions:" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td align="left"  id="tdInstructions" runat="server">
                    <asp:Label ID="lblInstructionValue" runat="server" Width="90%" />
                </td>
            </tr>
            <tr style="height: 30px">
                <td align="center" style="vertical-align: middle; font-size: large; ">
                    <table width="90%" cellpadding="5" style="vertical-align: middle; 
                        font-size: small;">
                        <tr>
                            <td align="center" style="padding: 10px 10px 10px 10px">
                                <asp:Label ID="lblAbout" runat="server" Text="Click the Begin button to start the exam/survey." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 20px">
                <td align="right">
                    <asp:Label ID="lblDuration" runat="server" Text="Duration:" />
                    <asp:Label ID="lblDurationVal" runat="server" />
                </td>
            </tr>
            <tr style="height: 40px">
                <td align="right" valign="middle" style="">
                    <asp:Button ID="btnContinue" runat="server" Text="Begin" OnClick="btnContinue_Click"
                        Width="80px" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
