<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site.master" autoeventwireup="true" inherits="takentests, App_Web_i3xfoyb2" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Width="100%">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblAllAvailableTests" runat="server" Text="All Taken Tests" Font-Size="large"
                        Font-Names="Arial" /><br />
                    <hr style="color: black" />
                </td>
            </tr>
            <tr>
                <td>
                   <!-- <asp:UpdatePanel ID="up" runat="server" UpdateMode="Conditional">
                        <ContentTemplate> -->
                            <asp:GridView ID="gvRecentTests" runat="server" AutoGenerateColumns="false" AllowPaging="False"
                                Width="100%" EmptyDataText="There are no available exams." CellPadding="5" BorderWidth="1"
                                BorderColor="black" OnRowCommand="gvRecentTests_RowCommand"
                                AllowSorting="true" OnSorting="gv_Sorting">
                                <PagerSettings Visible="false" />
                                <HeaderStyle HorizontalAlign="center" Font-Names="Arial" Font-Size="12px" />
                                <RowStyle Font-Names="Arial" Font-Size="12px" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Exam ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exam Name" SortExpression= "TestName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Started" SortExpression= "DateStartTaken" >
                                        <ItemStyle Width="140px" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("DateStartTaken", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Finished" SortExpression= "DateEndTaken">
                                        <ItemStyle Width="140px" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("DateEndTaken", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemStyle HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="center">
                                        <ItemStyle Width="80px" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateLastSave" runat="server" Text='<%#Bind("DateLastSave") %>'
                                                Visible="false" />
                                            <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' Visible="false" />
                                            <asp:Label ID="lblResponseCount" runat="server" Text='<%#Bind("ResponseCount") %>'
                                                Visible="false" />
                                            <asp:Label ID="lblHideScores" runat="server" Text='<%#Bind("HideScores") %>' Visible="false" />
                                            <asp:Image ID="imgTestStatus" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="middle" Width="25" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewAnswers" runat="server" Text="View" Font-Bold="true" ForeColor="black"
                                                CommandName="View" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                       <!-- </ContentTemplate>
                    </asp:UpdatePanel> -->
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle">
                    <asp:Image ID="imgPass" runat="server" ImageUrl="~/images/icon-pass.gif" /><asp:Label
                        ID="lblPass" runat="server" Text=" = Pass;" />
                    <asp:Image ID="imgFail" runat="server" ImageUrl="~/images/icon-fail.gif" /><asp:Label
                        ID="lblFail" runat="server" Text=" = Fail;" />
                    <asp:Image ID="imgPending" runat="server" ImageUrl="~/images/icon-pending.gif" /><asp:Label
                        ID="lblPending" runat="server" Text=" = Pending" />
                    <asp:Image ID="imgResume" runat="server" ImageUrl="~/images/icon-resume.gif" /><asp:Label
                        ID="lblResume" runat="server" Text=" = Resumable" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle">
                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click"
                        CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeMessage" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidMessage" PopupControlID="pnlMessage" CancelControlID="btnMessage" />
    <asp:HiddenField ID="hidMessage" runat="server" />
    <asp:Panel ID="pnlMessage" runat="server" CssClass="modalPopup" HorizontalAlign="center"
        Width="200px">
        <table width="100%" cellpadding="15" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnMessage" runat="server" Text="Return" OnClick="btnMessage_Click"
                        CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeAnswers" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidAnswers" PopupControlID="pnlAnswers" />
    <asp:HiddenField ID="hidAnswers" runat="server" />
    <asp:Panel ID="pnlAnswers" runat="server" CssClass="modalPopup" ScrollBars="vertical"
        Width="680px" Height="500px">
        <table cellpadding="10" cellspacing="0" style="background-color: White;" width="660px">
            <%--background-color: #C1D7E7;">--%>
            <%--<tr>
                <td colspan="2">
                    <asp:Label ID="lblResultsFor" runat="server" Text="Results for Exam:" />
                </td>
            </tr>--%>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblResultsExamName" runat="server" Font-Size="large" />
                </td>
            </tr>
            <tr>
                <td style="width: 50%">
                    <asp:Label ID="lblDateStarted" runat="server" Text="Date Started:" />
                    <br />
                    <asp:Label ID="lblDateStartedVal" runat="server" Font-Bold="true" />
                </td>
                <td style="width: 50%">
                    <asp:Label ID="lblDateFinished" runat="server" Text="Date Finished:" />
                    <br />
                    <asp:Label ID="lblDateFinishedVal" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr style="color: Black; width: 100%" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvExamResults" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                        AllowSorting="false" CellPadding="5" EmptyDataText="This exam has no answers."
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Question">
                                <ItemStyle HorizontalAlign="left" Width="20%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblTestResponseID" runat="server" Text='<%#Bind("TestResponseID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblQuestion" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Your Answer">
                                <ItemStyle HorizontalAlign="left" Width="20%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblYourAnswer" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Checker Comments">
                                <ItemStyle HorizontalAlign="left" Width="50%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblCheckerComments" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                                <ItemTemplate>
                                    <asp:Image ID="imgResult" runat="server" />
                                    <asp:Label ID="lblEssayScore" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnCloseSummary" runat="server" Text="Back" OnClick="btnCloseSummary_Click"
                        CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
