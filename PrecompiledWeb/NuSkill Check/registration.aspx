<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site.master" autoeventwireup="true" inherits="registration, App_Web_w1c0ke3z" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Width="100%">
        <table cellpadding="5" >
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblRequired" runat="server" Text="*All fields are required." CssClass="warning" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblLastName" runat="server" Text="Last Name:" />
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" />
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvLastName" runat="server" ErrorMessage="Last Name is required."
                        CssClass="errorline" Display="static" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblFirstName" runat="server" Text="First Name:" />
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" />
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvFirstName" runat="server" ErrorMessage="First Name is required."
                        CssClass="errorline" Display="static" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblPhoneNumber" runat="server" Text="Phone Number:" />
                </td>
                <td>
                    <asp:TextBox ID="txtPhoneNumber" runat="server" MaxLength="20" />
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvPhoneNumber" runat="server" ErrorMessage="Phone Number is required."
                        CssClass="errorline" Display="static" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblLocation" runat="server" Text="Location:" />
                </td>
                <td>
                    <asp:TextBox ID="txtLocation" runat="server" MaxLength="50" />
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvLocation" runat="server" ErrorMessage="Location is required."
                        CssClass="errorline" Display="static" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="buttons" OnClick="btnRegister_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeFinished" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidFinished" PopupControlID="pnlFinished" />
    <asp:HiddenField ID="hidFinished" runat="server" />
    <asp:Panel ID="pnlFinished" runat="server" Width="300px" CssClass="modalPopup" HorizontalAlign="center">
        <table width="100%" cellpadding="3">
            <tr>
                <td>
                    <asp:Label ID="lblResult" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="buttons" OnClick="btnReturn_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
