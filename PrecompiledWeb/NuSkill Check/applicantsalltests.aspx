<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site_applicant.master" autoeventwireup="true" inherits="applicantsalltests, App_Web_pgeoefhq" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Width="100%">
        <table cellpadding="10"cellspacing="0" style="text-align: left" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblAllAvailableTests" runat="server" Text="Take Tests" Font-Size="large"
                        Font-Names="Arial" /><br />
                    <hr style="color: Black" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvAvailableTests" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                        Width="100%" EmptyDataText="There are no available exams." CellPadding="3" BorderWidth="1"
                        OnRowCommand="gvAvailableTests_RowCommand" BorderColor="black">
                        <PagerSettings Visible="false" />
                        <HeaderStyle HorizontalAlign="center" Font-Names="Arial" Font-Size="12px" />
                        <RowStyle Font-Names="Arial" Font-Size="12px" />
                        <Columns>
                            <asp:TemplateField HeaderText="Exam Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Start Date">
                                <ItemStyle Width="140px" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("StartDate", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Date">
                                <ItemStyle Width="140px" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("EndDate", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tries Left">
                                <ItemStyle Width="80px" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblTriesLeft" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Top Score">
                                <ItemStyle Width="80px" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblTopScore" runat="server" Text='<%#Bind("TopScore") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Passed">
                                <ItemStyle Width="80px" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPassingScore" runat="server" Visible="false" Text='<%#Bind("PassingScore") %>' />
                                    <asp:Label ID="lblPassed" runat="server" Visible="false" Text='<%#Bind("Passed") %>' />
                                    <asp:Image ID="imgPassOrFail" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Attempts (Made/Allowed)">
                                <ItemStyle Width="100px" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblTries" runat="server" Text='<%#Bind("TriesMade") %>' />
                                    <asp:Label ID="lblSlash" runat="server" Text="/" />
                                    <asp:Label ID="lblMaxTries" runat="server" Text='<%#Bind("MaxTries") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="center" VerticalAlign="middle" Width="25" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkTake" runat="server" Text="Take" Font-Bold="true" ForeColor="black"
                                        CommandName="Take" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeMessage" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidMessage" PopupControlID="pnlMessage" CancelControlID="btnMessage" />
    <asp:HiddenField ID="hidMessage" runat="server" />
    <asp:Panel ID="pnlMessage" runat="server" CssClass="modalPopup" HorizontalAlign="center"
        Width="200px">
        <table width="100%" cellpadding="15" cellspacing="0" style="border-style: solid;
            border-color: black;">
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnMessage" runat="server" Text="Return" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
