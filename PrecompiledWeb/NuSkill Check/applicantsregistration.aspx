<%@ page language="C#" masterpagefile="MasterPage.master" autoeventwireup="true" inherits="applicantsregistration, App_Web_vsxsz0gr" title="Transcom University Testing" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <div class="container">
            <asp:Panel runat="server" DefaultButton="btnRegister">
                <table width="100%" class="tabledefault">
                    <tr>
                        <td class="tblLabel">
                            <br />
                        </td>
                        <td class="tblfield">
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                        </td>
                        <td class="tblfield">
                            <asp:LinkButton ID="lnkLogin" runat="server" Text="<< Back to Login page" CssClass="linkbutton"
                                OnClick="lnkLogin_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                        </td>
                        <td class="tblfield">
                            <br />
                            <br />
                            <asp:Label ID="lblRequired" runat="server" Text="*All fields are required." CssClass="warning"
                                ForeColor="red" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                            ATS ID
                        </td>
                        <td class="tblfield">
                            <asp:TextBox ID="txtATSID" runat="server" />
                            <asp:CustomValidator ID="cvATSID" runat="server" ErrorMessage="*" CssClass="errorline"
                                Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                            First Name
                        </td>
                        <td class="tblfield">
                            <asp:TextBox ID="txtFirstName" runat="server" />
                            <asp:CustomValidator ID="cvFirstName" runat="server" ErrorMessage="*" CssClass="errorline"
                                Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                            Last Name
                        </td>
                        <td class="tblfield">
                            <asp:TextBox ID="txtLastName" runat="server" />
                            <asp:CustomValidator ID="cvLastName" runat="server" ErrorMessage="*" CssClass="errorline"
                                Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                            Site
                        </td>
                        <td class="tblfield">
                            <asp:DropDownList ID="ddlSites" runat="server" DataTextField="CompanySite" DataValueField="CompanySiteShort"
                                AppendDataBoundItems="true">
                                <asp:ListItem Text="- select -" Value="" />
                                <asp:ListItem Text="Manila 1 - Pasig" Value="Manila 1 - Pasig" />
                                <asp:ListItem Text="Manila 2 - EDSA" Value="Manila 2 - EDSA" />
                                <asp:ListItem Text="ILOILO" Value="ILOILO" />
                                <asp:ListItem Text="BACOLOD" Value="BACOLOD" />
                                <asp:ListItem Text="North America" Value="North America" />
                            </asp:DropDownList>
                            <asp:CustomValidator ID="cvLocation" runat="server" ErrorMessage="*" CssClass="errorline"
                                Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                            <br />
                            <br />
                        </td>
                        <td class="tblfield">
                            <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="buttons" OnClick="btnRegister_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tblLabel">
                            <br />
                        </td>
                        <td class="tblfield">
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    <ajax:ModalPopupExtender ID="mpeFinished" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidFinished" PopupControlID="pnlFinished" />
    <asp:HiddenField ID="hidFinished" runat="server" />
    <asp:Panel ID="pnlFinished" runat="server" Width="300px" CssClass="modalPopup" HorizontalAlign="center">
        <table width="100%" cellpadding="3">
            <tr>
                <td>
                    <asp:Label ID="lblResult" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnReturn" runat="server" Text="Return to Login" CssClass="buttons"
                        OnClick="btnReturn_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
