<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site_applicant.master" autoeventwireup="true" inherits="testend, App_Web_i3xfoyb2" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Width="100%">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td align="center" style="vertical-align: middle; font-size: large; 
                    padding: 20px 10px 10px 10px">
                    <asp:Label ID="lblExamName" runat="server" CssClass="testQuestion" />
                </td>
            </tr>
            <tr>
                <td align="center" style="vertical-align: middle; font-size: large; 
                    height: 400px">
                    <table width="90%" cellpadding="5" style="vertical-align: middle; 
                        font-size: small;">
                        <tr>
                            <td align="center" style="padding: 30px 10px 10px 10px">
                                <asp:Label ID="lblAbout" runat="server" Text="Click the rate button to grade your exam." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" valign="middle">
                    <asp:Button ID="btnContinue" runat="server" Text="Rate" OnClick="btnContinue_Click"
                        Width="80px" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
