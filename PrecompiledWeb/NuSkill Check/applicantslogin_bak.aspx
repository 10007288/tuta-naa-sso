<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site_applicant.master" autoeventwireup="true" inherits="applicantslogin_bak, App_Web_w1c0ke3z" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center">
        <asp:UpdatePanel ID="udpSub" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUpdate" runat="server" Height="80px" HorizontalAlign="Center">
                    <asp:UpdateProgress ID="udpProgress" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="pnlBuffer" runat="server" Height="80px" HorizontalAlign="Center">
                                <table align="center">
                                    <tr>
                                        <td align="center" style="height: 80px">
                                            <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" ImageAlign="Middle" />
                                            <br />
                                            <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:Panel>
                <asp:Panel ID="pnlMain" runat="server" DefaultButton="lnkLogin" HorizontalAlign="center"
                    CssClass="panelStandard" BorderWidth="0px">
                    <table align="center" style="border: 1px solid black; padding: 20px">
                        <tr>
                            <td colspan="3" align="left">
                                <asp:CustomValidator ID="cvLoginError" runat="server" ErrorMessage="Invalid Login Credentials!"
                                    Display="dynamic" CssClass="validatorStyle" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label>
                                    ATS ID:</label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtATSID" runat="server" Width="120px" />
                            </td>
                            <td align="left">
                                <asp:CustomValidator ID="cvATSID" runat="server" ErrorMessage="ATS ID is required."
                                    CssClass="errorline" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label>
                                    First Name:</label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFirstName" runat="server" Width="120px" />
                            </td>
                            <td align="left">
                                <asp:CustomValidator ID="cvFirstName" runat="server" ErrorMessage="First Name is required."
                                    CssClass="errorline" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label>
                                    Last Name:</label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" runat="server" Width="120px" />
                            </td>
                            <td align="left">
                                <asp:CustomValidator ID="cvLastName" runat="server" ErrorMessage="Last Name is required."
                                    CssClass="errorline" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSite" runat="server" Text="Site:" />
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlSites" runat="server" DataTextField="CompanySite" DataValueField="CompanySiteShort"
                                    AppendDataBoundItems="true" Width="120">
                                    <asp:ListItem Text="- select -" Value="" />
                                    <asp:ListItem Text="Manila 1 - Pasig" Value="Manila 1 - Pasig" />
                                    <asp:ListItem Text="Manila 2 - EDSA" Value="Manila 2 - EDSA" />
                                    <asp:ListItem Text="ILOILO" Value="ILOILO" />
                                    <asp:ListItem Text="BACOLOD" Value="BACOLOD" />
                                    <asp:ListItem Text="NA" Value="North America" />
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:CustomValidator ID="cvLocation" runat="server" ErrorMessage="Site is required."
                                    CssClass="errorline" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <br />
                            </td>
                            <td align="left" colspan="2">
                                <asp:Button ID="lnkLogin" runat="server" Text="Login" CssClass="buttons" OnClick="lnkLogin_Click" />
                                <%--<asp:LinkButton ID="lnkLogin" runat="server" Text="Login" CssClass="linkbutton" OnClick="lnkLogin_Click" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left" colspan="2">
                                <label>
                                    or</label>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left" colspan="2">
                                <asp:LinkButton ID="lnkRegister" runat="server" Text="Register" CssClass="linkbutton"
                                    OnClick="lnkRegister_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
