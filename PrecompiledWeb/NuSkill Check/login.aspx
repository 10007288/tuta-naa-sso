<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site.master" autoeventwireup="true" inherits="login, App_Web_i3xfoyb2" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center">
        <asp:UpdatePanel ID="udpSub" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUpdate" runat="server" Height="150px">
                    <asp:UpdateProgress ID="udpProgress" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="pnlBuffer" runat="server" Height="150px">
                                <table align="center">
                                    <tr>
                                        <td align="center" style="height: 200px">
                                            <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                            <br />
                                            <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:Panel>
                <asp:Panel ID="pnlMain" runat="server" DefaultButton="lnkLogin" HorizontalAlign="center"
                    CssClass="panelStandard" BorderWidth="0px">
                    <table align="center">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:CustomValidator ID="cvLoginError" runat="server" ErrorMessage="Invalid Login Credentials."
                                    Display="dynamic" CssClass="validatorStyle" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblUsername" runat="server" Text="Username" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUsername" runat="server" Width="120px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblPassword" runat="server" Text="Password" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtPassword" runat="server" Width="120px" TextMode="password" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" CssClass="linkbutton" OnClick="lnkLogin_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblOr" runat="server" Text="- or -" Visible="false" />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:LinkButton ID="lnkRegister" runat="server" Text="Register" CssClass="linkbutton"
                                    OnClick="lnkRegister_Click" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label>
                                    or</label>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:HyperLink ID="LinkButton1" runat="server" Text="Click here if Applicant" NavigateUrl="~/applicantslogin.aspx"
                                    CssClass="linkbutton" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
