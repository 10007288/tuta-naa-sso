﻿<%@ WebHandler Language="C#" Class="questionimage" %>

using System;
using System.Configuration;
using System.Web;
using System.IO;
using System.Data;
using System.Data.SqlClient;

public class questionimage : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
  {
        try
        {
        Int32 questionId;
        if (context.Request.QueryString["id"] != null)
            questionId = Convert.ToInt32(context.Request.QueryString["id"]);
        else
            throw new ArgumentException("No parameter specified");

        context.Response.ContentType = "image/jpeg";
        Stream strm = ShowEmpImage(questionId);
        byte[] buffer = new byte[4096];
        int byteSeq = strm.Read(buffer, 0, 4096);

        while (byteSeq > 0)
        {
            context.Response.OutputStream.Write(buffer, 0, byteSeq);
            byteSeq = strm.Read(buffer, 0, 4096);
        }
        }
        catch (Exception)
        {
        }
    }

    public Stream ShowEmpImage(int empno)
    {
        string conn = ConfigurationManager.ConnectionStrings["NuSkillCheck"].ConnectionString;
        SqlConnection connection = new SqlConnection(conn);
        string sql = "SELECT [QuestionImg] FROM [tbl_testing_questionnaire] WHERE [QuestionnaireID] = @QuestionnaireID";
        SqlCommand cmd = new SqlCommand(sql, connection);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@QuestionnaireID", empno);
        connection.Open();
        object img = cmd.ExecuteScalar();
        try
        {
            return new MemoryStream((byte[])img);
        }
        catch
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}