<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site_applicant.master" autoeventwireup="true" inherits="applicantsgradeexam, App_Web_i3xfoyb2" title="Transcom University Testing" enableeventvalidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Font-Names="Arial" HorizontalAlign="left"
        Width="100%">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="lblExamResults" runat="server" Text="Exam Results" CssClass="testQuestion" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr style="color: black" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblResult" runat="server" CssClass="testQuestion" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="false" Width="80%"
                        CellPadding="5" AllowPaging="false" AllowSorting="false" EmptyDataText="No items found."
                        BorderColor="black">
                        <Columns>
                            <asp:TemplateField HeaderText="Question" HeaderStyle-HorizontalAlign="center">
                                <ItemStyle Width="90%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestionName" runat="server" Text='<%#Bind("Question") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Correct" HeaderStyle-HorizontalAlign="center">
                                <ItemStyle Width="10%" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Image ID="imgTestStatus" runat="server" ImageUrl='<%#Bind("ImageUrl") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 20%">
                    <asp:Label ID="lblTotal" runat="server" Text="Total Items:" />
                </td>
                <td style="width: 80%">
                    <asp:Label ID="lblTotalValue" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCorrect" runat="server" Text="Correct Answers:" />
                </td>
                <td>
                    <asp:Label ID="lblCorrectValue" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPassing" runat="server" Text="Score Needed to Pass:" />
                </td>
                <td>
                    <asp:Label ID="lblPassingValue" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTries" runat="server" Text="Tries Used:" />
                </td>
                <td>
                    <asp:Label ID="lblTriesValue" runat="server" Font-Bold="true" />
                </td>
            </tr>
           <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
            <tr>
                <td>
                    <asp:Label ID="lblRate" runat="server" Text="Rate:" />
                </td>
                <td>
                    <asp:Label ID="lblRateValue" runat="server" Font-Bold="true" />
                </td>
            </tr>
           <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
            <tr>
                <td>
                    <asp:Label ID="lblComments" runat="server" Text="Comments:" />
                </td>
                <td>
                    <asp:Label ID="lblCommentsValue" runat="server" Font-Bold="true" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="pnlThanks" runat="server" Font-Names="Arial" HorizontalAlign="left"
        Width="100%" Visible="false">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="lblThanks" runat="server" CssClass="testQuestion" Text="Thank you for taking the exam." />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
    <asp:Panel ID="pnlThanksSurvey" runat="server" Font-Names="Arial" HorizontalAlign="left"
        Width="100%" Visible="false">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Thank you for taking the time to provide us with your feedback." />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="We will incorporate your suggestions into upcoming sessions." />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlBackButton" runat="server" Font-Names="Arial" HorizontalAlign="left"
        Width="100%">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="btnRetry" runat="server" Text="Retake" Width="80px" OnClick="btnRetry_Click"
                        CssClass="buttons" Visible="False" />
                    <asp:Button ID="btnContinue" runat="server" Text="Return" Width="80px" OnClick="btnContinue_Click"
                        CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="TestTakenID" runat="server" />
    <asp:HiddenField ID="TestCategoryID" runat="server" />
    <ajax:ModalPopupExtender ID="mpeMessage" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidMessage" PopupControlID="pnlMessage" CancelControlID="btnMessage" />
    <asp:HiddenField ID="hidMessage" runat="server" />
    <asp:Panel ID="pnlMessage" runat="server" CssClass="modalPopup" HorizontalAlign="center"
        Width="400px">
        <table width="100%" cellpadding="15" cellspacing="0">
            <tr>
                <td style="width: 392px">
                    <asp:Label ID="lblMessage" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 392px">
                    <asp:Button ID="btnMessage" runat="server" Text="Return" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeRetake" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidRetake" PopupControlID="pnlRetake" CancelControlID="btnCancelRetake" />
    <asp:HiddenField ID="hidRetake" runat="server" />
    <asp:Panel ID="pnlRetake" runat="server" CssClass="modalPopup" HorizontalAlign="center"
        Width="400px">
        <table width="100%" cellpadding="15" cellspacing="0">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblRetake" runat="server" Text="You have already taken and passed this test." />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnCancelRetake" runat="server" Text="Cancel" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
