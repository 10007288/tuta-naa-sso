<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="essaytests.aspx.cs" Inherits="essaytests" Title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblEssays" runat="server" Text="Essays" ForeColor="black" Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td align="left">
                                <asp:LinkButton ID="lnkGoToBySingleItem" runat="server" Text="By Single Item" Font-Size="10"
                                    ForeColor="black" Font-Names="Arial" Font-Bold="true" OnClick="lnkGoToBySingleItem_Click" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:Label ID="lblUnchecked" runat="server" Text="Unchecked Essays" ForeColor="black" Font-Size="12" /><br />
                                <hr style="color: Black; width: 100%" /><br />
                                <asp:Label ID="lblFilterByUser" runat="server" Text="Choose Exam:" />
                                <asp:DropDownList ID="ddlExams" runat="server" DataValueField="TestCategoryID" DataTextField="TestName" />
                                <asp:Button ID="btnExams" runat="server" Text="Show" CssClass="buttons" OnClick="btnExams_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="gvEssays" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    PageSize="20" CellPadding="5" BorderColor="black" Font-Names="Arial" Font-Size="12px"
                                    EmptyDataText="There are no pending essays." Width="100%" OnPageIndexChanging="gvEssays_PageIndexChanging"
                                    OnSelectedIndexChanging="gvEssays_SelectedIndexChanging">
                                    <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                                    <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                                        NextPageText=">" Position="Bottom" PreviousPageText="<" />
                                    <RowStyle BorderColor="black" />
                                    <RowStyle ForeColor="black" HorizontalAlign="left" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="CIM">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsername" runat="server" Text='<%#Bind("Username") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FirstName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("FirstName") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Test Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                    Visible="false" />
                                                <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                                <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle ForeColor="black" Font-Bold="true" HorizontalAlign="center" Width="60px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkView" runat="server" Text="View" CommandName="Select" CssClass="lbuttons" ForeColor="black" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
<%--                                <br/>
                                <asp:Label ID="lblExportCSV" runat="server" Text="Export To CSV:" Visible="false" />
                                <asp:Button ID="btnExportCSV" runat="server" Text="Export" CssClass="buttons" OnClick="btnExportCSV_Click" Visible="false" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblExisting" runat="server" Text="Checked Essays" ForeColor="black" Font-Size="12" /><br />
                                <hr style="color: Black; width: 100%" /><br />
                                <asp:Label ID="lblUserID" runat="server" Text="Search User ID:" />
                                <asp:TextBox ID="txtUserID" runat="server" />
                                <asp:Button ID="btnUserID" runat="server" Text="Search" CssClass="buttons" OnClick="btnUserID_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="gvChecked" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    PageSize="20" CellPadding="5" BorderColor="black" Font-Names="Arial" Font-Size="12px"
                                    EmptyDataText="No essay exams taken on record." Width="100%" OnPageIndexChanging="gvChecked_PageIndexChanging"
                                    OnSelectedIndexChanging="gvChecked_SelectedIndexChanging">
                                    <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                                    <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                                        NextPageText=">" Position="Bottom" PreviousPageText="<" />
                                    <RowStyle BorderColor="black" />
                                    <RowStyle ForeColor="black" HorizontalAlign="left" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Username">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsername" runat="server" Text='<%#Bind("Username") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Test Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                    Visible="false" />
                                                <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                                <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UserID" HeaderText="Checked By" ItemStyle-Width="80px" />
                                        <asp:TemplateField>
                                            <ItemStyle ForeColor="black" Font-Bold="true" HorizontalAlign="center" Width="60px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkView" runat="server" Text="Recheck" CommandName="Select" CssClass="lbuttons" ForeColor="black" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
