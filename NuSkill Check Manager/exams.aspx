<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="exams.aspx.cs" Inherits="exams" Title="Transcom University Testing Admin" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="wus" TagName="examstatuscontrol" Src="~/controls/examstatuscontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <script language="javascript" type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlExam" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" cellspacing="0" width="100%" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblExams" runat="server" Text="Exams" ForeColor="black" Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ajax:Accordion ID="accExamsSearch" runat="server" Width="100%" RequireOpenedPane="false"
                                    AutoSize="none" SelectedIndex="-1">
                                    <Panes>
                                        <ajax:AccordionPane ID="accPain1" runat="server">
                                            <Header>
                                                <asp:Label ID="lblShowHide" runat="server" Text="Show/Hide Search menu..." />
                                            </Header>
                                            <Content>
                                                <table cellpadding="10" cellspacing="0" width="100%" style="font-family: Arial; text-align: left">
                                                    <tr>
                                                        <td valign="middle" align="left">
                                                            <asp:Label ID="Label1" runat="server" Text="Search by" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">
                                                            <asp:Label ID="lblAccountCampaign" runat="server" Text="Test Category:" CssClass="Indent" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAccountCampaign" runat="server" DataTextField="Campaign"
                                                                DataValueField="CampaignID" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlAccountCampaign_SelectedIndexChanged">
                                                                <asp:ListItem Text="View All" Value="0" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">
                                                            <asp:Label ID="lblSubCategory" runat="server" Text="Subcategory:" CssClass="Indent" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSubCategory" runat="server" DataTextField="Campaign" DataValueField="CampaignID" />
                                                        </td>
                                                    </tr>
                                                    <tr id="trExamsTestGroup" runat="server" visible="false">
                                                        <td valign="middle" align="left">
                                                            <asp:Label ID="lblGroup" runat="server" Text="Test Group:" CssClass="Indent" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlGroup" runat="server" DataTextField="Group" DataValueField="GroupID"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Text="View All" Value="0" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblSearch" runat="server" Text="Exam Name:" ForeColor="black" CssClass="Indent" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtSearch" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">
                                                            <asp:Label ID="Label2" runat="server" Text="Course ID:" ForeColor="black" CssClass="Indent" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCourseID" runat="server" onkeypress="return isNumber(event)" />
                                                            <asp:CheckBox ID="chkIncludeElapsed" runat="server" Text="Include elapsed exams"
                                                                AutoPostBack="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="buttons" ForeColor="black"
                                                                OnClick="btnSearch_Click" />
                                                            <%--<asp:Label ID="lblVert" runat="server" Text="|" ForeColor="black" />
                                                    <asp:LinkButton ID="lnkViewAll" runat="server" Text="View All" ForeColor="black"
                                                        OnClick="lnkViewAll_Click" />--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </Content>
                                        </ajax:AccordionPane>
                                    </Panes>
                                </ajax:Accordion>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" colspan="2">
                                <asp:Button CssClass="buttons" ID="btnAddNew" runat="server" Text="Add New Exam"
                                    ForeColor="black" OnClick="btnNewExam_Click" Width="150px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Panel ID="pnlUpdate" runat="server">
                                    <asp:UpdateProgress ID="udpProgress" runat="server">
                                        <ProgressTemplate>
                                            <asp:Panel ID="pnlBuffer" runat="server">
                                                <table>
                                                    <tr>
                                                        <td align="center" valign="middle">
                                                            <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader-small.gif" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </asp:Panel>
                                <%--VirtualItemCount="-1"--%>
                                <cc1:PagingGridView ID="gvExams" runat="server" AutoGenerateColumns="false" AllowPaging="True"
                                    ForeColor="black" CellPadding="5" BorderColor="black" Font-Bold="false"
                                    Width="100%" EmptyDataText="No exams found." OnSelectedIndexChanged="gvExams_SelectedIndexChanged"
                                    OnRowCommand="gvExams_RowEditing" AllowSorting="true" OnSorting="gvExams_Sorting"
                                    OrderBy="" OnDataBound="gvExams_DataBound" PageSize="10">
                                    <headerstyle horizontalalign="center" />
                                    <rowstyle forecolor="Black" font-size="12px" bordercolor="black" wrap="true" />
                                    <alternatingrowstyle forecolor="Black" font-size="12px" bordercolor="black" />
                                    <pagersettings mode="NumericFirstLast" pagebuttoncount="7" firstpagetext="�" lastpagetext="�" />
                                    <columns>
                                        <asp:TemplateField HeaderText="Exam Name" SortExpression="TestName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblExamID" runat="server" Text='<%#Bind("TestCategoryID") %>' Visible="false" />
                                                <asp:LinkButton ID="lnkExam" runat="server" Text='<%#Bind("TestName") %>' ForeColor="black"
                                                    Font-Bold="true" CommandName="Select" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCategory" runat="server" Text='<%#Bind("Category") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Subcategory" SortExpression="Subcategory">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubCategory1" runat="server" Text='<%#Bind("Subcategory") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CourseID" SortExpression="CourseID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("CourseID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("EndDate") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Limit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTimeLimitVal" runat="server" Text='<%#Bind("TimeLimit") %>' Visible="false" />
                                                <asp:Label ID="lblTimeLimit" runat="server" Text="" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tries Allowed">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestLimitVal" runat="server" Text='<%#Bind("TestLimit") %>' Visible="false" />
                                                <asp:Label ID="lblTestLimit" runat="server" Text="" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Campaign ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCampaignID" runat="server" Text='<%#Bind("CampaignID") %>' Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="EditCommand" Text="Edit"
                                                    CommandArgument='<%#Bind("TestCategoryID") %>' ForeColor="black" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </columns>
                                    <pagertemplate>
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 60%">
                                                    <asp:Label ID="MessageLabel" Text="Select a page:" runat="server" />
                                                    <asp:DropDownList ID="PageDropDownList" AutoPostBack="true" OnSelectedIndexChanged="PageDropDownList_SelectedIndexChanged"
                                                        runat="server" CssClass="Trigger" />
                                                </td>
                                                <td style="width: 20%; text-align: right">
                                                    <asp:Label ID="CurrentPageLabel" runat="server" />
                                                </td>
                                                <td style="width: 20%; text-align: right">
                                                </td>
                                            </tr>
                                        </table>
                                    </pagertemplate>
                                </cc1:PagingGridView>
                                <asp:GridView ID="gvExamsSearch" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                    ForeColor="black" CellPadding="5" BorderColor="black" Font-Bold="false" Width="100%"
                                    EmptyDataText="No exams found." OnSelectedIndexChanged="gvExams_SelectedIndexChanged"
                                    OnRowCommand="gvExams_RowEditing" AllowSorting="true" OnSorting="gvExams_Sorting">
                                    <HeaderStyle HorizontalAlign="center" />
                                    <RowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" Wrap="true" />
                                    <AlternatingRowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" />
                                    <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                                        NextPageText=">" Position="Bottom" PreviousPageText="<" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Exam Name" SortExpression="TestName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblExamID" runat="server" Text='<%#Bind("TestCategoryID") %>' Visible="false" />
                                                <asp:LinkButton ID="lnkExam" runat="server" Text='<%#Bind("TestName") %>' ForeColor="black"
                                                    Font-Bold="true" CommandName="Select" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCategory" runat="server" Text='<%#Bind("Category") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Subcategory" SortExpression="Subcategory">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubCategory2" runat="server" Text='<%#Bind("Subcategory") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CourseID" SortExpression="CourseID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("CourseID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("EndDate") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Limit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTimeLimitVal" runat="server" Text='<%#Bind("TimeLimit") %>' Visible="false" />
                                                <asp:Label ID="lblTimeLimit" runat="server" Text="" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tries Allowed">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestLimitVal" runat="server" Text='<%#Bind("TestLimit") %>' Visible="false" />
                                                <asp:Label ID="lblTestLimit" runat="server" Text="" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="EditCommand" Text="Edit"
                                                    CommandArgument='<%#Bind("TestCategoryID") %>' ForeColor="black" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" colspan="2">
                                <asp:Button CssClass="buttons" ID="btnNewExam" runat="server" Text="Add New Exam"
                                    ForeColor="black" OnClick="btnNewExam_Click" Width="150px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--<asp:HiddenField ID="hidWus" runat="server" />
                <ajax:ModalPopupExtender ID="mpeWus" runat="server" BackgroundCssClass="modalBackground"
                    TargetControlID="hidWus" PopupControlID="pnlPopup" />
                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup">
                    <wus:examstatuscontrol ID="wusMain" runat="server" />
                </asp:Panel>--%>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeNoRightsToViewSingleExam" BackgroundCssClass="modalBackground"
        runat="server" TargetControlID="hidNoRightsToViewSingleExam" PopupControlID="pnlNoRightsToViewSingleExam" />
    <asp:HiddenField ID="hidNoRightsToViewSingleExam" runat="server" />
    <asp:Panel ID="pnlNoRightsToViewSingleExam" runat="server" CssClass="modalPopup"
        HorizontalAlign="center" Width="200px">
        <table width="100%" cellpadding="5" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblNoRightsToViewSingleExam" runat="server" Text="You do not have enough rights to view this exam." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnNoRightsToViewSingleExam" runat="server" Text="Return" CssClass="buttons"
                        OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeNoRightsToEditSingleExam" BackgroundCssClass="modalBackground"
        runat="server" TargetControlID="hidNoRightsToEditSingleExam" PopupControlID="pnlNoRightsToEditSingleExam" />
    <asp:HiddenField ID="hidNoRightsToEditSingleExam" runat="server" />
    <asp:Panel ID="pnlNoRightsToEditSingleExam" runat="server" CssClass="modalPopup"
        HorizontalAlign="center" Width="200px">
        <table width="100%" cellpadding="5" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblNoRightsToEditSingleExam" runat="server" Text="You do not have enough rights to edit this exam." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnNoRightsToEditSingleExam" runat="server" Text="Return" CssClass="buttons"
                        OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
