using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class recheckessays : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/default.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            TestTaken test = TestTaken.Select(SessionManager.SessionTestTakenID);
            if (test != null)
            {
                TestCategory category = TestCategory.Select(test.TestCategoryID, true);
                if (category != null)
                {
                    this.lblUserIDVal.Text = SessionManager.SessionExamineeName;
                    this.lblExamIDVal.Text = category.TestName;
                    DataSet responses = TestResponse.GetMultipleEssays(SessionManager.SessionTestTakenID);
                    this.gvEssays.DataSource = responses;
                    this.gvEssays.DataBind();
                }
            }
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(10))
            {
                this.pnlGrade.Visible = true;
                //foreach (GridViewRow row in this.gvEssays.Rows)
                //{
                //    Panel pnlScoreData = row.FindControl("pnlScoreData") as Panel;
                //    if (pnlScoreData != null)
                //        pnlScoreData.Visible = true;
                //}
            }
        }
    }
    protected void btnGrade_Click(object sender, EventArgs e)
    {
        bool hasError = false;

        foreach (GridViewRow row in this.gvEssays.Rows)
        {
            int score = 0;
            TextBox txtGradeVal = row.FindControl("txtGradeVal") as TextBox;
            Label lblMaxScore = row.FindControl("lblMaxScore") as Label;
            CustomValidator cvScoreOverflow = row.FindControl("cvScoreOverflow") as CustomValidator;
            if (!int.TryParse(txtGradeVal.Text.Trim(), out score))
            {
                cvScoreOverflow.IsValid = false;
                hasError = true;
            }
            if (score > Convert.ToInt32(lblMaxScore.Text.Trim()))
            {
                cvScoreOverflow.IsValid = false;
                hasError = true;
            }
        }
        if (hasError)
            return;
        //check if there are empty score fields
        foreach (GridViewRow row in this.gvEssays.Rows)
        {
            TextBox txtGradeVal = row.FindControl("txtGradeVal") as TextBox;
            TextBox txtAnswerVal = row.FindControl("txtAnswerVal") as TextBox;
            Label lblMaxScore = row.FindControl("lblMaxScore") as Label;
            Label lblQuestionnaireID = row.FindControl("lblQuestionnaireIDVal") as Label;
            Label lblSaveTestResponseID = row.FindControl("lblSaveTestResponseIDVal") as Label;
            CustomValidator cvScoreOverflow = row.FindControl("cvScoreOverflow") as CustomValidator;
            CustomValidator cvAlreadyScored = row.FindControl("cvAlreadyScored") as CustomValidator;

            Questionnaire questionnaire = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), true);
            TestResponse response = TestResponse.Select(Convert.ToInt32(lblSaveTestResponseID.Text.Trim()));
            TestCategory category = TestCategory.Select(questionnaire.TestCategoryID, true);


            int score = 0;

            try
            {
                int temp = 0;
                if (int.TryParse(txtGradeVal.Text.Trim(), out temp))
                {
                    if (temp > Convert.ToInt32(lblMaxScore.Text.Trim()))
                    {
                        cvScoreOverflow.IsValid = false;
                        hasError = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(Config.ApplicationID(), "recheckessays.aspx", "btnGrade_Click", ex.Message);
            }
        }
        if (hasError)
            return;
        //check i
        else
        {
            foreach (GridViewRow row in this.gvEssays.Rows)
            {
                Label lblSaveTestResponseID = row.FindControl("lblSaveTestResponseIDVal") as Label;
                Label lblQuestionnaireID = row.FindControl("lblQuestionnaireIDVal") as Label;
                TextBox txtAnswerVal = row.FindControl("txtAnswerVal") as TextBox;
                TextBox txtGradeVal = row.FindControl("txtGradeVal") as TextBox;
                TextBox txtCheckerComments = row.FindControl("txtCheckerComments") as TextBox;
                Label lblMaxScore = row.FindControl("lblMaxScore") as Label;
                CustomValidator cvScoreOverflow = row.FindControl("cvScoreOverflow") as CustomValidator;
                CustomValidator cvAlreadyScored = row.FindControl("cvAlreadyScored") as CustomValidator;

                TestResponse response = TestResponse.Select(Convert.ToInt32(lblSaveTestResponseID.Text.Trim()));
                Questionnaire questionnaire = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), true);
                TestCategory category = TestCategory.Select(questionnaire.TestCategoryID, true);

                try
                {
                    int temp = 0;
                    if (int.TryParse(txtGradeVal.Text.Trim(), out temp))
                    {
                        if (temp > Convert.ToInt32(lblMaxScore.Text.Trim()))
                        {
                            cvScoreOverflow.IsValid = false;
                            return;
                        }
                        if (response != null)
                        {
                            response.GradeEssayQuestion(Convert.ToInt32(txtGradeVal.Text.Trim()));
                            EssayMarker marker = new EssayMarker(SessionManager.SessionUsername, response.TestResponseID, response.EssayScore, txtCheckerComments.Text.Trim());
                            marker.Insert();

                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                    service.WriteError(Config.ApplicationID(), "recheckessays.aspx", "btnGrade_Click", ex.Message);
                }
            }
        }
        if (hasError)
            return;
        else
        {
            try
            {
                int totalGrade = 0;
                TestTaken taken = TestTaken.Select(SessionManager.SessionTestTakenID);
                TestCategory category = TestCategory.Select(taken.TestCategoryID, true);
                Questionnaire[] questions = Questionnaire.SelectByCategory(category.TestCategoryID);
                foreach (Questionnaire question in questions)
                {
                    TestResponse response = TestResponse.Select(taken.TestTakenID, question.QuestionnaireID, SessionManager.SessionExamineeName);
                    if (question.TypeCode == "matching")
                    {
                        if (response.Response1 == question.Ans1.Split('|')[0] && response.Response2 == question.Ans2.Split('|')[0] &&
                        response.Response3 == question.Ans3.Split('|')[0] && response.Response4 == question.Ans4.Split('|')[0] &&
                        response.Response5 == question.Ans5.Split('|')[0] && response.Response6 == question.Ans6.Split('|')[0] &&
                        response.Response7 == question.Ans7.Split('|')[0] && response.Response8 == question.Ans8.Split('|')[0] &&
                        response.Response9 == question.Ans9.Split('|')[0] && response.Response10 == question.Ans10.Split('|')[0])
                        {
                            totalGrade++;
                        }
                    }
                    else if (response != null)
                    {
                        if (response.Response1 == question.Ans1 && response.Response2 == question.Ans2 &&
                            response.Response3 == question.Ans3 && response.Response4 == question.Ans4 &&
                            response.Response5 == question.Ans5 && response.Response6 == question.Ans6 &&
                            response.Response7 == question.Ans7 && response.Response8 == question.Ans8 &&
                            response.Response9 == question.Ans9 && response.Response10 == question.Choice10 && question.TypeCode != "essay")
                        {
                            totalGrade++;
                        }
                    }
                }

                TestResponse[] responses = TestResponse.SelectByTestTaken(taken.TestTakenID);
                if (responses.Length > 0)
                {
                    foreach (TestResponse response in responses)
                        totalGrade += response.EssayScore;
                }
                taken.Score += totalGrade;
                taken.UpdateScore(totalGrade, category.PassingGrade);
                this.lblScored.Text = "Essays graded successfully.";
                this.mpeScored.Show();
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(Config.ApplicationID(), "recheckessays.aspx", "btnGrade_Click", ex.Message);
            }
        }
    }
    protected void lblReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essaytests.aspx?" + Request.QueryString.ToString());
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essaytests.aspx?" + Request.QueryString.ToString());
    }
}
