<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="testtaken.aspx.cs" Inherits="testtaken" Title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td>
                <ajax:Accordion ID="accTestTaken" runat="server" RequireOpenedPane="true" HeaderCssClass="accordionHeader" ContentCssClass="accordionContent">
                    <Panes>
                        <ajax:AccordionPane ID="acpUsers" runat="server">
                            <Header>
                                <table width="100%">
                                    <tr>
                                        <td style="width:30%">
                                            <asp:Label ID="lblUserID" runat="server" Text="User ID:" />
                                        </td>
                                        <td style="width:70%">
                                            <asp:DropDownList ID="ddlUsers" runat="server" Width="150px" />
                                        </td>
                                    </tr>
                                </table>
                            </Header>
                            <Content>
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="acpTests" runat="server">
                            <Header>
                                <table width="100%">
                                    <tr>
                                        <td style="width:30%">
                                            <asp:Label ID="lblTestID" runat="server" Text="Exam ID:" />
                                        </td>
                                        <td style="width:70%">
                                            <asp:DropDownList ID="ddlTests" runat="server" Width="150px" />
                                        </td>
                                    </tr>
                                </table>
                            </Header>
                        </ajax:AccordionPane>
                    </Panes>
                </ajax:Accordion>
            </td>
        </tr>
    </table>
</asp:Content>
