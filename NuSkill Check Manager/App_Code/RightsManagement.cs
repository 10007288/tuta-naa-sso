using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

/// <summary>
/// Summary description for RightsManagement
/// </summary>
public class RightsManagement
{
    public RightsManagement()
    {
    }

    public static string CombineRights(UserRights[] rights)
    {
        //get the length of the rights string
        int[] rightsNums = new int[rights[0].Rights.ToCharArray().Length];


        char[] array = rights[0].Rights.ToCharArray();

        //assign the rights values of the first right string as default
        for (int x = 0; x < rights[0].Rights.ToCharArray().Length; x++)
        {
            rightsNums[x] = Convert.ToInt32(array[x].ToString());
        }

        //add all remaining rights to the rights string
        for (int x = 1; x < rights.Length; x++)
        {
            for (int y = 0; y < rights[x].Rights.ToCharArray().Length; y++)
            {
                rightsNums[y] += Convert.ToInt32(rights[x].Rights.ToCharArray()[y]);
            }
        }

        StringBuilder builder = new StringBuilder();
        for (int x = 0; x < rightsNums.Length; x++)
        {
            if (rightsNums[x] > 0)
                builder.Append("1");
            else
                builder.Append("0");
        }

        return builder.ToString();
    }

    public static int[] ProcessRights(string rights)
    {
        List<int> outlist = new List<int>();
        char[] array = rights.ToCharArray();
        Array.Reverse(array);
        for (int x = 0; x < array.Length; x++)
        {
            if (Convert.ToInt32(array[x].ToString()) >= 1)
                outlist.Add(x);
        }
        return outlist.ToArray();
    }

    public static List<int> ProcessRightsList(string rights)
    {
        List<int> outlist = new List<int>();
        char[] array = rights.ToCharArray();
        Array.Reverse(array);
        for (int x = 0; x < array.Length; x++)
        {
            if (Convert.ToInt32(array[x].ToString()) >= 1)
                outlist.Add(x);
        }
        return outlist;
    }

    public static List<int> ProcessMultipleTierRightsLists(params string[] rights)
    {
        List<int> outlist = new List<int>();
        int longest = 0;
        foreach (string right in rights)
        {
            if (right.Length >= longest)
                longest = rights.Length;

        }
        foreach (string right in rights)
        {

        }
        return outlist;
    }

    public static bool ContainsOnly(string rights, params int[] rightstofind)
    {
        List<int> outRights = ProcessRightsList(rights);
        foreach (int x in rightstofind)
        {
            if (!outRights.Contains(x))
                return false;
        }
        if (outRights.Count != rightstofind.Length)
            return false;
        return true;
    }
}
