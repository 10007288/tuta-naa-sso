using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class massusercreation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(30))
            Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
    }
    protected void btnRight_Click(object sender, EventArgs e)
    {
        bool isValid = true;
        if (string.IsNullOrEmpty(this.txtBatchName.Text.Trim()))
        {
            this.cvBatchName.IsValid = false;
            isValid = false;
        }
        if (string.IsNullOrEmpty(this.txtPrefix.Text.Trim()) && string.IsNullOrEmpty(this.txtSuffix.Text.Trim()))
        {
            this.cvPrefixSuffix.IsValid = false;
            isValid = false;
        }
        if (string.IsNullOrEmpty(this.txtEnding.Text.Trim()) && string.IsNullOrEmpty(this.txtStarting.Text.Trim()))
        {
            this.cvNoValue.IsValid = false;
            return;
        }
        if (Convert.ToInt32(this.txtEnding.Text.Trim()) < Convert.ToInt32(this.txtStarting.Text.Trim()))
        {
            this.cvOverflow.IsValid = false;
            isValid = false;
        }
        if (this.txtPrefix.Text.Trim().Length + this.txtEnding.Text.Trim().Length + this.txtSuffix.Text.Trim().Length > 20)
        {
            this.cvOverload.IsValid = false;
            isValid = false;
        }
        UserBatch batch = UserBatch.Select(this.txtBatchName.Text.Trim());
        if (batch != null)
        {
            this.cvBatchNameExists.IsValid = false;
            return;
        }
        if (isValid == false)
            return;

        List<Registration> list = new List<Registration>();
        for (int i = Convert.ToInt32(this.txtStarting.Text.Trim()); i <= Convert.ToInt32(this.txtEnding.Text.Trim()); i++)
        {
            if (Convert.ToInt32(this.txtEnding.Text.Trim()).ToString().Length == 3)
                list.Add(new Registration(this.txtPrefix.Text.Trim() + i.ToString("000") + this.txtSuffix.Text.Trim()));
            if (Convert.ToInt32(this.txtEnding.Text.Trim()).ToString().Length == 2)
                list.Add(new Registration(this.txtPrefix.Text.Trim() + i.ToString("00") + this.txtSuffix.Text.Trim()));
            if (Convert.ToInt32(this.txtEnding.Text.Trim()).ToString().Length == 1)
                list.Add(new Registration(this.txtPrefix.Text.Trim() + i.ToString("0") + this.txtSuffix.Text.Trim()));
        }
        this.gvUsers.DataSource = list;
        this.gvUsers.DataBind();
        List<Registration> newlist = new List<Registration>();
        newlist.AddRange(Registration.GetExistingByBatch(this.txtPrefix.Text.Trim(), Convert.ToInt32(this.txtStarting.Text.Trim()), Convert.ToInt32(this.txtEnding.Text.Trim()), this.txtSuffix.Text.Trim()));
        foreach (DataListItem item in this.gvUsers.Items)
        {
            Label lblUsername = item.FindControl("lblUsername") as Label;
            foreach(Registration registration in newlist)
            {
                if (registration.AutoUserID == lblUsername.Text.Trim())
                {
                    item.ForeColor = System.Drawing.Color.Red;
                    item.Font.Bold = true;
                }
            }
        }
        this.mpePreview.Show();
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            for(int x = Convert.ToInt32(this.txtStarting.Text.Trim()); x<=Convert.ToInt32(this.txtEnding.Text.Trim()); x++)
            {
                Registration newUser = new Registration(this.txtPrefix.Text.Trim() + x.ToString() + this.txtSuffix.Text.Trim(), NuComm.Security.Encryption.UTF8.EncryptText(this.txtPrefix.Text.Trim() + x.ToString() + this.txtSuffix.Text.Trim()), 0);
                newUser.Insert();
            }
            int userBatchNum = UserBatch.Insert(this.txtBatchName.Text.Trim());
            Registration.InsertByBatch(userBatchNum, this.txtPrefix.Text.Trim(), Convert.ToInt32(this.txtStarting.Text.Trim()), Convert.ToInt32(this.txtEnding.Text.Trim()), this.txtSuffix.Text.Trim());
            this.lblResult.Text = "Creation successful.";
            this.mpeResult.Show();
        }
        catch(Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "massusercreation.aspx", "btnContinue_Click", ex.Message);
            this.lblResult.Text = "Creation failed.";
            this.mpeResult.Show();
        }
    }
    protected void btnConfirmResult_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/massusercreation.aspx?" + Request.QueryString.ToString());
    }
}
