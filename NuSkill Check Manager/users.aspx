<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="users.aspx.cs" Inherits="users" Title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblUsers" runat="server" Text="Users" ForeColor="black" Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblSearchCim" runat="server" Text="View Results for Employee:" /><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCimNumber" runat="server" Text="Cim Number:" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCim" runat="server" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCim" runat="server" FilterMode="validchars" FilterType="numbers" TargetControlID="txtCim" />
                                            <asp:Button ID="btnSearch" runat="server" CssClass="buttons" Text="Search" OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:CustomValidator ID="cvNoSearch" runat="server" ErrorMessage="Enter a correct and active Cim Number." Display="static" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblSearch" runat="server" Text="Search for Non-Cim User:" ForeColor="black" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblBatch" runat="server" Text="User Batch:" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlBatch" runat="server" AppendDataBoundItems="true" DataValueField="BatchID"
                                                DataTextField="BatchName">
                                                <asp:ListItem Text="Non-generated users" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblUsername" runat="server" Text="Username:" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearch" runat="server" />
                                            <asp:Button ID="btnUserSearch" runat="server" CssClass="buttons" Text="Search" OnClick="btnUserSearch_Click" />
                                            <asp:Label ID="lblVert" runat="server" Text="          " ForeColor="black" />
                                            <asp:Button ID="btnViewAll" runat="server" CssClass="buttons" Text="View All" OnClick="lnkViewAll_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    PageSize="20" CellPadding="5" BorderColor="black" OnPageIndexChanging="gvUsers_PageIndexChanging"
                                    Width="100%" EmptyDataText="No users found." OnSelectedIndexChanging="gvUsers_SelectedIndexChanging">
                                    <RowStyle BorderColor="black" ForeColor="Black" Font-Bold="true" Font-Size="12px" />
                                    <AlternatingRowStyle ForeColor="Black" Font-Bold="true" Font-Size="12px" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        Position="Bottom" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="User ID">
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsername" runat="server" Text='<%#Bind("AutoUserID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle Width="20%" HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelecte" runat="server" Text="View" CommandName="Select" ForeColor="black"
                                                    Font-Bold="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpePopup" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlPopup" TargetControlID="hidPopup" />
    <asp:HiddenField ID="hidPopup" runat="server" />
    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Font-Size="12px">
        <table cellpadding="10" cellspacing="0" style="width: 300">
            <tr>
                <td>
                    <asp:Label ID="lblUserID" runat="server" Text="UserID:" />
                </td>
                <td>
                    <asp:Label ID="lblUserIDVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCim" runat="server" Text="CIM Number:" />
                </td>
                <td>
                    <asp:Label ID="lblCimVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:LinkButton ID="lnkCompleteInformation" runat="server" Text="View complete user information"
                        CssClass="linkButtons" OnClick="lnkCompleteInformation_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button ID="btnPopup" runat="server" Text="Back" CssClass="buttons" Width="60"
                        OnClick="btnPopup_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeNoRights" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlNoRights" TargetControlID="hidNoRights" />
    <asp:HiddenField ID="hidNoRights" runat="server" />
    <asp:Panel ID="pnlNoRights" runat="server" CssClass="modalPopip" Font-Size="12px">
        <table cellpadding="10" cellspacing="0" style="width: 300">
            <tr>
                <td>
                    <asp:Label ID="lblNoRights" runat="server" Text="You have no rights to view this user." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnNoRights" runat="server" Text="Return" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
