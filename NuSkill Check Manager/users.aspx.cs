using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class users : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(11))
            Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            this.gvUsers.DataSource = Registration.SelectByBatch(0);
            this.gvUsers.DataBind();
            this.BindBatches();
            if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(13))
                this.lnkCompleteInformation.Visible = false;
        }
    }
    protected void btnUserSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.txtSearch.Text.Trim()))
        {
            //Registration[] registration = Registration.Search(this.txtSearch.Text.Trim());
            this.gvUsers.DataSource = Registration.SearchByBatch(this.txtSearch.Text.Trim(), Convert.ToInt32(this.ddlBatch.Text));
            this.gvUsers.DataBind();

        }
        else
        {
            this.gvUsers.DataSource = Registration.SelectByBatch(Convert.ToInt32(this.ddlBatch.Text));
            this.gvUsers.DataBind();
        }
    }

    protected void BindGrid()
    {
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(11))
        {
            this.gvUsers.DataSource = null;
            this.btnUserSearch.Visible = false;
        }
        else
        {
            //Registration[] registration = Registration.SelectByBatch(0);
            this.gvUsers.DataSource = Registration.SearchByBatch(this.txtSearch.Text.Trim(), Convert.ToInt32(this.ddlBatch.Text));
        }
        this.gvUsers.DataBind();
    }

    protected void BindBatches()
    {
        this.ddlBatch.DataSource = UserBatch.SelectAll();
        this.ddlBatch.DataBind();
    }

    protected void gvUsers_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridViewRow row = this.gvUsers.Rows[e.NewSelectedIndex];
        if (row != null)
        {
            if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(12))
                this.mpeNoRights.Show();
            else
            {
                Label lblUsername = row.FindControl("lblUsername") as Label;
                Registration user = Registration.Select(lblUsername.Text);
                this.lblUserIDVal.Text = user.AutoUserID;
                this.lblCimVal.Text = "connect to DB";
                SessionManager.SessionExamineeName = this.lblUserIDVal.Text.Trim();
                Response.Redirect("~/userinfo.aspx?" + Request.QueryString.ToString());
            }
        }
    }

    protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvUsers.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }

    protected void lnkCompleteInformation_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/userinfo.aspx?" + Request.QueryString.ToString());
    }
    protected void lnkViewAll_Click(object sender, EventArgs e)
    {
        Registration[] registration = Registration.SelectAll();
        this.gvUsers.DataSource = registration;
        this.gvUsers.DataBind();
    }
    protected void btnPopup_Click(object sender, EventArgs e)
    {
        Registration[] registration = Registration.SelectAll();
        this.gvUsers.DataSource = registration;
        this.gvUsers.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //int x = 0;
        //if (int.TryParse(this.txtCim.Text.Trim(), out x))
        //{
        //    if (x > 0)
        //    {
        //        if (Registration.SearchEmployee(Convert.ToInt32(x)) == 0)
        //        {
        //            this.cvNoSearch.IsValid = false;
        //            return;
        //        }
        //        SessionManager.SessionExamineeName = this.txtCim.Text.Trim();
        //        Response.Redirect("~/userinfo.aspx?" + Request.QueryString.ToString());
        //    }
        //    else
        //    {
        //        this.cvNoSearch.IsValid = false;
        //        return;
        //    }
        //}
        //else
        //{
        //    this.cvNoSearch.IsValid = false;
        //    return;
        //}
        SessionManager.SessionExamineeName = this.txtCim.Text.Trim();
        Response.Redirect("~/userinfo.aspx?" + Request.QueryString.ToString());
    }
}
