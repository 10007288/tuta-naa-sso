using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ViewReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
            try
            {
                string reportName = Request.QueryString["Report"].ToString();
                //this.ReportViewer1.ServerReport.ReportServerUrl = new System.Uri("http://DB042k3CN1/reportserver");
                this.ReportViewer1.ServerReport.ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["ReportServerURL"]);
                //this.ReportViewer1.ServerReport.ReportServerUrl = new System.Uri("http://mnldev005/reportserver");
                //this.ReportViewer1.ServerReport.ReportPath = @"/Reports/Transcom University Testing/" + reportName;
                //this.ReportViewer1.ServerReport.ReportPath = @"/Reports/" + reportName;
                this.ReportViewer1.ServerReport.ReportPath = ConfigurationManager.AppSettings["ReportPath"] + reportName;
                this.ReportViewer1.Visible = true;
            }
            catch (Exception ex)
            {

            }
        }
    }
}
