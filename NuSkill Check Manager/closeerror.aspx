<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="closeerror.aspx.cs" Inherits="closeerror" Title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Width="100%">
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvErrorExams" runat="server" AutoGenerateColumns="false" Width="80%" AllowPaging="false" AllowSorting="false" BorderColor="black">
                        <Columns>
                            <asp:TemplateField HeaderText="User ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserID" runat="server" Text='<%#Bind("UserID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TestTakenID">
                                <ItemTemplate>
                                    <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Start Taken">
                                <ItemTemplate>
                                    <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Delete" CssClass="linkButtons" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
