<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="reports.aspx.cs" Inherits="reports" Title="Transcom University Testing Admin" %>

<%@ Register TagPrefix="wus" TagName="reportcontrol" Src="~/controls/reportcontrol.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">

    <%--<wus:reportcontrol ID="wusReport" runat="server" />--%>
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="4" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblReports" runat="server" Text="Reports" ForeColor="black" Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvReports" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                    Width="100%" OnRowCommand="gvReports_RowCommand" EmptyDataText="No reports available."
                                    CellPadding="3">
                                    <EmptyDataRowStyle Font-Size="small" Font-Italic="true" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkReport" runat="server" Text='<%#Bind("DisplayName") %>' CommandArgument='<%#Bind("ReportURL") %>'
                                                    CommandName="Report" ForeColor="black" OnClientClick="aspnetForm.target='_blank';" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
