using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class registration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            this.pnlOption.Visible = true;
            this.pnlCreateAdmin.Visible = false;
            this.pnlRights.Visible = false;
            this.pnlMain.Visible = false;
        }

    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            Int64 x;
            bool isValid = true;
            if (string.IsNullOrEmpty(this.txtUsername.Text.Trim()))
            {
                isValid = false;
                this.cvUsername.IsValid = false;
            }
            if(Int64.TryParse(this.txtUsername.Text.Trim(), out x))
            {
                isValid = false;
                this.cvUsername.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtPassword.Text.Trim()))
            {
                isValid = false;
                this.cvPassword.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtFirstName.Text.Trim()))
            {
                isValid = false;
                this.cvFirstName.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtLastName.Text.Trim()))
            {
                isValid = false;
                this.cvLastName.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtLocation.Text.Trim()))
            {
                isValid = false;
                this.cvLocation.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtPhoneNumber.Text.Trim()))
            {
                isValid = false;
                this.cvPhoneNumber.IsValid = false;
            }
            if (!isValid)
                return;

            if (!Registration.CheckExists(this.txtFirstName.Text.Trim(), this.txtLastName.Text.Trim(), this.txtPhoneNumber.Text.Trim(), this.txtLocation.Text.Trim()))
            {
                try
                {
                    Registration reg = new Registration(this.txtLastName.Text.Trim(), this.txtFirstName.Text.Trim(), this.txtPhoneNumber.Text.Trim(), this.txtLocation.Text.Trim(), this.txtUsername.Text.Trim(), this.txtPassword.Text.Trim(), 0);
                    reg.Insert();
                    this.txtFirstName.Text = string.Empty;
                    this.txtLastName.Text = string.Empty;
                    this.txtLocation.Text = string.Empty;
                    this.txtPassword.Text = string.Empty;
                    this.txtPhoneNumber.Text = string.Empty;
                    this.txtUsername.Text = string.Empty;
                    this.cvExists.ErrorMessage = "User saved successfully";
                    this.cvExists.IsValid = false;
                    return;
                }
                catch(Exception ex)
                {
                    throw;
                }
            }
            else
            {
                this.cvExists.IsValid = false;
                return;
            }

        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "registration.aspx", "btnRegister_Click", ex.Message);
            this.cvExists.ErrorMessage = "An error occured. User was not saved. Please try again.";
            this.cvExists.IsValid = false;
            return;
        }
    }
    protected void btnCreateAdmin_Click(object sender, EventArgs e)
    {
        this.pnlOption.Visible = false;
        this.pnlCreateAdmin.Visible = true;
        this.pnlMain.Visible = false;
    }
    protected void btnCreateUser_Click(object sender, EventArgs e)
    {
        this.pnlOption.Visible = false;
        this.pnlCreateAdmin.Visible = false;
        this.pnlMain.Visible = true;
    }
    protected void btnFind_Click(object sender, EventArgs e)
    {

    }
}
