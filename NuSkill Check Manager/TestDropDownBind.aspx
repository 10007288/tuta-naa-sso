<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="TestDropDownBind.aspx.cs" Inherits="TestDropDownBind" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:TextBox ID="txtDropDownTest" runat="server" Width="200px" />
    <ajax:AutoCompleteExtender ID="aceDropDownTest" runat="server" TargetControlID="txtDropDownTest"
        ServiceMethod="GetDropDownLists" MinimumPrefixLength="2" CompletionInterval="100"
        FirstRowSelected="true" CompletionListCssClass="TestBind" />
</asp:Content>
