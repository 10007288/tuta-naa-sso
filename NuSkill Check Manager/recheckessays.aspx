<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master" AutoEventWireup="true" CodeFile="recheckessays.aspx.cs" Inherits="recheckessays" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" Font-Names="Arial" HorizontalAlign="Left"
                    Width="100%">
                    <table cellpadding="10" width="100%" style="font-family: Arial; text-align: left" border="1">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblEssay" runat="server" Text="Essay Grading" CssClass="testQuestion" />
                                <br />
                                <hr style="color: black" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:10%">
                                <asp:Label ID="lblUserID" runat="server" Text="Examinee:" />
                            </td>
                            <td style="width:90%">
                                <asp:Label ID="lblUserIDVal" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:10%">
                                <asp:Label ID="lblExamID" runat="server" Text="Exam Name:" />
                            </td>
                            <td style="width:90%">
                                <asp:Label ID="lblExamIDVal" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="gvEssays" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                    AllowSorting="false" GridLines="none" Width="100%" BorderColor="Black">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Panel ID="pnlSubPanel" runat="server" HorizontalAlign="left" BorderColor="black"
                                                    BorderWidth="1">
                                                    <table cellpadding="5" width="100%" cellspacing="0" style="font-size: 12px;">
                                                        <tr>
                                                            <td style="width:100px">
                                                                <asp:Label ID="lblQuestion" runat="server" Text="Question:" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSaveTestResponseIDVal" runat="server" Text='<%#Bind("TestResponseID") %>'
                                                                    Visible="false" />
                                                                <asp:Label ID="lblQuestionnaireIDVal" runat="server" Text='<%#Bind("QuestionnaireID") %>'
                                                                    Visible="false" />
                                                                <asp:Label ID="lblQuestionVal" runat="server" Text='<%#Bind("Question") %>' Font-Bold="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="txtAnswerVal" runat="server" MaxLength="8000" TextMode="multiline"
                                                                    Columns="80" Rows="10" ReadOnly="true" Text='<%#Bind("EssayResponse") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblGrade" runat="server" Text="Score:" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtGradeVal" runat="server" Width="20px" Text='<%#Bind("EssayScore") %>' />
                                                                <asp:CustomValidator ID="cvScoreOverflow" runat="server" ErrorMessage="Score must be whole number that does not exceed maximum score."
                                                                    Display="static" CssClass="validatorStyle" />
                                                                <asp:CustomValidator ID="cvAlreadyScored" runat="server" ErrorMessage="This exam has already been graded."
                                                                    Display="static" CssClass="validatorStyle" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMaximumScore" runat="server" Text="Maximum Score:" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMaxScore" runat="server" Text='<%#Bind("EssayMaxScore") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCheckerComments" runat="server" Text="Comments?" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCheckerComments" runat="server" MaxLength="8000" TextMode="multiline"
                                                                    Columns="50" Rows="5" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Panel ID="pnlGrade" runat="server" BorderWidth="0px" Visible="false">
                                    <asp:Label ID="lblSingleClick" runat="server" Text="Please click only once." BorderStyle="none" />
                                    <asp:Button CssClass="buttons" ID="btnGrade" runat="server" Text="OK" Width="80px"
                                        OnClick="btnGrade_Click" />
                                </asp:Panel>
                                <br />
                                <asp:Panel ID="pnlNoRightsToGradeEssay" runat="server" BorderWidth="0px" Visible="false">
                                    <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="buttons" OnClick="btnReturn_Click"
                                        Width="80px" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeScored" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlScored" TargetControlID="hidScored" />
    <asp:HiddenField ID="hidScored" runat="server" />
    <asp:Panel ID="pnlScored" runat="server" CssClass="modalPopup" HorizontalAlign="center">
        <table width="300" cellpadding="10" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblScored" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button CssClass="buttons" ID="lblReturn" runat="server" Text="Return" OnClick="lblReturn_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
