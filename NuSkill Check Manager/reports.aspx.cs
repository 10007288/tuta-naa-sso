using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuComm.Security.Encryption;
using NuSkill.Business;

public partial class reports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            this.gvReports.DataSource = TestReports.SelectAll();
            this.gvReports.DataBind();
            foreach (GridViewRow row in this.gvReports.Rows)
            {
                LinkButton lnkReport = row.FindControl("lnkReport") as LinkButton;
                lnkReport.Attributes.Add("onClick", "window.open('" + UTF8.DecryptText(Server.UrlDecode(lnkReport.CommandArgument)).Replace("http://home.nucomm.net/applications/ReportingServicesPrototype/CustomView.aspx?Report=[NuSkillCheckV2]", "ViewReports.aspx?report=") + "')");
            }
        }
    }

    protected void gvReports_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Report")
        {
        }
    }
}
