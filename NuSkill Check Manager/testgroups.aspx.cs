using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class testgroups : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            this.gvGroups.DataSource = TestGroup.SelectAll();
            this.gvGroups.DataBind();
        }
    }

    protected void gvGroups_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            this.gvExams.DataSource = TestCategory.SearchByTestGroup(Convert.ToInt32(e.CommandArgument));
            this.gvExams.DataBind();
            this.mpeViewExams.Show();
        }
    }

    protected void gvExams_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(2))
        {
            Label lblExamID = this.gvExams.Rows[this.gvExams.SelectedIndex].FindControl("lblExamID") as Label;
            TestCategory category = null;
            category = TestCategory.Select(Convert.ToInt32(lblExamID.Text.Trim()), true);
            if (category != null)
            {
                SessionManager.SessionTestCategoryID = category.TestCategoryID;
                Response.Redirect("~/exam.aspx?" + Request.QueryString.ToString());
            }
        }
        else
        {

        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        this.gvGroups.DataSource = TestGroup.SelectAll();
        this.gvGroups.DataBind();
    }
}
