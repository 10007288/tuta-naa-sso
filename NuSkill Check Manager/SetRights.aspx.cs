using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class SetRights : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
            this.ddlRightsLevel.DataSource = RightsLevel.SelectAll();
            this.ddlRightsLevel.DataBind();
            if (Convert.ToInt32(this.ddlRightsLevel.SelectedValue) == 1)
            {
                this.trRoles.Visible = false;
                this.trUsers.Visible = true;
            }
            else if (Convert.ToInt32(this.ddlRightsLevel.SelectedValue) == 2)
            {
                this.ddlRoles.DataSource = UserRights.GetRoles();
                this.ddlRoles.DataBind();
                this.trRoles.Visible = true;
                this.trUsers.Visible = false;
            }
        }
    }
    protected void ddlRightsLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(this.ddlRightsLevel.SelectedValue) == 1)
        {
            this.trRoles.Visible = false;
            this.trUsers.Visible = true;
        }
        else if (Convert.ToInt32(this.ddlRightsLevel.SelectedValue) == 2)
        {
            this.ddlRoles.DataSource = UserRights.GetRoles();
            this.ddlRoles.DataBind();
            this.trRoles.Visible = true;
            this.trUsers.Visible = false;
        }
    }
    protected void btnApplyRules_Click(object sender, EventArgs e)
    {
        this.tblRights.Visible = true;
        this.tblSelect.Visible = false;
        bool retval = false;
        if (Convert.ToInt32(this.ddlRightsLevel.SelectedValue) == 1)
        {
            retval = this.usrUserRightsControl.PopulateRights(this.txtUser.Text.Trim(), 1);
            this.lblApplyingTo.Text = "Applying to user: " + this.txtUser.Text.Trim();
        }
        else if (Convert.ToInt32(this.ddlRightsLevel.SelectedValue) == 2)
        {
            retval = this.usrUserRightsControl.PopulateRights(this.ddlRoles.SelectedValue, 2);
            this.lblApplyingTo.Text = "Applying to role: " + this.ddlRoles.SelectedValue + " - " + this.ddlRoles.SelectedItem.Text;
        }
        if (retval)
            this.lblHasRights.Text = "true";
        this.tblRights.Visible = true;
    }
    protected void btnRightChangeOk_Click(object sender, EventArgs e)
    {
        try
        {
            string saveString = "";
            int counter = 0;
            foreach (GridViewRow row in this.usrUserRightsControl.GvRights.Rows)
            {
                Label lblAppRights = row.FindControl("lblAppRights") as Label;
                CheckBox chkAppRights = row.FindControl("chkAppRights") as CheckBox;
                if (lblAppRights != null && chkAppRights != null)
                {
                    int temp = Convert.ToInt32(lblAppRights.Text.Trim());
                    if (temp == counter)
                    {
                        if (chkAppRights.Checked)
                            saveString = "1" + saveString;
                        else
                            saveString = "0" + saveString;
                        counter++;
                    }
                    else
                    {
                        throw new Exception("Rights are missing.");
                    }
                }
            }
            UserRights rights = new UserRights();
            if (Convert.ToInt32(ddlRightsLevel.SelectedValue) == 1)
                rights = UserRights.Select(this.txtUser.Text.Trim(), Convert.ToInt32(ddlRightsLevel.SelectedValue));
            else if (Convert.ToInt32(ddlRightsLevel.SelectedValue) == 2)
                rights = UserRights.Select(this.ddlRoles.SelectedValue, Convert.ToInt32(ddlRightsLevel.SelectedValue));
            if (lblHasRights.Text == "true")
            {
                rights.Rights = saveString;
                rights.EditRights();
            }
            else
            {
                rights = new UserRights(this.txtUser.Text.Trim(), saveString);
                rights.Insert();
            }
            this.lblStatus.Text = "Success";
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "setrights.aspx", "btnRightChangeOk_Click", ex.Message);
            this.lblStatus.Text = "Failed to set rights.";
        }
    }
    protected void btnRightChangeCancel_Click(object sender, EventArgs e)
    {
        this.tblRights.Visible = false;
        this.lblHasRights.Text = "false";
    }
    protected void btnStatusConfirm_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/SetRights.aspx?" + Request.QueryString.ToString());
    }
}
