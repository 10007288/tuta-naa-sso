<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="customcampaigns.aspx.cs" Inherits="customcampaigns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnAddNew" runat="server" Text="Add New" CssClass="buttons" OnClick="btnAddNew_Click" />
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px" Width="100%">
                    <table cellpadding="10" cellspacing="0" style="font-family: Arial; text-align: left;
                        width: 100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTeamSummaries" runat="server" Text="Test Categories" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ajax:Accordion ID="accCampaigns" runat="server" Width="100%" OnItemCommand="accCampaigns_RowEditing" RequireOpenedPane ="false" SelectedIndex="-1">
                                    <HeaderTemplate>
                                        <br />
                                        <table width="100%" cellpadding="5" cellspacing="0" style="border-bottom:solid 1px gray;background-color:#D8D8D8;">
                                            <tr>
                                                <td style="width:70px">
                                                    <asp:Label ID="lblCampaignID" runat="server" Text='<%#Bind("CampaignID") %>' />
                                                </td>
                                                <td style="width: 626px">
                                                    <asp:Label ID="lblCampaign" runat="server" Text='<%#Bind("Campaign") %>' />
                                                </td>
                                                <td style="widows: 80px" align="right">
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Modify" CommandArgument='<%#Bind("CampaignID") %>' Visible="false" />
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table width="100%" cellpadding="3" cellspacing="0">
                                            <tr>
                                                <td align="right">
                                                    <asp:GridView ID="gvCampaigns" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                        ForeColor="black" CellPadding="5" BorderColor="black" Font-Bold="false" Width="95%"
                                                        EmptyDataText="No Subcategories found." OnRowCommand="gvCampaigns_RowEditing">
                                                        <HeaderStyle HorizontalAlign="center" />
                                                        <RowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" Wrap="true" />
                                                        <AlternatingRowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" />
                                                        <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                                                            NextPageText=">" Position="Bottom" PreviousPageText="<" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCampaignID" runat="server" Text='<%#Bind("CampaignID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Category">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCampaignName" runat="server" Text='<%#Bind("Campaign") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Start Date">
                                                                <ItemStyle HorizontalAlign="center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("StartDate") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="End Date">
                                                                <ItemStyle HorizontalAlign="center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("EndDate") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Parent ID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblParentCampaignID" runat="server" Text='<%#Bind("ParentCampaignID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Hidden">
                                                                <ItemStyle HorizontalAlign="center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHide" runat="server" Text='<%#Bind("Hide") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemStyle HorizontalAlign="center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandArgument='<%#Bind("CampaignID") %>'
                                                                        CommandName="Modify" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </ajax:Accordion>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeEdit" runat="server" TargetControlID="hidEdit" PopupControlID="pnlEdit"
        BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidEdit" runat="server" />
    <asp:Panel ID="pnlEdit" runat="server" Width="500px" CssClass="modalPopup">
        <table width="100%">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblRequired" runat="server" Font-Size="10px" Text="All fields are required." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblParentCampaignID" runat="server" Text="Parent Campaign ID:" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlParentCampaignID" runat="server" DataValueField="CampaignID"
                        DataTextField="Campaign" AppendDataBoundItems="true">
                        <asp:ListItem Text="None" Value="0" />
                    </asp:DropDownList>
                    <asp:Label ID="lblParentCampaignIDValue" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCampaignVal" runat="server" Visible="false" />
                    <asp:Label ID="lblCampaignName" runat="server" Text="Test Category:" />
                </td>
                <td>
                    <asp:TextBox ID="txtCampaignName" runat="server" MaxLength="50" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblStartDate" runat="server" Text="Start Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtStartDate" runat="server" />
                    <asp:Image ID="imgStartDate" runat="server" ImageUrl="~/images/icon-calendar.gif" />
                    <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtStartDate"
                        PopupButtonID="imgStartDate" Format="yyyy/MM/dd" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEndDate" runat="server" Text="End Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtEndDate" runat="server" />
                    <asp:Image ID="imgEndDate" runat="server" ImageUrl="~/images/icon-calendar.gif" />
                    <ajax:CalendarExtender ID="calEndDate" runat="server" TargetControlID="txtEndDate"
                        PopupButtonID="imgEndDate" Format="yyyy/MM/dd" />
                </td>
            </tr>
            <tr id="trHide" runat="server">
                <td colspan="2">
                    <asp:CheckBox ID="chkHide" runat="server" Text="Hide" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="buttons" OnClick="btnOK_Click" />
                </td>
                <td align="right">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
