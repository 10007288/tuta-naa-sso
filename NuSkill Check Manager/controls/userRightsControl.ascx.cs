using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class controls_userRightsControl : System.Web.UI.UserControl
{

    public GridView GvRights
    {
        get { return this.gvRights; }
    }

    public CheckBoxList CblBasicRights
    {
        get { return this.cblBasicRights; }
    }

    public Panel PnlRightsPopup
    {
        get { return this.pnlRightsPopup; }
    }

    public Panel PnlBasicRightsPopup
    {
        get { return this.pnlBasicRightsPopup; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.cblBasicRights.DataSource = PresetRights.SelectAll();
            this.cblBasicRights.DataBind();
        }
    }

    public bool PopulateRights()
    {
        return this.PopulateRights(SessionManager.SessionExamineeName, 1);
    }

    public bool PopulateRights(string rightsName, int rightsLevel)
    {
        try
        {

            int counter = 0;
            this.gvRights.DataSource = AppRights.SelectAll();
            this.gvRights.DataBind();
            UserRights rights = UserRights.Select(rightsName, rightsLevel);
            int[] x = new int[rights.Rights.Length];
            for (int len = 0; len < rights.Rights.Length; len++)
            {
                x[len] = Convert.ToInt32(rights.Rights.Substring(len, 1));
            }
            if (rights != null)
            {
                Array.Reverse(x);
                foreach (GridViewRow row in this.gvRights.Rows)
                {
                    Label lblAppRights = row.FindControl("lblAppRights") as Label;
                    CheckBox chkAppRights = row.FindControl("chkAppRights") as CheckBox;
                    if (lblAppRights != null && chkAppRights != null)
                    {
                        for (int len = rights.Rights.Length - 1; len >= 0; len--)
                        {
                            if (x[len] == 1 && len == Convert.ToInt32(lblAppRights.Text.Trim()) - 1)
                            {
                                chkAppRights.Checked = true;
                                break;
                            }
                        }
                    }
                }
                Array.Reverse(x);
                foreach (ListItem item in this.cblBasicRights.Items)
                {
                    bool isFit = true;
                    string[] itemrights = new string[item.Value.Length];
                    for (int len2 = 0; len2 < rights.Rights.Length; len2++)
                    {
                        itemrights[len2] = Convert.ToString(item.Value.Substring(len2, 1));
                    }
                    //= item.Value.ToCharArray();
                    for (int len = 0; len < rights.Rights.Length; len++)
                    {
                        if (Convert.ToInt32(x[len]) < Convert.ToInt32(itemrights[len]))
                        {
                            isFit = false;
                            break;
                        }
                    }
                    if (isFit)
                        item.Selected = true;
                    else
                        item.Selected = false;
                }
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userrightscontrol.ascx", "PopulateRights", ex.Message);
            }
            return false;
        }
    }
}
