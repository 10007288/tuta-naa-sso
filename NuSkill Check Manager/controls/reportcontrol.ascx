<%@ Control Language="C#" AutoEventWireup="true" CodeFile="reportcontrol.ascx.cs"
    Inherits="controls_reportcontrol" %>
<asp:Panel ID="pnlExam" runat="server" HorizontalAlign="center" Font-Size="12px">
        <table cellpadding="10" width="100%" cellspacing="0">
            <tr>
                <td colspan="2" align="right">
                    <asp:Label ID="lblReportType" runat="server" Text="Choose Report:" />
                </td>
                <td colspan="2" align="left">
                    <asp:DropDownList ID="ddlReportType" runat="server" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged"
                        AutoPostBack="true" />
                </td>
            </tr>
        </table>
        <table cellpadding="10" cellspacing="0">
            <tr id="trByCourse" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblByCourseCourse" runat="server" Text="Course:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByCourseCourse" runat="server" />
                </td>
                <td align="right">
                    <asp:Label ID="lblByCourseTest" runat="server" Text="Test:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByCourse" runat="server" />
                </td>
            </tr>
            <tr id="trBySSCE0" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblBySSCETest" runat="server" Text="Test:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlBySSCETest" runat="server" />
                </td>
                <td align="right">
                    <asp:Label ID="lblBySSCESite" runat="server" Text="Site:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlBySSCESite" runat="server" />
                </td>
            </tr>
            <tr id="trBySSCE1" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblBySSCECampaign" runat="server" Text="Campaign:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlBySSCECampaign" runat="server" />
                </td>
                <td align="right">
                    <asp:Label ID="lblBySSCECim" runat="server" Text="CIMNumber:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlBySSCECim" runat="server" />
                </td>
            </tr>
            <tr id="trByEmployee" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblByEmployeeTest" runat="server" Text="Test:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByEmployeeTest" runat="server" />
                </td>
                <td align="right">
                    <asp:Label ID="lblByEmployeeCim" runat="server" Text="CIM Number:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByEmployeeCim" runat="server" />
                </td>
            </tr>
            <tr id="trByQE0" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblByQETest" runat="server" Text="Test:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByQETest" runat="server" />
                </td>
                <td align="right">
                    <asp:Label ID="lblByQEQuestion" runat="server" Text="Question:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByQEQuestion" runat="server" />
                </td>
            </tr>
            <tr id="trByQE1" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblByQECim" runat="server" Text="CIM Number:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByQECim" runat="server" />
                </td>
            </tr>
            <tr id="trByETR" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblByETRTest" runat="server" Text="Test:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByETRTest" runat="server" />
                </td>
            </tr>
            <tr id="trByTQPR" runat="server" visible="false">
                <td align="right">
                    <asp:Label ID="lblByTQPRTest" runat="server" Text="Test:" />
                </td>
                <td align="right">
                    <asp:DropDownList ID="ddlByTQPRTest" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button CssClass="buttons" ID="btnViewReport" runat="server" Text="View Report" />
                </td>
            </tr>
        </table>
        <table cellpadding="10" cellspacing="0" width="900px">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvReport" runat="server" AllowPaging="true" PageSize="20" EmptyDataText="No records found." BorderColor="black">
                        <HeaderStyle Font-Size="16px" />
                        <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                        <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                            NextPageText=">" Position="Bottom" PreviousPageText="<" />
                        <RowStyle BorderColor="black" />
                        <RowStyle ForeColor="black" Font-Size="14px" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
</asp:Panel>
