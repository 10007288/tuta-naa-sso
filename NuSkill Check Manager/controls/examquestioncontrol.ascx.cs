using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using AjaxControlToolkit;

public partial class controls_examquestioncontrol : System.Web.UI.UserControl
{
    private string rolItems = "rolItems";
    private string rolSet = "rolSet";

    //private List<Interface> RolItems
    //{
    //    get { return (List<Interface>)HttpContext.Current.Session[rolItems]; }
    //    set { HttpContext.Current.Session[rolItems] = value; }
    //}

    private const string wrongImage = @"~/images/icon-fail.gif";

    public string WrongImage
    {
        get { return wrongImage; }
    }

    private const string rightImage = @"~/images/icon-pass.gif";

    public string RightImage
    {
        get { return rightImage; }
    }

    private DataSet RolSet
    {
        get { return (DataSet)HttpContext.Current.Session[rolSet]; }
        set { HttpContext.Current.Session[rolSet] = value; }
    }

    private bool IsFromSaveTestResponse
    {
        get { return (bool)this.ViewState["isFromTestResponse"]; }
        set { this.ViewState["isFromTestResponse"] = value; }
    }

    #region Public Properties
    public Panel PnlMultipleChoice
    {
        get { return this.pnlMultipleChoice; }
    }

    public Panel PnlTrueOrFalse
    {
        get { return this.pnlTrueOrFalse; }
    }

    public Panel PnlSequencing
    {
        get { return this.PnlSequencing; }
    }

    public Panel PnlFillInTheBlanks
    {
        get { return this.pnlFillInTheBlanks; }
    }

    public Panel PnlHotspot
    {
        get { return this.PnlHotspot; }
    }

    public HtmlTableRow MultChoice1
    {
        get { return this.trCheck1; }
    }

    public HtmlTableRow MultChoice2
    {
        get { return this.trCheck2; }
    }

    public HtmlTableRow MultChoice3
    {
        get { return this.trCheck3; }
    }

    public HtmlTableRow MultChoice4
    {
        get { return this.trCheck4; }
    }

    public HtmlTableRow MultChoice5
    {
        get { return this.trCheck5; }
    }

    public HtmlTableRow MultChoice6
    {
        get { return this.trCheck6; }
    }

    public HtmlTableRow MultChoice7
    {
        get { return this.trCheck7; }
    }

    public HtmlTableRow MultChoice8
    {
        get { return this.trCheck8; }
    }

    public HtmlTableRow MultChoice9
    {
        get { return this.trCheck9; }
    }

    public HtmlTableRow MultChoice10
    {
        get { return this.trCheck10; }
    }

    public HtmlTableCell HotspotChoice1
    {
        get { return this.imgCell1; }
    }

    public HtmlTableCell HotspotChoice2
    {
        get { return this.imgCell2; }
    }

    public HtmlTableCell HotspotChoice3
    {
        get { return this.imgCell3; }
    }

    public HtmlTableCell HotspotChoice4
    {
        get { return this.imgCell4; }
    }

    public HtmlTableCell HotspotChoice5
    {
        get { return this.imgCell5; }
    }

    public HtmlTableCell HotspotChoice6
    {
        get { return this.imgCell6; }
    }

    public HtmlTableCell HotspotChoice7
    {
        get { return this.imgCell7; }
    }

    public HtmlTableCell HotspotChoice8
    {
        get { return this.imgCell8; }
    }

    public HtmlTableCell HotspotChoice9
    {
        get { return this.imgCell9; }
    }

    public HtmlTableCell HotspotChoice10
    {
        get { return this.imgCell10; }
    }

    //public ImageButton HotspotImage1
    //{
    //    get { return this.imgChoice1; }
    //}

    //public ImageButton HotspotImage2
    //{
    //    get { return this.imgChoice2; }
    //}

    //public ImageButton HotspotImage3
    //{
    //    get { return this.imgChoice3; }
    //}

    //public ImageButton HotspotImage4
    //{
    //    get { return this.imgChoice4; }
    //}

    //public ImageButton HotspotImage5
    //{
    //    get { return this.imgChoice5; }
    //}

    //public ImageButton HotspotImage6
    //{
    //    get { return this.imgChoice6; }
    //}

    //public ImageButton HotspotImage7
    //{
    //    get { return this.imgChoice7; }
    //}

    //public ImageButton HotspotImage8
    //{
    //    get { return this.imgChoice8; }
    //}

    //public ImageButton HotspotImage9
    //{
    //    get { return this.imgChoice9; }
    //}

    //public ImageButton HotspotImage10
    //{
    //    get { return this.imgChoice10; }
    //}
    #endregion

    public void Page_Populate()
    {
        try
        {
            this.pnlExamQuestion.Visible = true;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMultipleChoice.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            //Questionnaire q = new Questionnaire();
            //Get question from the session
            Questionnaire q = SessionManager.SessionSingleQuestion;
            SaveTestResponse response = null;
            //See if the user has saved the exam before
            if (SessionManager.SessionTestTaken != null)
                response = SaveTestResponse.Retrieve(SessionManager.SessionExamineeName, SessionManager.SessionTestTaken.TestTakenID, q.QuestionnaireID);

            if (q != null)
            {
                this.lblQuestion.Text = q.Question;
                if (string.IsNullOrEmpty(q.Hyperlink.Trim()))
                    this.hypHyperlink.Visible = false;
                else
                    this.hypHyperlink.NavigateUrl = q.Hyperlink;
                if (q.TypeCode == "multiple")
                {
                    pnlMultipleChoice.Visible = true;
                    if (string.IsNullOrEmpty(q.Choice10))
                        this.trCheck10.Visible = false;
                    else
                    {
                        this.chkChoice10.Text = q.Choice10;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice9))
                        this.trCheck9.Visible = false;
                    else
                    {
                        this.chkChoice9.Text = q.Choice9;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice8))
                        this.trCheck8.Visible = false;
                    else
                    {
                        this.chkChoice8.Text = q.Choice8;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice7))
                        this.trCheck7.Visible = false;
                    else
                    {
                        this.chkChoice7.Text = q.Choice7;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice6))
                        this.trCheck6.Visible = false;
                    else
                    {
                        this.chkChoice6.Text = q.Choice6;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice5))
                        this.trCheck5.Visible = false;
                    else
                    {
                        this.chkChoice5.Text = q.Choice5;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice4))
                        this.trCheck4.Visible = false;
                    else
                    {
                        this.chkChoice4.Text = q.Choice4;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice3))
                        this.trCheck3.Visible = false;
                    else
                    {
                        this.chkChoice3.Text = q.Choice3;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice2))
                        this.trCheck2.Visible = false;
                    else
                    {
                        this.chkChoice2.Text = q.Choice2;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (string.IsNullOrEmpty(q.Choice1))
                        this.trCheck1.Visible = false;
                    else
                    {
                        this.chkChoice1.Text = q.Choice1;
                        if (response != null)
                            this.chkChoice1.Enabled = false;
                    }

                    if (!string.IsNullOrEmpty(q.Ans1))
                        this.lblCorrectAnswerVal.Text += q.Ans1 + ",";
                    if (!string.IsNullOrEmpty(q.Ans2))
                        this.lblCorrectAnswerVal.Text += q.Ans2 + ",";
                    if (!string.IsNullOrEmpty(q.Ans3))
                        this.lblCorrectAnswerVal.Text += q.Ans3 + ",";
                    if (!string.IsNullOrEmpty(q.Ans4))
                        this.lblCorrectAnswerVal.Text += q.Ans4 + ",";
                    if (!string.IsNullOrEmpty(q.Ans5))
                        this.lblCorrectAnswerVal.Text += q.Ans5 + ",";
                    if (!string.IsNullOrEmpty(q.Ans6))
                        this.lblCorrectAnswerVal.Text += q.Ans6 + ",";
                    if (!string.IsNullOrEmpty(q.Ans7))
                        this.lblCorrectAnswerVal.Text += q.Ans7 + ",";
                    if (!string.IsNullOrEmpty(q.Ans8))
                        this.lblCorrectAnswerVal.Text += q.Ans8 + ",";
                    if (!string.IsNullOrEmpty(q.Ans9))
                        this.lblCorrectAnswerVal.Text += q.Ans9 + ",";
                    if (!string.IsNullOrEmpty(q.Ans10))
                        this.lblCorrectAnswerVal.Text += q.Ans10 + ",";
                    if (this.lblCorrectAnswerVal.Text.EndsWith(","))
                        this.lblCorrectAnswerVal.Text = this.lblCorrectAnswerVal.Text.Remove(this.lblCorrectAnswerVal.Text.Length - 1, 1);
                    this.lblCorrectAnswerVal.Visible = true;
                }
                else if (q.TypeCode == "hotspot")
                {
                    pnlHotspot.Visible = true;
                    if (string.IsNullOrEmpty(q.Choice10))
                        this.imgCell10.Visible = false;
                    else if (response != null)
                        this.imgChoice10.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice9))
                        this.imgCell9.Visible = false;
                    else if (response != null)
                        this.imgChoice9.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice8))
                        this.imgCell8.Visible = false;
                    else if (response != null)
                        this.imgChoice8.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice7))
                        this.imgCell7.Visible = false;
                    else if (response != null)
                        this.imgChoice7.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice6))
                        this.imgCell6.Visible = false;
                    else if (response != null)
                        this.imgChoice6.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice5))
                        this.imgCell5.Visible = false;
                    else if (response != null)
                        this.imgChoice5.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice4))
                        this.imgCell4.Visible = false;
                    else if (response != null)
                        this.imgChoice4.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice3))
                        this.imgCell3.Visible = false;
                    else if (response != null)
                        this.imgChoice3.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice2))
                        this.imgCell2.Visible = false;
                    else if (response != null)
                        this.imgChoice2.Enabled = false;

                    if (string.IsNullOrEmpty(q.Choice1))
                        this.imgCell1.Visible = false;
                    else if (response != null)
                        this.imgChoice1.Enabled = false;

                    if (!string.IsNullOrEmpty(q.Ans1))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans1;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans2))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans2;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans3))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans3;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans4))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans4;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans5))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans5;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans6))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans6;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans7))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans7;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans8))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans8;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans9))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans9;
                        this.imgCorrectAnswer1.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(q.Ans10))
                    {
                        this.imgCorrectAnswer1.ImageUrl = q.Ans10;
                        this.imgCorrectAnswer1.Visible = true;
                    }

                }
                else if (q.TypeCode == "torf")
                {
                    pnlTrueOrFalse.Visible = true;
                    if (response != null)
                    {
                        this.rdoFalse.Enabled = false;
                        this.rdoTrue.Enabled = false;
                    }
                    this.lblCorrectAnswerVal.Text = q.Ans1;
                    this.lblCorrectAnswerVal.Visible = true;
                }
                else if (q.TypeCode == "sequencing")
                {
                    List<TempQuestionnaire> list = new List<TempQuestionnaire>();
                    if (!string.IsNullOrEmpty(q.Choice1))
                        list.Add(new TempQuestionnaire(q.Choice1));

                    if (!string.IsNullOrEmpty(q.Choice2))
                        list.Add(new TempQuestionnaire(q.Choice2));

                    if (!string.IsNullOrEmpty(q.Choice3))
                        list.Add(new TempQuestionnaire(q.Choice3));

                    if (!string.IsNullOrEmpty(q.Choice4))
                        list.Add(new TempQuestionnaire(q.Choice4));

                    if (!string.IsNullOrEmpty(q.Choice5))
                        list.Add(new TempQuestionnaire(q.Choice5));

                    if (!string.IsNullOrEmpty(q.Choice6))
                        list.Add(new TempQuestionnaire(q.Choice6));

                    if (!string.IsNullOrEmpty(q.Choice7))
                        list.Add(new TempQuestionnaire(q.Choice7));

                    if (!string.IsNullOrEmpty(q.Choice8))
                        list.Add(new TempQuestionnaire(q.Choice8));

                    if (!string.IsNullOrEmpty(q.Choice9))
                        list.Add(new TempQuestionnaire(q.Choice9));

                    if (!string.IsNullOrEmpty(q.Choice10))
                        list.Add(new TempQuestionnaire(q.Choice10));

                    this.rolSequencing.DataSource = list;
                    this.rolSequencing.DataBind();
                    this.rolSequencing.AllowReorder = false;
                    pnlSequencing.Visible = true;
                    if (response != null)
                        this.rolSequencing.Enabled = false;

                    this.lstCorrectAnswers.Items.Clear();
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    this.lstCorrectAnswers.Items.Add(new ListItem(!string.IsNullOrEmpty(q.Ans1) ? q.Ans1 : string.Empty));
                    if (this.lstCorrectAnswers.Items.Count > 0)
                        this.lstCorrectAnswers.Visible = true;
                }
                else if (q.TypeCode == "fillblanks")
                {
                    pnlFillInTheBlanks.Visible = true;
                    this.lblCorrectAnswerVal.Text = q.Ans1;
                }
                else if (q.TypeCode == "essay")
                {
                    this.lblCorrectAnswerVal.Text = "There is no exact answer for an essay question.";
                }
                int totalCorrect = TestResponse.GetCorrectAnswer(q.QuestionnaireID);
                int totalAnswers = TestResponse.CountTestResponse(q.QuestionnaireID);
                Decimal percentage = totalAnswers == 0 ? 0 : (Convert.ToDecimal(totalCorrect) / Convert.ToDecimal(totalAnswers) * 100);
                this.lblPercentageCorrectVal.Text = percentage.ToString("00.00") + "% (" + totalCorrect.ToString() + " out of " + totalAnswers.ToString() + " exams)";
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "examstatuscontrol.ascx", "Page_Populate", ex.Message);
            }
        }
    }

    protected void btnList_Click(object sender, EventArgs e)
    {
        //foreach (Interface face in this.RolItems)
        //{
        //    this.txtEssay.Text += face.ADPEmployeeID + Environment.NewLine;
        //}
    }

    protected void rolSequencing_onItemReorder(object sender, ReorderListItemReorderEventArgs e)
    {
        //this.RolItems = (List<type>)this.rolSequencing.DataSource;
    }

    protected void imgClick(object sender, ImageClickEventArgs e)
    {
        ImageButton button = (ImageButton)sender;
        if (button.BorderWidth.Value > 0)
        {
            button.BorderColor = System.Drawing.Color.Empty;
            button.BorderWidth = 0;
        }
        else
        {
            button.BorderColor = System.Drawing.Color.Red;
            button.BorderWidth = 5;
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (SessionManager.SessionQuestionAction == "viewsinglequestion" && SessionManager.SessionExamAction == "viewsingleexam")
        {
            Response.Redirect("~/exam.aspx?" + Request.QueryString.ToString());
        }
        else if (SessionManager.SessionQuestionAction == "viewsinglequestion" && SessionManager.SessionExamAction == "editexam")
        {
            Response.Redirect("~/editexam.aspx?" + Request.QueryString.ToString());
        }

    }

    protected void btnSaveAndQuit_Click(object sender, EventArgs e)
    {

    }

    protected void btnConfirmSaveAndQuit_Click(object sender, EventArgs e)
    {
        foreach (SaveTestResponse response in SessionManager.SessionSaveTestResponse)
        {
            response.Insert();
        }
    }

    protected void GetScores()
    {

    }
    protected void lnkPeopleWhoAnswered_Click(object sender, EventArgs e)
    {
        try
        {
            //this.gvPeopleWhoAnswered.DataSource =
            if (SessionManager.SessionSingleQuestion.QuestionnaireID != 0 && SessionManager.SessionSingleQuestion.QuestionnaireID != null)
            {
                TestResponse[] responses = TestResponse.SelectByQuestion(SessionManager.SessionSingleQuestion.QuestionnaireID);

                DataTable table = new DataTable();
                table.Columns.Add("Username");
                table.Columns.Add("ImageUrl");
                DataRow row;

                foreach (TestResponse response in responses)
                {
                    row = table.NewRow();
                    row["Username"] = response.UserID;
                    Questionnaire question = Questionnaire.Select(response.QuestionnaireID, true);
                    if (response.Response1 == question.Ans1 && response.Response2 == question.Ans2 && response.Response3 == question.Ans3 &&
                         response.Response4 == question.Ans4 && response.Response5 == question.Ans5 && response.Response6 == question.Ans6 &&
                         response.Response7 == question.Ans7 && response.Response8 == question.Ans8 && response.Response9 == question.Ans9 &&
                         response.Response10 == question.Ans10)
                        row["Imageurl"] = this.RightImage;
                    else
                        row["ImageUrl"] = this.WrongImage;
                    table.Rows.Add(row);
                }
                DataSet set = new DataSet();
                set.Tables.Clear();
                set.Tables.Add(table);
                this.gvPeopleWhoAnswered.DataSource = set;
                this.gvPeopleWhoAnswered.DataBind();
                this.Page_Populate();
                this.mpePeopleWhoAnswered.Show();
            }
            else
            {
                this.mpeNotSaved.Show();
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "examstatuscontrol.ascx", "lnkPeopleWhoAnswered_Click", ex.Message);
            }
        }
    }
}
