<%@ Control Language="C#" AutoEventWireup="true" CodeFile="suggestedquestioncontrol.ascx.cs" Inherits="controls_suggestedquestioncontrol" %>
<asp:Panel ID="pnlExamQuestion" runat="server" Width="100%" HorizontalAlign="center">
    <%--border-color: #4682b4; background-color: White;">--%>
    <table cellpadding="5" width="100%" cellspacing="0" border="1">
        <tr>
            <td align="center" style="vertical-align: middle; font-size: 16px; font-family: Arial;" colspan="2">
                <asp:Label ID="lblQuestion" runat="server" CssClass="testQuestion"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" style="vertical-align: middle; font-size: large; font-family: Arial;" colspan="2">
                <table width="100%" cellpadding="5" style="vertical-align: middle; font-family: Arial;
                    font-size: 12px;">
                    <tr>
                        <td align="left" style="padding: 30px 10px 10px 10px">
                            <asp:Panel ID="pnlMultipleChoice" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr id="trCheck1" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck2" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck3" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice3" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck4" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice4" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck5" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice5" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck6" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice6" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck7" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice7" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck8" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice8" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck9" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice9" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck10" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice10" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlTrueOrFalse" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="rdoTrue" runat="server" Text="True" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="rdoFalse" runat="server" Text="False" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlSequencing" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <ajax:ReorderList ID="rolSequencing" runat="server" DragHandleAlignment="left" PostBackOnReorder="false"
                                                OnItemReorder="rolSequencing_onItemReorder" EnableViewState="true" AllowReorder="false">
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlTemplate" runat="server" BackColor="#C1D7E7" Height="18px">
                                                        <asp:Label ID="lblTemplate" runat="server" Text='<%#Bind("Item") %>' />
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <DragHandleTemplate>
                                                    <asp:Panel ID="pnlHandle" runat="server" BackColor="#80A7F0" Width="24px" Height="18px"
                                                        BorderWidth="1">
                                                        <asp:Image ID="imgArrow" runat="server" ImageUrl="~/images/arrow.JPG" />
                                                    </asp:Panel>
                                                </DragHandleTemplate>
                                            </ajax:ReorderList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlFillInTheBlanks" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFillInTheBlanks" runat="server" Width="400px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlHotspot" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5" width="100%">
                                    <tr>
                                        <td id="imgCell1" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice1" runat="server" />
                                        </td>
                                        <td id="imgCell2" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice2" runat="server" />
                                        </td>
                                        <td id="imgCell3" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice3" runat="server" />
                                        </td>
                                        <td id="imgCell4" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice4" runat="server" />
                                        </td>
                                        <td id="imgCell5" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice5" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="imgCell6" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice6" runat="server" />
                                        </td>
                                        <td id="imgCell7" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice7" runat="server" />
                                        </td>
                                        <td id="imgCell8" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice8" runat="server" />
                                        </td>
                                        <td id="imgCell9" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice9" runat="server" />
                                        </td>
                                        <td id="imgCell10" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice10" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; font-family: Arial; font-size: small;">
                            <asp:Label ID="lblCorrectAnswer" runat="server" Text="Correct Answer:" />
                            <br />
                            <asp:Label ID="lblCorrectAnswerVal" runat="server" Visible="false" Text="" />
                            <asp:ListBox ID="lstCorrectAnswers" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer1" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer2" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer3" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer4" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer5" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer6" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer7" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer8" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer9" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer10" runat="server" Visible="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>