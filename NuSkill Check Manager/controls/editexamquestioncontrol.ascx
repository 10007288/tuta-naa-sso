<%@ Control Language="C#" AutoEventWireup="true" CodeFile="editexamquestioncontrol.ascx.cs"
    Inherits="controls_editexamquestioncontrol" %>
<style type="text/css">
    .style1
    {
        width: 111px;
    }
    .style2
    {
        height: 34px;
        width: 111px;
    }
    </style>
<asp:Panel ID="pnlExamQuestion" runat="server" Width="900px" HorizontalAlign="center"
    ForeColor="black">
    <%--border-color: #4682b4; background-color: White;">--%>
    <table cellpadding="5" width="100%" cellspacing="0" style="height: 400px; vertical-align: text-top">
        <tr>
            <td align="center">
                <table cellpadding="5" cellspacing="0" style="vertical-align: middle;
                    font-family: Arial; font-size: 12px;" width="100%">
                    <tr>
                        <td align="left" 
                            style="vertical-align: middle; font-size: 12px; font-family: Arial;">
                            <asp:Label ID="lblQuestion" runat="server" 
                                Text="Enter question/instructions here:" />
                            <br />
                            <asp:TextBox ID="txtQuestion" runat="server" MaxLength="2000" Width="600px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" 
                            style="vertical-align: middle; font-size: 12px; font-family: Arial">
                            <asp:Label ID="lblHyperlink" runat="server" 
                                Text="Enter a URL for the question here, if applicable:" />
                            <br />
                            <asp:TextBox ID="txtHyperlink" runat="server" MaxLength="2000" Width="600px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="vsmItems" runat="server" DisplayMode="List" 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoQuestion" runat="server" Display="none" 
                                ErrorMessage="Provide a question." ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsMultipleChoice" runat="server" 
                                Display="none" 
                                ErrorMessage="Provide at least two choices and at least one correct answer." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateMultipleChoice" runat="server" 
                                Display="none" ErrorMessage="Make sure there are no duplicate answers." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvItemSkipMultipleChoice" runat="server" 
                                Display="none" 
                                ErrorMessage="Make sure there are no skipped spaces for the answers." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvCorrectBlankMultipleChoice" runat="server" 
                                Display="none" 
                                ErrorMessage="Make sure no blanks are marked as correct answers." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsTrueOrFalse" runat="server" Display="none" 
                                ErrorMessage="Provide the correct answer." ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsYesOrNo" runat="server" Display="none" 
                                ErrorMessage="Provide the correct answer." ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsSequencing" runat="server" Display="none" 
                                ErrorMessage="Provide at least two choices and their proper order." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateSequencing" runat="server" Display="none" 
                                ErrorMessage="Make sure there are no duplicate answers." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvOptionMismatchSequencing" runat="server" 
                                Display="None" 
                                ErrorMessage="Make sure the answers in the Random Order and the Correct Order are the same." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvItemSkipSequencing" runat="server" Display="none" 
                                ErrorMessage="Make sure there are no skipped spaces for both columns." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvCountMismatchSequencing" runat="server" 
                                Display="none" 
                                ErrorMessage="Provide the same number of items in the Random Order and the Correct Order columns." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvItemSkipMatching" runat="server" Display="none" 
                                ErrorMessage="Make sure there are no skipped spaces for all columns." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsMatching" runat="server" Display="none" 
                                ErrorMessage="Provide at least two choices on the right column." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoItemsMatching" runat="server" Display="none" 
                                ErrorMessage="Provide at least two items on the left column." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvInvalidChoiceMatching" runat="server" Display="none" 
                                ErrorMessage="Make sure all answers are valid (no blanks)." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateMatching" runat="server" Display="none" 
                                ErrorMessage="Make sure there are no duplicate items in each column." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsHotspot" runat="server" Display="none" 
                                ErrorMessage="Provide at least two choices and at least one correct answer." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoScoreEssay" runat="server" Display="none" 
                                ErrorMessage="Provide the maximum score for the essay." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoChoiceFillBlanks" runat="server" Display="none" 
                                ErrorMessage="Choose a non-blank correct answer." ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoSkipFillBlanks" runat="server" Display="none" 
                                ErrorMessage="Make sure there are no skipped spaces for the answers." 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateFillBlanks" runat="server" Display="none" 
                                ErrorMessage="Make sure there are no duplicate answers." 
                                ValidationGroup="ValItems" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblItemTypes" runat="server" Text="Choose the item type:" />
                            <br />
                            <asp:DropDownList ID="ddlItemtypes" runat="server" AutoPostBack="true" 
                                OnSelectedIndexChanged="ddlItemtypes_SelectedIndexChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Panel ID="pnlMultipleChoice" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5" cellspacing="0">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblInsertAnswers" runat="server" 
                                                Text="Enter answers here(up to a maximum of 10), then mark the correct ones." />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck1" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice1" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice1" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck2" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice2" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice2" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck3" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice3" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice3" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck4" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice4" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice4" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck5" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice5" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice5" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck6" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice6" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice6" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck7" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice7" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice7" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck8" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice8" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice8" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck9" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice9" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice9" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="trCheck10" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice10" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice10" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlTrueOrFalse" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTrueOrFalseCorrect" runat="server" 
                                                Text="Please mark the correct answer:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoTrue" runat="server" GroupName="torf" Text="True" 
                                                TextAlign="right" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoFalse" runat="server" GroupName="torf" Text="False" 
                                                TextAlign="right" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlYesOrno" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblYesOrNoCorrect" runat="server" 
                                                Text="Please mark the correct answer:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoYes" runat="server" GroupName="yorn" Text="Yes" 
                                                TextAlign="right" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoNo" runat="server" GroupName="yorn" Text="No" 
                                                TextAlign="right" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlSequencing" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblSequencing" runat="server" 
                                                Text="Enter the answers in the order they will appear in the test in the left column. Enter the correct order in the right column." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblRandomOrder" runat="server" Text="Randomized Order:" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCorrectOrder" runat="server" Text="Correct Order:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox1" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox1" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox2" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox2" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox3" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox3" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox4" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox4" runat="server" Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox5" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox5" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox6" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox6" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox7" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox7" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox8" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox8" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox9" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox9" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox10" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox10" runat="server" MaxLength="1000" 
                                                Width="310px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlFillInTheBlanks" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label1" runat="server" 
                                                Text="Enter answers here(up to a maximum of 10), then mark the correct answer." />
                                        </td>
                                    </tr>
                                    <tr ID="tr1" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill1" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill1" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr2" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill2" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill2" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr3" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill3" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill3" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr4" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill4" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill4" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr5" runat="server">
                                        <td style="height: 34px">
                                            <asp:TextBox ID="txtFill5" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style2">
                                            <asp:RadioButton ID="rdoFill5" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr6" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill6" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill6" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr7" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill7" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill7" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr8" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill8" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill8" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr9" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill9" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill9" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr ID="tr10" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill10" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoFill10" runat="server" GroupName="FillInTheBlanks" 
                                                Text="Correct Answer" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlEssay" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEssayScore" runat="server" 
                                                Text="Indicate the maximum score for this essay:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtEssayScore" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlMatchingType" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" 
                                                Text="Enter the items in the order they will appear in the test in the second column. Enter the list of choices in the right column. Enter the corresponding letter of the correct choice in the left column." />
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch1" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch1" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch1" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem1" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter1" runat="server" Text="A:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice1" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch2" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch2" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch2" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem2" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter2" runat="server" Text="B:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice2" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch3" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch3" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch3" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem3" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter3" runat="server" Text="C:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice3" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch4" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch4" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch4" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem4" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter4" runat="server" Text="D:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice4" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch5" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch5" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch5" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem5" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter5" runat="server" Text="E:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice5" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch6" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch6" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch6" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem6" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter6" runat="server" Text="F:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice6" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch7" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch7" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch7" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem7" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter7" runat="server" Text="G:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice7" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch8" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch8" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch8" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem8" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter8" runat="server" Text="H:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice8" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch9" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch9" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch9" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem9" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter9" runat="server" Text="I:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice9" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch10" runat="server" MaxLength="1" Width="20px" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch10" runat="server" 
                                                FilterMode="validchars" FilterType="Custom" TargetControlID="txtCorrectMatch10" 
                                                ValidChars="ABCDEFGHIJabcdefghij" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem10" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter10" runat="server" Text="J:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice10" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlHotspot" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5" width="100%">
                                    <tr>
                                        <td colspan="5">
                                            <asp:Label ID="lblHotspot" runat="server" 
                                                Text="Choose up to ten images and mark the correct answers by clicking on them." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <asp:FileUpload ID="fupHotspot" runat="server" />
                                            <br />
                                            <asp:Button ID="btnAdd" runat="server" CssClass="buttons" 
                                                OnClick="btnAdd_Click" Text="Add" Width="60px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <asp:CustomValidator ID="cvImageExists" runat="server" Display="dynamic" 
                                                ErrorMessage="File already exists." />
                                            <asp:CustomValidator ID="cvTooMany" runat="server" Display="dynamic" 
                                                ErrorMessage="There are already ten images in this question." />
                                            <asp:CustomValidator ID="cvNoImage" runat="server" Display="dynamic" 
                                                ErrorMessage="Browse for an image before clicking the Add button." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td ID="imgCell1" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice1" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove1" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell2" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice2" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove2" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell3" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice3" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove3" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell4" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice4" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove4" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell5" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice5" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove5" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td ID="imgCell6" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice6" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove6" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell7" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice7" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove7" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell8" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice8" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove8" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell9" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice9" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove9" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                        <td ID="imgCell10" runat="server" align="center" valign="middle" 
                                            visible="false">
                                            <asp:ImageButton ID="imgChoice10" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button ID="btnRemove10" runat="server" CssClass="buttons" 
                                                OnClick="RemoveImage" Text="Remove" Width="100px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                            <asp:Panel ID="pnlRate" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label7" runat="server" 
                                                Text="Enter rating here(up to a maximum of 10)." />
                                        </td>
                                    </tr>
                                    <tr ID="tr11" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate1" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate1" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr12" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate2" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate2" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr13" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate3" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate3" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr14" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate4" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate4" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr15" runat="server">
                                        <td style="height: 34px">
                                            <asp:TextBox ID="txtFillRate5" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate5" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr16" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate6" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate6" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr17" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate7" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate7" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr18" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate8" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate8" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr19" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate9" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate9" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr20" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate10" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate10" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr41" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillRate11" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td class="style1">
                                            <asp:RadioButton ID="rdoRate11" runat="server" GroupName="Rate" Text="" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                            <asp:Panel ID="pnlComments" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" 
                                                Text="Comments" />
                                           <table cellpadding="5" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                            <asp:Panel ID="pnlDropDown" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label3" runat="server" 
                                                Text="Enter values for drop down here(up to a maximum of 10)" />
                                        </td>
                                    </tr>
                                    <tr ID="tr21" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD1" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr22" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD2" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr23" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD3" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr24" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD4" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr25" runat="server">
                                        <td style="height: 34px">
                                            <asp:TextBox ID="txtFillDD5" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr26" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD6" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr27" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD7" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr28" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD8" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr29" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD9" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr ID="tr30" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFillDD10" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                            <asp:Panel ID="pnlMultipleAnswer" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5" cellspacing="0">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label4" runat="server" 
                                                Text="Enter choices here(up to a maximum of 10)." />
                                        </td>
                                    </tr>
                                    <tr ID="tr31" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA1" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA1" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr32" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA2" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA2" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr33" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA3" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA3" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr34" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA4" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA4" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr35" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA5" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA5" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr36" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA6" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA6" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr37" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA7" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA7" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr38" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA8" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA8" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr39" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA9" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA9" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr ID="tr40" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtMA10" runat="server" MaxLength="1000" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkMA10" runat="server" Text="" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                            <asp:Panel ID="pnlCalendar" runat="server" Visible="false" Width="100%">
                                <table cellpadding="5" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" 
                                                Text="Calendar" />
                                           <table cellpadding="5" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDate" runat="server" /><asp:ImageButton ID="imgCalendarStartDate"
                                                            runat="server" ImageUrl="~/images/icon-calendar.gif" />
                                                        <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtDate"
                                                            PopupButtonID="imgCalendarStartDate" Format="yyyy/MM/dd" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <%--ADDED BY JAY MILLARE <01/12/2015>--%>
                    <tr>
                        <td align="left" 
                            style="vertical-align: middle; font-size: 12px; font-family: Arial">
                            <label>
                            Upload a reference picture here, if applicable:</label><br />
                            <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:FileUpload ID="imgUpload" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnContinue" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" valign="middle" style="font-family: Arial;">
                <asp:Button CssClass="buttons" ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
