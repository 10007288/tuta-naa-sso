/******************************************************************************/
/* Class Filename   -   editexamcontrol.ascx.cs                               */
/* Namespace        -   None                                                  */
/* Description      -   Codefile for creating and editing exams               */
/* Used By          -   NuSkillCheck V2 Manager                               */
/* Created          -   20080702, Anna Aleta P. Oandasan (30988)              */
/* Tandim           -   730367                                                */
/******************************************************************************/
/* ChangeLog                                                                  */
/* 20080702         -   Initial Version                                       */
/******************************************************************************/
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using NuSkill.Business;
using NuComm.Security.Encryption;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class controls_editexamcontrol : System.Web.UI.UserControl
{
    const string pageFormName = "editexamcontrol.ascx";

    private int Deleted
    {
        get { return (int)this.ViewState["delete"]; }
        set { this.ViewState["delete"] = value; }
    }

    string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());

        CheckUserRole();

        if (!this.IsPostBack)
        {
            try
            {
                if (SessionManager.SessionTestCategoryID != 0)
                {
                    this.gvGridItems.DataSource = Questionnaire.SelectByCategory(SessionManager.SessionTestCategoryID);
                    this.gvGridItems.DataBind();
                }

                CheckUserRole();
                this.Populategroups();
                this.ddlAccounts.DataSource = NonCimCampaign.SelectParents();
                this.ddlAccounts.DataBind();

                if (_Role != null)
                {
                    this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
                    this.ddlSubCategory.DataBind();
                }
                else
                {
                    this.ddlSubCategory.DataSource = NonCimCampaign.SelectCatSubFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), Convert.ToInt32(SessionManager.SessionUsername));
                    this.ddlSubCategory.DataBind();
                }

                this.PopulateHourDropDownList(this.ddlStartHour);;
                this.PopulateHourDropDownList(this.ddlEndHour);
                this.PopulateMinuteDropDownList(this.ddlEndMinute);
                this.PopulateMinuteDropDownList(this.ddlStartMinute);

                if (SessionManager.SessionTestCategoryID != 0)
                {
                    this.txtHotlink.Text = "http://home.nucomm.net/applications/nuskillcheckv2/hotlink.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(SessionManager.SessionTestCategoryID.ToString()));
                }

                #region Creating a New Exam
                if (SessionManager.SessionExamAction == "newexam")
                {
                    this.ddlStartHour.SelectedIndex = -1;
                    this.ddlStartMinute.SelectedIndex = -1;
                    this.ddlEndHour.SelectedIndex = -1;
                    this.ddlEndMinute.SelectedIndex = -1;

                    if (SessionManager.SessionQuestionnaire == null)
                        SessionManager.SessionQuestionnaire = new List<Questionnaire>();

                    TestCategory category = SessionManager.SessionTempCategory;
                    if (category != null)
                    {
                        this.txtExamName.Text = category.TestName;
                        if (!string.IsNullOrEmpty(category.Instructions))
                            this.txtInstructions.Text = category.Instructions;

                        this.txtStartDate.Text = category.StartDate.ToString().Contains("01/01/1901") || category.StartDate.ToString().Contains("1/1/0001") ? string.Empty : category.StartDate.ToString("yyyy/MM/dd");
                        this.txtEndDate.Text = category.EndDate.ToString().Contains("01/01/1901") || category.EndDate.ToString().Contains("1/1/0001") ? string.Empty : category.EndDate.ToString("yyyy/MM/dd");
                        
                        foreach (ListItem item in this.ddlStartHour.Items)
                            if (Convert.ToInt32(item.Value) == category.StartDate.Hour)
                            {
                                item.Selected = true;
                                break;
                            }

                        foreach (ListItem item in this.ddlEndHour.Items)
                            if (Convert.ToInt32(item.Value) == category.EndDate.Hour)
                            {
                                item.Selected = true;
                                break;
                            }

                        foreach (ListItem item in this.ddlStartMinute.Items)
                            if (Convert.ToInt32(item.Value) == category.StartDate.Minute)
                            {
                                item.Selected = true;
                                break;
                            }

                        foreach (ListItem item in this.ddlEndMinute.Items)
                            if (Convert.ToInt32(item.Value) == category.EndDate.Minute)
                            {
                                item.Selected = true;
                                break;
                            }

                        int tempVal = category.TimeLimit / 60;
                        this.txtMinutes.Text = tempVal == 0 ? string.Empty : tempVal.ToString();
                        this.txtNumTries.Text = category.TestLimit == 0 ? string.Empty : category.TestLimit.ToString();
                        this.txtPassingGrade.Text = category.PassingGrade == 0 ? string.Empty : category.PassingGrade.ToString();
                        this.txtCourseID.Text = category.CourseID.ToString();
                    }

                    foreach (ListItem item in this.ddlAccounts.Items)
                    {
                        if (SessionManager.SessionTestCampaignAccount.IsCampaign)
                        {
                            if (item.Value.Trim() == SessionManager.SessionTestCampaignAccount.CampaignID.ToString().Trim())
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                        else
                        {
                            if (item.Value.Trim() == SessionManager.SessionTestCampaignAccount.AccountID.ToString().Trim())
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                }

                #endregion

                #region Editing an Existing Exam

                else if (SessionManager.SessionExamAction == "editexam")
                {
                    this.ddlAccounts.SelectedIndex = -1;
                    this.ddlStartHour.SelectedIndex = -1;
                    this.ddlStartMinute.SelectedIndex = -1;
                    this.ddlEndHour.SelectedIndex = -1;
                    this.ddlEndMinute.SelectedIndex = -1;

                    TestCategory category = SessionManager.SessionSingleExam;
                    this.ddlAccounts.SelectedValue = category.AccountID.ToString();

                    if (_Role != null)
                    {
                        this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
                        this.ddlSubCategory.DataBind();
                        this.ddlSubCategory.SelectedValue = category.CampaignID.ToString();
                    }
                    else
                    {
                        this.ddlSubCategory.DataSource = NonCimCampaign.SelectCatSubFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), Convert.ToInt32(SessionManager.SessionUsername));
                        this.ddlSubCategory.DataBind();
                        this.ddlSubCategory.SelectedValue = category.CampaignID.ToString();
                    }

                    if (!string.IsNullOrEmpty(category.Instructions))
                        this.txtInstructions.Text = category.Instructions;

                    this.txtExamName.Text = category.TestName;
                    this.txtStartDate.Text = category.StartDate.ToString("yyyy/MM/dd");
                    this.txtEndDate.Text = category.EndDate.ToString("yyyy/MM/dd");

                    foreach (ListItem item in this.ddlStartHour.Items)
                        if (Convert.ToInt32(item.Value) == category.StartDate.Hour)
                        {
                            item.Selected = true;
                            break;
                        }

                    foreach (ListItem item in this.ddlEndHour.Items)
                        if (Convert.ToInt32(item.Value) == category.EndDate.Hour)
                        {
                            item.Selected = true;
                            break;
                        }

                    foreach (ListItem item in this.ddlStartMinute.Items)
                        if (Convert.ToInt32(item.Value) == category.StartDate.Minute)
                        {
                            item.Selected = true;
                            break;
                        }

                    foreach (ListItem item in this.ddlEndMinute.Items)
                        if (Convert.ToInt32(item.Value) == category.EndDate.Minute)
                        {
                            item.Selected = true;
                            break;
                        }

                    int timeLimit = category.TimeLimit / 60;
                    this.txtMinutes.Text = timeLimit.ToString();
                    this.txtNumTries.Text = category.TestLimit.ToString();
                    this.txtPassingGrade.Text = category.PassingGrade.ToString();
                    this.txtCourseID.Text = category.CourseID.ToString();
                    this.chkHideGrades.Checked = category.HideScores;
                    this.chkRetake.Checked = category.Retake;

                    //tandim #2265866 - to add additional option to allow to randomize question checkbox.
                    this.chkRandomizeQuestions.Checked = !category.RandomizeQuestions;
                    this.chkHideExamFromTesting.Checked = category.HideExamFromTesting;

                    Questionnaire[] questionnaires = Questionnaire.SelectByCategory(category.TestCategoryID);
                    if (SessionManager.SessionQuestionnaire == null)
                        SessionManager.SessionQuestionnaire = new List<Questionnaire>();

                    bool found = false;

                    if (questionnaires.Length > 0)
                        foreach (Questionnaire questionnaire in questionnaires)
                        {
                            found = false;
                            foreach (Questionnaire inQuestion in SessionManager.SessionQuestionnaire)
                                if (questionnaire.QuestionnaireID == inQuestion.QuestionnaireID)
                                {
                                    found = true;
                                    break;
                                }
                            if (!found)
                                SessionManager.SessionQuestionnaire.Add(questionnaire);
                        }

                    this.btnContinue.Text = "Update";
                }

                #endregion

                else
                {
                    Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
                }

                this.BindQuestions();

                if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(6))
                    this.btnAddItem.Visible = false;
            }
            catch (Exception ex)
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "Page_Load", ex.Message);
                }
                ///TODO:Catch error when page can't load an exam/load the page for a new exam
            }
        }
    }

    protected void PopulateHourDropDownList(DropDownList list)
    {
        list.Items.Clear();
        for (int i = 0; i < 24; i++)
            list.Items.Add(new ListItem(i.ToString("00"), i.ToString("00")));
    }

    protected void PopulateMinuteDropDownList(DropDownList list)
    {
        list.Items.Clear();
        for (int i = 0; i < 60; i++)
            list.Items.Add(new ListItem(i.ToString("00"), i.ToString("00")));
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        try
        {
            int tempCategoryID = this.CreateExam(true);
            if (tempCategoryID > 0)
            {
                SessionManager.SessionExamAction = "editexam";
                SessionManager.SessionQuestionAction = "newquestion";
                SessionManager.SessionTestCategoryID = tempCategoryID;
                #region Commented
                //TestCategory category = new TestCategory
                //    (
                //        this.txtExamName.Text.Trim(),
                //        string.IsNullOrEmpty(this.txtStartDate.Text.Trim()) ? new DateTime() : new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(6, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(0, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(3, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0),
                //        string.IsNullOrEmpty(this.txtEndDate.Text.Trim()) ? new DateTime() : new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(6, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(0, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(3, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0),
                //        this.gvGridItems.Rows.Count,
                //        string.IsNullOrEmpty(this.txtPassingGrade.Text.Trim()) ? 0 : Convert.ToInt32(this.txtPassingGrade.Text.Trim()),
                //        string.IsNullOrEmpty(this.txtNumTries.Text.Trim()) ? 0 : Convert.ToInt32(this.txtNumTries.Text.Trim()),
                //        string.IsNullOrEmpty(this.txtMinutes.Text.Trim()) ? 0 : Convert.ToInt32(this.txtMinutes.Text.Trim()) * 60,
                //        true,
                //        System.DateTime.Now,
                //        !string.IsNullOrEmpty(SessionManager.SessionUsername) ? SessionManager.SessionUsername : "test",
                //        false
                //    );
                //TestCampaignAccount tca = new TestCampaignAccount();
                //AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                //for (int x = 0; x < accounts.Length; x++)
                //{
                //    if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                //    {
                //        tca.TestCategoryID = 0;
                //        tca.CampaignID = 0;
                //        tca.AccountID = Math.Abs(Convert.ToInt32(this.ddlAccounts.SelectedValue));
                //        tca.IsCampaign = false;
                //        SessionManager.SessionTestCampaignAccount = tca;
                //        break;
                //    }
                //}
                //if (!tca.IsCampaign)
                //{
                //    accounts = AccountList.SelectGenericCampaignsVader2(1);
                //    for (int x = 0; x < accounts.Length; x++)
                //    {
                //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                //        {
                //            tca.TestCategoryID = 0;
                //            tca.CampaignID = Math.Abs(Convert.ToInt32(this.ddlAccounts.SelectedValue));
                //            tca.AccountID = 0;
                //            tca.IsCampaign = true;
                //            SessionManager.SessionTestCampaignAccount = tca;
                //            break;
                //        }
                //    }
                //}
                //SessionManager.SessionTempCategory = category;
                //DataSet set = new DataSet();
                //DataTable table = new DataTable();
                //table.Columns.Add("TestGroupName");
                //table.Columns.Add("IsPartOfGroup");
                //foreach (GridViewRow row in this.gvGroupsEdit.Rows)
                //{
                //    Label lblGroupName = row.FindControl("lblGroupName") as Label;
                //    CheckBox chkGroup = row.FindControl("chkGroup") as CheckBox;
                //    if (lblGroupName != null && chkGroup != null)
                //    {
                //        if (chkGroup.Checked)
                //        {
                //            DataRow inrow = table.NewRow();
                //            inrow.ItemArray.SetValue(lblGroupName.Text, 0);
                //            inrow.ItemArray.SetValue(chkGroup.Checked, 1);
                //            table.Rows.Add(inrow);
                //        }
                //    }
                //}
                //set.Tables.Add(table);
                //SessionManager.SessionTestCategoryGroupMatrixDataSet = set;
                //SessionManager.SessionQuestionAction = "newquestion";
                #endregion
                Response.Redirect("~/newquestion.aspx?" + Request.QueryString.ToString());
            }
        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("aborted"))
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "btnAddItem_Click", ex.Message);
                }
                this.mpeStatus.Show();
            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (this.CreateExam(false) != 0)
            //if (this.gvGridItems.Rows.Count > 0 && SessionManager.SessionQuestionnaire.Count > 0)
            //    this.CreateExam();
            //else
            //    this.mpeNoItems.Show();
            Response.Redirect("~/exam.aspx" + Request.QueryString.ToString());
    }

    protected void BindQuestions()
    {
        try
        {
            if (SessionManager.SessionQuestionnaire.Count > 0)
            {
                this.gvGridItems.DataSource = SessionManager.SessionQuestionnaire;
                this.gvGridItems.DataBind();
                foreach (GridViewRow row in this.gvGridItems.Rows)
                {
                    LinkButton lnkDelete = row.FindControl("lnkDelete") as LinkButton;
                    LinkButton lnkEdit = row.FindControl("lnkEdit") as LinkButton;

                    if (lnkDelete != null && !RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(8))
                        lnkDelete.Visible = false;
                    if (lnkEdit != null && !RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(7))
                        lnkEdit.Visible = false;
                    LinkButton lnkItemQuestion = row.FindControl("lnkItemQuestion") as LinkButton;
                    Label lblItemQuestion = row.FindControl("lblItemQuestion") as Label;
                    Label lblQuestionnaireID = row.FindControl("lblQuestionnaireID") as Label;
                    if (lblQuestionnaireID.Text == "0")
                    {
                        lnkItemQuestion.Visible = false;
                    }
                    else
                    {
                        if (SessionManager.SessionExamAction == "editexam")
                        {
                            if (lblItemQuestion != null)
                                lblItemQuestion.Visible = false;
                        }
                        else if (SessionManager.SessionExamAction == "newexam")
                        {
                            if (lnkItemQuestion != null)
                                lnkItemQuestion.Visible = false;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "BindQuestions", ex.Message);
        }
    }

    protected int CreateExam(bool isCreateItem)
    {
        int testCategoryID = 0;
        try
        {
            bool isValid = true;
            int tempVal;
            DateTime tempDate;
            if (string.IsNullOrEmpty(this.txtExamName.Text.Trim()))
            {
                this.cvExamName.IsValid = false;
                isValid = false;
            }
            if (!DateTime.TryParse(this.txtStartDate.Text.Trim(), out tempDate))
            {
                this.cvStartDate.IsValid = false;
                isValid = false;
            }
            if (!DateTime.TryParse(this.txtEndDate.Text.Trim(), out tempDate))
            {
                this.cvEndDate.IsValid = false;
                isValid = false;
            }
            if (!int.TryParse(this.txtMinutes.Text.Trim(), out tempVal))
            {
                this.cvMinutes.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtMinutes.Text.Trim()) < 0)
            {
                this.cvMinutes.IsValid = false;
                isValid = false;
            }
            if (!int.TryParse(this.txtNumTries.Text.Trim(), out tempVal))
            {
                this.cvNumTries.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtNumTries.Text.Trim()) < 0)
            {
                this.cvNumTries.IsValid = false;
                isValid = false;
            }
            if (!int.TryParse(this.txtPassingGrade.Text.Trim(), out tempVal))
            {
                this.cvPassingGrade.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtPassingGrade.Text.Trim()) > 100)
            {
                this.cvPassingGrade.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtPassingGrade.Text.Trim()) < 1)
            {
                this.cvPassingGrade.IsValid = false;
                isValid = false;
            }
            if (isValid)
            {
                try
                {
                    if (SessionManager.SessionExamAction == "newexam")
                    {
                        bool isEssay = false;
                        foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                            if (question.TypeCode == "essay")
                            {
                                isEssay = true;
                                break;
                            }
                        TestCategory category = new TestCategory
                        (
                            this.txtExamName.Text.Trim(),
                            new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0),
                            new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0),
                            this.gvGridItems.Rows.Count,
                            Convert.ToInt32(this.txtPassingGrade.Text.Trim()),
                            Convert.ToInt32(this.txtNumTries.Text.Trim()),
                            Convert.ToInt32(this.txtMinutes.Text.Trim()) * 60,
                            isEssay,
                            System.DateTime.Now,
                            !string.IsNullOrEmpty(SessionManager.SessionUsername) ? SessionManager.SessionUsername : "test",
                            false,
                            this.txtInstructions.Text.Trim(),
                            this.chkHideGrades.Checked,
                            this.chkRetake.Checked
                            //this.chkExternalExam.Checked,
                            //this.chkInternalExam.Checked
                        );

                        if (string.IsNullOrEmpty(this.txtCourseID.Text.Trim()))
                            category.CourseID = 0;
                        else
                            category.CourseID = Convert.ToInt32(this.txtCourseID.Text.Trim());
                        #region commented
                        //foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                        //{
                        //    question.TestCategoryID = catID;
                        //    question.LastUpdated = !string.IsNullOrEmpty(SessionManager.SessionUsername) ? SessionManager.SessionUsername : "test";
                        //    question.Insert();
                        //}
                        #endregion
                        this.lblStatus.Text = "Exam creation complete.";
                        //TestCampaignAccount tca = new TestCampaignAccount(catID);
                        //AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                        #region 1634854 - switched to categories
                        //AccountList[] accounts = AccountList.SelectAccounts(1);
                        //{
                        //    //accounts = AccountList.SelectGenericCampaignsVader2(1);
                        //    accounts = AccountList.SelectGenericCampaigns(1);
                        //    for (int x = 0; x < accounts.Length; x++)
                        //    {
                        //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                        //        {
                        //            category.CampaignID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            category.AccountID = 0;
                        //            category.IsCampaign = true;
                        //            //tca.Insert();
                        //            break;
                        //        }
                        //    }
                        //}
                        //if (!category.IsCampaign)
                        //    for (int x = 0; x < accounts.Length; x++)
                        //    {
                        //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                        //        {
                        //            #region commented
                        //            //tca.TestCategoryID = catID;
                        //            //tca.CampaignID = 0;
                        //            //tca.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            //tca.IsCampaign = false;
                        //            #endregion
                        //            category.CampaignID = 0;
                        //            category.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            category.IsCampaign = false;
                        //            //tca.Insert();
                        //            break;
                        //        }
                        //    }
                        #endregion
                        category.AccountID = Convert.ToInt32(ddlAccounts.SelectedValue);
                        category.CampaignID = Convert.ToInt32(ddlSubCategory.SelectedValue);
                        category.IsCampaign = true;
                        category.RandomizeQuestions = !chkRandomizeQuestions.Checked;
                        category.HideExamFromTesting = chkHideExamFromTesting.Checked;
                        int catID = category.Insert();
                        testCategoryID = catID;
                        if (SessionManager.SessionTestCategoryGroupMatrixDataSet != null)
                            foreach (GridViewRow row in this.gvGroupsEdit.Rows)
                            {
                                Label lblTestGroupID = row.FindControl("lblTestGroupID") as Label;
                                CheckBox chkGroup = row.FindControl("chkGroup") as CheckBox;
                                if (lblTestGroupID != null && chkGroup != null)
                                {
                                    TestCategoryGroupMatrix matrix = new TestCategoryGroupMatrix(catID, Convert.ToInt32(lblTestGroupID.Text));
                                    try
                                    {
                                        matrix.Insert();
                                    }
                                    catch
                                    { }
                                }
                            }
                        else
                            foreach (GridViewRow row in this.gvGroups.Rows)
                            {
                                Label lblTestGroupID = row.FindControl("lblTestGroupID") as Label;
                                CheckBox chkGroup = row.FindControl("chkGroup") as CheckBox;
                                if (lblTestGroupID != null && chkGroup != null)
                                {
                                    TestCategoryGroupMatrix matrix = new TestCategoryGroupMatrix(catID, Convert.ToInt32(lblTestGroupID.Text));
                                    try
                                    {
                                        matrix.Insert();
                                    }
                                    catch
                                    { }
                                }
                            }
                        SessionManager.SessionSingleExam = TestCategory.Select(testCategoryID, true);
                        return testCategoryID;
                    }
                    else if (SessionManager.SessionExamAction == "editexam")
                    {
                        bool isEssay = false;
                        foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                            if (question.TypeCode == "essay") 
                            {
                                isEssay = true;
                                break;
                            }
                        TestCategory category = SessionManager.SessionSingleExam;

                        category.DateLastUpdated = System.DateTime.Now;
                        category.EndDate = new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0);
                        category.IsEssay = isEssay;
                        category.LastUpdatedBy = SessionManager.SessionUsername;
                        category.NumberOfQuestions = SessionManager.SessionQuestionnaire.Count;
                        category.PassingGrade = Convert.ToInt32(this.txtPassingGrade.Text.Trim());
                        category.StartDate = new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0);
                        category.TestLimit = Convert.ToInt32(this.txtNumTries.Text.Trim());
                        category.TestName = this.txtExamName.Text.Trim();
                        category.TimeLimit = Convert.ToInt32(this.txtMinutes.Text.Trim()) * 60;
                        category.Instructions = this.txtInstructions.Text.Trim();
                        if (string.IsNullOrEmpty(this.txtCourseID.Text.Trim()))
                            category.CourseID = 0;
                        else
                        category.CourseID = Convert.ToInt32(this.txtCourseID.Text.Trim());
                        category.AccountID = Convert.ToInt32(ddlAccounts.SelectedValue);
                        category.CampaignID = Convert.ToInt32(ddlSubCategory.SelectedValue);
                        category.IsCampaign = true;
                        category.HideScores = this.chkHideGrades.Checked;
                        category.Retake = this.chkRetake.Checked;
                        //category.IsExternal = this.chkExternalExam.Checked;
                        //category.IsInternal = this.chkInternalExam.Checked;
                        category.RandomizeQuestions = !this.chkRandomizeQuestions.Checked;
                        category.HideExamFromTesting = this.chkHideExamFromTesting.Checked;
                        category.Update();
                        this.lblStatus.Text = "Edit exam complete.";
                        foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                        {
                            Questionnaire dbQuestion = Questionnaire.Select(question.QuestionnaireID, false);
                            if (dbQuestion == null)
                            {
                                question.TestCategoryID = category.TestCategoryID;
                                question.Insert();
                            }
                            else
                                question.Update();
                        }
                        TestCampaignAccount tca = TestCampaignAccount.SelectByTestCategory(SessionManager.SessionSingleExam.TestCategoryID);
                        //AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                        //AccountList[] accounts = AccountList.SelectAccounts(1);

                        #region 1634854 - switched to categories
                        //AccountList[] accounts = AccountList.SelectAccounts(1);
                        //{
                        //    //accounts = AccountList.SelectGenericCampaignsVader2(1);
                        //    accounts = AccountList.SelectGenericCampaigns(1);
                        //    for (int x = 0; x < accounts.Length; x++)
                        //    {
                        //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                        //        {
                        //            category.CampaignID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            category.AccountID = 0;
                        //            category.IsCampaign = true;
                        //            //tca.Insert();
                        //            break;
                        //        }
                        //    }
                        //}
                        //if (!category.IsCampaign)
                        //    for (int x = 0; x < accounts.Length; x++)
                        //    {
                        //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                        //        {
                        //            #region commented
                        //            //tca.TestCategoryID = catID;
                        //            //tca.CampaignID = 0;
                        //            //tca.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            //tca.IsCampaign = false;
                        //            #endregion
                        //            category.CampaignID = 0;
                        //            category.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            category.IsCampaign = false;
                        //            //tca.Insert();
                        //            break;
                        //        }
                        //    }
                        #endregion
                        TestCategoryGroupMatrix.DeleteByCategory(category.TestCategoryID);
                        foreach (GridViewRow row in this.gvGroupsEdit.Rows)
                        {
                            Label lblTestGroupID = row.FindControl("lblTestGroupID") as Label;
                            CheckBox chkGroup = row.FindControl("chkGroup") as CheckBox;
                            if (chkGroup.Checked)
                            {
                                TestCategoryGroupMatrix matrix = new TestCategoryGroupMatrix(category.TestCategoryID, Convert.ToInt32(lblTestGroupID.Text));
                                matrix.Insert();
                            }
                        }
                        testCategoryID = category.TestCategoryID;
                        return testCategoryID;
                    }
                }
                catch (Exception ex)
                {
                    using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                    {
                        service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "CreateExam - Inner", ex.Message);
                    }
                    this.lblStatus.Text = "An error occured.";
                }
                finally
                {
                    if (isCreateItem)
                        this.mpeStatus.Show();
                    SessionManager.SessionTestCampaignAccount = null;
                }
            }
        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("aborted"))
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "CreateExam - Outer", ex.Message);
                }
                return 0;
            }
        }
        return 0;
    }

    protected void btnStatus_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
    }

    protected void gvGridItems_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GridViewRow row = this.gvGridItems.Rows[e.NewSelectedIndex];
            Label lblQuestionnaireID = row.FindControl("lblQuestionnaireID") as Label;
            if (lblQuestionnaireID != null)
            {
                Questionnaire question = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), false);
                SessionManager.SessionSingleQuestion = question;
                SessionManager.SessionSingleQuestionIndex = e.NewSelectedIndex;
                SessionManager.SessionQuestionAction = "viewsinglequestion";
                Response.Redirect("~/viewsinglequestion.aspx?" + Request.QueryString.ToString());
            }
            else
            {
                throw new Exception("No questionnaireID.");
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "gvGridItems_SelectedIndexChanging", ex.Message);
            }
        }
    }

    protected void gvGridItems_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            SessionManager.SessionSingleQuestion = SessionManager.SessionQuestionnaire[e.NewEditIndex];
            SessionManager.SessionSingleQuestionIndex = e.NewEditIndex;
            SessionManager.SessionQuestionAction = "editquestion";
            Response.Redirect("~/newquestion.aspx?" + Request.QueryString.ToString());
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "gvGriditems_RowEditing", ex.Message);
            }
        }
    }

    protected void gvGridItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            this.Deleted = e.RowIndex;
            this.mpeDeleteExam.Show();
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "gvGridItems_RowDeleting", ex.Message);
            }
        }
    }

    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        try
        {
            Questionnaire temp = SessionManager.SessionQuestionnaire[this.Deleted];
            if (temp.QuestionnaireID != 0)
                temp.Delete();
            SessionManager.SessionQuestionnaire.RemoveAt(this.Deleted);
            this.BindQuestions();
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "btnConfirmDelete_Click", ex.Message);
            }
        }
    }
    #region commented
    //Temporarily commented out, may be delete if determined to be unused by deployment
    //protected void gvGridItems_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "View")
    //    {
    //        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
    //        GridView view = (GridView)row.NamingContainer;
    //        if (view != null)
    //        {
    //            ~
    //        }
    //    }
    //}
    #endregion

    protected void btnNoItemsConfirm_Click(object sender, EventArgs e)
    {
        this.CreateExam(true);
    }

    protected void Populategroups()
    {
        try
        {
            if (SessionManager.SessionExamAction == "editexam" || SessionManager.SessionTestCategoryGroupMatrixDataSet != null)
            {
                this.gvGroupsEdit.Visible = true;
                this.gvGroupsEdit.DataSource = TestCategoryGroupMatrix.SelectAllGroupByCategory(SessionManager.SessionSingleExam.TestCategoryID);
                this.gvGroupsEdit.DataBind();

                foreach (GridViewRow row in this.gvGroupsEdit.Rows)
                {
                    Label lblIsPartOfGroup = row.FindControl("lblIsPartOfGroup") as Label;
                    CheckBox chkGroup = row.FindControl("chkGroup") as CheckBox;
                    chkGroup.Checked = Convert.ToBoolean(lblIsPartOfGroup.Text);
                }
            }
            else
            {
                this.gvGroups.Visible = true;
                this.gvGroups.DataSource = TestGroup.SelectAll();
                this.gvGroups.DataBind();
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "Populategroups", ex.Message);
            }
        }
    }

    protected void btnAddNewGroup_Click(object sender, EventArgs e)
    {
        this.pnlAddGroup.Visible = true;
        this.btnAddNewGroup.Visible = false;
    }

    protected void btnAddGroupCancel_Click(object sender, EventArgs e)
    {
        this.pnlAddGroup.Visible = false;
        this.btnAddNewGroup.Visible = true;
    }

    protected void btnAddGroupOK_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(this.txtAddNewGroup.Text.Trim()))
            {
                TestGroup group = new TestGroup(this.txtAddNewGroup.Text.Trim(), SessionManager.SessionUsername);
                group.Insert();
                this.Populategroups();
                this.txtAddNewGroup.Text = string.Empty;
                this.pnlAddGroup.Visible = false;
                this.btnAddNewGroup.Visible = true;
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "btnAddGroupOK_Click", ex.Message);
            }
        }
    }

    protected void ddlAccounts_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (_Role != null)
        {
            this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
            this.ddlSubCategory.DataBind();
        }
        else
        {
            this.ddlSubCategory.DataSource = NonCimCampaign.SelectCatSubFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), Convert.ToInt32(SessionManager.SessionUsername));
            this.ddlSubCategory.DataBind();
        }
    }

    protected void CheckUserRole()
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT DISTINCT CR.[Role] FROM susl3psqldb02.CIMEnterprise.dbo.tbl_Personnel_Cor_Employee E " +
                         "INNER JOIN susl3psqldb02.CIMEnterprise.dbo.tbl_Personnel_Cor_Role CR " +
                         "ON E.RoleID = CR.RoleID " +
                         "WHERE (CR.RoleID = 4196 OR CR.[Role] LIKE '%Manager%' OR CR.[Role] LIKE '%Director%') " +
                         "AND E.CIMNumber = @CIMNumber ORDER BY 1 ";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@CIMNumber", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }
}