using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using NuComm.Security.Encryption;

public partial class controls_examstatuscontrol : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
            if (!this.IsPostBack)
            {
                this.PopulateHourDropDownList(this.ddlEndHour);
                this.PopulateHourDropDownList(this.ddlStartHour);
                this.PopulateMinuteDropDownList(this.ddlEndMinute);
                this.PopulateMinuteDropDownList(this.ddlStartMinute);
                //this.ddlCampaign.DataSource = AccountList.SelectAllVader2(1);
                //this.ddlCampaign.DataSource = AccountList.SelectAll(1);
                //this.ddlCampaign.DataBind();
                this.ddlCampaign.DataSource = NonCimCampaign.SelectParents();
                this.ddlCampaign.DataBind();
                this.ddlSubcategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlCampaign.SelectedValue), true);
                this.ddlSubcategory.DataBind();
                if (SessionManager.SessionTestCategoryID > 0)
                {

                    this.txtHotlink.Text = "http://home.nucomm.net/applications/nuskillcheckv2/hotlink.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(SessionManager.SessionTestCategoryID.ToString()));
                    TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
                    if (category != null)
                    {
                        TestCampaignAccount tca = TestCampaignAccount.SelectByTestCategory(category.TestCategoryID);
                        if (tca != null)
                        {
                            if (tca.IsCampaign)
                            {
                                AccountList[] accounts = AccountList.SelectGenericCampaignsVader2(1);
                                //AccountList[] accounts = AccountList.SelectGenericCampaigns(1);
                                for (int x = 0; x < accounts.Length; x++)
                                {
                                    if (tca.CampaignID == accounts[x].AccountID)
                                    {
                                        this.lblCampaignAccount.Text = accounts[x].Account;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                                //AccountList[] accounts = AccountList.SelectAccounts(1);
                                for (int x = 0; x < accounts.Length; x++)
                                {
                                    if (tca.AccountID == accounts[x].AccountID)
                                    {
                                        this.lblCampaignAccount.Text = accounts[x].Account;
                                        break;
                                    }
                                }
                            }
                        }
                        if (string.IsNullOrEmpty(category.Instructions))
                            this.lblInstructionsVal.Text = String.Empty;
                        else
                            this.lblInstructionsVal.Text = category.Instructions.Trim();
                        this.lblTestCategoryID.Text = category.TestCategoryID.ToString();
                        this.lblTestInfoValue.Text = category.TestName;
                        int timeLimit = category.TimeLimit / 60;
                        if (timeLimit == 0)
                            this.lblDurationVal.Text = "No Time Limit";
                        else
                            this.lblDurationVal.Text = timeLimit.ToString() + " minutes";
                        this.lblEndDateVal.Text = category.EndDate.ToString("yyyy/MM/dd hh:mm:ss tt");
                        if (category.TestLimit >= 1)
                            this.lblNumberOfTriesVal.Text = category.TestLimit.ToString();
                        else
                            this.lblNumberOfTriesVal.Text = "No Test Limit";
                        this.lblPassingScoreVal.Text = category.PassingGrade.ToString();
                        this.lblStartDateVal.Text = category.StartDate.ToString("yyyy/MM/dd hh:mm:ss tt");

                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        this.gvItems.DataSource = questionnaire;
                        this.gvItems.DataBind();

                        foreach (GridViewRow row in this.gvItems.Rows)
                        {
                            Label lblExamType = row.FindControl("lblExamType") as Label;
                            Label lblExamTypeVal = row.FindControl("lblExamTypeVal") as Label;
                            QuestionType questionType = QuestionType.Select(lblExamType.Text.Trim());
                            if (questionType != null)
                                lblExamTypeVal.Text = questionType.Description;
                            else
                                lblExamTypeVal.Text = "Unknown type";
                        }

                        TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                        this.gvExamTakers.DataSource = taken;
                        this.gvExamTakers.DataBind();
                        foreach (GridViewRow row in this.gvExamTakers.Rows)
                        {
                            Label lblDateEndTaken = row.FindControl("lblDateEndTaken") as Label;
                            Label lblPassed = row.FindControl("lblPassed") as Label;
                            Image imgPassOrFail = row.FindControl("imgPassOrFail") as Image;
                            if (lblPassed.Text == "True")
                                imgPassOrFail.ImageUrl = "~/images/icon-pass.gif";
                            else if (string.IsNullOrEmpty(lblDateEndTaken.Text.Trim()))
                                imgPassOrFail.ImageUrl = "~/images/icon-pending.gif";
                            else
                                imgPassOrFail.ImageUrl = "~/images/icon-fail.gif";
                        }
                    }
                    if (!(RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(3)))
                        this.lnkEditExam.Enabled = false;
                    if (!(RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(4)))
                        this.lnkCopyExam.Enabled = false;
                    if (!(RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(5)))
                        this.lnkDeleteExam.Enabled = false;
                    if (!(RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(20)))
                        this.lnkExamTakers.Enabled = false;

                }
                else if (!Request.Url.ToString().Contains("exams.aspx"))
                    Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "examstatuscontrol.ascx", "Page_Load", ex.Message);
            }
        }
    }

    protected void PopulateHourDropDownList(DropDownList list)
    {
        list.Items.Clear();
        for (int i = 0; i < 24; i++)
            list.Items.Add(new ListItem(i.ToString("00"), i.ToString("00")));
    }

    protected void PopulateMinuteDropDownList(DropDownList list)
    {
        list.Items.Clear();
        for (int i = 0; i < 60; i++)
            list.Items.Add(new ListItem(i.ToString("00"), i.ToString("00")));
    }
    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        try
        {
            TestCategory category = TestCategory.Select(Convert.ToInt32(this.lblTestCategoryID.Text.Trim()), true);
            category.DeleteByHide();
            lblStatus.Text = "Delete successful.";
            SessionManager.SessionTestCategoryID = 0;
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "examstatuscontrol.ascx", "btnConfirmDelete_Click", ex.Message);
            }
            this.lblStatus.Text = "An error occured while deleting. Process could not be completed.";
        }
        finally
        {
            this.mpeStatus.Show();
        }
    }
    protected void btnCopyToNew_Click(object sender, EventArgs e)
    {
        bool isValid = true;
        DateTime tempDate;
        if (string.IsNullOrEmpty(this.txtExamNameVal.Text.Trim()))
            isValid = false;
        if (string.IsNullOrEmpty(this.txtStartDate.Text.Trim()))
            isValid = false;
        else if (!DateTime.TryParse(this.txtStartDate.Text.Trim(), out tempDate))
            isValid = false;
        if (string.IsNullOrEmpty(this.txtEndDate.Text.Trim()))
            isValid = false;
        else if (!DateTime.TryParse(this.txtEndDate.Text.Trim(), out tempDate))
            isValid = false;
        if (isValid)
        {
            try
            {
                int catID = 0;
                TestCategory category = TestCategory.Select(Convert.ToInt32(this.lblTestCategoryID.Text.Trim()), true);
                category.TestName = this.txtExamNameVal.Text.Trim();
                category.StartDate = new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0);
                category.EndDate = new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0);
                category.AccountID = Convert.ToInt32(ddlCampaign.SelectedValue);
                category.CampaignID = Convert.ToInt32(ddlSubcategory.SelectedValue);
                catID = category.Insert();
                this.lblStatus.Text = "Copy Successful";
                Questionnaire[] questions = Questionnaire.SelectByCategory(category.TestCategoryID);
                if (questions != null)
                {
                    foreach (Questionnaire question in questions)
                    {
                        Questionnaire newQuestion = question;
                        newQuestion.TestCategoryID = catID;
                        newQuestion.Insert();
                    }
                }
                //TestCampaignAccount tca = new TestCampaignAccount(catID);
                //AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                ////AccountList[] accounts = AccountList.SelectAccounts(1);
                //for (int x = 0; x < accounts.Length; x++)
                //{
                //    if (Convert.ToInt32(this.ddlCampaign.SelectedValue) == accounts[x].AccountID)
                //    {
                //        tca.TestCategoryID = catID;
                //        tca.CampaignID = 0;
                //        tca.AccountID = Math.Abs(Convert.ToInt32(this.ddlCampaign.SelectedValue));
                //        tca.IsCampaign = false;
                //        tca.Insert();
                //        break;
                //    }
                //}
                //if (!tca.IsCampaign)
                //{
                //    accounts = AccountList.SelectGenericCampaignsVader2(1);
                //    //accounts = AccountList.SelectGenericCampaigns(1);
                //    for (int x = 0; x < accounts.Length; x++)
                //    {
                //        if (Convert.ToInt32(this.ddlCampaign.SelectedValue) == accounts[x].AccountID)
                //        {
                //            tca.TestCategoryID = catID;
                //            tca.CampaignID = Math.Abs(Convert.ToInt32(this.ddlCampaign.SelectedValue));
                //            tca.AccountID = 0;
                //            tca.IsCampaign = true;
                //            tca.Insert();
                //            break;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "examstatuscontrol.ascx", "btnCopyToNew_Click", ex.Message);
                }
                this.lblStatus.Text = "An error occured while copying. Process could not be completed.";
            }
            finally
            {
                this.mpeStatus.Show();
            }
        }
        else
        {
            this.mpeCopyExam.Show();
        }
    }
    protected void btnStatus_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
    }

    //protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "Select")
    //    {

    //    }
    //    if (e.CommandName == "Edit")
    //    {
    //        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
    //        Label lblQuestionnaireID = row.FindControl("lblQuestionnaireID") as Label;
    //        SessionManager.SessionSingleQuestion = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()));
    //        SessionManager.SessionQuestionAction = "editquestion";
    //        Response.Redirect("~/newquestion.aspx");
    //    }
    //    if (e.CommandName == "Delete")
    //    {

    //    }
    //}
    protected void lnkEditExam_Click(object sender, EventArgs e)
    {
        SessionManager.SessionExamAction = "editexam";
        SessionManager.SessionSingleExam = TestCategory.Select(Convert.ToInt32(this.lblTestCategoryID.Text.Trim()), true);
        SessionManager.SessionQuestionnaire = new List<Questionnaire>();
        Response.Redirect("~/editexam.aspx?" + Request.QueryString.ToString());
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
    }
    protected void lnkExamTakers_Click(object sender, EventArgs e)
    {
        this.mpeTakers.Show();
    }

    protected void gvExamTakers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
            this.gvExamTakers.PageIndex = e.NewPageIndex;
            TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
            this.gvExamTakers.DataSource = taken;
            this.gvExamTakers.DataBind();
            foreach (GridViewRow row in this.gvExamTakers.Rows)
            {
                Label lblDateEndTaken = row.FindControl("lblDateEndTaken") as Label;
                Label lblPassed = row.FindControl("lblPassed") as Label;
                Image imgPassOrFail = row.FindControl("imgPassOrFail") as Image;
                if (lblPassed.Text == "True")
                    imgPassOrFail.ImageUrl = "~/images/icon-pass.gif";
                else if (string.IsNullOrEmpty(lblDateEndTaken.Text.Trim()))
                    imgPassOrFail.ImageUrl = "~/images/icon-pending.gif";
                else
                    imgPassOrFail.ImageUrl = "~/images/icon-fail.gif";
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "examstatuscontrol.ascx", "gvExamTakers_PageIndexChanging", ex.Message);
        }
        finally
        {
            this.mpeTakers.Show();
        }
    }

    protected void ddlCampaign_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ddlSubcategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlCampaign.SelectedValue), true);
        this.ddlSubcategory.DataBind();
        this.mpeCopyExam.Show();
    }
}
