<%@ Control Language="C#" AutoEventWireup="true" CodeFile="examquestioncontrol.ascx.cs"
    Inherits="controls_examquestioncontrol" %>
<asp:Panel ID="pnlExamQuestion" runat="server" Width="100%" HorizontalAlign="center">
    <%--border-color: #4682b4; background-color: White;">--%>
    <table cellpadding="5" width="100%" cellspacing="0" border="1">
        <tr>
            <td align="center" style="vertical-align: middle; font-size: 16px; font-family: Arial;" colspan="2">
                <asp:Label ID="lblQuestion" runat="server" CssClass="testQuestion"></asp:Label>
                <br />
                <asp:HyperLink ID="hypHyperlink" runat="server" Target="_blank" />
            </td>
        </tr>
        <tr>
            <td align="center" style="vertical-align: middle; font-size: large; font-family: Arial;" colspan="2">
                <table width="100%" cellpadding="5" style="vertical-align: middle; font-family: Arial;
                    font-size: 12px;">
                    <tr>
                        <td align="left" style="padding: 30px 10px 10px 10px">
                            <asp:Panel ID="pnlMultipleChoice" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr id="trCheck1" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck2" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck3" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice3" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck4" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice4" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck5" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice5" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck6" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice6" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck7" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice7" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck8" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice8" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck9" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice9" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck10" runat="server">
                                        <td>
                                            <asp:Label ID="chkChoice10" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlTrueOrFalse" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="rdoTrue" runat="server" Text="True" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="rdoFalse" runat="server" Text="False" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlSequencing" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <ajax:ReorderList ID="rolSequencing" runat="server" DragHandleAlignment="left" PostBackOnReorder="false"
                                                OnItemReorder="rolSequencing_onItemReorder" EnableViewState="true" AllowReorder="false">
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlTemplate" runat="server" BackColor="#C1D7E7" Height="18px">
                                                        <asp:Label ID="lblTemplate" runat="server" Text='<%#Bind("Item") %>' />
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <DragHandleTemplate>
                                                    <asp:Panel ID="pnlHandle" runat="server" BackColor="#80A7F0" Width="24px" Height="18px"
                                                        BorderWidth="1">
                                                        <asp:Image ID="imgArrow" runat="server" ImageUrl="~/images/arrow.JPG" />
                                                    </asp:Panel>
                                                </DragHandleTemplate>
                                            </ajax:ReorderList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlFillInTheBlanks" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFillInTheBlanks" runat="server" Width="400px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlHotspot" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5" width="100%">
                                    <tr>
                                        <td id="imgCell1" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice1" runat="server" />
                                        </td>
                                        <td id="imgCell2" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice2" runat="server" />
                                        </td>
                                        <td id="imgCell3" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice3" runat="server" />
                                        </td>
                                        <td id="imgCell4" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice4" runat="server" />
                                        </td>
                                        <td id="imgCell5" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice5" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="imgCell6" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice6" runat="server" />
                                        </td>
                                        <td id="imgCell7" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice7" runat="server" />
                                        </td>
                                        <td id="imgCell8" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice8" runat="server" />
                                        </td>
                                        <td id="imgCell9" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice9" runat="server" />
                                        </td>
                                        <td id="imgCell10" runat="server" align="center" valign="middle">
                                            <asp:Image ID="imgChoice10" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; font-family: Arial; font-size: small;">
                            <asp:Label ID="lblCorrectAnswer" runat="server" Text="Correct Answer:" />
                            <br />
                            <asp:Label ID="lblCorrectAnswerVal" runat="server" Visible="false" Text="" />
                            <asp:ListBox ID="lstCorrectAnswers" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer1" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer2" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer3" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer4" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer5" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer6" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer7" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer8" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer9" runat="server" Visible="false" />
                            <asp:Image ID="imgCorrectAnswer10" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPercentageCorrect" runat="server" Text="Percentage of examiness that got this item correct:" />
                            <asp:Label ID="lblPercentageCorrectVal" runat="server" Font-Bold="true" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            <asp:LinkButton ID="lnkPeopleWhoAnswered" runat="server" Text="View examinees who've answered this question"
                                CssClass="linkButtons" OnClick="lnkPeopleWhoAnswered_Click" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="2" align="right" valign="middle" style="font-family: Arial; background-color: #c1d7e7">
                            <asp:Button CssClass="buttons" ID="btnContinue" runat="server" Text="OK" OnClick="btnContinue_Click"
                                Width="80px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeSaveAndQuit" runat="server" PopupControlID="pnlSaveAndQuit"
    TargetControlID="hidSaveAndQuit" BackgroundCssClass="modalBackground" />
<asp:HiddenField ID="hidSaveAndQuit" runat="server" />
<asp:Panel ID="pnlSaveAndQuit" runat="server">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;font-family: Arial; font-size: 12px; width: 400px">
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblSaveAndQuitPrompt" runat="server" Text="When you save and quit, you will be able to resume this exam but you will not be able to modify your other answers." />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button CssClass="buttons" ID="btnConfirmSaveAndQuit" runat="server" Text="OK" Width="80px" OnClick="btnConfirmSaveAndQuit_Click" />
            </td>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnCancelSaveAndQuit" runat="server" Text="Cancel" Width="80px" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpePeopleWhoAnswered" runat="server" TargetControlID="hidPeopleWhoAnswered"
    PopupControlID="pnlPeopleWhoAnswered" BackgroundCssClass="modalBackground" />
<asp:HiddenField ID="hidPeopleWhoAnswered" runat="server" />
<asp:Panel ID="pnlPeopleWhoAnswered" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;font-family: Arial; font-size: 12px; width: 400px">
        <tr>
            <td>
                <asp:GridView ID="gvPeopleWhoAnswered" runat="server" AllowPaging="true" AllowSorting="false" BorderColor="black"
                    AutoGenerateColumns="false" PageSize="20" CellPadding="5" Width="100%" EmptyDataText="This question has not been answered yet, or the exam to which this question belongs to has not been taken yet.">
                    <HeaderStyle Font-Size="16px" />
                    <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                    <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                        NextPageText=">" Position="Bottom" PreviousPageText="<" />
                    <RowStyle BorderColor="black" />
                    <RowStyle ForeColor="black" Font-Size="14px" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="lblUser" runat="server" Text='<%#Bind("Username") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle Width="40px" HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Image ID="imgCorrect" runat="server" ImageUrl='<%#Bind("ImageUrl") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnOkPeopleWhoHaveAnswered" runat="server" Text="Return" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeNotSaved" runat="server" BackgroundCssClass="modalBackground"
    PopupControlID="pnlNotSaved" TargetControlID="hidNotSaved" />
<asp:HiddenField ID="hidNotSaved" runat="server" />
<asp:Panel ID="pnlNotSaved" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;font-family: Arial; font-size: 12px; width: 400px">
        <tr>
            <td align="center">
                <asp:Label ID="lblNotSaved" runat="server" Text="This question has not been saved in the database yet. Save the exam so that its items will also be saved." />
                <br />
                <br />
                <asp:Button CssClass="buttons" ID="btnNotSavedOK" runat="server" Text="OK" Width="80px" />
            </td>
        </tr>
    </table>
</asp:Panel>
