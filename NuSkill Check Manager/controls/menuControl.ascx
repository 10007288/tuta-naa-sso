<%@ Control Language="C#" AutoEventWireup="true" CodeFile="menuControl.ascx.cs" Inherits="controls_menuControl" %>
<table align="center" width="960px"  cellpadding="0" cellspacing="0" border="1">
    <tr>
        <td id="trExams" runat="server" style="height: 19px" align="center">
            <asp:LinkButton ID="lnkExams" runat="server" Text="Exams" CssClass="lbuttons" OnClick="lnkExams_Click"
                ForeColor="black" />
        </td>
        <td id="trExamSetup" runat="server" style="height: 19px" align="center">
            <asp:LinkButton ID="lnkQuickItems" runat="server" Text="Quick Items" CssClass="lbuttons"
                OnClick="lnkQuickItems_Click" ForeColor="black" />
        </td>
        <td id="trEssays" runat="server" style="height: 19px" align="center">
            <asp:LinkButton ID="lnkEssays" runat="server" Text="Mark Essays" CssClass="lbuttons"
                OnClick="lnkEssays_Click" ForeColor="black" />
        </td>
        <td id="trExaminees" runat="server" style="height: 19px" align="center">
            <asp:LinkButton ID="lnkExaminees" runat="server" Text="Users" CssClass="lbuttons"
                OnClick="lnkExaminees_Click" ForeColor="black" />
        </td>
        <td id="trReports" runat="server" style="height: 19px" align="center">
            <asp:LinkButton ID="lnkReports" runat="server" Text="Reports" CssClass="lbuttons"
                OnClick="lnkReports_Click" ForeColor="black" />
        </td>
    </tr>
    <tr>
        <td id="trNonCimCampaigns" runat="server" style="height: 19px" align="center">
            <asp:LinkButton ID="lnkNonCimCampaigns" runat="server" Text="Test Categories" CssClass="lbuttons"
                ForeColor="black" OnClick="lnkNonCimCampaigns_Click" />
        </td>
        <td id="trTeamSummaries" runat="server" style="height: 19px" align="center" colspan="2">
            <asp:LinkButton ID="lnkTeamSummaries" runat="server" Text="Test Summaries" CssClass="lbuttons"
                OnClick="lnkTeamSummaries_Click" ForeColor="black" />
        </td>
        <td id="trRecheckExams" runat="server" style="height: 19px" align="center">
            <asp:LinkButton ID="lnkRecheckExams" runat="server" Text="Recheck Exams" CssClass="lbuttons"
                OnClick="lnkRecheckExams_Click" ForeColor="black" />
        </td>
        <%--<td id="trCheckSuggestions" runat="server" style="height: 19px" align="center" visible="false">
            <asp:LinkButton ID="lnkCheckSuggestions" runat="server" Text="Check Suggested Questions"
                CssClass="lbuttons" ForeColor="black" OnClick="lnkCheckSuggestions_Click" />
        </td>
        <td id="trSuggest" runat="server" style="height: 19px" align="center" visible="false">
            <asp:LinkButton ID="lnkSuggest" runat="server" Text="Suggest New Question" CssClass="lbuttons"
                OnClick="lnkSuggest_Click" ForeColor="black" />
        </td>
        <td id="trCreateUserBatch" runat="server" style="height: 19px" align="center" visible="false">
            <asp:LinkButton ID="lnkCreateUserBatch" runat="server" Text="Create User Batch" CssClass="lbuttons"
                ForeColor="black" OnClick="lnkCreateUserBatch_Click" />
        </td>
        <td id="trViewUserBatch" runat="server" style="height: 19px" align="center" visible="false">
            <asp:LinkButton ID="lnkViewUserBatch" runat="server" Text="View User Batches" CssClass="lbuttons"
                ForeColor="black" OnClick="lnkViewUserBatch_Click" />
        </td>--%>
        <td id="trLogout" runat="server" style="height: 19px" align="center" valign="middle">
            <asp:LinkButton ID="lnkLogout" runat="server" Text="Logout" CssClass="lbuttons" OnClick="lnkLogout_Click"
                ForeColor="black" />
        </td>
    </tr>
</table>
