using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using System.Data.SqlClient;

public partial class controls_userinfocontrol : System.Web.UI.UserControl
{
    string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());

        CheckUserRole();

        if (!this.IsPostBack)
        {
            this.lblUserInfoValue.Text = SessionManager.SessionExamineeName;
            this.ProcessFinished();
            gvExamsFinished.Sort("StartDate", SortDirection.Descending);
            this.ProcessPending();
            gvExamsPending.Sort("DateStartTaken", SortDirection.Descending);
            this.ProcessUnfinished("StartDate", " DESC");
            this.ProcessEssays();
            gvEssays.Sort("DateEndTaken", SortDirection.Descending);

            CheckUserRole();

            if (_Role != null)
            {
                tdCatSub.Visible = true;

                this.lblExaminee.Text = SessionManager.SessionExamineeName;
                this.ddlAccounts.DataSource = NonCimCampaign.SelectParents();
                this.ddlAccounts.DataBind();
                this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
                this.ddlSubCategory.DataBind();
            }
            else
            {
                tdCatSub.Visible = false;
            }
        }
    }

    protected void gvExamsUnfinished_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvExamsUnfinished.PageIndex = e.NewPageIndex;
        this.ProcessUnfinished();
    }

    protected void gvExamsUnfinished_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                int testTakenID = Convert.ToInt32(e.CommandArgument);
                TestTaken taken = TestTaken.Select(testTakenID);
                if (taken != null)
                {
                    this.lblUnfinishedExamNameVal.Text = TestCategory.Select(taken.TestCategoryID, true).TestName;
                    this.lblUnfinishedDateStartVal.Text = taken.DateStartTaken.ToString("yyyy/MM/dd hh:mm tt");
                    if (taken.DateLastSave.HasValue)
                        this.lblUnfinishedDatelastSaveVal.Text = taken.DateLastSave.Value.ToString("yyyy/MM/dd hh:mm tt");
                    else
                        this.lblUnfinishedDatelastSaveVal.Text = "Has not been saved";
                    this.gvUnfinishedInfoItems.DataSource = SaveTestResponse.GetPendingExamAnswerStatus(testTakenID);
                    this.gvUnfinishedInfoItems.DataBind();
                    this.mpePending.Show();

                }
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "gvExamsUnfinished_RowCommand", ex.Message);
            }
        }
    }

    protected void gvExamsFinished_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvExamsFinished.PageIndex = e.NewPageIndex;
        this.ProcessFinished();
    }

    protected void gvExamsFinished_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                Label lblDateStartTaken = row.FindControl("lblDateStartTaken") as Label;
                Label lblDateEndTaken = row.FindControl("lblDateEndTaken") as Label;
                Label lblTestName = row.FindControl("lblTestName") as Label;
                if (e.CommandName == "View")
                {

                    TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);
                    if (category != null)
                    {
                        this.lblResultsExamName.Text = lblTestName.Text;
                        this.lblDateStartedVal.Text = lblDateStartTaken.Text;
                        this.lblDateFinishedVal.Text = lblDateEndTaken.Text;
                        Questionnaire[] questions = Questionnaire.SelectByCategory(category.TestCategoryID);
                        TestResponse[] responses = TestResponse.SelectByTestTaken(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                        this.gvExamResults.DataSource = responses;
                        this.gvExamResults.DataBind();
                        if (responses.Length < 1)
                            this.gvExamResults.EmptyDataText = "No questions were answered in this exam.";
                        foreach (GridViewRow inRow in this.gvExamResults.Rows)
                        {
                            Label lblQuestionnaireID = inRow.FindControl("lblQuestionnaireID") as Label;
                            Label lblTestResponseID = inRow.FindControl("lblTestResponseID") as Label;
                            Label lblQuestion = inRow.FindControl("lblQuestion") as Label;
                            Label lblEssayScore = inRow.FindControl("lblEssayScore") as Label;
                            Image imgResult = inRow.FindControl("imgResult") as Image;
                            Questionnaire question = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), true);
                            if (question != null)
                                lblQuestion.Text = question.Question;
                            TestResponse tResponse = TestResponse.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()), question.QuestionnaireID, SessionManager.SessionExamineeName);
                            if (tResponse != null)
                            {
                                if (!string.IsNullOrEmpty(tResponse.EssayResponse))
                                {
                                    imgResult.ImageUrl = "~/images/icon-essay.gif";
                                    lblEssayScore.Text = tResponse.EssayScore.ToString();
                                }
                                else if (
                                    tResponse.Response1.Trim() == question.Ans1.Trim() && tResponse.Response2.Trim() == question.Ans2.Trim() && tResponse.Response3.Trim() == question.Ans3.Trim() &&
                                    tResponse.Response4.Trim() == question.Ans4.Trim() && tResponse.Response5.Trim() == question.Ans5.Trim() && tResponse.Response6.Trim() == question.Ans6.Trim() &&
                                    tResponse.Response7.Trim() == question.Ans7.Trim() && tResponse.Response8.Trim() == question.Ans8.Trim() && tResponse.Response9.Trim() == question.Ans9.Trim() &&
                                    tResponse.Response10.Trim() == question.Ans10.Trim()
                                  )
                                    imgResult.ImageUrl = "~/images/icon-pass.gif";
                                else
                                    imgResult.ImageUrl = "~/images/icon-fail.gif";
                            }
                        }
                        this.mpeAnswers.Show();
                    }
                }
                else if (e.CommandName == "Remove")
                {
                    TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    if (taken != null)
                        taken.Delete();
                    this.ProcessFinished();
                }
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "gvExamsFinished_RowCommand", ex.Message);
            }
        }
    }

    protected void gvExamsPending_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvExamsPending.PageIndex = e.NewPageIndex;
        this.ProcessPending();
    }

    protected void gvExamsPending_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {

        }
    }

    protected void gvEssays_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            int testTakenID = Convert.ToInt32(e.CommandArgument.ToString());
            this.gvEssayInfo.DataSource = TestTaken.GetResponsesWithChecker(testTakenID);
            this.gvEssayInfo.DataBind();
            this.mpeEssays.Show();
        }
    }

    protected void gvEssays_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvEssays.PageIndex = e.NewPageIndex;
        this.ProcessEssays();
    }

    protected void gvEssayExams_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvEssayInfo.PageIndex = e.NewPageIndex;
        this.ProcessEssays();
    }

    protected void ProcessUnfinished()
    {
        try
        {
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(16))
            {
                DataSet pendingCompletion = TestTaken.SelectPendingCompletion(SessionManager.SessionExamineeName);
                this.gvExamsUnfinished.DataSource = pendingCompletion;
                this.gvExamsUnfinished.DataBind();
                if (this.gvExamsUnfinished.Rows.Count > 0)
                    this.gvExamsUnfinished.BorderColor = System.Drawing.Color.Black;

                foreach (GridViewRow row in this.gvExamsUnfinished.Rows)
                {
                    Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                    Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                    Label lblTimeRemaining = row.FindControl("lblTimeRemaining") as Label;
                    Label lblTimeRemainingVal = row.FindControl("lblTimeRemainingVal") as Label;
                    TestTaken test = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);
                    if (category != null)
                    {
                        if (category.TimeLimit > 0)
                        {
                            decimal timeRemaining = Convert.ToDecimal(lblTimeRemaining.Text.Trim());
                            timeRemaining /= 60;
                            lblTimeRemainingVal.Text = timeRemaining.ToString("00.00");
                        }
                        else
                            lblTimeRemainingVal.Text = "No Time Limit";
                    }
                }
            }
            else
            {
                this.gvExamsFinished.DataSource = null;
                this.gvExamsUnfinished.DataBind();
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "ProcessUnfinished", ex.Message);
            }
        }
    }

    protected void ProcessUnfinished(string sortExpression, string direction)
    {
        try
        {
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(16))
            {
                DataTable pendingCompletion = TestTaken.SelectPendingCompletion(SessionManager.SessionExamineeName).Tables[0];
                DataView dw = pendingCompletion.DefaultView;

                dw.Sort = sortExpression + direction;
                this.gvExamsUnfinished.DataSource = pendingCompletion;
                this.gvExamsUnfinished.DataBind();

                if (this.gvExamsUnfinished.Rows.Count > 0)
                    this.gvExamsUnfinished.BorderColor = System.Drawing.Color.Black;

                foreach (GridViewRow row in this.gvExamsUnfinished.Rows)
                {
                    Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                    Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                    Label lblTimeRemaining = row.FindControl("lblTimeRemaining") as Label;
                    Label lblTimeRemainingVal = row.FindControl("lblTimeRemainingVal") as Label;
                    TestTaken test = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);

                    if (category != null)
                    {
                        if (category.TimeLimit > 0)
                        {
                            decimal timeRemaining = Convert.ToDecimal(lblTimeRemaining.Text.Trim());
                            timeRemaining /= 60;
                            lblTimeRemainingVal.Text = timeRemaining.ToString("00.00");
                        }
                        else
                            lblTimeRemainingVal.Text = "No Time Limit";
                    }
                }
            }
            else
            {
                this.gvExamsFinished.DataSource = null;
                this.gvExamsUnfinished.DataBind();
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "ProcessUnfinished", ex.Message);
            }
        }
    }

    protected void ProcessFinished()
    {
        try
        {
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(15))
            {
                BindFinished();
            }
            else
            {
                this.gvExamsFinished.DataSource = null;
                this.gvExamsFinished.DataBind();
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "ProcessFinished", ex.Message);
            }
        }
    }

    void BindFinished(string sortExpression, string direction)
    {
        DataSet completed = TestTaken.SelectCompleted(SessionManager.SessionExamineeName);
        DataTable dt = completed.Tables[0];

        DataView dv = new DataView(dt);
        dv.Sort = sortExpression + direction;

        gvExamsFinished.DataSource = dv;
        gvExamsFinished.DataBind();

        if (this.gvExamsFinished.Rows.Count > 0)
            this.gvExamsFinished.BorderColor = System.Drawing.Color.Black;

        foreach (GridViewRow row in this.gvExamsFinished.Rows)
        {
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Image imgPassOrFail = row.FindControl("imgPassOrFail") as Image;
            LinkButton lnkDelete = row.FindControl("lnkDelete") as LinkButton;
            if (lblTestTakenID != null)
            {
                TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                if (taken.Passed && imgPassOrFail != null)
                {
                    imgPassOrFail.ImageUrl = "~/images/icon-pass.gif";
                    lnkDelete.Enabled = false;
                }
                else if (!taken.Passed && imgPassOrFail != null)
                    imgPassOrFail.ImageUrl = "~/images/icon-fail.gif";
            }
        }
    }

    void BindFinished()
    {
        DataSet completed = TestTaken.SelectCompleted(SessionManager.SessionExamineeName);
        this.gvExamsFinished.DataSource = completed;
        this.gvExamsFinished.DataBind();

        if (this.gvExamsFinished.Rows.Count > 0)
            this.gvExamsFinished.BorderColor = System.Drawing.Color.Black;

        foreach (GridViewRow row in this.gvExamsFinished.Rows)
        {
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Image imgPassOrFail = row.FindControl("imgPassOrFail") as Image;
            LinkButton lnkDelete = row.FindControl("lnkDelete") as LinkButton;
            if (lblTestTakenID != null)
            {
                TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                if (taken.Passed && imgPassOrFail != null)
                {
                    imgPassOrFail.ImageUrl = "~/images/icon-pass.gif";
                    lnkDelete.Enabled = false;
                }
                else if (!taken.Passed && imgPassOrFail != null)
                    imgPassOrFail.ImageUrl = "~/images/icon-fail.gif";
            }
        }
    }

    protected void ProcessPending(string sortExpression, string direction)
    {
        try
        {
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(14))
            {
                TestTaken[] pendingRating = TestTaken.SelectPendingRating(SessionManager.SessionExamineeName);

                if (direction == " ASC")
                {
                    Array.Sort(pendingRating);
                }
                else if (direction == " DESC")
                {
                    Array.Sort(pendingRating);
                }

                this.gvExamsPending.DataSource = pendingRating;
            }
            else
            {
                this.gvExamResults.DataSource = null;
            }

            this.gvExamsPending.DataBind();
            if (this.gvExamsPending.Rows.Count > 0)
                this.gvExamsPending.BorderColor = System.Drawing.Color.Black;
            if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(10))
            {
                foreach (GridViewRow row in this.gvExamsPending.Rows)
                {
                    LinkButton lnkRate = row.FindControl("lnkRate") as LinkButton;
                    if (lnkRate != null)
                        lnkRate.Visible = false;
                }
            }
        }
        catch(Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "ProcessPending", ex.Message);
            }
        }
    }

    protected void ProcessPending()
    {
        try
        {
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(14))
            {
                TestTaken[] pendingRating = TestTaken.SelectPendingRating(SessionManager.SessionExamineeName);
                this.gvExamsPending.DataSource = pendingRating;
            }
            else
            {
                this.gvExamResults.DataSource = null;
            }

            this.gvExamsPending.DataBind();
            if (this.gvExamsPending.Rows.Count > 0)
                this.gvExamsPending.BorderColor = System.Drawing.Color.Black;
            if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(10))
            {
                foreach (GridViewRow row in this.gvExamsPending.Rows)
                {
                    LinkButton lnkRate = row.FindControl("lnkRate") as LinkButton;
                    if (lnkRate != null)
                        lnkRate.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "ProcessPending", ex.Message);
            }
        }
    }
    
    protected void ProcessEssays()
    {
        this.gvEssays.DataSource = TestTaken.SelectEssaysByUser(SessionManager.SessionExamineeName);
        this.gvEssays.DataBind();
    }

    protected void ProcessEssays(string sortExpression, string direction)
    {
        DataTable dt = TestTaken.SelectEssaysByUser(SessionManager.SessionExamineeName).Tables[0];
        DataView dw = dt.DefaultView;

        dw.Sort = sortExpression + direction;

        this.gvEssays.DataSource = dw;

        this.gvEssays.DataBind();
    }

    protected void btnCloseSummary_Click(object sender, EventArgs e)
    {

    }

    protected void lnkIsAdmin_Click(object sender, EventArgs e)
    {
        this.usrRightsControl.PnlRightsPopup.Visible = true;
        this.usrRightsControl.PnlBasicRightsPopup.Visible = false;
        this.usrRightsControl.PopulateRights();
        this.mpeRightsPopup.Show();
    }

    protected void btnRightChangeOk_Click(object sender, EventArgs e)
    {
        try
        {
            string saveString = "";
            if (this.usrRightsControl.PnlRightsPopup.Visible == true)
            {
                int counter = 1;
                foreach (GridViewRow row in this.usrRightsControl.GvRights.Rows)
                {
                    Label lblAppRights = row.FindControl("lblAppRights") as Label;
                    CheckBox chkAppRights = row.FindControl("chkAppRights") as CheckBox;
                    if (lblAppRights != null && chkAppRights != null)
                    {
                        int temp = Convert.ToInt32(lblAppRights.Text.Trim());
                        if (temp == counter)
                        {
                            if (chkAppRights.Checked)
                                saveString = "1" + saveString;
                            else
                                saveString = "0" + saveString;
                            counter++;
                        }
                        else
                        {
                            throw new Exception("Rights are missing.");
                        }
                    }
                }
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                AppRights[] rights = AppRights.SelectAll();
                int len = rights.Length;
                char[] array = new char[rights.Length];
                for (int x = 0; x < rights.Length; x++)
                {
                    array[x] = '0';
                }
                foreach (ListItem item in this.usrRightsControl.CblBasicRights.Items)
                {
                    if (item.Selected)
                    {
                        char[] rightarray = item.Value.ToCharArray();
                        for (int x = 0; x < len; x++)
                            if (rightarray[x] == '1')
                                array[x] = '1';
                    }
                }
                for (int x = 0; x < rights.Length; x++)
                {
                    builder.Append(array[x]);
                }
                saveString = builder.ToString();
            }
            //if (lblHasRights.Text == "true")
            //{
            //    UserRights rights = UserRights.Select(SessionManager.SessionExamineeName);
            //    rights.Rights = saveString;
            //    rights.EditRights();
            //}
            //else
            //{
            UserRights right = new UserRights(SessionManager.SessionExamineeName, saveString);
            right.Insert();
            //}
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "userinfocontrol.ascx", "btnRightChangeOk_Click", ex.Message);
            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/users.aspx?" + Request.QueryString.ToString());
    }

    protected void btnEndExam_Click(object sender, EventArgs e)
    {

    }

    protected void lnkIsAdminBasic_Click(object sender, EventArgs e)
    {
        this.usrRightsControl.PnlBasicRightsPopup.Visible = true;
        this.usrRightsControl.PnlRightsPopup.Visible = false;
        this.usrRightsControl.PopulateRights();
        this.mpeRightsPopup.Show();
    }
    
    protected void gvExamsFinished_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            SortGridViewFinished(sortExpression, DESCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            SortGridViewFinished(sortExpression, ASCENDING);
        }   

    }

    private void SortGridViewFinished(string sortExpression, string direction)
    {
        BindFinished(sortExpression, direction);
    }

    private const string ASCENDING = " ASC";
    private const string DESCENDING = " DESC";

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void gvExamsPending_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            SortGridViewPending(sortExpression, DESCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            SortGridViewPending(sortExpression, ASCENDING);
        }   
    }

    private void SortGridViewPending(string sortExpression, string DESCENDING)
    {
       ProcessPending(sortExpression, DESCENDING);
    }

    protected void gvExamsUnfinished_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            ProcessUnfinished(sortExpression, ASCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ProcessUnfinished(sortExpression, DESCENDING);
        }   
    }

    protected void gvEssays_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            ProcessEssays(sortExpression, ASCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ProcessEssays(sortExpression, DESCENDING);
        }
    }

    protected void ddlAccounts_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
        this.ddlSubCategory.DataBind();
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        InsertSubCat();
        ddlAccounts.DataBind();
        ddlSubCategory.DataBind();

        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Campaign sucessfully updated.');", true);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        DeleteSubCat();
        ddlAccounts.DataBind();
        ddlSubCategory.DataBind();

        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Campaign sucessfully deleted.');", true);
    }

    protected void InsertSubCat()
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString))
        {
            connection.Open();

            string sql = "IF NOT EXISTS(SELECT CampaignID FROM NuSkillCheck.dbo.tbl_testing_CatSub WHERE CampaignID = @CampaignID AND CIM = @CIM) " +
                            "BEGIN " +
                                "INSERT INTO NuSkillCheck.dbo.tbl_testing_CatSub(CIM, CampaignID, Campaign, ParentCampaignID) VALUES(@CIM, @CampaignID, @Campaign, @ParentCampaignID) " +
                            "END ";

            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@CIM", SqlDbType.Int).Value = SessionManager.SessionExamineeName;
            cmd.Parameters.Add("@CampaignID", SqlDbType.Int).Value = ddlSubCategory.SelectedValue;
            cmd.Parameters.Add("@Campaign", SqlDbType.VarChar, 50).Value = ddlSubCategory.SelectedItem.Text;
            cmd.Parameters.Add("@ParentCampaignID", SqlDbType.Int).Value = ddlAccounts.SelectedValue;
            cmd.CommandType = CommandType.Text;

            cmd.ExecuteNonQuery();
        }
    }

    protected void DeleteSubCat()
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString))
        {
            connection.Open();

            string sql = "DELETE FROM NuSkillCheck.dbo.tbl_testing_CatSub WHERE CIM = @CIM AND CampaignID = @CampaignID AND ParentCampaignID = @ParentCampaignID";

            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@CIM", SqlDbType.Int).Value = SessionManager.SessionExamineeName;
            cmd.Parameters.Add("@CampaignID", SqlDbType.Int).Value = ddlSubCategory.SelectedValue;
            cmd.Parameters.Add("@ParentCampaignID", SqlDbType.Int).Value = ddlAccounts.SelectedValue;
            cmd.CommandType = CommandType.Text;

            cmd.ExecuteNonQuery();
        }
    }

    protected void CheckUserRole()
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT DISTINCT CR.[Role] FROM susl3psqldb02.CIMEnterprise.dbo.tbl_Personnel_Cor_Employee E " +
                         "INNER JOIN susl3psqldb02.CIMEnterprise.dbo.tbl_Personnel_Cor_Role CR " +
                         "ON E.RoleID = CR.RoleID " +
                         "WHERE (CR.RoleID = 4196 OR CR.[Role] LIKE '%Manager%' OR CR.[Role] LIKE '%Director%') " +
                         "AND E.CIMNumber = @CIMNumber ORDER BY 1 ";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@CIMNumber", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }
}