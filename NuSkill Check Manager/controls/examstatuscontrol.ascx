<%@ Control Language="C#" AutoEventWireup="true" CodeFile="examstatuscontrol.ascx.cs"
    Inherits="controls_examstatuscontrol" %>
<asp:Panel ID="pnlExam" runat="server" Width="100%" HorizontalAlign="center" ForeColor="black">
    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td style="vertical-align: middle; font-size: large" align="center">
                <asp:Panel ID="pnlMainStatus" runat="server" Visible="true">
                    <table cellpadding="5" cellspacing="0" style="vertical-align: middle; font-family: Arial;
                        font-size: 12px" width="100%" border="1">
                        <tr>
                            <td rowspan="11">
                            </td>
                            <td colspan="6" style="font-size: large; font-family: Arial">
                                <asp:Label ID="lblCampaignAccount" runat="server" />
                            </td>
                            <td rowspan="11">
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle; font-size: large; font-family: Arial;" colspan="6"
                                align="center">
                                <asp:Label ID="lblTestInfo" runat="server" Text="Information for Test:" />
                                <asp:Label ID="lblTestInfoValue" runat="server" />
                                <asp:Label ID="lblTestCategoryID" runat="server" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="vertical-align: middle;">
                                <asp:Label ID="lblInstructions" runat="server" Text="Instructions:" /><br />
                                <asp:Label ID="lblInstructionsVal" runat="server" Width="80%" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="6">
                                <asp:Label ID="lblCreationInfo" runat="server" Font-Size="10px" />
                                <!--Format: Created on <CreateDate> by <CreatedBy>. Last edited on <LastEditDate> by <LastEditedBy>. -->
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="6">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date:" />
                                <br />
                                <asp:Label ID="lblStartDateVal" runat="server" />
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblEndDate" runat="server" Text="End Date:" />
                                <br />
                                <asp:Label ID="lblEndDateVal" runat="server" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="3">
                                <asp:Label ID="lblStartDateVal" runat="server" />
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblEndDateVal" runat="server" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2" style="width: 33%">
                                <asp:Label ID="lblDuration" runat="server" Text="Duration:" />
                                <br />
                                <asp:Label ID="lblDurationVal" runat="server" />
                            </td>
                            <td colspan="2" style="width: 34%">
                                <asp:Label ID="lblNumberOfTries" runat="server" Text="Number of Tries Allowed:" />
                                <br />
                                <asp:Label ID="lblNumberOfTriesVal" runat="server" />
                            </td>
                            <td colspan="2" style="width: 33%">
                                <asp:Label ID="lblPassingScore" runat="server" Text="Passing Score (Percentage):" />
                                <br />
                                <asp:Label ID="lblPassingScoreVal" runat="server" />
                            </td>
                        </tr>
                        <%-- <tr>
                            <td colspan="2">
                                <asp:Label ID="lblDurationVal" runat="server" />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="lblNumberOfTriesVal" runat="server" />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="lblPassingScoreVal" runat="server" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="6">
                                <asp:Label ID="lblItems" runat="server" Text="Exam Items:" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" BorderColor="black"
                                    Font-Size="12px" CellPadding="5" EmptyDataText="There are no items in this exam."
                                    Width="100%">
                                    <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                                    <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                                        NextPageText=">" Position="Bottom" PreviousPageText="<" />
                                    <RowStyle BorderColor="black" />
                                    <RowStyle ForeColor="black" Font-Size="11px" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="center" ItemStyle-Width="80px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblExamType" runat="server" Visible="false" Text='<%#Bind("TypeCode") %>' />
                                                <asp:Label ID="lblExamTypeVal" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Question" HeaderText="Question">
                                            <HeaderStyle HorizontalAlign="center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <%--<asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSelect" runat="server" CommandName="Select" Text="View" CssClass="gridLinkButton"
                                                Font-Bold="true" ForeColor="black" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit" CssClass="gridLinkButton"
                                                Font-Bold="true" ForeColor="black" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Delete"
                                                CssClass="gridLinkButton" Font-Bold="true" ForeColor="black" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:Label ID="lblHotlink" runat="server" Text="Exam Hotlink:" />
                                <br />
                                <asp:TextBox ID="txtHotlink" runat="server" ReadOnly="true" Width="70%" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:LinkButton ID="lnkExamTakers" runat="server" Text="Click here for a list of users who have taken the exam"
                                    Font-Bold="true" OnClick="lnkExamTakers_Click" ForeColor="black" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:LinkButton ID="lnkCopyExam" runat="server" Text="Copy Exam" Font-Size="11px"
                                    Font-Bold="true" ForeColor="black" />
                            </td>
                            <td colspan="2">
                                <asp:LinkButton ID="lnkEditExam" runat="server" Text="Edit Exam" Font-Size="11px"
                                    Font-Bold="true" ForeColor="black" OnClick="lnkEditExam_Click" />
                            </td>
                            <td colspan="2">
                                <asp:LinkButton ID="lnkDeleteExam" runat="server" Text="Delete Exam" Font-Size="11px"
                                    Font-Bold="true" ForeColor="black" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" valign="middle">
                <asp:Button CssClass="buttons" ID="btnContinue" runat="server" Text="Return" OnClick="btnContinue_Click" />
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeCopyExam" runat="server" TargetControlID="lnkCopyExam"
    PopupControlID="pnlCopyParams" CancelControlID="btnCancelCopyToNew" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlCopyParams" runat="server" CssClass="modalPopup" HorizontalAlign="center">
    <table cellpadding="5" width="610" cellspacing="0">
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="lblTeamSummaries" runat="server" Text="Copy Exam" ForeColor="black"
                    Font-Size="16" /><br />
                <hr style="color: Black; width: 100%" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="lblRequiredFields" runat="server" Text="All fields are required."
                    Font-Size="smaller" ForeColor="black" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblExamName" runat="server" Text="New Exam Name:" />
                <br />
                <asp:TextBox ID="txtExamNameVal" runat="server" Width="80%" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCampaign" runat="server" Text="New Exam Category:" />
                <br />
                <asp:DropDownList ID="ddlCampaign" runat="server" DataTextField="Campaign" DataValueField="CampaignID"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlCampaign_SelectedIndexChanged" />
            </td>
            <td>
                <asp:Label ID="lblSubcategory" runat="server" Text="New Exam Subcategory:" />
                <br />
                <asp:DropDownList ID="ddlSubcategory" runat="server" DataTextField="Campaign" DataValueField="CampaignID" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblStartDateNew" runat="server" Text="Start Date (yyyy/MM/dd):" />
                <br />
                <asp:TextBox ID="txtStartDate" runat="server" /><asp:ImageButton ID="imgCalendarStartDate"
                    runat="server" ImageUrl="~/images/icon-calendar.gif" />
                <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtStartDate"
                    PopupButtonID="imgCalendarStartDate" Format="yyyy/MM/dd" />
                <asp:DropDownList ID="ddlStartHour" runat="server" />
                <asp:Label ID="lblStartDateColon" runat="server" Text=":" Font-Bold="true" />
                <asp:DropDownList ID="ddlStartMinute" runat="server" />
            </td>
            <td>
                <asp:Label ID="lblEndDateNew" runat="server" Text="End Date (yyyy/MM/dd):" />
                <br />
                <asp:TextBox ID="txtEndDate" runat="server" /><asp:ImageButton ID="imgCalendarEndDate"
                    runat="server" ImageUrl="~/images/icon-calendar.gif" />
                <ajax:CalendarExtender ID="calEndDate" runat="server" TargetControlID="txtEndDate"
                    PopupButtonID="imgCalendarEndDate" Format="yyyy/MM/dd" />
                <asp:DropDownList ID="ddlEndHour" runat="server" />
                <asp:Label ID="lblEndDateColon" runat="server" Text=":" Font-Bold="true" />
                <asp:DropDownList ID="ddlEndMinute" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button CssClass="buttons" ID="btnCopyToNew" runat="server" Text="Create" OnClick="btnCopyToNew_Click" />
            </td>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnCancelCopyToNew" runat="server" Text="Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeDeleteExam" runat="server" TargetControlID="lnkDeleteExam"
    PopupControlID="pnlDeleteExam" CancelControlID="btnConfirmCancel" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlDeleteExam" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0">
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblConfirmDelete" runat="server" Text="Delete Exam?" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button CssClass="buttons" ID="btnConfirmDelete" runat="server" Text="Delete"
                    OnClick="btnConfirmDelete_Click" />
            </td>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnConfirmCancel" runat="server" Text="Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID="hidStatus" runat="server" />
<ajax:ModalPopupExtender ID="mpeStatus" runat="server" TargetControlID="hidStatus"
    PopupControlID="pnlStatus" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlStatus" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0">
        <tr>
            <td align="center">
                <asp:Label ID="lblStatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button CssClass="buttons" ID="btnStatus" runat="server" Text="Return" OnClick="btnStatus_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID="hidTakers" runat="server" />
<ajax:ModalPopupExtender ID="mpeTakers" runat="server" TargetControlID="hidTakers"
    PopupControlID="pnlTakers" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlTakers" runat="server" CssClass="modalPopup" Width="400px">
    <table cellpadding="10" cellspacing="0" width="100%">
        <tr>
            <td colspan="6">
                <asp:GridView ID="gvExamTakers" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                    Font-Size="12px" PageSize="10" CellPadding="5" BorderColor="black" EmptyDataText="There are no users who have taken this exam."
                    Width="100%" OnPageIndexChanging="gvExamTakers_PageIndexChanging">
                    <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                    <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                        NextPageText=">" Position="Bottom" PreviousPageText="<" />
                    <RowStyle BorderColor="black" />
                    <Columns>
                        <asp:BoundField DataField="UserID" HeaderText="User Name" />
                        <asp:BoundField DataField="DateStartTaken" HeaderText="Date Taken:" />
                        <asp:TemplateField HeaderText="Result">
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblDateEndTaken" runat="server" Text='<%#Bind("DateEndTaken") %>'
                                    Visible="false" />
                                <asp:Label ID="lblPassed" Visible="false" runat="server" Text='<%#Bind("Passed") %>' />
                                <asp:Image ID="imgPassOrFail" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <asp:Button ID="btnCloseTakers" runat="server" Text="Close" CssClass="buttons" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeNoRightsToEditSingleExam" BackgroundCssClass="modalBackground"
    runat="server" TargetControlID="hidNoRightsToEditSingleExam" PopupControlID="pnlNoRightsToEditSingleExam" />
<asp:HiddenField ID="hidNoRightsToEditSingleExam" runat="server" />
<asp:Panel ID="pnlNoRightsToEditSingleExam" runat="server" CssClass="modalPopup"
    HorizontalAlign="center" Width="200px">
    <table width="100%" cellpadding="5" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="lblNoRightsToEditSingleExam" runat="server" Text="You do not have enough rights to edit this exam." />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnNoRightsToEditSingleExam" runat="server" Text="Return" CssClass="buttons" />
            </td>
        </tr>
    </table>
</asp:Panel>
