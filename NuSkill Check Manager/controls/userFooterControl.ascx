<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userFooterControl.ascx.cs" Inherits="controls_userFooterControl" %>

<link href="~/styles/style.css" rel="stylesheet" type="text/css" />

<table align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="height: 25px;">
    <tr align="center" style="border-right: 0px; border-top: 0px; font-size: 11px; border-left: 0px;
        color: #000000; border-bottom: 0px; font-family: Tahoma,Verdana,Arial;">
        <td style="background-color: #CACDD6; color: #001231; vertical-align: middle;">
                <asp:Label ID="lblNuSkillVersion" runat="server" Text="Transcom University Testing Admin" Font-Names="Arial"
                    Font-Size="10px" />
                <br />
            Copyright � 2014 Transcom North America and Asia
            <br />
                <asp:HyperLink ID="hypHelp" runat="server" NavigateUrl="http://information.nucomm.net/NuSkillImages/HowTo/admin.htm" Text="Help" />
            </td>
    </tr>
</table>