using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class controls_menuControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
        {
            this.trExamSetup.Visible = false;
            this.trReports.Visible = false;
            this.trExams.Visible = false;
            this.trExaminees.Visible = false;
            this.trEssays.Visible = false;
            //this.trCheckSuggestions.Visible = false;
            this.trLogout.Visible = false;
            this.trTeamSummaries.Visible = false;
            //this.trViewUserBatch.Visible = false;
            //this.trCreateUserBatch.Visible = false;
            //this.trCheckSuggestions.Visible = false;
        }
        else
        {
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(1))
            {
                this.trExams.Visible = true;
                this.trExamSetup.Visible = true;
            }
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(11))
                this.trExaminees.Visible = true;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(9))
                this.trEssays.Visible = true;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(0))
                this.trLogout.Visible = true;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(21))
                this.trTeamSummaries.Visible = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(29))
            //    this.trCheckSuggestions.Visible = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(30))
            //    this.trCreateUserBatch.Visible = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(31))
            //    this.trViewUserBatch.Visible = true;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(32))
                this.trNonCimCampaigns.Visible = true;
        }
        //if (string.IsNullOrEmpty(SessionManager.SessionUsername))
        //    this.trSuggest.Visible = false;
    }

    protected void lnkExams_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkEssays_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essaytests.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkExaminees_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/users.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkReports_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/reports.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkTeamSummaries_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/teamsummaries.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();

        FormsAuthentication.SignOut();
        Response.Redirect("~/login.aspx");
    }

    protected void lnkSuggest_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/suggestnew.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkCreateUserBatch_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/massusercreation.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkViewUserBatch_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/userbatches.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkCheckSuggestions_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/viewsuggestions.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkNonCimCampaigns_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/customcampaigns.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkQuickItems_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/newautoexam.aspx" + Request.QueryString.ToString());
    }

    protected void lnkRecheckExams_Click(object sender, EventArgs e)
    {
        Response.Redirect("recheckexam.aspx" + Request.QueryString.ToString());
    }
}
