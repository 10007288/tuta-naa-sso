<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userRightsControl.ascx.cs"
    Inherits="controls_userRightsControl" %>

<asp:Panel ID="pnlBasicRightsPopup" runat="server" CssClass="modalPopup" HorizontalAlign="center"
    Width="100%" Height="500px" ScrollBars="vertical" BorderWidth="0" BorderStyle="none">
    <table cellpadding="5" cellspacing="0" border="0" width="95%">
        <tr>
            <td>
                <asp:Label ID="lblBasicRights" runat="server" Text="Basic Rights" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:CheckBoxList ID="cblBasicRights" runat="server" DataValueField="PresetRightsValue" DataTextField="RightsName" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlRightsPopup" runat="server" CssClass="modalPopup" HorizontalAlign="center"
    Width="100%" Height="500px" ScrollBars="vertical" BorderWidth="0" BorderStyle="none">
    <table cellpadding="5" cellspacing="0" border="0" width="95%">
        <tr>
            <td>
                <asp:Label ID="lblHasRights" runat="server" Visible="false" />
                <asp:GridView ID="gvRights" runat="server" HorizontalAlign="left" Width="100%" CellPadding="5"
                    AutoGenerateColumns="false" EmptyDataText="There are no rights.">
                    <RowStyle BackColor="white" HorizontalAlign="left" />
                    <AlternatingRowStyle BackColor="white" HorizontalAlign="left" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="lblAppRights" runat="server" Text='<%#Bind("AppRightsID") %>' Visible="false" />
                                <asp:CheckBox ID="chkAppRights" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" Text='<%#Bind("Title") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%#Bind("Description") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>