using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class controls_reportcontrol : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
            this.PopulateReports();
    }

    protected void PopulateReports()
    {
        this.ddlReportType.Items.Clear();
        this.ddlReportType.Items.Add(new ListItem("Test By Courses Report", "ByCourse"));
        this.ddlReportType.Items.Add(new ListItem("Test Scores By Site By Campaign by Employee Report", "BySSCE"));
        this.ddlReportType.Items.Add(new ListItem("Test Answer By Employee Report", "ByEmployee"));
        this.ddlReportType.Items.Add(new ListItem("Test Answer By Question By Employee Report", "ByQE"));
        this.ddlReportType.Items.Add(new ListItem("Employees Who Still Need to Take Test Report", "ByETR"));
        this.ddlReportType.Items.Add(new ListItem("Test Question Percentage Report", "ByTQPR"));
    }

    protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReportType.SelectedValue == "ByCourse")
        {
            this.trByCourse.Visible = true;
            this.trByEmployee.Visible = false;
            this.trByETR.Visible = false;
            this.trByQE0.Visible = false;
            this.trByQE1.Visible = false;
            this.trBySSCE0.Visible = false;
            this.trBySSCE1.Visible = false;
            this.trByTQPR.Visible = false;
        }
        else if (this.ddlReportType.SelectedValue == "BySSCE")
        {
            this.trByCourse.Visible = false;
            this.trByEmployee.Visible = false;
            this.trByETR.Visible = false;
            this.trByQE0.Visible = false;
            this.trByQE1.Visible = false;
            this.trBySSCE0.Visible = true;
            this.trBySSCE1.Visible = true;
            this.trByTQPR.Visible = false;
        }
        else if (this.ddlReportType.SelectedValue == "ByEmployee")
        {
            this.trByCourse.Visible = false;
            this.trByEmployee.Visible = true;
            this.trByETR.Visible = false;
            this.trByQE0.Visible = false;
            this.trByQE1.Visible = false;
            this.trBySSCE0.Visible = false;
            this.trBySSCE1.Visible = false;
            this.trByTQPR.Visible = false;
        }
        else if (this.ddlReportType.SelectedValue == "ByQE")
        {
            this.trByCourse.Visible = false;
            this.trByEmployee.Visible = false;
            this.trByETR.Visible = false;
            this.trByQE0.Visible = true;
            this.trByQE1.Visible = true;
            this.trBySSCE0.Visible = false;
            this.trBySSCE1.Visible = false;
            this.trByTQPR.Visible = false;
        }
        else if (this.ddlReportType.SelectedValue == "ByETR")
        {
            this.trByCourse.Visible = false;
            this.trByEmployee.Visible = false;
            this.trByETR.Visible = true;
            this.trByQE0.Visible = false;
            this.trByQE1.Visible = false;
            this.trBySSCE0.Visible = false;
            this.trBySSCE1.Visible = false;
            this.trByTQPR.Visible = false;
        }
        else if (this.ddlReportType.SelectedValue == "ByTQPR")
        {
            this.trByCourse.Visible = false;
            this.trByEmployee.Visible = false;
            this.trByETR.Visible = false;
            this.trByQE0.Visible = false;
            this.trByQE1.Visible = false;
            this.trBySSCE0.Visible = false;
            this.trBySSCE1.Visible = false;
            this.trByTQPR.Visible = true;
        }
    }
}
