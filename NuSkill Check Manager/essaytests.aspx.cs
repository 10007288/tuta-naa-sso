using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
//using System.Data.SqlClient;

public partial class essaytests : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(9))
            Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
            BindItems();
    }

    protected void gvEssays_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvEssays.PageIndex = e.NewPageIndex;
        this.gvEssays.DataSource = TestTaken.SelectTestTakensWithEssaysByTest(Convert.ToInt32(this.ddlExams.SelectedValue));
        this.gvEssays.DataBind();
    }

    protected void gvEssays_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GridViewRow row = this.gvEssays.Rows[e.NewSelectedIndex];
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblUsername = row.FindControl("lblUsername") as Label;
            if (lblTestTakenID != null)
            {
                TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                if (taken != null)
                {
                    SessionManager.SessionTestTakenID = taken.TestTakenID;
                    SessionManager.SessionExamineeName = lblUsername.Text.Trim();
                    Response.Redirect("~/markessays.aspx?" + Request.QueryString.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "essaytests.aspx", "gvEssays_SelectedIndexChanging", ex.Message);
        }
    }

    protected void BindItems()
    {
        if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(9))
        {
            this.ddlExams.DataSource = TestCategory.SelectPendingEssayExams();
        }
        else
        {
            this.gvEssays.DataSource = null;
        }
        this.ddlExams.DataBind();
        if (this.ddlExams.Items.Count < 1)
        {
            this.ddlExams.Visible = false;
            this.btnExams.Visible = false;
            this.lblFilterByUser.Text = "No pending essays available.";
        }
    }

    protected void lnkGoToBySingleItem_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essays.aspx?" + Request.QueryString.ToString());
    }

    protected void btnExams_Click(object sender, EventArgs e)
    {
        this.gvEssays.DataSource = TestTaken.SelectTestTakensWithEssaysByTest(Convert.ToInt32(this.ddlExams.SelectedValue));
        this.gvEssays.DataBind();

        //this.lblExportCSV.Visible = true;
        //this.btnExportCSV.Visible = true;
    }

    //protected void btnExportCSV_Click(object sender, EventArgs e)
    //{
    //    string constr = ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString;
    //    using (SqlConnection con = new SqlConnection(constr))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("SELECT * FROM Customers"))
    //        {
    //            using (SqlDataAdapter sda = new SqlDataAdapter())
    //            {
    //                cmd.Connection = con;
    //                sda.SelectCommand = cmd;
    //                using (DataTable dt = new DataTable())
    //                {
    //                    sda.Fill(dt);

    //                    //Build the CSV file data as a Comma separated string.
    //                    string csv = string.Empty;

    //                    foreach (DataColumn column in dt.Columns)
    //                    {
    //                        //Add the Header row for CSV file.
    //                        csv += column.ColumnName + ',';
    //                    }

    //                    //Add new line.
    //                    csv += "\r\n";

    //                    foreach (DataRow row in dt.Rows)
    //                    {
    //                        foreach (DataColumn column in dt.Columns)
    //                        {
    //                            //Add the Data rows.
    //                            csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
    //                        }

    //                        //Add new line.
    //                        csv += "\r\n";
    //                    }

    //                    //Download the CSV file.
    //                    Response.Clear();
    //                    Response.Buffer = true;
    //                    Response.AddHeader("content-disposition", "attachment;filename=SqlExport.csv");
    //                    Response.Charset = "";
    //                    Response.ContentType = "application/text";
    //                    Response.Output.Write(csv);
    //                    Response.Flush();
    //                    Response.End();
    //                }
    //            }
    //        }
    //    }
    //}

    protected void btnUserID_Click(object sender, EventArgs e)
    {
        this.gvChecked.DataSource = TestTaken.SelectCheckedcEssays(this.txtUserID.Text.Trim());
        this.gvChecked.DataBind();
    }

    protected void gvChecked_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvChecked.PageIndex = e.NewPageIndex;
        this.gvChecked.DataSource = TestTaken.SelectCheckedcEssays(this.txtUserID.Text.Trim());
        this.gvChecked.DataBind();
    }

    protected void gvChecked_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GridViewRow row = this.gvChecked.Rows[e.NewSelectedIndex];
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblUsername = row.FindControl("lblUsername") as Label;
            if (lblTestTakenID != null)
            {
                TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                if (taken != null)
                {
                    SessionManager.SessionTestTakenID = taken.TestTakenID;
                    SessionManager.SessionExamineeName = lblUsername.Text.Trim();
                    Response.Redirect("~/recheckessays.aspx?" + Request.QueryString.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "essaytests.aspx", "gvChecked_SelectedIndexChanging", ex.Message);
        }
    }
}
