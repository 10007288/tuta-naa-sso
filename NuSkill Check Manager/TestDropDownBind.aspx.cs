using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using System.Collections.Generic;

public partial class TestDropDownBind : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
    }

    [WebMethod]
    public static string[] GetDropDownLists(string prefixText, int count)
    {
        List<string> list = new List<string>();
        TestCategory[] categories = TestCategory.SelectAll(false, false);
        foreach (TestCategory category in categories)
        {
            if (category.TestName.ToLower().StartsWith(prefixText.ToLower()))
                list.Add(category.TestName);
        }
        return list.ToArray();
    }
}
