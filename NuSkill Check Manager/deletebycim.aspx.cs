using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class deletebycim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(19))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    }
    protected void btnFindCim_Click(object sender, EventArgs e)
    {
        this.gvExams.DataSource = TestTaken.SelectByUserPassed(this.txtEnterCim.Text.Trim(), false, false);
        this.gvExams.DataBind();
        this.pnlExams.Visible = true;
    }

    protected void gvExams_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Delete")
            {
                TestTaken taken = TestTaken.Select(Convert.ToInt32(e.CommandArgument));
                if (taken != null)
                    taken.Delete();
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "deletebycim.aspx", "gvExams_RowCommand", ex.Message);
        }
    }
}
