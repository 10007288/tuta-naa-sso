using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class sugestnew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.ddlForTest.DataSource = TestCategory.SelectAll(false, false);
            this.ddlForTest.DataBind();
            this.gvOwnSuggestions.DataSource = Questionnaire.SelectSuggestedUser(SessionManager.SessionUsername);
            this.gvOwnSuggestions.DataBind();
            foreach (GridViewRow row in this.gvOwnSuggestions.Rows)
            {
                Label lblRecommendedQuestionStatusID = row.FindControl("lblRecommendedQuestionStatusID") as Label;
                Label lblApprovedBy = row.FindControl("lblApprovedBy") as Label;
                Label lblApproverComments = row.FindControl("lblApproverComments") as Label;
                Label lblDescription = row.FindControl("lblDescription") as Label;
                LinkButton lnkDelete = row.FindControl("lnkDelete") as LinkButton;
                if (lblRecommendedQuestionStatusID.Text.Trim() == "1")
                {
                    lblDescription.ForeColor = System.Drawing.Color.SteelBlue;
                    lblApprovedBy.Text = Server.HtmlDecode("<br />");
                    lblApproverComments.Text = Server.HtmlDecode("<br />");
                }
                if (lblRecommendedQuestionStatusID.Text.Trim() == "2")
                {
                    lblDescription.ForeColor = System.Drawing.Color.Green;
                    lnkDelete.Text = Server.HtmlDecode("<br />");
                }
                if (lblRecommendedQuestionStatusID.Text.Trim() == "3")
                {
                    lblDescription.ForeColor = System.Drawing.Color.Red;
                    lnkDelete.Text = Server.HtmlDecode("<br />");
                }
            }
        }
    }
    protected void btnCreateItem_Click(object sender, EventArgs e)
    {
        SessionManager.SessionQuestionAction = "suggestquestion";
        SessionManager.SessionRecTestCategoryID = Convert.ToInt32(this.ddlForTest.Text);
        SessionManager.SessionCreatorComments = this.txtComments.Text.Trim();
        Response.Redirect("~/newquestion.aspx?" + Request.QueryString.ToString());
    }

    protected void gvOwnSuggestions_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "CancelQuestion")
        {
            this.lblQuestionnaireID.Text = Convert.ToInt32(e.CommandArgument).ToString();
            this.mpeCancelConfirm.Show();
        }
    }

    protected void gvOwnSuggestions_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvOwnSuggestions.PageIndex = e.NewPageIndex;
        this.gvOwnSuggestions.DataSource = Questionnaire.SelectSuggestedUser(SessionManager.SessionUsername);
        this.gvOwnSuggestions.DataBind();
    }
}
