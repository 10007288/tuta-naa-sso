using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
//using TheLibrary.ErrorLogger;

public partial class newautoexam : System.Web.UI.Page
{

    Questionnaire Question
    {
        get { return this.ViewState["question"] as Questionnaire; }
        set { this.ViewState["question"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            try
            {
                this.ddlExams.DataSource = TestCategory.SelectAll(false, false);
                this.ddlExams.DataBind();
            }
            catch (Exception ex)
            {
                throw;
                //ErrorLogger.Write(Config.ApplicationName, "Quick Items", "CreateExam", ex.Message);
            }
        }
    }
    protected void btnCreateQuestions_Click(object sender, EventArgs e)
    {
        try
        {
            string temp = this.txtExamItems.Text;
            temp = temp.Replace("\r\n", "|");
            string[] qValues = temp.Split('|');
            string modString = string.Empty;
            Questionnaire question = new Questionnaire();
            question.TestCategoryID = Convert.ToInt32(this.ddlExams.SelectedValue);
            question.EssayMaxScore = 0;
            question.HideFromList = false;
            question.LastUpdatedBy = SessionManager.SessionUsername;
            question.TypeCode = "fillblanks";
            foreach (string tempstring in qValues)
            {
                if (tempstring.StartsWith("Question:"))
                {
                    question.Question = tempstring.Replace("Question:", "").Trim();
                }
                else if (tempstring.StartsWith("Answer "))
                {
                    #region question1
                    if (tempstring.StartsWith("Answer 1:"))
                    {
                        question.Choice1 = tempstring.Replace("Answer 1:", "").Trim();
                        if (question.Choice1.EndsWith("**"))
                        {
                            question.Choice1 = question.Choice1.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice1;
                        }
                        else
                            question.Ans1 = string.Empty;
                    }
                    #endregion
                    #region question2
                    if (tempstring.StartsWith("Answer 2:"))
                    {
                        question.Choice2 = tempstring.Replace("Answer 2:", "").Trim();
                        if (question.Choice2.EndsWith("**"))
                        {
                            question.Choice2 = question.Choice2.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice2;
                        }
                        else
                            question.Ans2 = string.Empty;
                    }
                    #endregion
                    #region question3
                    if (tempstring.StartsWith("Answer 3:"))
                    {
                        question.Choice3 = tempstring.Replace("Answer 3:", "").Trim();
                        if (question.Choice3.EndsWith("**"))
                        {
                            question.Choice3 = question.Choice3.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice3;
                        }
                        else
                            question.Ans3 = string.Empty;
                    }
                    #endregion
                    #region question4
                    if (tempstring.StartsWith("Answer 4:"))
                    {
                        question.Choice4 = tempstring.Replace("Answer 4:", "").Trim();
                        if (question.Choice4.EndsWith("**"))
                        {
                            question.Choice4 = question.Choice4.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice4;
                        }
                        else
                            question.Ans4 = string.Empty;
                    }
                    #endregion
                    #region question5
                    if (tempstring.StartsWith("Answer 5:"))
                    {
                        question.Choice5 = tempstring.Replace("Answer 5:", "").Trim();
                        if (question.Choice5.EndsWith("**"))
                        {
                            question.Choice5 = question.Choice5.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice5;
                        }
                        else
                            question.Ans5 = string.Empty;
                    }
                    #endregion
                    #region question6
                    if (tempstring.StartsWith("Answer 6:"))
                    {
                        question.Choice6 = tempstring.Replace("Answer 6:", "").Trim();
                        if (question.Choice6.EndsWith("**"))
                        {
                            question.Choice6.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice6;
                        }
                        else
                            question.Ans6 = string.Empty;
                    }
                    #endregion
                    #region question7
                    if (tempstring.StartsWith("Answer 7:"))
                    {
                        question.Choice7 = tempstring.Replace("Answer 7:", "").Trim();
                        if (question.Choice7.EndsWith("**"))
                        {
                            question.Choice7.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice7;
                        }
                        else
                            question.Ans7 = string.Empty;
                    }
                    #endregion
                    #region question8
                    if (tempstring.StartsWith("Answer 8:"))
                    {
                        question.Choice8 = tempstring.Replace("Answer 8:", "").Trim();
                        if (question.Choice8.EndsWith("**"))
                        {
                            question.Choice8.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice8;
                        }
                        else
                            question.Ans8 = string.Empty;
                    }
                    #endregion
                    #region question9
                    if (tempstring.StartsWith("Answer 9:"))
                    {
                        question.Choice9 = tempstring.Replace("Answer 9:", "").Trim();
                        if (question.Choice9.EndsWith("**"))
                        {
                            question.Choice9.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice9;
                        }
                        else
                            question.Ans9 = string.Empty;
                    }
                    #endregion
                    #region question10
                    if (tempstring.StartsWith("Answer 10:"))
                    {
                        question.Choice10 = tempstring.Replace("Answer 10:", "").Trim();
                        if (question.Choice10.EndsWith("**"))
                        {
                            question.Choice10.Replace(" **", "").Trim();
                            question.Ans1 = question.Choice10;
                        }
                        else
                            question.Ans10 = string.Empty;
                    }
                    #endregion
                }
            }
            this.Question = question;
            this.lblPopupQuestion.Text = this.Question.Question;
            this.lblQuestion1.Text = this.Question.Choice1;
            this.lblQuestion2.Text = this.Question.Choice2;
            this.lblQuestion3.Text = this.Question.Choice3;
            this.lblQuestion4.Text = this.Question.Choice4;
            this.lblQuestion5.Text = this.Question.Choice5;
            this.lblQuestion6.Text = this.Question.Choice6;
            this.lblQuestion7.Text = this.Question.Choice7;
            this.lblQuestion8.Text = this.Question.Choice8;
            this.lblQuestion9.Text = this.Question.Choice9;
            this.lblQuestion10.Text = this.Question.Choice10;

            if (this.Question.Ans1 == this.lblQuestion1.Text)
            {
                this.imgCorrect1.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect1.Visible = true;
            }
            else
                this.imgCorrect1.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion2.Text)
            {
                this.imgCorrect2.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect2.Visible = true;
            }
            else
                this.imgCorrect2.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion3.Text)
            {
                this.imgCorrect3.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect3.Visible = true;
            }
            else
                this.imgCorrect3.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion4.Text)
            {
                this.imgCorrect4.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect4.Visible = true;
            }
            else
                this.imgCorrect4.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion5.Text)
            {
                this.imgCorrect5.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect5.Visible = true;
            }
            else
                this.imgCorrect5.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion6.Text)
            {
                this.imgCorrect6.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect6.Visible = true;
            }
            else
                this.imgCorrect6.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion7.Text)
            {
                this.imgCorrect7.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect7.Visible = true;
            }
            else
                this.imgCorrect7.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion8.Text)
            {
                this.imgCorrect8.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect8.Visible = true;
            }
            else
                this.imgCorrect8.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion9.Text)
            {
                this.imgCorrect9.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect9.Visible = true;
            }
            else
                this.imgCorrect9.Visible = false;

            if (this.Question.Ans1 == this.lblQuestion10.Text)
            {
                this.imgCorrect10.ImageUrl = "~/images/icon-pass.gif";
                this.imgCorrect10.Visible = true;
            }
            else
                this.imgCorrect10.Visible = false;
            this.mpeQuestionPopup.Show();
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "newautoexam.aspx", "btnCreateQuestions_Click", ex.Message);
        }
    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        try
        {
            this.Question.Insert();
            this.lblResult.Text = "Add item succesful.";
            this.mpeResult.Show();
            this.txtExamItems.Text = string.Empty;
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "newautoexam.aspx", "btnCreate_Click", ex.Message);
            this.lblResult.Text = "Add item has failed. Please check that the format is correct.";
            this.mpeResult.Show();
        }
    }
}
