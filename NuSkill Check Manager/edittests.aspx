<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="edittests.aspx.cs" Inherits="edittests"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Font-Bold="true"
        ForeColor="black" Font-Size="12px">
        <table width="300px" style="font-family: Arial; text-align: left">
            <tr>
                <td>
                    <asp:Label ID="lblYear" runat="server" Text="Year" />
                </td>
                <td>
                    <asp:TextBox ID="txtYear" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCIMNo" runat="server" Text="CIM" />
                </td>
                <td>
                    <asp:TextBox ID="txtCIMNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblQuarter" runat="server" Text="Date Range" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlQuarter" runat="server">
                        <asp:ListItem Text="1st Quarter" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2nd Quarter" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3rd Quarter" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4th Quarter" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlResults" runat="server" HorizontalAlign="center" Font-Bold="true"
        ForeColor="black" Font-Size="12px">
        <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="false" EmptyDataText="There are no tests for the given parameters."
            CellPadding="5" BorderColor="black">
            <Columns>
                <asp:TemplateField HeaderText="CIM">
                    <ItemTemplate>
                        <asp:Label ID="lblCIM" runat="server" Text='<%#Bind("CIM") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Taken">
                    <ItemTemplate>
                        <asp:Label ID="lblDateTaken" runat="server" Text='<%#Bind("DateStartTaken", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Test ID">
                    <ItemTemplate>
                        <asp:Label ID="lblTestID" runat="server" Text='<%#Bind("TestCategoryID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Test Name">
                    <ItemTemplate>
                        <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Answers (Correct/Total)">
                    <ItemTemplate>
                        <asp:Label ID="lblAnswers" runat="server" Text="" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Passed">
                    <ItemTemplate>
                        <asp:Label ID="lblPassFail" runat="server" Text='<%#Bind("Passed") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%#Bind("TestTakenID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
