<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="massusercreation.aspx.cs" Inherits="massusercreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="4" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMassUserCreation" runat="server" Text="Mass User Creation" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="lblInstructions" runat="server" Font-Size="10px" Text="Enter a batch name, a username prefix and/or a username suffix, and starting and ending numbers for increment names (Maximum total length of 20 characters)." />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="40%">
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:ValidationSummary ID="vsmMass" runat="server" ValidationGroup="mass" DisplayMode="List" />
                                            <asp:CustomValidator ID="cvBatchName" runat="server" ErrorMessage="Enter a batch name."
                                                Display="none" ValidationGroup="mass" />
                                            <asp:CustomValidator ID="cvPrefixSuffix" runat="server" ErrorMessage="Enter a prefix and/or suffix."
                                                Display="none" ValidationGroup="mass" />
                                            <asp:CustomValidator ID="cvNoValue" runat="server" ErrorMessage="Enter a starting seed and an ending seed."
                                                Display="none" ValidationGroup="mass" />
                                            <asp:CustomValidator ID="cvOverload" runat="server" ErrorMessage="Total length must not exceed 20 characters."
                                                Display="none" ValidationGroup="mass" />
                                            <asp:CustomValidator ID="cvOverflow" runat="server" ErrorMessage="Ending Seed must not be less than the Starting Seed."
                                                Display="none" ValidationGroup="mass" />
                                            <asp:CustomValidator ID="cvBatchNameExists" runat="server" ErrorMessage="BatchName exists."
                                                Display="none" ValidationGroup="mass" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblBatchName" runat="server" Text="Batch Name:" />
                                        </td>
                                        <td align="right">
                                            <asp:TextBox ID="txtBatchName" runat="server" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%" align="left">
                                            <asp:Label ID="lblPrefix" runat="server" Text="Username Prefix:" />
                                        </td>
                                        <td style="width: 50%" align="right">
                                            <asp:TextBox ID="txtPrefix" runat="server" MaxLength="20" />
                                            <ajax:FilteredTextBoxExtender ID="ftbPrefix" runat="server" FilterType="LowercaseLetters, UppercaseLetters"
                                                TargetControlID="txtPrefix" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblStarting" runat="server" Text="Starting Seed:" />
                                        </td>
                                        <td align="right">
                                            <asp:TextBox ID="txtStarting" runat="server" MaxLength="3" />
                                            <ajax:FilteredTextBoxExtender ID="ftbStarting" runat="server" FilterType="Numbers"
                                                TargetControlID="txtStarting" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblEnding" runat="server" Text="Ending Seed:" />
                                        </td>
                                        <td align="right">
                                            <asp:TextBox ID="txtEnding" runat="server" MaxLength="3" />
                                            <ajax:FilteredTextBoxExtender ID="ftbEnding" runat="server" FilterType="Numbers"
                                                TargetControlID="txtEnding" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblSuffix" runat="server" Text="Username Suffix:" />
                                            <ajax:FilteredTextBoxExtender ID="ftbSuffix" runat="server" FilterType="LowercaseLetters, UppercaseLetters"
                                                TargetControlID="txtSuffix" />
                                        </td>
                                        <td align="right">
                                            <asp:TextBox ID="txtSuffix" runat="server" MaxLength="20" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <asp:Button ID="btnRight" runat="server" Text="Preview" CssClass="buttons" OnClick="btnRight_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpePreview" runat="server" TargetControlID="hidPreview"
        PopupControlID="pnlPreview" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidPreview" runat="server" />
    <asp:Panel ID="pnlPreview" runat="server" CssClass="modalPopup" Width="800px">
        <table width="100%">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblPreview" runat="server" Text="Preview" ForeColor="black" Font-Size="12"
                        Font-Bold="true" /><br />
                    <hr style="color: Black; width: 100%" />
                    <br />
                    <asp:Label ID="lblPreviewNote" runat="server" Font-Size="12px" Text="Usernames in red belong to another batch. Continuing will not generate red usernames for this batch." />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Panel ID="pnlUsers" runat="server" ScrollBars="Vertical" Height="400px">
                        <asp:DataList ID="gvUsers" runat="server" Width="95%" CellPadding="5" RepeatColumns="3"
                            BorderColor="black" BorderWidth="1px" GridLines="both" RepeatDirection="horizontal">
                            <ItemStyle BackColor="white" HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblUsername" runat="server" Text='<%#Bind("AutoUserID") %>' />
                            </ItemTemplate>
                        </asp:DataList>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Button ID="btnContinue" runat="server" Text="Create" CssClass="buttons" OnClick="btnContinue_Click" />
                </td>
                <td align="right">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeResult" runat="server" TargetControlID="hidResult" PopupControlID="pnlResult" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidResult" runat="server" />
    <asp:Panel ID="pnlResult" runat="server" CssClass="modalPopup" Width="400px" HorizontalAlign="center">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblResult" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnConfirmResult" runat="server" Text="OK" CssClass="buttons" OnClick="btnConfirmResult_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
