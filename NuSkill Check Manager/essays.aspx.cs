using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class essays : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(9))
            Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
            BindItems();
    }

    protected void gvEssays_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvEssays.PageIndex = e.NewPageIndex;
        BindItems();
    }

    protected void gvEssays_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridViewRow row = this.gvEssays.Rows[e.NewSelectedIndex];
        Label lblSaveTestResponseID = row.FindControl("lblSaveTestResponseID") as Label;
        if (lblSaveTestResponseID != null)
        {
            SaveTestResponse response = SaveTestResponse.GetByID(Convert.ToInt32(lblSaveTestResponseID.Text.Trim()));
            if (response != null)
            {
                SessionManager.SessionSingleSaveTestResponse = response;
                Response.Redirect("~/markessay.aspx?" + Request.QueryString.ToString());
            }
        }
    }

    protected void BindItems()
    {
        this.gvEssays.DataSource = SaveTestResponse.GetEssays();
        this.gvEssays.DataBind();
    }
    protected void lnkGoToByTestTaken_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essaytests.aspx?" + Request.QueryString.ToString());
    }
}
