﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master" AutoEventWireup="true" CodeFile="ExternalLandingPage.aspx.cs" Inherits="ExternalLandingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" Runat="Server">
    <div style="height: 300px;">
        <hgroup class="title">
            <h1 style="background-color: White;">Register with your Google account</h1>
            <h2 style="background-color: White;"><asp:Label ID="lblEmail" runat="server"></asp:Label><asp:Label ID="lblGoogleName" visible="false" runat="server"></asp:Label><asp:Label ID="lblProfileImage" visible="false" runat="server"></asp:Label></h2>
        </hgroup>
        <p>
            You've authenticated with <strong>Google</strong> as <strong class="primary">
                <asp:Label ID="lblUsername" runat="server"></asp:Label></strong>. Please confirm
            below by clicking the Login In button.
        </p>
        <asp:Button ID="Button1" runat="server" Text="Log in" CssClass="btn btn-teal btn-md" ValidationGroup="NewUser" OnClick="logIn_Click" />
        <asp:Button ID="Button2" runat="server" Text="Cancel" CssClass="btn btn-teal btn-md" CausesValidation="false" OnClick="cancel_Click" />
        <asp:Label ID="ModelErrorMessage1" runat="server" />
    </div>
</asp:Content>

