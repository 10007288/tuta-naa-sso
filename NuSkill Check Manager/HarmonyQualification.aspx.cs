using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class HarmonyQualification : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            this.ddlTestcategoryID.DataSource = TestCategory.SelectAll(false, false);
            this.ddlTestcategoryID.DataBind();
            this.gvHarmonyQualifications.DataSource = TestHarmonyQualification.SelectAll();
            this.gvHarmonyQualifications.DataBind();
        }
    }
}
