using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using AjaxControlToolkit;

public partial class viewsuggestions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(29))
            Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            this.PopulateSuggestedItems();
        }
    }

    protected void PopulateSuggestedItems()
    {
        this.gvSuggestedQuestions.DataSource = Questionnaire.SelectSuggested();
        this.gvSuggestedQuestions.DataBind();
        foreach (GridViewRow row in this.gvSuggestedQuestions.Rows)
        {
            Panel pnlMultipleChoice = row.FindControl("pnlMultipleChoice") as Panel;
            Panel pnlTrueOrFalse = row.FindControl("pnlTrueOrFalse") as Panel;
            Panel pnlSequencing = row.FindControl("pnlSequencing") as Panel;
            Panel pnlFillInTheBlanks = row.FindControl("pnlFillInTheBlanks") as Panel;
            Panel pnlHotspot = row.FindControl("pnlHotspot") as Panel;
            Panel pnlYesOrNo = row.FindControl("pnlYesOrNo") as Panel;
            Label lblTypeCode = row.FindControl("lblTypeCode") as Label;
            Label lblCorrectAnswerVal = row.FindControl("lblCorrectAnswerVal") as Label;
            Label lblAns1 = row.FindControl("lblAns1") as Label;
            Label lblAns2 = row.FindControl("lblAns2") as Label;
            Label lblAns3 = row.FindControl("lblAns3") as Label;
            Label lblAns4 = row.FindControl("lblAns4") as Label;
            Label lblAns5 = row.FindControl("lblAns5") as Label;
            Label lblAns6 = row.FindControl("lblAns6") as Label;
            Label lblAns7 = row.FindControl("lblAns7") as Label;
            Label lblAns8 = row.FindControl("lblAns8") as Label;
            Label lblAns9 = row.FindControl("lblAns9") as Label;
            Label lblAns10 = row.FindControl("lblAns10") as Label;
            ListBox lstCorrectAnswers = row.FindControl("lstCorrectAnswers") as ListBox;
            Label lblCreatorCommentsVal = row.FindControl("lblCreatorCommentsVal") as Label;
            Label lblHyperlink = row.FindControl("lblHyperlink") as Label;
            Label lblHyperlinkValue = row.FindControl("lblHyperlinkValue") as Label;
            if (string.IsNullOrEmpty(lblHyperlink.Text.Trim()))
                lblHyperlinkValue.Text = "<b>Hyperlink:</b> None.";
            else
                lblHyperlinkValue.Text = "<b>Hyperlink:</b> " + lblHyperlink.Text;

            if (string.IsNullOrEmpty(lblCreatorCommentsVal.Text.Trim()))
                lblCreatorCommentsVal.Text = "N/A";
            if (!string.IsNullOrEmpty(lblTypeCode.Text.Trim()))
            {
                if (lblTypeCode.Text.Trim() == "multiple")
                {
                    Label lblChoice1 = row.FindControl("lblChoice1") as Label;
                    Label lblChoice2 = row.FindControl("lblChoice2") as Label;
                    Label lblChoice3 = row.FindControl("lblChoice3") as Label;
                    Label lblChoice4 = row.FindControl("lblChoice4") as Label;
                    Label lblChoice5 = row.FindControl("lblChoice5") as Label;
                    Label lblChoice6 = row.FindControl("lblChoice6") as Label;
                    Label lblChoice7 = row.FindControl("lblChoice7") as Label;
                    Label lblChoice8 = row.FindControl("lblChoice8") as Label;
                    Label lblChoice9 = row.FindControl("lblChoice9") as Label;
                    Label lblChoice10 = row.FindControl("lblChoice10") as Label;
                    HtmlTableRow trCheck1 = row.FindControl("trCheck1") as HtmlTableRow;
                    HtmlTableRow trCheck2 = row.FindControl("trCheck2") as HtmlTableRow;
                    HtmlTableRow trCheck3 = row.FindControl("trCheck3") as HtmlTableRow;
                    HtmlTableRow trCheck4 = row.FindControl("trCheck4") as HtmlTableRow;
                    HtmlTableRow trCheck5 = row.FindControl("trCheck5") as HtmlTableRow;
                    HtmlTableRow trCheck6 = row.FindControl("trCheck6") as HtmlTableRow;
                    HtmlTableRow trCheck7 = row.FindControl("trCheck7") as HtmlTableRow;
                    HtmlTableRow trCheck8 = row.FindControl("trCheck8") as HtmlTableRow;
                    HtmlTableRow trCheck9 = row.FindControl("trCheck9") as HtmlTableRow;
                    HtmlTableRow trCheck10 = row.FindControl("trCheck10") as HtmlTableRow;
                    pnlMultipleChoice.Visible = true;
                    lblCorrectAnswerVal.Visible = true;
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice1.Text.Trim())
                        trCheck1.BgColor = "lightgreen";
                        //lblCorrectAnswerVal.Text = lblAns1.Text.Trim() + ",";
                    if (!string.IsNullOrEmpty(lblAns2.Text.Trim()) && lblAns2.Text.Trim() == lblChoice2.Text.Trim())
                        trCheck2.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns3.Text.Trim()) && lblAns3.Text.Trim() == lblChoice3.Text.Trim())
                        trCheck3.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns4.Text.Trim()) && lblAns4.Text.Trim() == lblChoice4.Text.Trim())
                        trCheck4.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns5.Text.Trim()) && lblAns5.Text.Trim() == lblChoice5.Text.Trim())
                        trCheck5.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns6.Text.Trim()) && lblAns6.Text.Trim() == lblChoice6.Text.Trim())
                        trCheck6.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns7.Text.Trim()) && lblAns7.Text.Trim() == lblChoice7.Text.Trim())
                        trCheck7.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns8.Text.Trim()) && lblAns8.Text.Trim() == lblChoice8.Text.Trim())
                        trCheck8.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns9.Text.Trim()) && lblAns9.Text.Trim() == lblChoice9.Text.Trim())
                        trCheck9.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns10.Text.Trim()) && lblAns10.Text.Trim() == lblChoice10.Text.Trim())
                        trCheck10.BgColor = "lightgreen";
                    lblCorrectAnswerVal.Text = lblCorrectAnswerVal.Text.TrimEnd(',');
                }
                else if (lblTypeCode.Text.Trim() == "torf")
                {
                    HtmlTableRow trTrue = row.FindControl("trTrue") as HtmlTableRow;
                    HtmlTableRow trFalse = row.FindControl("trFalse") as HtmlTableRow;
                    Label rdoTrue = row.FindControl("rdoTrue") as Label;
                    Label rdoFalse = row.FindControl("rdoFalse") as Label;
                    pnlTrueOrFalse.Visible = true;
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == rdoTrue.Text.Trim())
                        trTrue.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == rdoFalse.Text.Trim())
                        trFalse.BgColor = "lightgreen";

                }
                else if (lblTypeCode.Text.Trim() == "essay")
                {
                    
                }
                else if (lblTypeCode.Text.Trim() == "fillblanks")
                {
                    Label lblChoice1 = row.FindControl("lblChoice1") as Label;
                    Label lblChoice2 = row.FindControl("lblChoice2") as Label;
                    Label lblChoice3 = row.FindControl("lblChoice3") as Label;
                    Label lblChoice4 = row.FindControl("lblChoice4") as Label;
                    Label lblChoice5 = row.FindControl("lblChoice5") as Label;
                    Label lblChoice6 = row.FindControl("lblChoice6") as Label;
                    Label lblChoice7 = row.FindControl("lblChoice7") as Label;
                    Label lblChoice8 = row.FindControl("lblChoice8") as Label;
                    Label lblChoice9 = row.FindControl("lblChoice9") as Label;
                    Label lblChoice10 = row.FindControl("lblChoice10") as Label;
                    HtmlTableRow trCheck1 = row.FindControl("trCheck1") as HtmlTableRow;
                    HtmlTableRow trCheck2 = row.FindControl("trCheck2") as HtmlTableRow;
                    HtmlTableRow trCheck3 = row.FindControl("trCheck3") as HtmlTableRow;
                    HtmlTableRow trCheck4 = row.FindControl("trCheck4") as HtmlTableRow;
                    HtmlTableRow trCheck5 = row.FindControl("trCheck5") as HtmlTableRow;
                    HtmlTableRow trCheck6 = row.FindControl("trCheck6") as HtmlTableRow;
                    HtmlTableRow trCheck7 = row.FindControl("trCheck7") as HtmlTableRow;
                    HtmlTableRow trCheck8 = row.FindControl("trCheck8") as HtmlTableRow;
                    HtmlTableRow trCheck9 = row.FindControl("trCheck9") as HtmlTableRow;
                    HtmlTableRow trCheck10 = row.FindControl("trCheck10") as HtmlTableRow;
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice1.Text.Trim())
                        trCheck1.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice2.Text.Trim())
                        trCheck2.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice3.Text.Trim())
                        trCheck3.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice4.Text.Trim())
                        trCheck4.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice5.Text.Trim())
                        trCheck5.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice6.Text.Trim())
                        trCheck6.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice7.Text.Trim())
                        trCheck7.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice8.Text.Trim())
                        trCheck8.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice9.Text.Trim())
                        trCheck9.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == lblChoice10.Text.Trim())
                        trCheck10.BgColor = "lightgreen";
                    pnlMultipleChoice.Visible = true;
                    pnlFillInTheBlanks.Visible = true;
                }
                else if (lblTypeCode.Text.Trim() == "hotspot")
                {
                    HtmlTableCell imgCell1 = row.FindControl("imgCell1") as HtmlTableCell;
                    HtmlTableCell imgCell2 = row.FindControl("imgCell2") as HtmlTableCell;
                    HtmlTableCell imgCell3 = row.FindControl("imgCell3") as HtmlTableCell;
                    HtmlTableCell imgCell4 = row.FindControl("imgCell4") as HtmlTableCell;
                    HtmlTableCell imgCell5 = row.FindControl("imgCell5") as HtmlTableCell;
                    HtmlTableCell imgCell6 = row.FindControl("imgCell6") as HtmlTableCell;
                    HtmlTableCell imgCell7 = row.FindControl("imgCell7") as HtmlTableCell;
                    HtmlTableCell imgCell8 = row.FindControl("imgCell8") as HtmlTableCell;
                    HtmlTableCell imgCell9 = row.FindControl("imgCell9") as HtmlTableCell;
                    HtmlTableCell imgCell10 = row.FindControl("imgCell10") as HtmlTableCell;
                    Image imgChoice1 = row.FindControl("imgChoice1") as Image;
                    Image imgChoice2 = row.FindControl("imgChoice2") as Image;
                    Image imgChoice3 = row.FindControl("imgChoice3") as Image;
                    Image imgChoice4 = row.FindControl("imgChoice4") as Image;
                    Image imgChoice5 = row.FindControl("imgChoice5") as Image;
                    Image imgChoice6 = row.FindControl("imgChoice6") as Image;
                    Image imgChoice7 = row.FindControl("imgChoice7") as Image;
                    Image imgChoice8 = row.FindControl("imgChoice8") as Image;
                    Image imgChoice9 = row.FindControl("imgChoice9") as Image;
                    Image imgChoice10 = row.FindControl("imgChoice10") as Image;
                    Image imgCorrectAnswer1 = row.FindControl("imgCorrectAnswer1") as Image;
                    Image imgCorrectAnswer2 = row.FindControl("imgCorrectAnswer2") as Image;
                    Image imgCorrectAnswer3 = row.FindControl("imgCorrectAnswer3") as Image;
                    Image imgCorrectAnswer4 = row.FindControl("imgCorrectAnswer4") as Image;
                    Image imgCorrectAnswer5 = row.FindControl("imgCorrectAnswer5") as Image;
                    Image imgCorrectAnswer6 = row.FindControl("imgCorrectAnswer6") as Image;
                    Image imgCorrectAnswer7 = row.FindControl("imgCorrectAnswer7") as Image;
                    Image imgCorrectAnswer8 = row.FindControl("imgCorrectAnswer8") as Image;
                    Image imgCorrectAnswer9 = row.FindControl("imgCorrectAnswer9") as Image;
                    Image imgCorrectAnswer10 = row.FindControl("imgCorrectAnswer10") as Image;
                    pnlHotspot.Visible = true;
                    if (imgChoice1.ImageUrl == lblAns1.Text.Trim())
                        imgCell1.BgColor = "lightgreen";
                    if (imgChoice2.ImageUrl == lblAns2.Text.Trim())
                        imgCell2.BgColor = "lightgreen";
                    if (imgChoice3.ImageUrl == lblAns3.Text.Trim())
                        imgCell3.BgColor = "lightgreen";
                    if (imgChoice4.ImageUrl == lblAns4.Text.Trim())
                        imgCell4.BgColor = "lightgreen";
                    if (imgChoice5.ImageUrl == lblAns5.Text.Trim())
                        imgCell5.BgColor = "lightgreen";
                    if (imgChoice6.ImageUrl == lblAns6.Text.Trim())
                        imgCell6.BgColor = "lightgreen";
                    if (imgChoice7.ImageUrl == lblAns7.Text.Trim())
                        imgCell7.BgColor = "lightgreen";
                    if (imgChoice8.ImageUrl == lblAns8.Text.Trim())
                        imgCell8.BgColor = "lightgreen";
                    if (imgChoice9.ImageUrl == lblAns9.Text.Trim())
                        imgCell9.BgColor = "lightgreen";
                    if (imgChoice10.ImageUrl == lblAns10.Text.Trim())
                        imgCell10.BgColor = "lightgreen";
                }
                else if (lblTypeCode.Text.Trim() == "matching")
                {
                    Label lblCorrectChoice1 = row.FindControl("lblCorrectChoice1") as Label;
                    Label lblCorrectChoice2 = row.FindControl("lblCorrectChoice2") as Label;
                    Label lblCorrectChoice3 = row.FindControl("lblCorrectChoice3") as Label;
                    Label lblCorrectChoice4 = row.FindControl("lblCorrectChoice4") as Label;
                    Label lblCorrectChoice5 = row.FindControl("lblCorrectChoice5") as Label;
                    Label lblCorrectChoice6 = row.FindControl("lblCorrectChoice6") as Label;
                    Label lblCorrectChoice7 = row.FindControl("lblCorrectChoice7") as Label;
                    Label lblCorrectChoice8 = row.FindControl("lblCorrectChoice8") as Label;
                    Label lblCorrectChoice9 = row.FindControl("lblCorrectChoice9") as Label;
                    Label lblCorrectChoice10 = row.FindControl("lblCorrectChoice10") as Label;
                    Label lblMatchChoice1 = row.FindControl("lblMatchChoice1") as Label;
                    Label lblMatchChoice2 = row.FindControl("lblMatchChoice2") as Label;
                    Label lblMatchChoice3 = row.FindControl("lblMatchChoice3") as Label;
                    Label lblMatchChoice4 = row.FindControl("lblMatchChoice4") as Label;
                    Label lblMatchChoice5 = row.FindControl("lblMatchChoice5") as Label;
                    Label lblMatchChoice6 = row.FindControl("lblMatchChoice6") as Label;
                    Label lblMatchChoice7 = row.FindControl("lblMatchChoice7") as Label;
                    Label lblMatchChoice8 = row.FindControl("lblMatchChoice8") as Label;
                    Label lblMatchChoice9 = row.FindControl("lblMatchChoice9") as Label;
                    Label lblMatchChoice10 = row.FindControl("lblMatchChoice10") as Label;
                }
                else if (lblTypeCode.Text.Trim() == "sequencing")
                {
                    pnlSequencing.Visible = true;
                }
                else if (lblTypeCode.Text.Trim() == "yorn")
                {
                    HtmlTableRow trYes = row.FindControl("trYes") as HtmlTableRow;
                    HtmlTableRow trNo = row.FindControl("trNo") as HtmlTableRow;
                    Label lblYes = row.FindControl("lblYes") as Label;
                    Label lblNo = row.FindControl("lblNo") as Label;
                    pnlYesOrNo.Visible = true;
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == "True")
                        trYes.BgColor = "lightgreen";
                    if (!string.IsNullOrEmpty(lblAns1.Text.Trim()) && lblAns1.Text.Trim() == "False")
                        trNo.BgColor = "lightgreen";
                }
            }
        }
    }

    protected void gvSuggestedQuestions_RowCommand(object sender, CommandEventArgs e)
    {
        int x = Convert.ToInt32(e.CommandArgument.ToString());
        TextBox txtComments = gvSuggestedQuestions.Rows[x].FindControl("txtComments") as TextBox;
        CustomValidator cvNoComments = gvSuggestedQuestions.Rows[x].FindControl("cvNoComments") as CustomValidator;
        Label lblQuestionnaireID = gvSuggestedQuestions.Rows[x].FindControl("lblQuestionnaireID") as Label;
        if (e.CommandName == "Approve")
        {
            Questionnaire question = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), false);
            if (question != null)
            {
                RecommendedQuestion recQuestion = RecommendedQuestion.SelectByQuestionnaireID(question.QuestionnaireID);
                if (recQuestion != null)
                {
                    question.TestCategoryID = recQuestion.TestCategoryID;
                    question.Update();
                    recQuestion.RecommendedQuestionStatusID = 2;
                    recQuestion.ApprovedBy = SessionManager.SessionUsername;
                    recQuestion.ApproverComments = txtComments.Text.Trim();
                    recQuestion.Update();
                }
            }
        }
        else if (e.CommandName == "Disapprove")
        {
            if (string.IsNullOrEmpty(txtComments.Text.Trim()))
            {
                cvNoComments.IsValid = false;
                return;
            }
            RecommendedQuestion recQuestion = RecommendedQuestion.SelectByQuestionnaireID(Convert.ToInt32(lblQuestionnaireID.Text.Trim()));
            if (recQuestion != null)
            {
                recQuestion.RecommendedQuestionStatusID = 3;
                recQuestion.ApprovedBy = SessionManager.SessionUsername;
                recQuestion.ApproverComments = txtComments.Text.Trim();
                recQuestion.Update();
            }
        }
        this.PopulateSuggestedItems();
    }

    protected void gvSuggestedQuestions_PageIndexChangeing(object sender, GridViewPageEventArgs e)
    {
        this.gvSuggestedQuestions.PageIndex = e.NewPageIndex;
        this.PopulateSuggestedItems();
    }
}
