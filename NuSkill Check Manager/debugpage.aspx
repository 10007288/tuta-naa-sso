<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site_noajax.master"
    AutoEventWireup="true" CodeFile="debugpage.aspx.cs" Inherits="debugpage" Title="Transcom University Testing Admin" %>

<%@ Register TagPrefix="wus" TagName="editexamitemcontrol" Src="~/controls/editexamquestioncontrol.ascx" %>
<%@ Register TagPrefix="wus" TagName="editexamcontrol" Src="~/controls/editexamcontrol.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server" EnableViewState="true">
    <wus:editexamcontrol ID="wusEditQuestion" runat="server" EnableViewState="true" />
</asp:Content>
