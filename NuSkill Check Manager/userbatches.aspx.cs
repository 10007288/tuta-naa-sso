using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class userbatches : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(31))
                Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
            this.ddlUserBatches.DataSource = UserBatch.SelectAll();
            this.ddlUserBatches.DataBind();
        }
    }
    protected void btnUserBatches_Click(object sender, EventArgs e)
    {
        Registration[] registrations = Registration.SelectByBatch(Convert.ToInt32(this.ddlUserBatches.Text.Trim()));
        this.dtlUserBatches.DataSource = registrations;
        this.dtlUserBatches.DataBind();
        if (registrations.Length < 1)
            this.lblUserBatchesNull.Visible = true;
        else
            this.lblUserBatchesNull.Visible = false;
    }
}
