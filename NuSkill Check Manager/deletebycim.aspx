<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="deletebycim.aspx.cs" Inherits="deletebycim"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Width="100%" CssClass="panelStandard">
        <table id="tblSearch" runat="server">
            <tr>
                <td>
                    <asp:Label ID="lblEnterCim" runat="server" Text="Enter CIM Number:" />
                </td>
                <td>
                    <asp:TextBox ID="txtEnterCim" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnFindCim" runat="server" Text="Submit" CssClass="buttons" OnClick="btnFindCim_Click" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlExams" runat="server" HorizontalAlign="center" Visible="false">
            <asp:GridView ID="gvExams" runat="server" AutoGenerateColumns="false" Font-Size="12px"
                PageSize="10" CellPadding="5" BorderColor="black" EmptyDataText="This user has no failed exams."
                OnRowCommand="gvExams_RowCommand">
                <HeaderStyle HorizontalAlign="center" />
                <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                    NextPageText=">" Position="Bottom" PreviousPageText="<" />
                <RowStyle BorderColor="black" />
                <Columns>
                    <asp:TemplateField HeaderText="Test Name">
                        <ItemTemplate>
                            <asp:Label ID="lblTestID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("testName") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Started">
                        <ItemTemplate>
                            <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Finished">
                        <ItemTemplate>
                            <asp:Label ID="lblDateEndTaken" runat="server" Text='<%#Bind("DateEndTaken") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Score (Passing)">
                        <ItemTemplate>
                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Delete"
                                CommandArgument='<%#Bind("TestTakenID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </asp:Panel>
<%--    <ajax:ModalPopupExtender ID="mpeDelete" runat="server" TargetControlID="hidDelete" PopupControlID="pnlDelete" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidDelete" runat="server" />
    <asp:Panel ID="pnlDelete" runat="server" HorizontalAlign="center" CssClass="modalPopup">
        <table cellspacing="5" cellpadding="5">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblConfirmDelete" runat="server" Text="Confirm Deletion." />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Button ID="btnConfirmDeleteOk" runat="server" Text="OK" CssClass="buttons" />
                </td>
                <td align="right">
                    <asp:Button ID="btnConfirmDeleteCancel" runat="server" Text="Cancel" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>--%>
</asp:Content>
