using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class markessays : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/default.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            TestTaken test = TestTaken.Select(SessionManager.SessionTestTakenID);
            if (test != null)
            {
                TestCategory category = TestCategory.Select(test.TestCategoryID, true);
                if (category != null)
                {
                    this.lblUserIDVal.Text = SessionManager.SessionExamineeName;
                    this.lblExamIDVal.Text = category.TestName;
                    DataSet responses = SaveTestResponse.GetMultipleEssays(SessionManager.SessionTestTakenID);
                    this.gvEssays.DataSource = responses;
                    this.gvEssays.DataBind();
                }
            }
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(10))
            {
                this.pnlGrade.Visible = true;
                //foreach (GridViewRow row in this.gvEssays.Rows)
                //{
                //    Panel pnlScoreData = row.FindControl("pnlScoreData") as Panel;
                //    if (pnlScoreData != null)
                //        pnlScoreData.Visible = true;
                //}
            }
        }
    }
    protected void btnGrade_Click(object sender, EventArgs e)
    {
        bool hasError = false;

        foreach (GridViewRow row in this.gvEssays.Rows)
        {
            int score = 0;
            TextBox txtGradeVal = row.FindControl("txtGradeVal") as TextBox;
            Label lblMaxScore = row.FindControl("lblMaxScore") as Label;
            CustomValidator cvScoreOverflow = row.FindControl("cvScoreOverflow") as CustomValidator;
            if (!int.TryParse(txtGradeVal.Text.Trim(), out score))
            {
                cvScoreOverflow.IsValid = false;
                hasError = true;
            }
            if (score > Convert.ToInt32(lblMaxScore.Text.Trim()))
            {
                cvScoreOverflow.IsValid = false;
                hasError = true;
            }
        }
        if (hasError)
            return;
        foreach (GridViewRow row in this.gvEssays.Rows)
        {
            TextBox txtGradeVal = row.FindControl("txtGradeVal") as TextBox;
            TextBox txtAnswerVal = row.FindControl("txtAnswerVal") as TextBox;
            Label lblMaxScore = row.FindControl("lblMaxScore") as Label;
            Label lblQuestionnaireID = row.FindControl("lblQuestionnaireIDVal") as Label;
            Label lblSaveTestResponseID = row.FindControl("lblSaveTestResponseIDVal") as Label;
            CustomValidator cvScoreOverflow = row.FindControl("cvScoreOverflow") as CustomValidator;
            CustomValidator cvAlreadyScored = row.FindControl("cvAlreadyScored") as CustomValidator;

            Questionnaire questionnaire = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), true);
            SaveTestResponse response = SaveTestResponse.GetByID(Convert.ToInt32(lblSaveTestResponseID.Text.Trim()));
            TestCategory category = TestCategory.Select(questionnaire.TestCategoryID, true);


            int score = 0;

            try
            {
                int temp = 0;
                if (int.TryParse(txtGradeVal.Text.Trim(), out temp))
                {
                    if (temp > Convert.ToInt32(lblMaxScore.Text.Trim()))
                    {
                        cvScoreOverflow.IsValid = false;
                        hasError = true;
                    }
                    if (response != null)
                    {
                        if (response.EssayResponse == txtAnswerVal.Text.Trim())
                        {
                            TestResponse existing = TestResponse.Select(response.TestTakenID, response.QuestionnaireID, response.UserID);
                            if (existing != null)
                            {
                                cvAlreadyScored.IsValid = false;
                                hasError = true;
                            }

                        }
                        else
                        {
                            cvAlreadyScored.IsValid = false;
                            hasError = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(Config.ApplicationID(), "markessays.aspx", "btnGrade_Click", ex.Message);
            }
        }
        if (hasError)
            return;
        else
        {
            foreach (GridViewRow row in this.gvEssays.Rows)
            {
                Label lblSaveTestResponseID = row.FindControl("lblSaveTestResponseIDVal") as Label;
                Label lblQuestionnaireID = row.FindControl("lblQuestionnaireIDVal") as Label;
                TextBox txtAnswerVal = row.FindControl("txtAnswerVal") as TextBox;
                TextBox txtGradeVal = row.FindControl("txtGradeVal") as TextBox;
                TextBox txtCheckerComments = row.FindControl("txtCheckerComments") as TextBox;
                Label lblMaxScore = row.FindControl("lblMaxScore") as Label;
                CustomValidator cvScoreOverflow = row.FindControl("cvScoreOverflow") as CustomValidator;
                CustomValidator cvAlreadyScored = row.FindControl("cvAlreadyScored") as CustomValidator;

                SaveTestResponse response = SaveTestResponse.GetByID(Convert.ToInt32(lblSaveTestResponseID.Text.Trim()));
                Questionnaire questionnaire = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), true);
                TestCategory category = TestCategory.Select(questionnaire.TestCategoryID, true);

                try
                {
                    int temp = 0;
                    if (int.TryParse(txtGradeVal.Text.Trim(), out temp))
                    {
                        if (temp > Convert.ToInt32(lblMaxScore.Text.Trim()))
                        {
                            cvScoreOverflow.IsValid = false;
                            return;
                        }
                        if (response != null)
                        {
                            if (response.EssayResponse == txtAnswerVal.Text.Trim())
                            {
                                TestResponse existing = TestResponse.Select(response.TestTakenID, response.QuestionnaireID, response.UserID);
                                if (existing == null)
                                {
                                    TestResponse saveResponse = new TestResponse(response);
                                    int newResponseID = saveResponse.Insert();
                                    saveResponse.TestResponseID = newResponseID;
                                    saveResponse.EssayScore = Convert.ToInt32(txtGradeVal.Text.Trim());
                                    saveResponse.GradeEssayQuestion(Convert.ToInt32(txtGradeVal.Text.Trim()));
                                    //TestTaken taken = TestTaken.Select(saveResponse.TestTakenID);
                                    response.Delete();
                                    EssayMarker marker = new EssayMarker(SessionManager.SessionUsername, saveResponse.TestResponseID, saveResponse.EssayScore, txtCheckerComments.Text.Trim());
                                    marker.Insert();
                                }
                                else
                                {
                                    cvAlreadyScored.IsValid = false;
                                    hasError = true;
                                }
                            }
                        }
                        else
                        {
                            cvAlreadyScored.IsValid = false;
                            hasError = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                    service.WriteError(Config.ApplicationID(), "markessays.aspx", "btnGrade_Click", ex.Message);
                }
            }
        }
        if (hasError)
            return;
        else
        {
            try
            {
                int totalGrade = 0;
                TestTaken taken = TestTaken.Select(SessionManager.SessionTestTakenID);
                TestCategory category = TestCategory.Select(taken.TestCategoryID, true);
                Questionnaire[] questions = Questionnaire.SelectByCategory(category.TestCategoryID);
                foreach (Questionnaire question in questions)
                {
                    SaveTestResponse response = SaveTestResponse.Retrieve(SessionManager.SessionExamineeName, taken.TestTakenID, question.QuestionnaireID);
                    if (question.TypeCode == "matching")
                    {
                        if (response.Response1.Trim() == question.Ans1.Split('|')[0].Trim() && response.Response2.Trim() == question.Ans2.Split('|')[0].Trim() &&
                        response.Response3.Trim() == question.Ans3.Split('|')[0].Trim() && response.Response4.Trim() == question.Ans4.Split('|')[0].Trim() &&
                        response.Response5.Trim() == question.Ans5.Split('|')[0].Trim() && response.Response6.Trim() == question.Ans6.Split('|')[0].Trim() &&
                        response.Response7.Trim() == question.Ans7.Split('|')[0].Trim() && response.Response8.Trim() == question.Ans8.Split('|')[0].Trim() &&
                        response.Response9.Trim() == question.Ans9.Split('|')[0].Trim() && response.Response10.Trim() == question.Ans10.Split('|')[0].Trim())
                        {
                            totalGrade++;
                        }
                    }
                    else if (response != null)
                    {
                        if (response.Response1.Trim() == question.Ans1.Trim() && response.Response2.Trim() == question.Ans2.Trim() &&
                            response.Response3.Trim() == question.Ans3.Trim() && response.Response4.Trim() == question.Ans4.Trim() &&
                            response.Response5.Trim() == question.Ans5.Trim() && response.Response6.Trim() == question.Ans6.Trim() &&
                            response.Response7.Trim() == question.Ans7.Trim() && response.Response8.Trim() == question.Ans8.Trim() &&
                            response.Response9.Trim() == question.Ans9.Trim() && response.Response10.Trim() == question.Choice10.Trim() && question.TypeCode != "essay")
                        {
                            totalGrade++;
                        }
                    }
                }

                TestResponse[] responses = TestResponse.SelectByTestTaken(taken.TestTakenID);
                if (responses.Length > 0)
                {
                    foreach (TestResponse response in responses)
                        totalGrade += response.EssayScore;
                }
                taken.Score += totalGrade;
                taken.UpdateScore(totalGrade, category.PassingGrade);
                foreach (Questionnaire question in questions)
                {
                    SaveTestResponse response = SaveTestResponse.Retrieve(SessionManager.SessionExamineeName, taken.TestTakenID, question.QuestionnaireID);
                    if (response != null && question.TypeCode != "essay")
                    {
                        TestResponse newResponse = new TestResponse(response);
                        newResponse.Insert();
                        response.Delete();
                    }
                }
                this.lblScored.Text = "Essays graded successfully.";
                this.mpeScored.Show();
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(Config.ApplicationID(), "markessays.aspx", "btnGrade_Click", ex.Message);
            }
        }
    }
    protected void lblReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essaytests.aspx?" + Request.QueryString.ToString());
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essaytests.aspx?" + Request.QueryString.ToString());
    }
}