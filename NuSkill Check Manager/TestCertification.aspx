<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="TestCertification.aspx.cs" Inherits="TestCertification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblTestGroups" runat="server" Text="Test Certification" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnAddNew" runat="server" Text="Add New" CssClass="buttons" OnClick="btnAddNew_Click"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvGroups" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                    ForeColor="black" CellPadding="5" BorderColor="black" Font-Bold="false" EmptyDataText="No test groups."
                                    OnRowCommand="gvGroups_RowCommand">
                                    <HeaderStyle HorizontalAlign="center" />
                                    <RowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" Wrap="true" />
                                    <AlternatingRowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Test ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Test Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Harmony Qualification ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHarmonyQualificationID" runat="server" Text='<%#Bind("HarmonyQualificationID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Remove"
                                                    CommandArgument='<%#Bind("TestCategoryHarmonyQualificationMatrixID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeAddNew" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidAddNew" PopupControlID="pnlAddNew" />
    <asp:HiddenField ID="hidAddNew" runat="server" />
    <asp:Panel ID="pnlAddNew" runat="server" CssClass="modalPopup" HorizontalAlign="left"
        Width="400px">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblTestCategoryID" runat="server" Text="Test ID:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTestCategoryID" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblHarmonyQualificationID" runat="server" Text="Harmony Qualification ID:" />
                </td>
                <td>
                    <asp:TextBox ID="txtHarmonyQualificationID" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="buttons" OnClick="btnOK_Click" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="buttons" OnClick="btnCancel_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
