<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" Title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center">
        <asp:UpdatePanel ID="udpSub" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUpdate" runat="server" Height="150px">
                    <asp:UpdateProgress ID="udpProgress" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="pnlBuffer" runat="server" Height="150px">
                                <table align="center">
                                    <tr>
                                        <td align="center" valign="middle" style="height: 200px">
                                            <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                            <br />
                                            <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:Panel>
                <div align="center">
                <asp:Panel ID="pnlMain" runat="server" Width="300px" BorderColor="black" BorderStyle="None" HorizontalAlign="center" CssClass="panelStandard">
                    <table align="center">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:CustomValidator ID="cvLoginError" runat="server" ErrorMessage="Invalid Login Credentials/Insufficient Rights."
                                    Display="dynamic" CssClass="validatorStyle" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:LoginView ID="LoginView1" runat="server">
                                    <AnonymousTemplate>
                                        <div style="color: #001a33; text-align: center;">
                                            <asp:Label ID="lblWelHdr" runat="server" Text="Label">Welcome to Transcom University Testing Admin! To log in, use your Google account.</asp:Label>
                                        </div>
                                        <asp:Label ID="ModelErrorMessage1" runat="server"></asp:Label>
                                        <div style="text-align: center;">
                                            <asp:ImageButton ID="btnRequestLogin" runat="server" Text="Google Login" 
                                                OnClick="btnRequestLogin_Click" ImageUrl="~/images/btn_google_signin_dark_normal_web@2x.png" 
                                                ToolTip="Sign in with your Google Account" style="height: 60px; margin: 20px;" />
                                        </div>
                                    </AnonymousTemplate>
                                </asp:LoginView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
