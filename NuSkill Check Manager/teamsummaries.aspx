<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="teamsummaries.aspx.cs" Inherits="temasummaries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:UpdatePanel ID="udpTeamSummary" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                            Font-Bold="true" Font-Size="12px">
                            <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblTeamSummaries" runat="server" Text="Test Summaries" ForeColor="black"
                                            Font-Size="16" /><br />
                                        <hr style="color: Black; width: 100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <table width="80%" cellpadding="10" border="1">
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lnkTestSummary" runat="server" Text="Test Summary" Font-Size="12px"
                                                        ForeColor="black" OnClick="lnkTestSummary_Click" Enabled="false" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkGeneralQuery" runat="server" Text="General Query" Font-Size="12px"
                                                        ForeColor="black" OnClick="lnkGeneralQuery_Click" Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lnkTestsTakenByLocation" runat="server" Text="Tests Taken By Location (Quarterly)"
                                                        Font-Size="12px" ForeColor="black" OnClick="lnkTestsTakenByLocation_Click" Enabled="false" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkTestsTakenCampaign" runat="server" Text="Tests Taken Campaign (Quarterly)"
                                                        Font-Size="12px" ForeColor="black" OnClick="lnkTestsTakenCampaign_Click" Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lnkTestsTakenTSR" runat="server" Text="Tests Taken TSR" Font-Size="12px"
                                                        ForeColor="black" OnClick="lnkTestsTakenTSR_Click" Enabled="false" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkPassedFailed" runat="server" Text="Passed/Failed" Font-Size="12px"
                                                        ForeColor="black" OnClick="lnkPassedFailed_Click" Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lnkTeamSummaries" runat="server" Text="Team Summaries" Font-Size="12px"
                                                        ForeColor="black" OnClick="lnkTeamSummaries_Click" Enabled="false" />
                                                </td>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <ajax:ModalPopupExtender ID="mpePopup" runat="server" BackgroundCssClass="modalBackground"
                TargetControlID="hidPopup" PopupControlID="pnlPopup" />
            <asp:HiddenField ID="hidPopup" runat="server" />
            <asp:Panel ID="pnlPopup" runat="server" HorizontalAlign="left" CssClass="modalPopup">
                <table cellpadding="3">
                    <tr id="trYear" runat="server">
                        <td>
                            <asp:Label ID="lblWhatToDo" runat="server" Visible="false" />
                            <asp:Label ID="lblYear" runat="server" Text="Year:" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtYear" runat="server" MaxLength="4" />
                        </td>
                        <td>
                            <asp:CustomValidator ID="cvYear" runat="server" ErrorMessage="Invalid value." ValidationGroup="TeamSummary"
                                Display="dynamic" />
                        </td>
                    </tr>
                    <tr id="trCIM" runat="server">
                        <td>
                            <asp:Label ID="lblCIM" runat="server" Text="CIM:" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtCIM" runat="server" />
                        </td>
                        <td>
                            <asp:CustomValidator ID="cvCIM" runat="server" ErrorMessage="Invalid value." ValidationGroup="TeamSummary"
                                Display="dynamic" />
                        </td>
                    </tr>
                    <tr id="trPassedFailed" runat="server">
                        <td>
                            <asp:Label ID="lblPassedFailed" runat="server" Text="Passed/Failed:" />
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPassedFailed" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Passed" Value="True" />
                                <asp:ListItem Text="Failed" Value="False" />
                            </asp:RadioButtonList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="trSite" runat="server">
                        <td>
                            <asp:Label ID="lblSite" runat="server" Text="Site:" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSite" runat="server" DataValueField="CompanySiteShort" DataTextField="CompanySite" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="trCampaign" runat="server">
                        <td>
                            <asp:Label ID="lblCampaign" runat="server" Text="Campaign:" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCampaign" runat="server" DataTextField="Account" DataValueField="AccountID" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="trTeam" runat="server">
                        <td>
                            <asp:Label ID="lblTeam" runat="server" Text="Team:" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTeam" runat="server" DataValueField="CimNumber" DataTextField="TeamName" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="trTest" runat="server">
                        <td>
                            <asp:Label ID="lblPastExam" runat="server" Text="Test:" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPastExam" runat="server" DataValueField="TestCategoryID"
                                DataTextField="TestName" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="trStartDate" runat="server">
                        <td>
                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date (yyyy/mm/dd):" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtStartDate" runat="server" MaxLength="10" />
                            <asp:ImageButton ID="imgStartDate" runat="server" ImageUrl="~/images/icon-calendar.gif" />
                            <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgStartDate" Format="yyyy/MM/dd" />
                        </td>
                        <td>
                            <asp:CustomValidator ID="cvStartDate" runat="server" ErrorMessage="Invalid value."
                                ValidationGroup="TeamSummary" Display="dynamic" />
                        </td>
                    </tr>
                    <tr id="trEndDate" runat="server">
                        <td>
                            <asp:Label ID="lblEndDate" runat="server" Text="End Date (yyyy/mm/dd):" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtEndDate" runat="server" MaxLength="10" />
                            <asp:ImageButton ID="imgEndDate" runat="server" ImageUrl="~/images/icon-calendar.gif" />
                            <ajax:CalendarExtender ID="calEndDate" runat="server" TargetControlID="txtEndDate"
                                PopupButtonID="imgEndDate" Format="yyyy/MM/dd" />
                        </td>
                        <td>
                            <asp:CustomValidator ID="cvEndDate" runat="server" ErrorMessage="Invalid value."
                                ValidationGroup="TeamSummary" Display="dynamic" />
                        </td>
                    </tr>
                    <tr id="trStartQuarter" runat="server">
                        <td>
                            <asp:Label ID="lblStartQuarter" runat="server" Text="Start Quarter:" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlStartQuarter" runat="server">
                                <asp:ListItem Text="1st Quarter" Value="1" />
                                <asp:ListItem Text="2nd Quarter" Value="2" />
                                <asp:ListItem Text="3rd Quarter" Value="3" />
                                <asp:ListItem Text="4th Quarter" Value="4" />
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="buttons" Width="80px" OnClick="btnOK_Click" />
                        </td>
                        <td align="right">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="buttons" Width="80px" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%--<ajax:ModalPopupExtender ID="--%>
            <ajax:ModalPopupExtender ID="mpeResults" runat="server" BackgroundCssClass="modalBackground"
                TargetControlID="hidResults" PopupControlID="pnlResults" />
            <asp:HiddenField ID="hidResults" runat="server" />
            <asp:Panel ID="pnlResults" runat="server" CssClass="modalPopup" HorizontalAlign="center" ScrollBars="Auto" Width="680px" Height="80%">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:GridView ID="gvTestSummary" runat="server" AutoGenerateColumns="false" Visible="false"
                                BorderColor="lightgray" BorderStyle="solid" BorderWidth="1px" CellPadding="8"
                                ForeColor="black" GridLines="horizontal" Width="100%" EmptyDataText="No results found.">
                                <FooterStyle BackColor="#CCCC99" />
                                <RowStyle BackColor="white" HorizontalAlign="Center" BorderColor="black" ForeColor="black" />
                                <AlternatingRowStyle BackColor="lightblue" HorizontalAlign="center" BorderColor="black" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Test ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("StartDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView ID="gvGeneralQuery" runat="server" AutoGenerateColumns="false" Visible="false"
                                BorderColor="lightgray" BorderStyle="solid" BorderWidth="1px" CellPadding="8"
                                ForeColor="black" GridLines="horizontal" Width="100%" EmptyDataText="No results found.">
                                <FooterStyle BackColor="#CCCC99" />
                                <RowStyle BackColor="white" HorizontalAlign="Center" BorderColor="black" ForeColor="black" />
                                <AlternatingRowStyle BackColor="lightblue" HorizontalAlign="center" BorderColor="black" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CIM">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#Bind("UserID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passed">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Taken">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOffice" runat="server" Text='<%#Bind("Office") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Campaign">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCampaign" runat="server" Text='<%#Bind("Campaign") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView ID="gvTestsTakenByLocation" runat="server" AutoGenerateColumns="false"
                                Visible="false" BorderColor="lightgray" BorderStyle="solid" BorderWidth="1px"
                                CellPadding="8" ForeColor="black" GridLines="horizontal" Width="100%" EmptyDataText="No results found.">
                                <FooterStyle BackColor="#CCCC99" />
                                <RowStyle BackColor="white" HorizontalAlign="Center" BorderColor="black" ForeColor="black" />
                                <AlternatingRowStyle BackColor="lightblue" HorizontalAlign="center" BorderColor="black" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CIM">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#Bind("UserID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passed">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Taken">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOffice" runat="server" Text='<%#Bind("Office") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Campaign">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCampaign" runat="server" Text='<%#Bind("Campaign") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView ID="gvTestsTakenByCampaign" runat="server" AutoGenerateColumns="false"
                                Visible="false" BorderColor="lightgray" BorderStyle="solid" BorderWidth="1px"
                                CellPadding="8" ForeColor="black" GridLines="horizontal" Width="100%" EmptyDataText="No results found.">
                                <FooterStyle BackColor="#CCCC99" />
                                <RowStyle BackColor="white" HorizontalAlign="Center" BorderColor="black" ForeColor="black" />
                                <AlternatingRowStyle BackColor="lightblue" HorizontalAlign="center" BorderColor="black" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CIM">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#Bind("UserID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passed">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Taken">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOffice" runat="server" Text='<%#Bind("Office") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Campaign">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCampaign" runat="server" Text='<%#Bind("Campaign") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView ID="gvTestsTakenTSR" runat="server" AutoGenerateColumns="false" Visible="false"
                                BorderColor="lightgray" BorderStyle="solid" BorderWidth="1px" CellPadding="8"
                                ForeColor="black" GridLines="horizontal" Width="100%" EmptyDataText="No results found.">
                                <FooterStyle BackColor="#CCCC99" />
                                <RowStyle BackColor="white" HorizontalAlign="Center" BorderColor="black" ForeColor="black" />
                                <AlternatingRowStyle BackColor="lightblue" HorizontalAlign="center" BorderColor="black" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CIM">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#Bind("UserID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passed">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Taken">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOffice" runat="server" Text='<%#Bind("Office") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Campaign">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCampaign" runat="server" Text='<%#Bind("Campaign") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView ID="gvPassedFailed" runat="server" AutoGenerateColumns="false" Visible="false"
                                BorderColor="lightgray" BorderStyle="solid" BorderWidth="1px" CellPadding="8"
                                ForeColor="black" GridLines="horizontal" Width="100%" EmptyDataText="No results found.">
                                <FooterStyle BackColor="#CCCC99" />
                                <RowStyle BackColor="white" HorizontalAlign="Center" BorderColor="black" ForeColor="black" />
                                <AlternatingRowStyle BackColor="lightblue" HorizontalAlign="center" BorderColor="black" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CIM">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#Bind("UserID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOffice" runat="server" Text='<%#Bind("Office") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Taken">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Campaign">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCampaign" runat="server" Text='<%#Bind("Campaign") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passed">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView ID="gvTeamSummaries" runat="server" AutoGenerateColumns="false" BorderColor="lightgray"
                                BorderStyle="solid" BorderWidth="1px" CellPadding="8" ForeColor="black" GridLines="horizontal"
                                Width="100%" EmptyDataText="No results found.">
                                <FooterStyle BackColor="#CCCC99" />
                                <RowStyle BackColor="white" HorizontalAlign="Center" BorderColor="black" ForeColor="black" />
                                <AlternatingRowStyle BackColor="lightblue" HorizontalAlign="center" BorderColor="black" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CIM">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCim" runat="server" Text='<%#Bind("CIM") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRep" runat="server" Text='<%#Bind("FirstName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passed">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%#Bind("Score") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="buttons" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
