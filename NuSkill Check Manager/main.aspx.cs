using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class main : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
    }
    protected void lnkExam_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
    }
    protected void lnkExaminee_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/users.aspx?" + Request.QueryString.ToString());
    }
    protected void lnkReports_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/reports.aspx?" + Request.QueryString.ToString());
    }
    protected void lnkEssay_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essays.aspx?" + Request.QueryString.ToString());
    }
}
