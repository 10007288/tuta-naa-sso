<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="markessay.aspx.cs" Inherits="markessay" Title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Font-Names="Arial" HorizontalAlign="center"
        Width="100%">
        <asp:Panel ID="pnlSubPanel" runat="server" HorizontalAlign="left">
            <table cellpadding="10" width="100%" cellspacing="0" style="font-size: 14px;">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblEssay" runat="server" Text="Essay Grading" CssClass="testQuestion" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr style="color: black" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <asp:Label ID="lblUserID" runat="server" Text="Examinee:" />
                    </td>
                    <td style="width: 80%">
                        <asp:Label ID="lblUserIDVal" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblExamID" runat="server" Text="Exam Name:" />
                    </td>
                    <td>
                        <asp:Label ID="lblExamIDVal" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblQuestion" runat="server" Text="Question:" />
                    </td>
                    <td>
                        <asp:Label ID="lblQuestionVal" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblAnswer" runat="server" Text="Answer:" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtAnswerVal" runat="server" MaxLength="8000" TextMode="multiline"
                            Columns="80" Rows="20" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblGrade" runat="server" Text="Score:" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtGradeVal" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMaximumScore" runat="server" Text="Maximum Score:" />
                    </td>
                    <td>
                        <asp:Label ID="lblMaxScore" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:CustomValidator ID="cvScoreOverflow" runat="server" ErrorMessage="Score must be an integer that does not exceed maximum score."
                            Display="dynamic" CssClass="validatorStyle" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <asp:Label ID="lblSingleClick" runat="server" Text="Please click only once." CssClass="buttons" BorderStyle="none" />
                        <asp:Button CssClass="buttons" ID="btnGrade" runat="server" Text="OK" Width="80"
                            OnClick="btnGrade_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeScored" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlScored" TargetControlID="hidScored" />
    <asp:HiddenField ID="hidScored" runat="server" />
    <asp:Panel ID="pnlScored" runat="server" CssClass="modalPopup" HorizontalAlign="center">
        <table width="300" cellpadding="10" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblScored" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button CssClass="buttons" ID="lblReturn" runat="server" Text="Return" OnClick="lblReturn_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
