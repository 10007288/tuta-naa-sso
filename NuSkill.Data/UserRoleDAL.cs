using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class UserRoleDAL : TheLibrary.DBImportTool.Base
    {
        public UserRoleDAL()
        {
            //this.ConnectionString = NuSkill.Data.Properties.Settings.Default.LocalConnectionString;
        }

        public UserRoleDAL(string connectionstring)
        {
            this.ConnectionString = connectionstring;
        }

        public DataSet GetRole(string userID, string roleCode)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pRoleCode", SqlDbType.VarChar, roleCode));
            return Connection.ExecuteSPQuery("pr_userrole_lkp_getrole", paramList.ToArray());
        }

        public DataSet GetUserRoles(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_userrole_lkp_getuserroles", paramList.ToArray());
        }
    }
}
