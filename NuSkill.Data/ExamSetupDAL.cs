using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class ExamSetupDAL : TheLibrary.DBImportTool.Base
    {
        public ExamSetupDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public ExamSetupDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public void Insert(int testTakenID, int examLimit,int timeLimit)
        {
            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramlist.Add(Helper.CreateParam("@pExamLimit", SqlDbType.Int, examLimit));
            paramlist.Add(Helper.CreateParam("@pTimeLimit", SqlDbType.Int, timeLimit));
            Connection.ExecuteSP("pr_examsetup_sav_insert",paramlist.ToArray());
        }
    }
}
