using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestScoreSummaryDal : Base
    {
        public TestScoreSummaryDal()
        {}

        public TestScoreSummaryDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public void Insert(int testTakenID, int testCategoryID, int cimNumber, double score, bool passed, int accountID)
        {
            Connection.ExecuteSP("pr_testScoreSummary_Sav_InsertScoreSummary",
                Helper.CreateParam("@TestTakenID", SqlDbType.Int, testTakenID),
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID),
                Helper.CreateParam("@CimNumber", SqlDbType.Int, cimNumber),
                Helper.CreateParam("@Score", SqlDbType.Float, score),
                Helper.CreateParam("@Passed", SqlDbType.Bit, passed),
                Helper.CreateParam("@AccountID", SqlDbType.Int, accountID));
        }
    }
}
