using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class UserBatchDal : Base
    {
        public UserBatchDal()
        {
        }

        public UserBatchDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_userbatch_selectall");
        }

        public int Insert(string batchName)
        {
            return Convert.ToInt32(Connection.ExecuteScalar("pr_userbatch_sav_insert",
                Helper.CreateParam("@BatchName", SqlDbType.VarChar, batchName)));
        }

        public DataSet SelectBatchName(string batchName)
        {
            return Connection.ExecuteSPQuery("pr_userbatch_lkp_selectbatchname",
                Helper.CreateParam("@BatchName", SqlDbType.VarChar, batchName));
        }

    }
}
