using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestSummariesDal : Base
    {
        public TestSummariesDal()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public TestSummariesDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet TestSummary(int year, string site, int campaign)
        {
            return Connection.ExecuteSPQuery("pr_TestSummaries_TestSummary",
                Helper.CreateParam("@Year", SqlDbType.Int, year),
                Helper.CreateParam("@Site", SqlDbType.VarChar, site),
                Helper.CreateParam("@Campaign", SqlDbType.Int, campaign));
        }

        public DataSet GeneralQuery(string site, int campaign, DateTime startDate, DateTime endDate)
        {
            return Connection.ExecuteSPQuery("pr_TestSummaries_GeneralQuery",
                Helper.CreateParam("@Site", SqlDbType.VarChar, site),
                Helper.CreateParam("@Campaign", SqlDbType.Int, campaign),
                Helper.CreateParam("@StartDate", SqlDbType.DateTime, startDate),
                Helper.CreateParam("@EndDate", SqlDbType.DateTime, endDate));
        }

        public DataSet TestsTakenByLocation(int year, string site, int startQuarter)
        {
            return Connection.ExecuteSPQuery("pr_TestSummaries_TestsTakenByLocation",
                Helper.CreateParam("@Year", SqlDbType.Int, year),
                Helper.CreateParam("@Site", SqlDbType.VarChar, site),
                Helper.CreateParam("@StartQuarter", SqlDbType.Int, startQuarter));
        }

        public DataSet TestsTakenCampaign(int year, int campaign, int startQuarter)
        {
            return Connection.ExecuteSPQuery("pr_TestSummaries_TestsTakenByCampaign",
                Helper.CreateParam("@Year", SqlDbType.Int, year),
                Helper.CreateParam("@Campaign", SqlDbType.Int, campaign),
                Helper.CreateParam("@StartQuarter", SqlDbType.Int, startQuarter));
        }

        public DataSet TestsTakenTSR(int year, int cimNumber, int startQuarter)
        {
            return Connection.ExecuteSPQuery("pr_TestSummaries_TestsTakenTSR",
                Helper.CreateParam("@Year", SqlDbType.Int, year),
                Helper.CreateParam("@CimNumber", SqlDbType.Int, cimNumber),
                Helper.CreateParam("@StartQuarter", SqlDbType.Int, startQuarter));
        }

        public DataSet PassedFailed(int year, bool passed, string site, int campaign, int startQuarter)
        {
            return Connection.ExecuteSPQuery("pr_TestSummaries_PassedFailed",
                Helper.CreateParam("@Year", SqlDbType.Int, year),
                Helper.CreateParam("@Passed", SqlDbType.Bit, passed),
                Helper.CreateParam("@Site", SqlDbType.VarChar, site),
                Helper.CreateParam("@Campaign", SqlDbType.Int, campaign),
                Helper.CreateParam("@StartQuarter", SqlDbType.Int, startQuarter));
        }

        public DataSet TeamSummaries(int team, DateTime startDate, DateTime endDate, int testCategoryID)
        {
            return Connection.ExecuteSPQuery("pr_TeamSummaries_TeamSummaries",
                Helper.CreateParam("@Team", SqlDbType.Int, team),
                Helper.CreateParam("@StartDate", SqlDbType.DateTime, startDate),
                Helper.CreateParam("@EndDate", SqlDbType.DateTime, endDate),
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
        }

        public DataSet GetCompanySites(int cimNumber)
        {
            return Connection.ExecuteSPQuery("pr_WeeklyTesting_Lkp_CompanySiteByCIM",
                Helper.CreateParam("@CIMNumber", SqlDbType.Int, cimNumber));
        }
    }
}
