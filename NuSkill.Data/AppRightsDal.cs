using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class AppRightsDal : TheLibrary.DBImportTool.Base
    {
        public AppRightsDal()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public AppRightsDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_apprights_lkp_selectall");
        }

    }
}
