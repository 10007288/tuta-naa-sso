using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class EssayMarkerDal : Base
    {
        public EssayMarkerDal()
        { }

        public EssayMarkerDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public int Insert(string userID, int testResponseID, int score, string checkerComments)
        {
            return Convert.ToInt32(Connection.ExecuteScalar("pr_essaymarker_sav_insert",
                Helper.CreateParam("@UserID", SqlDbType.VarChar, userID),
                Helper.CreateParam("@TestResponseID", SqlDbType.Int, testResponseID),
                Helper.CreateParam("@Score", SqlDbType.Int, score),
                Helper.CreateParam("@CheckerComments", SqlDbType.VarChar, checkerComments)));
        }

        public DataSet SelectByTestTaken(int testTakenID)
        {
            return Connection.ExecuteSPQuery("pr_essaymarker_lkp_selectByTestTaken",
                Helper.CreateParam("@TestTakenID", SqlDbType.Int, testTakenID));
        }

        public DataSet SelectByTestResponse(int testResponseID)
        {
            return Connection.ExecuteSPQuery("pr_essaymarker_lkp_selectByTestResponseID",
                Helper.CreateParam("@TestResponseID", SqlDbType.Int, testResponseID));
        }
    }
}
