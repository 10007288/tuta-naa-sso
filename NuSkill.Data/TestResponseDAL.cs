using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestResponseDAL : TheLibrary.DBImportTool.Base
    {
        public TestResponseDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public TestResponseDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public int Insert(string userID, int testTakenID, int questionnaireID, string response1, string response2, string response3,
            string response4, string response5, string response6, string response7, string response8, string response9, string response10, string essayResponse)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            paramList.Add(Helper.CreateParam("@pResponse1", SqlDbType.VarChar, response1));
            paramList.Add(Helper.CreateParam("@pResponse2", SqlDbType.VarChar, response2));
            paramList.Add(Helper.CreateParam("@pResponse3", SqlDbType.VarChar, response3));
            paramList.Add(Helper.CreateParam("@pResponse4", SqlDbType.VarChar, response4));
            paramList.Add(Helper.CreateParam("@pResponse5", SqlDbType.VarChar, response5));
            paramList.Add(Helper.CreateParam("@pResponse6", SqlDbType.VarChar, response6));
            paramList.Add(Helper.CreateParam("@pResponse7", SqlDbType.VarChar, response7));
            paramList.Add(Helper.CreateParam("@pResponse8", SqlDbType.VarChar, response8));
            paramList.Add(Helper.CreateParam("@pResponse9", SqlDbType.VarChar, response9));
            paramList.Add(Helper.CreateParam("@pResponse10", SqlDbType.VarChar, response10));
            paramList.Add(Helper.CreateParam("@pEssayResponse", SqlDbType.VarChar, essayResponse));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testresponse_sav_insert", paramList.ToArray()));
        }

        public void GradeEssayQuestion(int essayScore, int testResponseID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pEssayScore", SqlDbType.Int, essayScore));
            paramList.Add(Helper.CreateParam("@pTestResponseID", SqlDbType.Int, testResponseID));
            Connection.ExecuteSP("pr_testresponse_sav_essayscore", paramList.ToArray());
        }

        public DataSet SelectByUser(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_userexams", paramList.ToArray());
        }

        public DataSet Select(int testResponseID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestResponseID", SqlDbType.Int, testResponseID));
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_getexam", paramList.ToArray());
        }

        public DataSet Select(int testTakenID, int questionnaireID, string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_checkexists", paramList.ToArray());
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_getall");
        }

        public int CountPerQuestion(int questionnaireID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testresponse_lkp_countanswers", paramList.ToArray()));
        }

        public int GetCorrectAnswer(int questionnaireID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testresponse_lkp_getcorrect", paramList.ToArray()));
        }

        public DataSet SelectByQuestion(int questionnaireID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_getbyquestion", paramList.ToArray());
        }

        public DataSet SelectByTestTaken(int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_getbytesttaken", paramList.ToArray());
        }

        public DataSet GetMultipleEssays(int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_getmultipleessays", paramList.ToArray());
        }

        //TODO: GLA 01092014
        public DataSet RetrievePreviousAnswer(string userID, int testTakenID, int questionnaireID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            return Connection.ExecuteSPQuery("pr_testresponse_lkp_getpreviousanswer", paramList.ToArray());
        }
    }
}