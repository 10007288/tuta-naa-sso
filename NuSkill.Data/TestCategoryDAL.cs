using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestCategoryDAL : TheLibrary.DBImportTool.Base
    {
        public TestCategoryDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public TestCategoryDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public int Insert(DateTime dateCreated, DateTime dateLastUpdated, DateTime endDate, bool hideFromList, bool isEssay,
            string lastUpdated, string lastUpdatedBy, int numberOfQuestions, int passingGrade, DateTime startDate,
            int testLimit, string testName, int timeLimit, int accountID, int campaignID, int courseID, bool isCampaign, string instructions, bool hideScores, bool retake)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pDateCreated", SqlDbType.DateTime, dateCreated));
            paramList.Add(Helper.CreateParam("@pDateLastUpdated", SqlDbType.DateTime, dateLastUpdated));
            paramList.Add(Helper.CreateParam("@pEndDate", SqlDbType.DateTime, endDate));
            paramList.Add(Helper.CreateParam("@pHideFromList", SqlDbType.Bit, hideFromList));
            paramList.Add(Helper.CreateParam("@pIsEssay", SqlDbType.Bit, isEssay));
            paramList.Add(Helper.CreateParam("@pLastUpdated", SqlDbType.VarChar, lastUpdated));
            paramList.Add(Helper.CreateParam("@pLastUpdatedBy", SqlDbType.VarChar, lastUpdatedBy));
            paramList.Add(Helper.CreateParam("@pNumberOfQuestions", SqlDbType.Int, numberOfQuestions));
            paramList.Add(Helper.CreateParam("@pPassingGrade", SqlDbType.Int, passingGrade));
            paramList.Add(Helper.CreateParam("@pStartDate", SqlDbType.DateTime, startDate));
            paramList.Add(Helper.CreateParam("@pTestLimit", SqlDbType.Int, testLimit));
            paramList.Add(Helper.CreateParam("@pTestName", SqlDbType.VarChar, testName));
            paramList.Add(Helper.CreateParam("@pTimeLimit", SqlDbType.Int, timeLimit));
            paramList.Add(Helper.CreateParam("@AccountID", SqlDbType.Int, accountID));
            paramList.Add(Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            paramList.Add(Helper.CreateParam("@CourseID", SqlDbType.Int, courseID));
            paramList.Add(Helper.CreateParam("@Instructions", SqlDbType.VarChar, instructions));
            paramList.Add(Helper.CreateParam("@HideScores", SqlDbType.Bit, hideScores));
            paramList.Add(Helper.CreateParam("@Retake", SqlDbType.Bit, retake));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testCategory_sav_insert", paramList.ToArray()));
        }

        public int Insert(DateTime dateCreated, DateTime dateLastUpdated, DateTime endDate, bool hideFromList, bool isEssay,
            string lastUpdated, string lastUpdatedBy, int numberOfQuestions, int passingGrade, DateTime startDate,
            int testLimit, string testName, int timeLimit, int accountID, int campaignID, int courseID, bool isCampaign, string instructions, bool hideScores, bool randomizeQuestions, bool hideExamFromTesting, bool retake)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pDateCreated", SqlDbType.DateTime, dateCreated));
            paramList.Add(Helper.CreateParam("@pDateLastUpdated", SqlDbType.DateTime, dateLastUpdated));
            paramList.Add(Helper.CreateParam("@pEndDate", SqlDbType.DateTime, endDate));
            paramList.Add(Helper.CreateParam("@pHideFromList", SqlDbType.Bit, hideFromList));
            paramList.Add(Helper.CreateParam("@pIsEssay", SqlDbType.Bit, isEssay));
            paramList.Add(Helper.CreateParam("@pLastUpdated", SqlDbType.VarChar, lastUpdated));
            paramList.Add(Helper.CreateParam("@pLastUpdatedBy", SqlDbType.VarChar, lastUpdatedBy));
            paramList.Add(Helper.CreateParam("@pNumberOfQuestions", SqlDbType.Int, numberOfQuestions));
            paramList.Add(Helper.CreateParam("@pPassingGrade", SqlDbType.Int, passingGrade));
            paramList.Add(Helper.CreateParam("@pStartDate", SqlDbType.DateTime, startDate));
            paramList.Add(Helper.CreateParam("@pTestLimit", SqlDbType.Int, testLimit));
            paramList.Add(Helper.CreateParam("@pTestName", SqlDbType.VarChar, testName));
            paramList.Add(Helper.CreateParam("@pTimeLimit", SqlDbType.Int, timeLimit));
            paramList.Add(Helper.CreateParam("@AccountID", SqlDbType.Int, accountID));
            paramList.Add(Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            paramList.Add(Helper.CreateParam("@CourseID", SqlDbType.Int, courseID));
            paramList.Add(Helper.CreateParam("@Instructions", SqlDbType.VarChar, instructions));
            paramList.Add(Helper.CreateParam("@HideScores", SqlDbType.Bit, hideScores));
            paramList.Add(Helper.CreateParam("@RandomizeQuestions", SqlDbType.Bit, randomizeQuestions));
            paramList.Add(Helper.CreateParam("@HideExamFromTesting", SqlDbType.Bit, hideExamFromTesting));
            paramList.Add(Helper.CreateParam("@Retake", SqlDbType.Bit, retake));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testCategory_sav_insert", paramList.ToArray()));
        }

        public DataSet Select(int testCategoryID, bool includeElapsed)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@pIncludeElapsed", SqlDbType.Bit, includeElapsed));
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_select", paramList.ToArray());
        }

        public int checkRetake(int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testcategory_lkp_retake", paramList.ToArray()));
        }

        public DataSet SelectByAccountCampaign(int accountCampaignID, int subCategory, bool isCampaign, bool includeElapsed)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@AccountCampaignID", SqlDbType.Int, accountCampaignID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            paramList.Add(Helper.CreateParam("@SubCategory", SqlDbType.Int, subCategory)); 
            paramList.Add(Helper.CreateParam("@IncludeElapsed", SqlDbType.Bit, includeElapsed));
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_selectbyaccountcampaign", paramList.ToArray());
        }

        public DataSet Search(string searchParam, bool includeElapsed)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pSearchParam", SqlDbType.VarChar, searchParam));
            paramList.Add(Helper.CreateParam("@pIncludeElapsed", SqlDbType.Bit, includeElapsed));
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_search", paramList.ToArray());
        }

        public DataSet SearchByAccountCampaign(string searchParam, int accountCampaignID, int subCategory, bool isCampaign, bool includeElapsed)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@SearchParam", SqlDbType.VarChar, searchParam));
            paramList.Add(Helper.CreateParam("@AccountCampaignID", SqlDbType.Int, accountCampaignID));
            paramList.Add(Helper.CreateParam("@SubCategory", SqlDbType.Int, subCategory));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            paramList.Add(Helper.CreateParam("@IncludeElapsed", SqlDbType.Bit, includeElapsed));
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_searchbyaccountcampaign", paramList.ToArray());
        }

        public DataSet SearchByTestGroup(int testGroupID)
        {
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_searchbytestgroup",
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public DataSet SelectAll(bool includeElapsed, bool recentOnly)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pIncludeElapsed", SqlDbType.Bit, includeElapsed));
            paramList.Add(Helper.CreateParam("@pRecentOnly", SqlDbType.Bit, recentOnly));
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_selectall", paramList.ToArray());
        }

        public int SelectExamsGetRowCount(bool includeElapsed, string searchParam, string courseID, int accountCampaignID, int subCategory)
        {
            int? account = null;
            int? subcategory = null;
            int? course = null;

            if (courseID != string.Empty)
            {
                course = Convert.ToInt32(courseID);
            }

            if (accountCampaignID != 0)
            {
                account = accountCampaignID;
            }

            if (subCategory != 0)
            {
                subcategory = subCategory;
            }
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pIncludeElapsed", SqlDbType.Bit, includeElapsed));
            paramList.Add(Helper.CreateParam("@pSearchParam", SqlDbType.VarChar, searchParam));
            paramList.Add(Helper.CreateParam("@pCourseIDParam", SqlDbType.Int, course));
            paramList.Add(Helper.CreateParam("@AccountCampaignID", SqlDbType.Int, account));
            paramList.Add(Helper.CreateParam("@SubCategory", SqlDbType.Int, subcategory));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testcategory_lkp_selectexams_getrowcount", paramList.ToArray()));
        }

        public DataSet SelectExams(bool includeElapsed, string searchParam, string courseID, int accountCampaignID, int subCategory, int startRowIndex, int maximumRows, string sortExpression)
        {
            int? account = null;
            int? subcategory = null;
            int? course = null;

            if (courseID != string.Empty)
            {
                course = Convert.ToInt32(courseID);
            }

            if (accountCampaignID != 0)
            {
                account = accountCampaignID;
            }

            if (subCategory != 0)
            {
                subcategory = subCategory;
            }
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pIncludeElapsed", SqlDbType.Bit, includeElapsed));
            paramList.Add(Helper.CreateParam("@pSearchParam", SqlDbType.VarChar, searchParam));
            paramList.Add(Helper.CreateParam("@pCourseIDParam", SqlDbType.Int, course));
            paramList.Add(Helper.CreateParam("@AccountCampaignID", SqlDbType.Int, account));
            paramList.Add(Helper.CreateParam("@SubCategory", SqlDbType.Int, subcategory));
            paramList.Add(Helper.CreateParam("@startRowIndex", SqlDbType.Int, startRowIndex));
            paramList.Add(Helper.CreateParam("@maximumRows", SqlDbType.Int, maximumRows));
            paramList.Add(Helper.CreateParam("@sortExpression", SqlDbType.VarChar, sortExpression));
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_selectExams", paramList.ToArray());
        }

        public DataSet SelectAllUserHistory(string userID, bool includeElapsed, bool recentOnly, bool isCampaign, int campaignAccountID, int subcategory)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@UserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pIncludeElapsed", SqlDbType.Bit, includeElapsed));
            paramList.Add(Helper.CreateParam("@pRecentOnly", SqlDbType.Bit, recentOnly));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            paramList.Add(Helper.CreateParam("@AccountCampaignID", SqlDbType.Int, campaignAccountID));
            paramList.Add(Helper.CreateParam("@SubCategory", SqlDbType.Int, subcategory));
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_selectalluserhistory", paramList.ToArray());
        }

        //public DataSet SelectAllIncludeElapsed()
        //{
        //    //return Connection
        //}

        public void DeleteByHide(int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategory", SqlDbType.Int, testCategoryID));
            this.Connection.ExecuteSP("pr_testcategory_sav_hidefromlist", paramList.ToArray());
        }

        public int Update(DateTime dateCreated, DateTime dateLastUpdated, DateTime endDate, bool hideFromList, bool isEssay,
            string lastUpdated, string lastUpdatedBy, int numberOfQuestions, int passingGrade, DateTime startDate, int testCategoryID,
            int testLimit, string testName, int timeLimit, int accountID, int campaignID, int courseID, bool isCampaign, string instructions, bool hideScores, bool retake)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pDateCreated", SqlDbType.DateTime, dateCreated));
            paramList.Add(Helper.CreateParam("@pDateLastUpdated", SqlDbType.DateTime, dateLastUpdated));
            paramList.Add(Helper.CreateParam("@pEndDate", SqlDbType.DateTime, endDate));
            paramList.Add(Helper.CreateParam("@pHideFromList", SqlDbType.Bit, hideFromList));
            paramList.Add(Helper.CreateParam("@pIsEssay", SqlDbType.Bit, isEssay));
            paramList.Add(Helper.CreateParam("@pLastUpdated", SqlDbType.VarChar, lastUpdated));
            paramList.Add(Helper.CreateParam("@pLastUpdatedBy", SqlDbType.VarChar, lastUpdatedBy));
            paramList.Add(Helper.CreateParam("@pNumberOfQuestions", SqlDbType.Int, numberOfQuestions));
            paramList.Add(Helper.CreateParam("@pPassingGrade", SqlDbType.Int, passingGrade));
            paramList.Add(Helper.CreateParam("@pStartDate", SqlDbType.DateTime, startDate));
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@pTestLimit", SqlDbType.Int, testLimit));
            paramList.Add(Helper.CreateParam("@pTestName", SqlDbType.VarChar, testName));
            paramList.Add(Helper.CreateParam("@pTimeLimit", SqlDbType.Int, timeLimit));
            paramList.Add(Helper.CreateParam("@AccountID", SqlDbType.Int, accountID));
            paramList.Add(Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            paramList.Add(Helper.CreateParam("@CourseID", SqlDbType.Int, courseID));
            paramList.Add(Helper.CreateParam("@Instructions", SqlDbType.VarChar, instructions));
            paramList.Add(Helper.CreateParam("@HideScores", SqlDbType.Bit, hideScores));
            paramList.Add(Helper.CreateParam("@Retake", SqlDbType.Bit, retake));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testCategory_sav_update", paramList.ToArray()));
        }

        public int Update(DateTime dateCreated, DateTime dateLastUpdated, DateTime endDate, bool hideFromList, bool isEssay,
            string lastUpdated, string lastUpdatedBy, int numberOfQuestions, int passingGrade, DateTime startDate, int testCategoryID,
            int testLimit, string testName, int timeLimit, int accountID, int campaignID, int courseID, bool isCampaign, string instructions, bool hideScores, bool randomizeQuestions, bool hideExamFromTesting, bool retake)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pDateCreated", SqlDbType.DateTime, dateCreated));
            paramList.Add(Helper.CreateParam("@pDateLastUpdated", SqlDbType.DateTime, dateLastUpdated));
            paramList.Add(Helper.CreateParam("@pEndDate", SqlDbType.DateTime, endDate));
            paramList.Add(Helper.CreateParam("@pHideFromList", SqlDbType.Bit, hideFromList));
            paramList.Add(Helper.CreateParam("@pIsEssay", SqlDbType.Bit, isEssay));
            paramList.Add(Helper.CreateParam("@pLastUpdated", SqlDbType.VarChar, lastUpdated));
            paramList.Add(Helper.CreateParam("@pLastUpdatedBy", SqlDbType.VarChar, lastUpdatedBy));
            paramList.Add(Helper.CreateParam("@pNumberOfQuestions", SqlDbType.Int, numberOfQuestions));
            paramList.Add(Helper.CreateParam("@pPassingGrade", SqlDbType.Int, passingGrade));
            paramList.Add(Helper.CreateParam("@pStartDate", SqlDbType.DateTime, startDate));
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@pTestLimit", SqlDbType.Int, testLimit));
            paramList.Add(Helper.CreateParam("@pTestName", SqlDbType.VarChar, testName));
            paramList.Add(Helper.CreateParam("@pTimeLimit", SqlDbType.Int, timeLimit));
            paramList.Add(Helper.CreateParam("@AccountID", SqlDbType.Int, accountID));
            paramList.Add(Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            paramList.Add(Helper.CreateParam("@CourseID", SqlDbType.Int, courseID));
            paramList.Add(Helper.CreateParam("@Instructions", SqlDbType.VarChar, instructions));
            paramList.Add(Helper.CreateParam("@HideScores", SqlDbType.Bit, hideScores));
            paramList.Add(Helper.CreateParam("@RandomizeQuestions", SqlDbType.Bit, randomizeQuestions));
            paramList.Add(Helper.CreateParam("@HideExamFromTesting", SqlDbType.Bit, hideExamFromTesting));
            paramList.Add(Helper.CreateParam("@Retake", SqlDbType.Bit, retake));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testCategory_sav_update", paramList.ToArray()));
        }

        public bool IsEssay(int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            return Convert.ToBoolean(Connection.ExecuteScalar("pr_testCategory_lkp_isessay", paramList.ToArray()));
        }

        public DataSet SelectAllTestGroup(string userID, bool includeElapsed, bool recentOnly, int testGroupID)
        {
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_selectalltestgroup",
                Helper.CreateParam("@UserID", SqlDbType.VarChar, userID),
                Helper.CreateParam("@IncludeElapsed", SqlDbType.Bit, includeElapsed),
                Helper.CreateParam("@RecentOnly", SqlDbType.Bit, recentOnly),
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public DataSet GetTeamSummaryTests()
        {
            return Connection.ExecuteSPQuery("pr_WeeklyTesting_Lkp_GetTeamSummaryTests");
        }

        public DataSet SelectPendingEssayExams()
        {
            return Connection.ExecuteSPQuery("pr_testcategory_lkp_selectpendingessayexams");
        }
    }
}
