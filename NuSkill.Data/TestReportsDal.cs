using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestReportsDal : Base
    {
        public TestReportsDal()
        { }

        public TestReportsDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_Reports_Lkp_SelectAll");
        }
    }
}
