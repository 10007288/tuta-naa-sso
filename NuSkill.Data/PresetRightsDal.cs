using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class PresetRightsDal : Base
    {
        public PresetRightsDal()
        { }

        public PresetRightsDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_presetrights_lkp_selectall");
        }
    }
}
