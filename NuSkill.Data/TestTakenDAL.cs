using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestTakenDAL : TheLibrary.DBImportTool.Base
    {
        public TestTakenDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public TestTakenDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public int Insert(int testCategoryID, string userID, string office, DateTime dateTaken)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pOffice", SqlDbType.VarChar, office));
            paramList.Add(Helper.CreateParam("@pDateTaken", SqlDbType.DateTime, dateTaken));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testTaken_sav_insert", paramList.ToArray()));
        }

        public DataSet Select(int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            return Connection.ExecuteSPQuery("pr_testtaken_lkp_single", paramList.ToArray());
        }

        public DataSet SelectByUser(string userID, bool recentOnly)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pRecentOnly", SqlDbType.Bit, recentOnly));
            return Connection.ExecuteSPQuery("pr_testtaken_lkp_byuser", paramList.ToArray());
        }

        public DataSet SelectByUserPassed(string userID, bool recentOnly, bool passed)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pRecentOnly", SqlDbType.Bit, recentOnly));
            paramList.Add(Helper.CreateParam("@pPassed", SqlDbType.Bit, passed));
            return Connection.ExecuteSPQuery("pr_testtaken_lkp_byuserpassed", paramList.ToArray());
        }

        public void UpdateTime(int testTakenID, DateTime dateLastSaved, int timeRemaining)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pDateLastSaved", SqlDbType.DateTime, dateLastSaved));
            paramList.Add(Helper.CreateParam("@pTimeRemaining", SqlDbType.Int, timeRemaining));
            Connection.ExecuteSPQuery("pr_testtaken_sav_updatetime", paramList.ToArray());
        }

        public int Count(string userID, int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testtaken_lkp_tries", paramList.ToArray()));
        }

        public DataSet SelectByExam(int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            return Connection.ExecuteSPQuery("pr_testtaken_lkp_examtakers", paramList.ToArray());
        }

        public void Finish(int testTakenID, DateTime dateEndTaken)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pDateEndTaken", SqlDbType.DateTime, dateEndTaken));
            Connection.ExecuteSP("pr_testtaken_sav_finish", paramList.ToArray());
        }

        public void SetPassed(int testTakenID, bool passed, int score)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pPassed", SqlDbType.Bit, passed));
            paramList.Add(Helper.CreateParam("@pScore", SqlDbType.Int, score));
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            Connection.ExecuteSP("pr_testtaken_setpassed", paramList.ToArray());
        }

        public DataSet SelectPendingRating(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_testtaken_selectpendingrating", paramList.ToArray());
        }

        public DataSet SelectCompleted(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_testtaken_selectcompleted", paramList.ToArray());
        }

        public DataSet SelectPendingCompletion(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_testtaken_selectpendingcompletion", paramList.ToArray());
        }

        public void UpdateScore(int testTakenID, int score, int passingScore)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pScore", SqlDbType.Int, score));
            paramList.Add(Helper.CreateParam("@pPassingScore", SqlDbType.Int, passingScore));
            Connection.ExecuteSP("pr_testtaken_sav_updatescore", paramList.ToArray());
        }

        public void Delete(int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            Connection.ExecuteSP("pr_testtaken_sav_delete", paramList.ToArray());
        }

        public DataSet GetTestTakensWithEssays()
        {
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_gettesttakenswithessays");
        }

        public DataSet GetTestTakensWithEssaysByTest(int testCategoryID)
        {
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_GetTestTakensWithEssaysByTest",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
        }

        public DataSet GetCheckedEssays(string userID)
        {
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_GetCheckedEssays",
                Helper.CreateParam("@UserID", SqlDbType.VarChar, userID));
        }

        public void UpdateScores(int testCategoryID)
        {
            Connection.ExecuteSP("pr_testtaken_sav_updatescores",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
        }

        public DataSet SelectEssaysByUser(string userID)
        {
            return Connection.ExecuteSPQuery("pr_testtaken_lkp_selectessaysbyuser",
                Helper.CreateParam("@userid", SqlDbType.VarChar, userID));
        }

        public DataSet GetResponsesWithChecker(int testtakenID)
        {
            return Connection.ExecuteSPQuery("pr_testtaken_lkp_getresponseswithchecker",
                Helper.CreateParam("@TestTakenID", SqlDbType.Int, testtakenID));
        }
    }
}
