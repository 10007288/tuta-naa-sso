using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class LoginSessionDAL : Base
    {
        public LoginSessionDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public LoginSessionDAL(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet Select(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_loginsession_lkp_getuser", paramList.ToArray());
        }

        public void Insert(string userID, Guid loginSessionID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pLoginSessionID", SqlDbType.UniqueIdentifier, loginSessionID));
            Connection.ExecuteSP("pr_loginsession_sav_insertnew", paramList.ToArray());
        }

        public void Update(string userID, Guid loginSessionID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pLoginSessionID", SqlDbType.UniqueIdentifier, loginSessionID));
            Connection.ExecuteSPQuery("pr_loginsession_sav_update", paramList.ToArray());
        }

        public void Delete(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            Connection.ExecuteSP("pr_loginsession_del_delete", paramList.ToArray());
        }
    }
}
