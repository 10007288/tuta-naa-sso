using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class RightsLevelDal : TheLibrary.DBImportTool.Base
    {
        public RightsLevelDal()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public RightsLevelDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_rightslevel_lkp_selectall");
        }

    }
}
