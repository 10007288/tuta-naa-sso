<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site_applicant.master"
    AutoEventWireup="true" CodeFile="applicantsallitems.aspx.cs" Inherits="applicantsallitems"
    Title="Transcom University Testing" EnableEventValidation="false" %>

<%@ Register TagPrefix="wus" TagName="examquestioncontrol" Src="~/controls/examquestioncontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table width="100%" cellspacing="0">
        <tr>
            <td colspan="3">
                <br />
                <asp:Label ID="lblNote" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="gvQuestions" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                    AllowSorting="false" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table width="100%" cellspacing="0">
                                    <tr>
                                        <td style="width: 100%">
                                            <asp:Label ID="lblGridQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %> '
                                                Visible="false" />
                                            <asp:Label ID="lblGridTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                Visible="false" />
                                            <wus:examquestioncontrol ID="wusQuestion" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                <asp:Label ID="lblOnce" runat="server" Text="Please click the buttons only once."
                    CssClass="smallText" />
            </td>
            <td align="center" style="width: 70%">
                <asp:Button ID="btnSaveAndQuit" runat="server" Text="Save and Resume Later" OnClick="btnSaveAndQuit_Click"
                    CssClass="buttons" Width="180px" Font-Size="larger" ForeColor="red" UseSubmitBehavior="false" />
                <asp:Button ID="btnContinue" runat="server" Text="I'm Finished!" OnClick="btnContinue_Click"
                    Width="180px" CssClass="buttons" Font-Size="larger" ForeColor="red" />
            </td>
            <td align="right" style="width: 15%">
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeNoReplacement" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlNoReplacement" TargetControlID="hidNoReplacement" />
    <asp:HiddenField ID="hidNoReplacement" runat="server" />
    <asp:Panel ID="pnlNoReplacement" runat="server" CssClass="modalPopup" Width="300px">
        <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
            vertical-align: middle; font-size: large; " width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="lblNoReplacement" runat="server" Text="You are not allowed to change your answer."
                        Font-Size="12px" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnNoReplacement" runat="server" Text="Next" OnClick="btnNoReplacement_Click"
                        Width="80" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeWarning" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidWarning" PopupControlID="pnlWarning" />
    <asp:HiddenField ID="hidWarning" runat="server" />
    <asp:Panel ID="pnlWarning" runat="server" CssClass="modalPopup" HorizontalAlign="center"
        Width="300px">
        <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
            vertical-align: middle; font-size: large; " width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblWarning" runat="server" Font-Size="12px" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnWarning" runat="server" Text="Return" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:AlwaysVisibleControlExtender ID="avcTimeRemaining" runat="server" TargetControlID="pnlTimer"
        VerticalSide="Top" HorizontalSide="Right" VerticalOffset="10" HorizontalOffset="10"
        ScrollEffectDuration=".1" />
    <asp:Panel ID="pnlTimer" runat="server" HorizontalAlign="center" BorderWidth="3px"
        Font-Size="12px" Width="200px" CssClass="modalPopup">
        <asp:UpdatePanel ID="udpTimer" runat="server" UpdateMode="conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="tmrTimeRemaining" EventName="Tick" />
            </Triggers>
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblClickWarning" runat="server" Text="If you did not click the Save and Resume/I'm Finished buttons, navigating away from this page MAY invalidate your test/survey!"
                                ForeColor="red" Font-Bold="true" />
                            <hr style="color: Black;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTimeRemaining" runat="server" Text="Time Remaining:" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblActualTimeLeft" runat="server" Visible="false" />
                            <asp:Label ID="lblTimeRemainingValue" runat="server" />
                            <asp:Timer ID="tmrTimeRemaining" runat="server" Interval="1000" Enabled="false" OnTick="tmrTimeRemaining_Tick" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpePnlTimeUp" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlTimeUp" TargetControlID="hidTimeUp" />
    <asp:HiddenField ID="hidTimeUp" runat="server" />
    <asp:Panel ID="pnlTimeUp" runat="server" HorizontalAlign="center" CssClass="modalPopup"
        Width="200px">
        <table width="100%" cellpadding="5">
            <tr>
                <td>
                    <asp:Label ID="lblTimeUp" runat="server" Text="Time's up!" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnContinuePopup" runat="server" Text="Continue" OnClick="btnContinue_Click"
                        Width="80px" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeMissingAnswer" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlMissingAnswer" TargetControlID="hidMissingAnswer" />
    <asp:HiddenField ID="hidMissingAnswer" runat="server" />
    <asp:Panel ID="pnlMissingAnswer" runat="server" HorizontalAlign="center" CssClass="modalPopup"
        Width="300px">
        <table width="100%" cellpadding="5">
            <tr>
                <td align="center">
                    Please check that you have answers for items in red.
                    <br />
                    <asp:Button ID="btnMissingAnswerOK" runat="server" Text="OK" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
