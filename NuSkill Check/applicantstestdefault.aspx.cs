using System;
using System.Web.UI;
using NuSkill.Business;

public partial class applicantstestdefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername) || SessionManager.SessionInstance != "ApplicantInstance")
            Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
        TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
        if (category != null)
        {
            //Questionnaire[] questionnaire = Questionnaire.SelectByCategory(SessionManager.SessionTestCategoryID);
            lblExamName.Text = category.TestName;
            if (category.TimeLimit == 0)
            {
                SessionManager.SessionTimedExam = false;
                lblDuration.Text = "No Time Limit";
            }
            else
            {
                SessionManager.SessionTimedExam = true;
                int trueTime = category.TimeLimit / 60;
                lblDurationVal.Text = trueTime.ToString() + " minutes";
            }
            if (!string.IsNullOrEmpty(category.Instructions))
                lblInstructions.Text = category.Instructions.Replace(Environment.NewLine, "<br />");
            else
            {
                tdInstructions.Align = "center";
                lblInstructionValue.Text = "None";
            }
        }
        else
            Response.Redirect("applicantstesthub.aspx?" + Request.QueryString);
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
        int tempQuestionNumber = 0;
        if (category.IsEssay)
            SessionManager.SessionIsEssay = true;
        else
            SessionManager.SessionIsEssay = false;
        if (SessionManager.SessionExamAction == "resumeexam")
        {
            foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
            {
                SaveTestResponse response = SaveTestResponse.Retrieve(SessionManager.SessionUsername, SessionManager.SessionTestTaken.TestTakenID, question.QuestionnaireID);
                if (response != null)
                    tempQuestionNumber++;
            }
            SessionManager.SessionTestTaken.DateLastSave = DateTime.Now;
            SessionManager.SessionTestTaken.UpdateTime();
        }
        else if (SessionManager.SessionExamAction == "newexam")
        {
            TestTaken taken = new TestTaken(SessionManager.SessionTestCategoryID, SessionManager.SessionUsername, SessionManager.SessionCompanySite, DateTime.Now);
            taken.Office = SessionManager.SessionSite;
            int v = taken.Insert();
            SessionManager.SessionTestTaken = TestTaken.Select(v);
            SessionManager.SessionTestTaken.MinutesRemaining = category.TimeLimit;
            SessionManager.SessionTestTaken.DateLastSave = DateTime.Now;
            SessionManager.SessionTestTaken.UpdateTime();
            SessionManager.SessionExamAction = "resumeexam";
        }
        SessionManager.SessionQuestionNumber = tempQuestionNumber;
        Response.Redirect("applicantsallitems.aspx?" + Request.QueryString);
    }
}
