using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class alltests : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            try
            {
                TestCategoryUserHistory[] categories = null;
                //if (SessionManager.SessionAccountCampaignID < 0)
                //    categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, SessionManager.SessionAccountCampaignID * -1, SessionManager.SessionSubCategory);
                if (SessionManager.SessionAccountCampaignID > 0 || SessionManager.SessionSubCategory > 0)
                    categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, true, SessionManager.SessionAccountCampaignID, SessionManager.SessionSubCategory);
                else if (SessionManager.SessionGroupID != 0)
                    categories = TestCategoryUserHistory.SelectAllTestGroup(SessionManager.SessionUsername, false, false, SessionManager.SessionGroupID);
                else
                    categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, 0, SessionManager.SessionSubCategory);
                this.gvAvailableTests.DataSource = categories;
                this.gvAvailableTests.DataBind();
                foreach (GridViewRow row in this.gvAvailableTests.Rows)
                {
                    Label lblPassed = row.FindControl("lblPassed") as Label;
                    Label lblTopScore = row.FindControl("lblTopScore") as Label;
                    Image imgPassOrFail = row.FindControl("imgPassOrFail") as Image;
                    if (lblPassed != null && imgPassOrFail != null && lblTopScore != null)
                    {
                        if (lblTopScore.Text == "-1")
                        {
                            lblTopScore.Text = string.Empty;
                            imgPassOrFail.Visible = false;
                        }
                        else if (lblPassed.Text == "True")
                            imgPassOrFail.ImageUrl = "~/images/icon-pass.gif";
                        else if (lblTopScore.Text != "0")
                            imgPassOrFail.ImageUrl = "~/images/icon-fail.gif";
                        else
                        {
                            lblTopScore.Text = string.Empty;
                            imgPassOrFail.Visible = false;
                        }
                    }
                    //    Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                    //    Label lblTriesLeft = row.FindControl("lblTriesLeft") as Label;
                    //    TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);
                    //    int totCount = TestTaken.Count(SessionManager.SessionUsername, Convert.ToInt32(lblTestCategoryID.Text.Trim()));
                    //    int triesLeft = category.TestLimit - totCount;
                    //    lblTriesLeft.Text = triesLeft.ToString() + "/" + category.TestLimit.ToString();
                    //    if (triesLeft < 1)
                    //        lblTriesLeft.ForeColor = System.Drawing.Color.Red;
                }
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(4, "alltests.aspx", "Page_Load", ex.Message);
                SessionManager.SessionMainText = "System could not populate the test list. Please contact your examiner/administrator.";
                Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
            }
        }
    }

    protected void gvAvailableTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool redirect = false;
        if (e.CommandName == "Take")
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                if (category != null)
                {
                    int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
                    if (testCount < category.TestLimit || category.TestLimit == 0)
                    {
                        SessionManager.SessionTestCategoryID = category.TestCategoryID;
                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questionnaire.Length > 0)
                        {
                            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                            redirect = true;
                        }
                        else
                        {
                            this.lblMessage.Text = "This exam has no items.";
                            this.mpeMessage.Show();
                        }
                    }
                    else
                    {
                        this.lblMessage.Text = "You have already used up all tries available for this test.";
                        this.mpeMessage.Show();
                    }
                }
                else
                {
                    lblMessage.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
                    this.mpeMessage.Show();
                }
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(4, "alltests.aspx", "gvAvailableTests_RowCommand", ex.Message);
                redirect = false;
                SessionManager.SessionMainText = "An error was encountered while trying to load an exam.";
                Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
            }
        }
        if (redirect)
        {
            SessionManager.SessionExamAction = "newexam";
            Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
        }
    }
}
