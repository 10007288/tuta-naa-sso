using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class registration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(this.txtFirstName.Text.Trim()))
            {
                isValid = false;
                this.cvFirstName.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtLastName.Text.Trim()))
            {
                isValid = false;
                this.cvLastName.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtLocation.Text.Trim()))
            {
                isValid = false;
                this.cvLocation.IsValid = false;
            }
            if (string.IsNullOrEmpty(this.txtPhoneNumber.Text.Trim()))
            {
                isValid = false;
                this.cvPhoneNumber.IsValid = false;
            }
            if (!isValid)
                return;

            if (!Registration.CheckExists(this.txtFirstName.Text.Trim(), this.txtLastName.Text.Trim(), this.txtPhoneNumber.Text.Trim(), this.txtLocation.Text.Trim()))
            {
                Registration reg = new Registration();
                reg.AutoUserID = "an" + DateTime.Now.Ticks.ToString().Substring(DateTime.Now.Ticks.ToString().Length-8, 8);
                reg.AutoPassword = "an" + DateTime.Now.Ticks.ToString().Substring(DateTime.Now.Ticks.ToString().Length - 8, 8);
                reg.FirstName = this.txtFirstName.Text.Trim();
                reg.LastName = this.txtLastName.Text.Trim();
                reg.Location = this.txtLocation.Text.Trim();
                reg.PhoneNumber = this.txtPhoneNumber.Text.Trim();
                reg.Insert();
                this.lblResult.Text = "Your username is " + reg.AutoUserID + ". \nYour password is " + reg.AutoPassword + ".";
                this.mpeFinished.Show();
            }
        }
        catch(Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "registration.aspx", "btnRegister_Click", ex.Message);
            SessionManager.SessionMainText = "An error occured while registering.";
            Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
        }
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/default.aspx?" + Request.QueryString.ToString());
    }
}
