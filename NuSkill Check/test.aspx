<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site.master"
    AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="test" Title="Transcom University Testing" %>

<%@ Register TagPrefix="wus" TagName="testcontrol" Src="~/controls/examquestioncontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:UpdatePanel ID="udpSub" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <wus:testcontrol ID="testControl1" runat="server" />
            <table width="100%">
                <tr>
                    <td style="width: 33%">
                        <asp:Label ID="lblOnce" runat="server" Text="Please click the buttons only once."
                            CssClass="smallText" />
                    </td>
                    <td align="center" style="width: 34%">
                        <asp:Button ID="btnSaveAndQuit" runat="server" Text="Save and Quit" OnClick="btnSaveAndQuit_Click" CssClass="buttons" />
                    </td>
                    <td align="right" style="width: 33%">
                        <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click"
                            Width="80px" CssClass="buttons" />
                    </td>
                </tr>
            </table>
            <ajax:ModalPopupExtender ID="mpeSaveAndQuit" runat="server" BackgroundCssClass="modalBackground"
                PopupControlID="pnlSaveAndQuit" TargetControlID="hidSaveAndQuit" />
            <asp:HiddenField ID="hidSaveAndQuit" runat="server" />
            <asp:Panel ID="pnlSaveAndQuit" runat="server" CssClass="modalPopup">
                <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
                    vertical-align: middle; font-size: large; ">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblSaveAndQuit" runat="server" Text="Save and Quit?" Font-Size="12" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnContinueSaveAndQuit" runat="server" Text="Save And Quit" Width="120px"
                                OnClick="btnContinueSaveAndQuit_Click" CssClass="buttons" />
                        </td>
                        <td align="right">
                            <asp:Button ID="btnCancelSaveAndQuit" runat="server" Text="Cancel" Width="120px"
                                OnClick="btnCancelSaveAndQuit_Click" CssClass="buttons" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajax:ModalPopupExtender ID="mpeNoReplacement" runat="server" BackgroundCssClass="modalBackground"
                PopupControlID="pnlNoReplacement" TargetControlID="hidNoReplacement" />
            <asp:HiddenField ID="hidNoReplacement" runat="server" />
            <asp:Panel ID="pnlNoReplacement" runat="server" CssClass="modalPopup" Width="300px">
                <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
                    vertical-align: middle; font-size: large; "
                    width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblNoReplacement" runat="server" Text="You are not allowed to change your answer."
                                Font-Size="12px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnNoReplacement" runat="server" Text="Next" OnClick="btnNoReplacement_Click"
                                Width="80" CssClass="buttons" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajax:ModalPopupExtender ID="mpeMessage" runat="server" BackgroundCssClass="modalBackground"
                PopupControlID="pnlNoReplacement" TargetControlID="hidNoReplacement" />
            <asp:HiddenField ID="hidMessage" runat="server" />
            <asp:Panel ID="pnlMessage" runat="server" CssClass="modalPopup" Width="300px">
                <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
                    vertical-align: middle; font-size: large; "
                    width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblMessage" runat="server" Font-Size="12px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnMessage" runat="server" Text="Finish" Width="80" CssClass="buttons" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
