using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class testend : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
        if (category != null)
        {
            if (category.IsEssay)
            {
                this.lblAbout.Text = "Your exam will be graded after an examiner checks the essay portion/s.";
                this.btnContinue.Text = "Return";
            }
        }
        else
        {
            this.lblAbout.Text = "System could not find this exam. Please contact your examiner/administrator.";
            SessionManager.SessionVarText = "404exam";
        }
            //Response.Redirect("~/testhub.aspx");
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (SessionManager.SessionVarText == "404exam")
        {
            SessionManager.SessionVarText = string.Empty;
            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
        }
        if (SessionManager.SessionIsEssay)
            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
        else
            Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
    }
}
