using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for GenericMethods
/// </summary>
public class GenericMethods
{
    public GenericMethods()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string ConvertSecondsToTime(int time)
    {
        string seconds = string.Empty;
        string mintues = string.Empty;
        string hour = string.Empty;
        int hr = 0;
        int min = 0;
        int sec = 0;

        hr = time / 3600;
        min = (time / 60) - (hr * 60);
        sec = time - ((hr * 3600) + (min * 60));
        seconds = sec.ToString("00");
        mintues = min.ToString("00");
        hour = hr.ToString("#00");
        return hour + ":" + mintues + ":" + seconds;
        /*
         * 123456 seconds
         * 36 seconds
         * 17 minutes
         * 34 hours
         */
    }
}
