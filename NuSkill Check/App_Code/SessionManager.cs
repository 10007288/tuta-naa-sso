using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

/// <summary>
/// Summary description for SessionManager
/// </summary>
public class SessionManager
{
    private const string sessionQuestionNumber = "sessionQuestionNumber";
    private const string sessionUsername = "sessionUsername";
    private const string sessionTestCategoryID = "sessionTestCategoryID";
    private const string sessionQuestionaire = "sessionQuestionnaire";
    private const string sessionSaveTestResponse = "sessionSaveTestResponse";
    private const string sessionTestTaken = "sessionTestTakenID";
    private const string sessionIsEssay = "sessionIsEssay";
    private const string sessionSessionID = "sessionSessionID";
    private const string sessionExamAction = "sessionExamAction";
    private const string sessionTimedExam = "sessionTimedExam";
    private const string sessionVarText = "sessionVarText";
    private const string sessionMainText = "sessionMainText";
    private const string sessionRolItems = "sessionRolItems";
    private const string sessionRolInitItems = "sessionRolInitItems";
    private const string sessionCompanySite = "sessionCompanySite";
    private const string sessionCim = "sessionCim";
    private const string sessionAccountCampaignID = "sessionAccountCampaignID";
    private const string sessionSubCategory = "sessionSubcategory";
    private const string sessionSite = "sessionSite";
    private const string sessionGroupID = "sessionGroupID";
    private const string sessionDT = "sessionTable";
    private const string sessionExamIsRetake = "sessionIsRetake";
    //Added by Jay Millare (01/12/2015)
    private const string sessionInstance = "sessionInstance";

    public static DataTable SessionDT
    {

        //get
        //{
        //    if (HttpContext.Current.Session[sessionRolItems] == null)
        //        HttpContext.Current.Session[sessionRolItems] = new List<TempQuestionnaire>();
        //    return HttpContext.Current.Session[sessionRolItems] as List<TempQuestionnaire>;
        //}
        get 
        { 
            if (HttpContext.Current.Session[sessionDT] == null)
                HttpContext.Current.Session[sessionDT] = new DataTable();
            return HttpContext.Current.Session[sessionDT] as DataTable;
            //return HttpContext.Current.Session[sessionDT] As DataTable; 
        }
        set { HttpContext.Current.Session[sessionDT] = value; }
    }

    public static int SessionQuestionNumber
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[sessionQuestionNumber]); }
        set { HttpContext.Current.Session[sessionQuestionNumber] = value; }
    }

    public static string SessionUsername
    {
        get
        {
            if (HttpContext.Current.Session[sessionUsername] == null)
                return string.Empty;
            return HttpContext.Current.Session[sessionUsername].ToString();
        }
        set { HttpContext.Current.Session[sessionUsername] = value; }
    }

    public static int SessionTestCategoryID
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[sessionTestCategoryID]); }
        set { HttpContext.Current.Session[sessionTestCategoryID] = value; }
    }

    public static List<Questionnaire> SessionQuestionnaire
    {
        get
        {
            if (HttpContext.Current.Session[sessionQuestionaire] == null)
                HttpContext.Current.Session[sessionQuestionaire] = new List<Questionnaire>();
            return HttpContext.Current.Session[sessionQuestionaire] as List<Questionnaire>;
        }
        set { HttpContext.Current.Session[sessionQuestionaire] = value; }
    }

    public static List<SaveTestResponse> SessionSaveTestResponse
    {
        get
        {
            if (HttpContext.Current.Session[sessionSaveTestResponse] == null)
                HttpContext.Current.Session[sessionSaveTestResponse] = new List<SaveTestResponse>();
            return HttpContext.Current.Session[sessionSaveTestResponse] as List<SaveTestResponse>;
        }
        set { HttpContext.Current.Session[sessionSaveTestResponse] = value; }
    }

    public static TestTaken SessionTestTaken
    {
        get
        {
            if (HttpContext.Current.Session[sessionTestTaken] == null)
                HttpContext.Current.Session[sessionTestTaken] = new TestTaken();
            return HttpContext.Current.Session[sessionTestTaken] as TestTaken;
        }
        set { HttpContext.Current.Session[sessionTestTaken] = value; }
    }

    public static bool SessionIsEssay
    {
        get
        {
            if (HttpContext.Current.Session[sessionIsEssay] == null)
                HttpContext.Current.Session[sessionIsEssay] = new bool();
            return (bool)HttpContext.Current.Session[sessionIsEssay];
        }
        set { HttpContext.Current.Session[sessionIsEssay] = value; }
    }

    public static Guid SessionSessionID
    {
        get { return (Guid)HttpContext.Current.Session[sessionSessionID]; }
        set { HttpContext.Current.Session[sessionSessionID] = value; }
    }

    public static string SessionExamAction
    {
        get { return HttpContext.Current.Session[sessionExamAction] as string; }
        set { HttpContext.Current.Session[sessionExamAction] = value; }
    }

    //TODO: GLA 01092014
    public static string SessionExamIsRetake
    {
        get { return HttpContext.Current.Session[sessionExamIsRetake] as string; }
        set { HttpContext.Current.Session[sessionExamIsRetake] = value; }
    }

    public static bool SessionTimedExam
    {
        get { return (bool)HttpContext.Current.Session[sessionTimedExam]; }
        set { HttpContext.Current.Session[sessionTimedExam] = value; }
    }

    public static string SessionVarText
    {
        get { return HttpContext.Current.Session[sessionVarText] as string; }
        set { HttpContext.Current.Session[sessionVarText] = value; }
    }

    public static string SessionMainText
    {
        get { return HttpContext.Current.Session[sessionMainText] as string; }
        set { HttpContext.Current.Session[sessionMainText] = value; }
    }

    public static List<TempQuestionnaire> SessionRolItems
    {
        get
        {
            if (HttpContext.Current.Session[sessionRolItems] == null)
                HttpContext.Current.Session[sessionRolItems] = new List<TempQuestionnaire>();
            return HttpContext.Current.Session[sessionRolItems] as List<TempQuestionnaire>;
        }
        set { HttpContext.Current.Session[sessionRolItems] = value; }
    }

    public static List<TempQuestionnaire> SessionRolInitItems
    {
        get
        {
            if (HttpContext.Current.Session[sessionRolInitItems] == null)
                HttpContext.Current.Session[sessionRolInitItems] = new List<TempQuestionnaire>();
            return HttpContext.Current.Session[sessionRolInitItems] as List<TempQuestionnaire>;
        }
        set { HttpContext.Current.Session[sessionRolInitItems] = value; }
    }

    public static string SessionCompanySite
    {
        get { return HttpContext.Current.Session[sessionCompanySite] as string; }
        set { HttpContext.Current.Session[sessionCompanySite] = value; }
    }

    public static int SessionCim
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[sessionCim]); }
        set { HttpContext.Current.Session[sessionCim] = value; }
    }

    public static int SessionAccountCampaignID
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[sessionAccountCampaignID]); }
        set { HttpContext.Current.Session[sessionAccountCampaignID] = value;}
    }

    public static int SessionSubCategory
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[sessionSubCategory]); }
        set { HttpContext.Current.Session[sessionSubCategory] = value; }
    }

    public static string SessionSite
    {
        get { return HttpContext.Current.Session[sessionSite] as string; }
        set { HttpContext.Current.Session[sessionSite] = value; }
    }

    public static int SessionGroupID
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[sessionGroupID]); }
        set { HttpContext.Current.Session[sessionGroupID] = value; }
    }
    //Added by Jay Millare (01/12/2015)
    public static string SessionInstance
    {
        get
        {
            if (HttpContext.Current.Session[sessionInstance] == null)
                return string.Empty;
            return HttpContext.Current.Session[sessionInstance].ToString();
        }
        set { HttpContext.Current.Session[sessionInstance] = value; }
    }

    public SessionManager()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
