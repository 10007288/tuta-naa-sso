using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Config
/// </summary>
public class Config
{
    public Config()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string ApplicationName
    {
        get { return String.IsNullOrEmpty(ConfigurationManager.AppSettings["ApplicationName"].Trim()) ? string.Empty : ConfigurationManager.AppSettings["ApplicationName"].Trim(); }
    }

    public static string Vader2DatabaseServer()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("Vader2DatabaseServer", typeof(System.String)));
    }

    public static string Vader2DatabaseName()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("Vader2DatabaseName", typeof(System.String)));
    }

    public static string Vader2DatabaseUsername()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("Vader2DatabaseUsername", typeof(System.String)));
    }

    public static string Vader2DatabasePassword()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("Vader2DatabasePassword", typeof(System.String)));
    }

    public static string TestingDatabaseServer()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("TestingDatabaseServer", typeof(System.String)));
    }

    public static string TestingDatabaseName()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("TestingDatabaseName", typeof(System.String)));
    }

    public static string TestingDatabaseUsername()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("TestingDatabaseUsername", typeof(System.String)));
    }

    public static string TestingDatabasePassword()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("TestingDatabasePassword", typeof(System.String)));
    }

    public static string HotspotImgeLocation()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("HotspotImageLocation", typeof(System.String)));
    }

    public static string Vader2CimEnterpriseConnectionString()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("Vader2CimEnterpriseConnectionString", typeof(System.String)));
    }

    public static bool AcceptInternalLogin()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("AcceptInternalLogin", typeof(System.String))) == "1" ? true : false;
    }

    public static bool AcceptExternalLogin()
    {
        AppSettingsReader ar = new AppSettingsReader();
        return Convert.ToString(ar.GetValue("AcceptExternalLogin", typeof(System.String))) == "1" ? true : false;
    }
}
