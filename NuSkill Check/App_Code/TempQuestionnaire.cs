using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

[Serializable]
/// <summary>
/// Summary description for TempQuestionnaire
/// </summary>
public class TempQuestionnaire
{
    public TempQuestionnaire(string str)
    {
        this._item = str;
    }

    private string _item;

    public string Item
    {
        get { return _item; }
        set { _item = value; }
    }
}
