using System;
using System.Web.UI;

public partial class applicantserror : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = SessionManager.SessionMainText;
    }
    protected void btnError_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantsmain.aspx?" + Request.QueryString);
    }
}
