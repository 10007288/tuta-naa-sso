using System;
using System.Web.UI;
using NuSkill.Business;

public partial class testend : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername) || SessionManager.SessionInstance != "ApplicantInstance")
            Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
        TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
        if (category != null)
        {
            if (category.IsEssay)
            {
                lblAbout.Text = "Your exam will be graded after an examiner checks the essay portion/s.";
                btnContinue.Text = "Return";
            }
        }
        else
        {
            lblAbout.Text = "System could not find this exam. Please contact your examiner/administrator.";
            SessionManager.SessionVarText = "404exam";
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (SessionManager.SessionVarText == "404exam")
        {
            SessionManager.SessionVarText = string.Empty;
            Response.Redirect("applicantstesthub.aspx?" + Request.QueryString);
        }
        if (SessionManager.SessionIsEssay)
            Response.Redirect("applicantstesthub.aspx?" + Request.QueryString);
        else
            Response.Redirect("applicantsgradeexam.aspx?" + Request.QueryString);
    }
}
