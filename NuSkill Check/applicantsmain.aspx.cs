using System;
using System.Web.UI;

public partial class applicantsmain : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void lnkTestHub_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername) || SessionManager.SessionInstance != "ApplicantInstance")
            Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
        Response.Redirect("applicantstesthub.aspx?" + Request.QueryString);
    }
}
