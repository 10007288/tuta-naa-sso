﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using TheLibrary.ErrorLogger;
using NuSkill.Business;

public partial class applicantslogin : Page
{
    private Int64 _applicantId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session.Clear();
        }
    }

    protected void lnkLogin_Click(object sender, EventArgs e)
    {
        bool isValid = true;
        if (string.IsNullOrEmpty(txtFirstName.Text.Trim()))
        {
            isValid = false;
            cvFirstName.IsValid = false;
        }
        if (string.IsNullOrEmpty(txtLastName.Text.Trim()))
        {
            isValid = false;
            cvLastName.IsValid = false;
        }
        if (string.IsNullOrEmpty(ddlSites.Text.Trim()))
        {
            isValid = false;
            cvLocation.IsValid = false;
        }
        if (string.IsNullOrEmpty(txtATSID.Text.Trim()))
        {
            isValid = false;
            cvATSID.IsValid = false;
        }

        if (!isValid)
            return;

        if (LoadVerifyUser2() && Config.AcceptExternalLogin())
        {
            //string loginName = txtFirstName.Text + " " + txtLastName.Text;
            string loginName = _applicantId.ToString();
            SessionManager.SessionSessionID = Guid.NewGuid();
            SessionManager.SessionUsername = loginName.Trim();
            SessionManager.SessionInstance = "ApplicantInstance";
            LoginSession session = LoginSession.Select(loginName.Trim());

            if (session == null)
            {
                try
                {
                    session = new LoginSession(loginName.Trim(), SessionManager.SessionSessionID);
                    session.Insert();
                }
                catch (Exception ex)
                {
                    ErrorLogger.Write(Config.ApplicationName, "applicantslogin.aspx", "lnkLogin_Click", ex.Message);
                    SessionManager.SessionMainText = "System could not log you in. Please try again later.";
                    Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
                }
            }
            else if (session.LoginSessionID != SessionManager.SessionSessionID)
                try
                {
                    session.Update();
                }
                catch (Exception ex)
                {
                    ErrorLogger.Write(Config.ApplicationName, "applicantslogin.aspx", "lnkLogin_Click", ex.Message);
                    SessionManager.SessionMainText = "System could not log you in. Please try again later.";
                    Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
                }

            SessionManager.SessionSite = "EXT";
            Response.Redirect("applicantstests.aspx?" + Request.QueryString);
        }
        else
            cvLoginError.IsValid = false;
            //lblNotify.Text = "Invalid login credentials!";
    }

    private bool LoadVerifyUser2()
    {
        bool retVal;
        string constring = ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constring))
        {
            using (SqlCommand cmd = new SqlCommand("pr_Validate_ApplicantLogin", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text.Trim());
                cmd.Parameters.AddWithValue("@LastName", txtLastName.Text.Trim());
                cmd.Parameters.AddWithValue("@ATSID", txtATSID.Text.Trim());
                cmd.Parameters.AddWithValue("@Site", ddlSites.Text.Trim());
                cmd.Parameters.Add("@Count", SqlDbType.NVarChar, 30);
                cmd.Parameters["@Count"].Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                int count = 0;
                if (!string.IsNullOrEmpty(cmd.Parameters["@Count"].Value.ToString()))
                {
                    count = Convert.ToInt32(cmd.Parameters["@Count"].Value);
                }

                if (count >= 1)
                {
                    retVal = true;
                    _applicantId = count;
                }
                else
                {
                    retVal = false;
                }
            }
        }

        return retVal;
    }

    protected void lnkRegister_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionMainText))
            Response.Redirect("applicantsregistration.aspx?" + Request.QueryString);
        else if (!string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("applicantstesthub.aspx?" + Request.QueryString);
    }
}
