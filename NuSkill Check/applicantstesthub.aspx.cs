using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuSkill.Business;

public partial class applicantstesthub : Page
{
    private const string viewstateCategoryID = "viewstateCategoryID";

    private int CategoryID
    {
        get { return (int)ViewState[viewstateCategoryID]; }
        set { ViewState[viewstateCategoryID] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername) || SessionManager.SessionInstance != "ApplicantInstance")
            Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
        if (!IsPostBack)
        {
            PopulateGrids();
            
            foreach (GridViewRow row in gvRecentTests.Rows)
             {
                Image image = row.FindControl("imgTestStatus") as Image;
                Label label = row.FindControl("lblTestName") as Label;
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                //Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                //Label lblStartDate = row.FindControl("lblStartDate") as Label;
                Label lblEndDate = row.FindControl("lblEndDate") as Label;
                LinkButton lnkRetry = row.FindControl("lnkRetry") as LinkButton;
                LinkButton lnkResume = row.FindControl("lnkResume") as LinkButton;
                //Label lblDateLastSave = row.FindControl("DateLastSave") as Label;
                Label lblPassed = row.FindControl("lblPassed") as Label;
                Label lblResponseCount = row.FindControl("lblResponseCount") as Label;

                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);
                if (category.HideScores)
                {
                    image.Visible = false;
                    lnkRetry.Visible = false;
                }
                if (string.IsNullOrEmpty(lblEndDate.Text))
                {
                    image.ImageUrl = "~/images/icon-resume.gif";
                    lnkRetry.Visible = false;
                    lnkResume.Visible = true;
                }
                else if (Convert.ToInt32(lblResponseCount.Text) > 0)
                    image.ImageUrl = "~/images/icon-pending.gif";
                else if (lblPassed.Text == "True" || lblPassed.Text == "")
                {
                    image.ImageUrl = "~/images/icon-pass.gif";
                }
                else
                    image.ImageUrl = "~/images/icon-fail.gif";

                if (label.Text.Length > 50)
                    label.Text = label.Text.Substring(0, 47) + "...";
            }

            PopulateDropDownLists();
        }
    }

    protected void PopulateGrids()
    {
        DataSet taken = TestTaken.SelectByUser(SessionManager.SessionUsername, true);
        gvRecentTests.DataSource = taken;
        gvRecentTests.DataBind();
    }

    protected void PopulateDropDownLists()
    {
        ddlCampaigns.DataSource = NonCimCampaign.SelectParents();
        ddlCampaigns.DataBind();
        ddlSubcategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(ddlCampaigns.SelectedValue), true);
        ddlSubcategory.DataBind();
    }

    protected void ddlCampaigns_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSubcategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(ddlCampaigns.SelectedValue),true);
        ddlSubcategory.DataBind();
    }

    protected void gvRecentTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Retry")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
            TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
            if (category != null)
            {
                int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
                int testRetake = TestCategory.checkRetake(category.TestCategoryID);
                if (testCount < category.TestLimit || category.TestLimit == 0 || testRetake ==1)
                {

                    SaveTestResponse[] responses = SaveTestResponse.CheckPendingEssays(SessionManager.SessionUsername, Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    if (responses.Length > 0)
                    {
                        lblMessage.Text = "You have taken this test. It is still pending completion/checking.";
                        mpeMessage.Show();
                    }
                    else
                    {
                        SessionManager.SessionTestCategoryID = category.TestCategoryID;
                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questionnaire.Length > 0)
                        {
                            bool hasPassed = false;
                            TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                            foreach (TestTaken test in taken)
                            {
                                if (test.Passed && test.UserID == SessionManager.SessionUsername)
                                    hasPassed = true;
                            }
                            if (hasPassed && testRetake != 1)
                            {
                                CategoryID = category.TestCategoryID;
                                mpeRetake.Show();
                            }
                            else
                            {
                                SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                                foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                                SessionManager.SessionExamAction = "newexam";
                                SessionManager.SessionExamIsRetake = "retake";
                                Response.Redirect("applicantstestdefault.aspx?" + Request.QueryString);
                            }
                        }
                        else
                        {
                            lblMessage.Text = "This test has no items.";
                            mpeMessage.Show();
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "You have already used up all tries available for this test.";
                    mpeMessage.Show();
                }
            }
            else
            {
                lblMessage.Text = "This exam has been removed from the system or has already expired.";
                mpeMessage.Show();
            }
        }
        else if (e.CommandName == "Resume")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
            TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
            if (category != null)
            {
                SessionManager.SessionTestCategoryID = category.TestCategoryID;
                Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                if (questionnaire.Length > 0)
                {
                    TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    if (taken != null && taken.DateEndTaken.ToString().Contains("1/1/0001"))
                    {
                        SessionManager.SessionTestTaken = taken;
                        SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                        foreach (Questionnaire question in questionnaire)
                            SessionManager.SessionQuestionnaire.Add(question);
                        SessionManager.SessionExamAction = "resumeexam";
                        SessionManager.SessionExamIsRetake = "";
                        Response.Redirect("applicantstestdefault.aspx?" + Request.QueryString);
                    }
                    else
                    {
                        lblMessage.Text = "This test has been completed.";
                        mpeMessage.Show();
                    }
                }
                else
                {
                    lblMessage.Text = "This test has no items.";
                    mpeMessage.Show();
                }
            }
            else
            {
                lblMessage.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
                mpeMessage.Show();
            }
        }
    }

    protected void gvAvailableTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Take")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
            TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
            if (category != null)
            {
                int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
                int testRetake = TestCategory.checkRetake(category.TestCategoryID);
                if (testCount < category.TestLimit || category.TestLimit == 0 || testRetake == 1)
                  {
                    bool hasPassed = false;
                    TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                    foreach (TestTaken test in taken)
                    {
                        if (test.Passed)
                        {
                            hasPassed = true;
                            break;
                        }
                    }
                    if (hasPassed && testRetake != 1)
                    {
                        CategoryID = category.TestCategoryID;
                        mpeRetake.Show();
                    }
                    else
                    {
                        SessionManager.SessionTestCategoryID = category.TestCategoryID;
                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questionnaire.Length > 0)
                        {
                            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                            SessionManager.SessionExamAction = "newexam";
                            Response.Redirect("applicantstestdefault.aspx?" + Request.QueryString);
                        }
                        else
                        {
                            lblMessage.Text = "This exam has no items.";
                            mpeMessage.Show();
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "You have already used up all tries available for this test.";
                    mpeMessage.Show();
                }
            }
            else
            {
                lblMessage.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
                mpeMessage.Show();
            }
        }
    }

    protected void lnkMoreAvailable_Click(object sender, EventArgs e)
    {
        SessionManager.SessionAccountCampaignID = 0;
        Response.Redirect("applicantsalltests.aspx?" + Request.QueryString);
    }
    protected void btnOKRetake_Click(object sender, EventArgs e)
    {
        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(CategoryID);
        if (questionnaire.Length < 1)
        {
            lblMessage.Text = "This exam has no items.";
            mpeMessage.Show();
        }
        else
        {
            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
            SessionManager.SessionExamAction = "newexam";
            SessionManager.SessionTestCategoryID = CategoryID;
            foreach (Questionnaire question in questionnaire)
                SessionManager.SessionQuestionnaire.Add(question);
            Response.Redirect("applicantstestdefault.aspx?" + Request.QueryString);
        }
    }
    protected void btnMessage_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantstesthub.aspx?" + Request.QueryString);
    }
    protected void lnkMoreTaken_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantstakentests.aspx?" + Request.QueryString);
    }
    protected void btnCancelRetake_Click(object sender, EventArgs e)
    {
        PopulateGrids();
    }

    protected void btnViewTests_Click(object sender, EventArgs e)
    {
        SessionManager.SessionAccountCampaignID = Convert.ToInt32(ddlCampaigns.SelectedValue);
        SessionManager.SessionSubCategory = Convert.ToInt32(ddlSubcategory.SelectedValue);
        Response.Redirect("applicantsalltests.aspx?" + Request.QueryString);
    }
}