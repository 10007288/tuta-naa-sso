<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site.master"
    AutoEventWireup="true" CodeFile="userinfo.aspx.cs" Inherits="userinfo" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Width="100%">
        <table cellpadding="10" cellspacing="0" style="text-align: left" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblUserInfo" runat="server" Text="User Information" Font-Size="large"
                        Font-Names="Arial" /><br />
                    <hr style="color: Black" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblFirstName" runat="server" Text="First Name:" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtFirstName" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLastName" runat="server" Text="Last Name:" />
                            </td>
                            <td>
                                <asp:Label ID="txtLastName" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLocation" runat="server" Text="Location:" />
                            </td>
                            <td>
                                <asp:Label ID="txtLocation" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPassword" runat="server" Text="Password:" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password:" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
