using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using TheLibrary.DBImportTool;
using AjaxControlToolkit;

public partial class test : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
            if (SessionManager.SessionQuestionnaire == null)
                Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
            if (SessionManager.SessionQuestionNumber < SessionManager.SessionQuestionnaire.Count)
            {
                Questionnaire question = SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber];
                if (question == null)
                    Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
            }
            else
            {
                Questionnaire question = SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionnaire.Count - 1];
                if (question == null)
                    Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
            }
            testControl1.PnlEssay.Visible = false;
            testControl1.PnlExamQuestion.Visible = true;
            testControl1.PnlFillInTheBlanks.Visible = false;
            testControl1.PnlHotspot.Visible = false;
            testControl1.PnlMultipleChoice.Visible = false;
            testControl1.PnlSequencing.Visible = false;
            testControl1.PnlTrueOrFalse.Visible = false;
            //Questionnaire q = new Questionnaire();
        }
        catch(Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "test.aspx", "Page_Load", ex.Message);
            SessionManager.SessionMainText = "An error was encountered while initializing the test page.";
            Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
        }
        if (!this.IsPostBack)
        {
            this.GeneratePage();
        }
    }

    protected void btnSaveAndQuit_Click(object sender, EventArgs e)
    {
        this.GeneratePage();
        this.mpeSaveAndQuit.Show();
    }
    protected void btnContinueSaveAndQuit_Click(object sender, EventArgs e)
    {
        //Set TimeRemaining
        TestTaken testTaken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
        TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
        TimeSpan span = new TimeSpan();

        if (testTaken.DateLastSave.ToString().Contains("1/1/0001"))
            span = System.DateTime.Now.Subtract(testTaken.DateStartTaken);
        else
            span = System.DateTime.Now.Subtract(testTaken.DateLastSave.Value);

        //check if time difference is greater than the test time limit
        if (span.TotalSeconds > category.TimeLimit)
        {
            ///TODO: Error trap tests that have no more time left
        }
        else
        {
            testTaken.DateLastSave = System.DateTime.Now;
            testTaken.MinutesRemaining = category.TimeLimit - Convert.ToInt32(span.TotalSeconds);
            testTaken.UpdateTime();
            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
        }
    }
    protected void btnCancelSaveAndQuit_Click(object sender, EventArgs e)
    {
        this.GeneratePage();
    }
    protected void btnNoReplacement_Click(object sender, EventArgs e)
    {
        if (SessionManager.SessionQuestionNumber == SessionManager.SessionQuestionnaire.Count)
        {
            TestTaken taken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
            if (taken != null)
            {
                taken.DateEndTaken = DateTime.Now;
                taken.Finish();
            }
            Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
        }
        Response.Redirect("~/test.aspx?" + Request.QueryString.ToString());
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        //Check if user has an answer
        bool hasAnswer = true;
        if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "multiple")
        {
            int totChecked = 0;
            totChecked += testControl1.ChkChoice1.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice2.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice3.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice4.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice5.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice6.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice7.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice8.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice9.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice10.Checked ? 1 : 0;
            if (totChecked < 1)
                hasAnswer = false;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "torf")
        {
            if (!testControl1.RdoFalse.Checked && !testControl1.RdoTrue.Checked)
                hasAnswer = false;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "fillblanks")
        {
            if (string.IsNullOrEmpty(testControl1.DdlAnswer.Text.Trim()))
                hasAnswer = false;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "essay")
        {
            if (string.IsNullOrEmpty(testControl1.TxtEssay.Text.Trim()))
                hasAnswer = false;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "hotspot")
        {
            int totChecked = 0;
            totChecked += testControl1.ImgChoice1.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice2.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice3.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice4.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice5.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice6.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice7.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice8.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice9.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice10.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            if (totChecked < 1)
                hasAnswer = false;
        }
        if (!hasAnswer)
        {
            testControl1.CvNoAnswer.IsValid = false;
            this.GeneratePage();
            return;
        }

        TestTaken testTaken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
        TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, false);
        if (testTaken != null && category != null && SessionManager.SessionTimedExam)
        {
            if (category.EndDate <= System.DateTime.Now)
            {
                testTaken.DateEndTaken = System.DateTime.Now;
                testTaken.Finish();
                Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
            }
        }
        else if (category == null)
        {
            testTaken.DateEndTaken = System.DateTime.Now;
            testTaken.Finish();
            Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
        }
        //save the answer into a session
        SaveTestResponse response = new SaveTestResponse();
        response.QuestionnaireID = SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].QuestionnaireID;
        response.TestTakenID = SessionManager.SessionTestTaken.TestTakenID;
        response.UserID = SessionManager.SessionUsername;
        if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "multiple")
        {
            response.Response1 = testControl1.ChkChoice1.Checked ? testControl1.ChkChoice1.Text : string.Empty;
            response.Response2 = testControl1.ChkChoice2.Checked ? testControl1.ChkChoice2.Text : string.Empty;
            response.Response3 = testControl1.ChkChoice3.Checked ? testControl1.ChkChoice3.Text : string.Empty;
            response.Response4 = testControl1.ChkChoice4.Checked ? testControl1.ChkChoice4.Text : string.Empty;
            response.Response5 = testControl1.ChkChoice5.Checked ? testControl1.ChkChoice5.Text : string.Empty;
            response.Response6 = testControl1.ChkChoice6.Checked ? testControl1.ChkChoice6.Text : string.Empty;
            response.Response7 = testControl1.ChkChoice7.Checked ? testControl1.ChkChoice7.Text : string.Empty;
            response.Response8 = testControl1.ChkChoice8.Checked ? testControl1.ChkChoice8.Text : string.Empty;
            response.Response9 = testControl1.ChkChoice9.Checked ? testControl1.ChkChoice9.Text : string.Empty;
            response.Response10 = testControl1.ChkChoice10.Checked ? testControl1.ChkChoice10.Text : string.Empty;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "torf")
        {
            response.Response1 = testControl1.RdoTrue.Checked ? "True" : "False";
            response.Response2 = string.Empty;
            response.Response3 = string.Empty;
            response.Response4 = string.Empty;
            response.Response5 = string.Empty;
            response.Response6 = string.Empty;
            response.Response7 = string.Empty;
            response.Response8 = string.Empty;
            response.Response9 = string.Empty;
            response.Response10 = string.Empty;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "yorn")
        {
            response.Response1 = ((RadioButton)testControl1.Objects["rdoYes"]).Checked ? "True" : "False";
            response.Response2 = string.Empty;
            response.Response3 = string.Empty;
            response.Response4 = string.Empty;
            response.Response5 = string.Empty;
            response.Response6 = string.Empty;
            response.Response7 = string.Empty;
            response.Response8 = string.Empty;
            response.Response9 = string.Empty;
            response.Response10 = string.Empty;
        }

        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "fillblanks")
        {
            response.Response1 = testControl1.DdlAnswer.Text.Trim();
            response.Response2 = string.Empty;
            response.Response3 = string.Empty;
            response.Response4 = string.Empty;
            response.Response5 = string.Empty;
            response.Response6 = string.Empty;
            response.Response7 = string.Empty;
            response.Response8 = string.Empty;
            response.Response9 = string.Empty;
            response.Response10 = string.Empty;
        }

        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "sequencing")
        {
            List<string> tempList = new List<string>();
            for (int i = 0; i < testControl1.RolSequencing.Items.Count; i++)
            {
                Label lbltemplate = testControl1.RolSequencing.Items[0].FindControl("lblTemplate") as Label;
                tempList.Add(lbltemplate.Text.Trim());
            }
            if (tempList.Count > 9)
                response.Response10 = tempList[9];
            if (tempList.Count > 8)
                response.Response9 = tempList[8];
            if (tempList.Count > 7)
                response.Response8 = tempList[7];
            if (tempList.Count > 6)
                response.Response7 = tempList[6];
            if (tempList.Count > 5)
                response.Response6 = tempList[5];
            if (tempList.Count > 4)
                response.Response5 = tempList[4];
            if (tempList.Count > 3)
                response.Response4 = tempList[3];
            if (tempList.Count > 2)
                response.Response3 = tempList[2];

            response.Response2 = tempList[1];
            response.Response1 = tempList[0];
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "essay")
        {
            response.EssayResponse = testControl1.TxtEssay.Text.Trim();
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "hotspot")
        {
            response.Response1 = testControl1.ImgChoice1.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice1.ImageUrl : string.Empty;
            response.Response2 = testControl1.ImgChoice2.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice2.ImageUrl : string.Empty;
            response.Response3 = testControl1.ImgChoice3.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice3.ImageUrl : string.Empty;
            response.Response4 = testControl1.ImgChoice4.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice4.ImageUrl : string.Empty;
            response.Response5 = testControl1.ImgChoice5.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice5.ImageUrl : string.Empty;
            response.Response6 = testControl1.ImgChoice6.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice6.ImageUrl : string.Empty;
            response.Response7 = testControl1.ImgChoice7.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice7.ImageUrl : string.Empty;
            response.Response8 = testControl1.ImgChoice8.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice8.ImageUrl : string.Empty;
            response.Response9 = testControl1.ImgChoice9.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice9.ImageUrl : string.Empty;
            response.Response10 = testControl1.ImgChoice10.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice10.ImageUrl : string.Empty;
        }
        SaveTestResponse response2 = SaveTestResponse.Retrieve(response.UserID, response.TestTakenID, response.QuestionnaireID);
        if (response2 == null)
        {
            response.Insert();
            SessionManager.SessionQuestionNumber++;
            if (SessionManager.SessionQuestionNumber == SessionManager.SessionQuestionnaire.Count)
            {
                TestTaken taken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
                if (taken != null)
                {
                    taken.DateEndTaken = DateTime.Now;
                    taken.Finish();
                }
                Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
            }
            Response.Redirect("~/test.aspx?" + Request.QueryString.ToString());
        }
        else
            this.mpeNoReplacement.Show();

        //replace an existing answer for a question with a new one
        //SaveTestResponse tempResponse = new SaveTestResponse();
        //for (int i = 0; i < SessionManager.SessionSaveTestResponse.Count; i++)
        //{
        //    if (SessionManager.SessionSaveTestResponse[i].QuestionnaireID == response.QuestionnaireID && SessionManager.SessionSaveTestResponse[i].UserID == response.UserID)
        //    {
        //        SessionManager.SessionSaveTestResponse[i] = response;
        //        break;
        //    }
        //}

        //move on to the next question
    }

    public void GeneratePage()
    {
        try
        {
            int qNum = SessionManager.SessionQuestionNumber + 1;
            testControl1.LblItemNumber.Text = "Question " + qNum.ToString() + "/" + SessionManager.SessionQuestionnaire.Count.ToString();
            //Get question from the session
            Questionnaire q = SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber];
            SaveTestResponse response = null;
            //See if the user has saved the exam before
            if (SessionManager.SessionTestTaken != null)
                response = SaveTestResponse.Retrieve(SessionManager.SessionUsername, SessionManager.SessionTestTaken.TestTakenID, q.QuestionnaireID);
            if (response != null)
            {
                SessionManager.SessionQuestionNumber++;
                Response.Redirect("~/test.aspx?" + Request.QueryString.ToString());
            }
            if (q != null)
            {
                testControl1.LblQuestion.Text = q.Question;
                if (q.TypeCode == "multiple")
                {
                    testControl1.PnlMultipleChoice.Visible = true;
                    if (string.IsNullOrEmpty(q.Choice10))
                        ((HtmlTableRow)testControl1.Objects["trCheck10"]).Visible = false;
                    else
                        testControl1.ChkChoice10.Text = q.Choice10;

                    if (string.IsNullOrEmpty(q.Choice9))
                        ((HtmlTableRow)testControl1.Objects["trCheck9"]).Visible = false;
                    else
                        testControl1.ChkChoice9.Text = q.Choice9;

                    if (string.IsNullOrEmpty(q.Choice8))
                        ((HtmlTableRow)testControl1.Objects["trCheck8"]).Visible = false;
                    else
                        testControl1.ChkChoice8.Text = q.Choice8;

                    if (string.IsNullOrEmpty(q.Choice7))
                        ((HtmlTableRow)testControl1.Objects["trCheck7"]).Visible = false;
                    else
                        testControl1.ChkChoice7.Text = q.Choice7;

                    if (string.IsNullOrEmpty(q.Choice6))
                        ((HtmlTableRow)testControl1.Objects["trCheck6"]).Visible = false;
                    else
                        testControl1.ChkChoice6.Text = q.Choice6;

                    if (string.IsNullOrEmpty(q.Choice5))
                        ((HtmlTableRow)testControl1.Objects["trCheck5"]).Visible = false;
                    else
                        testControl1.ChkChoice5.Text = q.Choice5;

                    if (string.IsNullOrEmpty(q.Choice4))
                        ((HtmlTableRow)testControl1.Objects["trCheck4"]).Visible = false;
                    else
                        testControl1.ChkChoice4.Text = q.Choice4;

                    if (string.IsNullOrEmpty(q.Choice3))
                        ((HtmlTableRow)testControl1.Objects["trCheck3"]).Visible = false;
                    else
                        testControl1.ChkChoice3.Text = q.Choice3;

                    if (string.IsNullOrEmpty(q.Choice2))
                        ((HtmlTableRow)testControl1.Objects["trCheck2"]).Visible = false;
                    else
                        testControl1.ChkChoice2.Text = q.Choice2;

                    if (string.IsNullOrEmpty(q.Choice1))
                        ((HtmlTableRow)testControl1.Objects["trCheck1"]).Visible = false;
                    else
                        testControl1.ChkChoice1.Text = q.Choice1;

                    if (response != null)
                    {
                        testControl1.ChkChoice1.Enabled = false;
                        testControl1.ChkChoice2.Enabled = false;
                        testControl1.ChkChoice3.Enabled = false;
                        testControl1.ChkChoice4.Enabled = false;
                        testControl1.ChkChoice5.Enabled = false;
                        testControl1.ChkChoice6.Enabled = false;
                        testControl1.ChkChoice7.Enabled = false;
                        testControl1.ChkChoice8.Enabled = false;
                        testControl1.ChkChoice9.Enabled = false;
                        testControl1.ChkChoice10.Enabled = false;
                    }
                }
                else if (q.TypeCode == "hotspot")
                {
                    testControl1.PnlHotspot.Visible = true;
                    if (string.IsNullOrEmpty(q.Choice10))
                        testControl1.ImgCell10.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice9))
                        testControl1.ImgCell9.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice8))
                        testControl1.ImgCell8.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice7))
                        testControl1.ImgCell7.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice6))
                        testControl1.ImgCell6.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice5))
                        testControl1.ImgCell5.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice4))
                        testControl1.ImgCell4.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice3))
                        testControl1.ImgCell3.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice2))
                        testControl1.ImgCell2.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice1))
                        testControl1.ImgCell1.Visible = false;

                    if (response != null)
                    {
                        testControl1.ImgChoice10.Enabled = false;
                        testControl1.ImgChoice9.Enabled = false;
                        testControl1.ImgChoice8.Enabled = false;
                        testControl1.ImgChoice7.Enabled = false;
                        testControl1.ImgChoice6.Enabled = false;
                        testControl1.ImgChoice5.Enabled = false;
                        testControl1.ImgChoice4.Enabled = false;
                        testControl1.ImgChoice3.Enabled = false;
                        testControl1.ImgChoice2.Enabled = false;
                        testControl1.ImgChoice1.Enabled = false;
                    }
                }
                else if (q.TypeCode == "torf")
                {
                    testControl1.PnlTrueOrFalse.Visible = true;
                    if (response != null)
                    {
                        testControl1.RdoFalse.Enabled = false;
                        testControl1.RdoTrue.Enabled = false;
                    }
                }
                else if (q.TypeCode == "sequencing")
                {
                    List<TempQuestionnaire> list = new List<TempQuestionnaire>();
                    if (!string.IsNullOrEmpty(q.Choice1))
                        list.Add(new TempQuestionnaire(q.Choice1));

                    if (!string.IsNullOrEmpty(q.Choice2))
                        list.Add(new TempQuestionnaire(q.Choice2));

                    if (!string.IsNullOrEmpty(q.Choice3))
                        list.Add(new TempQuestionnaire(q.Choice3));

                    if (!string.IsNullOrEmpty(q.Choice4))
                        list.Add(new TempQuestionnaire(q.Choice4));

                    if (!string.IsNullOrEmpty(q.Choice5))
                        list.Add(new TempQuestionnaire(q.Choice5));

                    if (!string.IsNullOrEmpty(q.Choice6))
                        list.Add(new TempQuestionnaire(q.Choice6));

                    if (!string.IsNullOrEmpty(q.Choice7))
                        list.Add(new TempQuestionnaire(q.Choice7));

                    if (!string.IsNullOrEmpty(q.Choice8))
                        list.Add(new TempQuestionnaire(q.Choice8));

                    if (!string.IsNullOrEmpty(q.Choice9))
                        list.Add(new TempQuestionnaire(q.Choice9));

                    if (!string.IsNullOrEmpty(q.Choice10))
                        list.Add(new TempQuestionnaire(q.Choice10));

                    testControl1.RolSequencing.DataSource = list;
                    testControl1.RolSequencing.DataBind();
                    testControl1.PnlSequencing.Visible = true;
                    if (response != null)
                        testControl1.RolSequencing.AllowReorder = false;
                }
                else if (q.TypeCode == "fillblanks")
                {
                    testControl1.PnlFillInTheBlanks.Visible = true;
                    if (response != null)
                        testControl1.DdlAnswer.Enabled = false;
                }
                else if (q.TypeCode == "essay")
                {
                    testControl1.PnlEssay.Visible = true;
                    if (response != null)
                        testControl1.TxtEssay.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "test.aspx", "GeneratePage", ex.Message);
            SessionManager.SessionMainText = "An error was encountered while trying to generate the test.";
            Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
        }
    }
}
    