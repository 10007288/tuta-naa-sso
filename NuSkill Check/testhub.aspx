<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site.master"
    AutoEventWireup="true" CodeFile="testhub.aspx.cs" Inherits="testhub" Title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:UpdatePanel ID="udpTestHub" runat="server" UpdateMode="conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvRecentTests" EventName="RowCommand" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel ID="pnlMain" runat="server" Width="100%" HorizontalAlign="center">
                <table cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:UpdateProgress ID="udpProgressLeft" runat="server">
                                <ProgressTemplate>
                                    <asp:Panel ID="pnlLeft" runat="server" Width="450px">
                                        <table width="100%">
                                            <tr>
                                                <td align="center" valign="middle">
                                                    <asp:Image ID="imgLeft" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblHeader" runat="server" Text="Available Tests" CssClass="midHeader" /><br />
                            <hr style="color: Black" />
                        </td>
                        <td>
                            <asp:Label ID="lblRecentTests" runat="server" Text="Recent Tests" CssClass="midHeader" /><br />
                            <hr style="color: Black" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width: 50%">
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblSelectCampaign" runat="server" Text="Category:" />
                                        <asp:DropDownList ID="ddlCampaigns" runat="server" DataTextField="Campaign" DataValueField="CampaignID"
                                            AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCampaigns_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblSubCategory" runat="server" Text="SubCategory" />
                                        <asp:DropDownList ID="ddlSubcategory" runat="server" DataTextField="Campaign" DataValueField="CampaignID" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <br />
                                        <asp:Button ID="btnViewTests" runat="server" Text="View Tests" CssClass="buttons"
                                            OnClick="btnViewTests_Click" />
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td colspan="2" align="center">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <hr style="color: #DDDDDD; width: 80%" />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="lblSelectGroup" runat="server" Text="Group:" />
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGroups" runat="server" DataTextField="TestGroupName" DataValueField="TestGroupID"
                                            AppendDataBoundItems="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <br />
                                        <asp:Button ID="btnViewGroups" runat="server" Text="View Tests" CssClass="buttons"
                                            OnClick="btnViewGroups_Click" />
                                    </td>
                                </tr>--%>
                            </table>
                        </td>
                        <td style="width: 50%">
                            <asp:UpdatePanel ID="udpRight" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvRecentTests" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        PageSize="5" Width="100%" EmptyDataText="You have not taken any exams yet." BorderWidth="1"
                                        OnRowCommand="gvRecentTests_RowCommand" BorderColor="black" CellPadding="5">
                                        <HeaderStyle HorizontalAlign="center" Font-Names="Arial" Font-Size="12px" />
                                        <RowStyle Font-Names="Arial" Font-Size="12px" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Exam Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                        Visible="false" />
                                                    <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                                    <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemStyle HorizontalAlign="center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDateLastSave" runat="server" Text='<%#Bind("DateLastSave") %>'
                                                        Visible="false" />
                                                    <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' Visible="false" />
                                                    <asp:Label ID="lblResponseCount" runat="server" Text='<%#Bind("ResponseCount") %>'
                                                        Visible="false" />
                                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("DateStartTaken", "{0:yyyy/MM/dd hh:mm tt}") %>'
                                                        Visible="false" />
                                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("DateEndTaken", "{0:yyyy/MM/dd hh:mm tt}") %>'
                                                        Visible="false" />
                                                    <asp:Image ID="imgTestStatus" runat="server" ImageUrl="~/images/icon-fail.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="middle" Width="25" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkRetry" runat="server" Text="Retry" Font-Bold="true" ForeColor="black"
                                                        CommandName="Retry" />
                                                    <asp:LinkButton ID="lnkResume" runat="server" Text="Resume" Font-Bold="true" ForeColor="black"
                                                        CommandName="Resume" Visible="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <br />
                            <asp:Image ID="imgPass" runat="server" ImageUrl="~/images/icon-pass.gif" /><asp:Label
                                ID="lblPass" runat="server" Text=" = Pass;" CssClass="smallText" />
                            <asp:Image ID="imgFail" runat="server" ImageUrl="~/images/icon-fail.gif" /><asp:Label
                                ID="lblFail" runat="server" Text=" = Fail;" CssClass="smallText" />
                            <asp:Image ID="imgPending" runat="server" ImageUrl="~/images/icon-pending.gif" /><asp:Label
                                ID="lblPending" runat="server" Text=" = Pending;" CssClass="smallText" />
                            <asp:Image ID="imgResume" runat="server" ImageUrl="~/images/icon-resume.gif" /><asp:Label
                                ID="lblResume" runat="server" Text=" = Resumable" CssClass="smallText" />
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td align="right">
                            <asp:LinkButton ID="lnkMoreAvailable" runat="server" Text="All Available Tests..."
                                CssClass="smallText" OnClick="lnkMoreAvailable_Click" />
                        </td>
                        <td align="right">
                            <asp:LinkButton ID="lnkMoreTaken" runat="server" Text="All Taken Tests..." CssClass="smallText"
                                OnClick="lnkMoreTaken_Click" />
                        </td>
                    </tr>
                    -->
                </table>
            </asp:Panel>
            <ajax:ModalPopupExtender ID="mpeMessage" runat="server" BackgroundCssClass="modalBackground"
                TargetControlID="hidMessage" PopupControlID="pnlMessage" CancelControlID="btnMessage" />
            <asp:HiddenField ID="hidMessage" runat="server" />
            <asp:Panel ID="pnlMessage" runat="server" CssClass="modalPopup" HorizontalAlign="center"
                Width="400px">
                <table width="100%" cellpadding="15" cellspacing="0">
                    <tr>
                        <td style="width: 392px">
                            <asp:Label ID="lblMessage" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 392px">
                            <asp:Button ID="btnMessage" runat="server" Text="Return" OnClick="btnMessage_Click"
                                CssClass="buttons" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajax:ModalPopupExtender ID="mpeRetake" runat="server" BackgroundCssClass="modalBackground"
                TargetControlID="hidRetake" PopupControlID="pnlRetake" CancelControlID="btnCancelRetake" />
            <asp:HiddenField ID="hidRetake" runat="server" />
            <asp:Panel ID="pnlRetake" runat="server" CssClass="modalPopup" HorizontalAlign="center"
                Width="400px">
                <table width="100%" cellpadding="15" cellspacing="0">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblRetake" runat="server" Text="You have already taken and passed this test." />
                        </td>
                    </tr>
                    <tr>
                        <%--<td align="left">
                            <asp:Button ID="btnOKRetake" runat="server" Text="Retake" OnClick="btnOKRetake_Click"
                                CssClass="buttons" />
                        </td>--%>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnCancelRetake" runat="server" Text="Cancel" CssClass="buttons"
                                OnClick="btnCancelRetake_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
