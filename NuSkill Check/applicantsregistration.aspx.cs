using System;
using System.Web.UI;
using NuSkill.Business;

public partial class applicantsregistration : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(txtFirstName.Text.Trim()))
            {
                isValid = false;
                cvFirstName.IsValid = false;
            }
            if (string.IsNullOrEmpty(txtLastName.Text.Trim()))
            {
                isValid = false;
                cvLastName.IsValid = false;
            }
            if (string.IsNullOrEmpty(ddlSites.Text.Trim()))
            {
                isValid = false;
                cvLocation.IsValid = false;
            }
            if (string.IsNullOrEmpty(txtATSID.Text.Trim()))
            {
                isValid = false;
                cvATSID.IsValid = false;
            }

            if (!isValid)
                return;

            if (!Registration_Applicant.CheckExists_Applicant(txtATSID.Text.Trim(), txtFirstName.Text.Trim(), txtLastName.Text.Trim(), ddlSites.Text.Trim()))
            {
                Registration_Applicant reg = new Registration_Applicant();

                reg.ATSID = txtATSID.Text.Trim();
                reg.FirstName = txtFirstName.Text.Trim();
                reg.LastName = txtLastName.Text.Trim();
                reg.Site = ddlSites.Text.Trim();
                reg.Insert_Applicant();

                lblResult.Text = "Congratulations! Please login using the same information you have provided.";
                lblResult.ForeColor = System.Drawing.Color.Green;
                mpeFinished.Show();
            }
            else
            {
                lblResult.Text = "The record already exist! Please login using the same information you have provided.";
                lblResult.ForeColor = System.Drawing.Color.Red;
                mpeFinished.Show();
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "applicantsregistration.aspx", "btnRegister_Click", ex.Message);
            SessionManager.SessionMainText = "An error occured while registering.";
            Response.Redirect("applicantserror.aspx?" + Request.QueryString);
        }
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantsdefault.aspx?" + Request.QueryString);
    }

    protected void lnkLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
    }
}
