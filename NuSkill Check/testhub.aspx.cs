using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class testhub : System.Web.UI.Page
{
    private const string viewstateCategoryID = "viewstateCategoryID";

    private int CategoryID
    {
        get { return (int)this.ViewState[viewstateCategoryID]; }
        set { this.ViewState[viewstateCategoryID] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            this.PopulateGrids();

            foreach (GridViewRow row in this.gvRecentTests.Rows)
             {
                Image image = row.FindControl("imgTestStatus") as Image;
                Label label = row.FindControl("lblTestName") as Label;
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblStartDate = row.FindControl("lblStartDate") as Label;
                Label lblEndDate = row.FindControl("lblEndDate") as Label;
                LinkButton lnkRetry = row.FindControl("lnkRetry") as LinkButton;
                LinkButton lnkResume = row.FindControl("lnkResume") as LinkButton;
                Label lblDateLastSave = row.FindControl("DateLastSave") as Label;
                Label lblPassed = row.FindControl("lblPassed") as Label;
                Label lblResponseCount = row.FindControl("lblResponseCount") as Label;

                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);
                if (category.HideScores)
                {
                    image.Visible = false;
                    lnkRetry.Visible = false;
                }

                if (string.IsNullOrEmpty(lblEndDate.Text))
                {
                    image.ImageUrl = "~/images/icon-resume.gif";
                    lnkRetry.Visible = false;
                    lnkResume.Visible = true;
                }
                else if (Convert.ToInt32(lblResponseCount.Text) > 0)
                    image.ImageUrl = "~/images/icon-pending.gif";
                else if (lblPassed.Text == "True" || lblPassed.Text == "")
                {
                    image.ImageUrl = "~/images/icon-pass.gif";
                }
                else
                    image.ImageUrl = "~/images/icon-fail.gif";

                if (label.Text.Length > 50)
                    label.Text = label.Text.Substring(0, 47) + "...";
            }

            List<AccountList> ddllist = new List<AccountList>();
            List<int> templist = new List<int>();
            this.PopulateDropDownLists();
        }
    }

    protected void PopulateGrids()
    {
        DataSet taken = TestTaken.SelectByUser(SessionManager.SessionUsername, true);
        this.gvRecentTests.DataSource = taken;
        this.gvRecentTests.DataBind();
    }

    protected void PopulateDropDownLists()
    {
        this.ddlCampaigns.DataSource = NonCimCampaign.SelectParents();
        this.ddlCampaigns.DataBind();
        this.ddlSubcategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlCampaigns.SelectedValue), true);
        this.ddlSubcategory.DataBind();
    }

    protected void ddlCampaigns_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ddlSubcategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlCampaigns.SelectedValue),true);
        this.ddlSubcategory.DataBind();
    }

    protected void gvRecentTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Retry")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
            TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
            if (category != null)
            {
                int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
                int testRetake = TestCategory.checkRetake(category.TestCategoryID);
                if (testCount < category.TestLimit || category.TestLimit == 0 || testRetake ==1)
                {

                    SaveTestResponse[] responses = SaveTestResponse.CheckPendingEssays(SessionManager.SessionUsername, Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    if (responses.Length > 0)
                    {
                        this.lblMessage.Text = "You have taken this test. It is still pending completion/checking.";
                        this.mpeMessage.Show();
                    }
                    else
                    {
                        SessionManager.SessionTestCategoryID = category.TestCategoryID;
                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questionnaire.Length > 0)
                        {
                            bool hasPassed = false;
                            TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                            foreach (TestTaken test in taken)
                            {
                                if (test.Passed && test.UserID == SessionManager.SessionUsername)
                                    hasPassed = true;
                            }
                            if (hasPassed == true && testRetake != 1)
                            {
                                this.CategoryID = category.TestCategoryID;
                                this.mpeRetake.Show();
                            }
                            else
                            {
                                SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                                foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                                SessionManager.SessionExamAction = "newexam";
                                SessionManager.SessionExamIsRetake = "retake"; //TODO: GLA 01092014
                                Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                            }
                        }
                        else
                        {
                            this.lblMessage.Text = "This test has no items.";
                            this.mpeMessage.Show();
                        }
                    }
                }
                else
                {
                    this.lblMessage.Text = "You have already used up all tries available for this test.";
                    this.mpeMessage.Show();
                }
            }
            else
            {
                lblMessage.Text = "This exam has been removed from the system or has already expired.";
                this.mpeMessage.Show();
            }
        }
        else if (e.CommandName == "Resume")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
            TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);

            if (category != null)
            {
                SessionManager.SessionTestCategoryID = category.TestCategoryID;
                Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);

                if (questionnaire.Length > 0)
                {
                    bool hasPending = false;

                    TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    if (taken != null && taken.DateEndTaken.ToString().Contains("1/1/0001"))
                    {
                        SessionManager.SessionTestTaken = taken;
                        SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                        foreach (Questionnaire question in questionnaire)
                            SessionManager.SessionQuestionnaire.Add(question);
                        SessionManager.SessionExamAction = "resumeexam";
                        SessionManager.SessionExamIsRetake = ""; //TODO: GLA 01092014
                        Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                    }
                    else
                    {
                        this.lblMessage.Text = "This test has been completed.";
                        this.mpeMessage.Show();
                    }
                }
                else
                {
                    this.lblMessage.Text = "This test has no items.";
                    this.mpeMessage.Show();
                }

            }
            else
            {
                lblMessage.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
                this.mpeMessage.Show();
            }
        }
    }

    protected void gvAvailableTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Take")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
            TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
            if (category != null)
            {
                int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
                int testRetake = TestCategory.checkRetake(category.TestCategoryID);
                if (testCount < category.TestLimit || category.TestLimit == 0 || testRetake == 1)
                  {
                    bool hasPassed = false;
                    TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                    foreach (TestTaken test in taken)
                    {
                        if (test.Passed)
                        {
                            hasPassed = true;
                            break;
                        }
                    }
                    if (hasPassed == true && testRetake != 1)
                    {
                        this.CategoryID = category.TestCategoryID;
                        this.mpeRetake.Show();
                    }
                    else
                    {
                        SessionManager.SessionTestCategoryID = category.TestCategoryID;
                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questionnaire.Length > 0)
                        {
                            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                            SessionManager.SessionExamAction = "newexam";
                            Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                        }
                        else
                        {
                            this.lblMessage.Text = "This exam has no items.";
                            this.mpeMessage.Show();
                        }
                    }
                }
                else
                {
                    this.lblMessage.Text = "You have already used up all tries available for this test.";
                    this.mpeMessage.Show();
                }
            }
            else
            {
                lblMessage.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
                this.mpeMessage.Show();
            }
        }
    }

    protected void lnkMoreAvailable_Click(object sender, EventArgs e)
    {
        SessionManager.SessionAccountCampaignID = 0;
        Response.Redirect("~/alltests.aspx?" + Request.QueryString.ToString());
    }

    protected void btnOKRetake_Click(object sender, EventArgs e)
    {
        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(this.CategoryID);
        if (questionnaire.Length < 1)
        {
            this.lblMessage.Text = "This exam has no items.";
            this.mpeMessage.Show();
        }
        else
        {
            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
            SessionManager.SessionExamAction = "newexam";
            SessionManager.SessionTestCategoryID = this.CategoryID;
            foreach (Questionnaire question in questionnaire)
                SessionManager.SessionQuestionnaire.Add(question);
            Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
        }
    }

    protected void btnMessage_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkMoreTaken_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/takentests.aspx?" + Request.QueryString.ToString());
    }

    protected void btnCancelRetake_Click(object sender, EventArgs e)
    {
        this.PopulateGrids();
    }

    protected void btnViewTests_Click(object sender, EventArgs e)
    {
        SessionManager.SessionAccountCampaignID = Convert.ToInt32(this.ddlCampaigns.SelectedValue);
        SessionManager.SessionSubCategory = Convert.ToInt32(this.ddlSubcategory.SelectedValue);
        Response.Redirect("~/alltests.aspx?" + Request.QueryString.ToString());
    }

    protected void btnViewGroups_Click(object sender, EventArgs e)
    {
        //SessionManager.SessionGroupID = Convert.ToInt32(this.ddlGroups.SelectedValue);
        //Response.Redirect("~/alltests.aspx?" + Request.QueryString.ToString());
    }
}