<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site.master"
    AutoEventWireup="true" CodeFile="testover.aspx.cs" Inherits="testover" Title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="10" cellspacing="0">
        <tr style="height:500px">
            <td align="center" style="vertical-align: middle; font-size: large; " valign="middle">
                <table width="90%" cellpadding="5" style="vertical-align: middle; 
                    font-size: small;">
                    <tr>
                        <td align="center" style="padding: 30px 10px 10px 10px">
                            <asp:Label ID="lblNoMoreTime" runat="server" Text="" />
                            <br /><br /><br />
                            <asp:Button ID="btnContinue" runat="server" Text="" OnClick="btnContinue_Click" CssClass="buttons" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
