using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls.WebParts;
using System.Threading;
using System.Globalization;
using Microsoft.AspNet.Membership.OpenAuth;


public partial class login : System.Web.UI.Page
{
    #region OLDTUT
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!this.IsPostBack)
    //    {
    //        this.Session.Clear();
    //    }
    //}

    //private DataSet LoadCompanySites()
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        NuComm.Connection connection = new NuComm.Connection();
    //        connection.DatabaseServer = Config.Vader2DatabaseServer();
    //        connection.DatabaseName = "Testing"; //Config.TestingDatabaseName();
    //        connection.UserName = "us_Survey";//Config.TestingDatabaseUsername();
    //        connection.Password = "";//Config.TestingDatabasePassword();
    //        connection.AddParameter("@CIMNumber", 0, SqlDbType.Int);
    //        connection.StoredProcedureName = "dbo.pr_WeeklyTesting_Lkp_CompanySiteByCIM";
    //        connection.Fill(ref ds);
    //        connection.DisconnectInstance();
    //        connection = null;
    //        return ds;
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
    //        service.WriteError(4, "login.aspx", "LoadCompanySites", ex.Message);
    //        return null;
    //    }
    //}

    //protected void lnkLogin_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int temp = 0;
    //        if (int.TryParse(this.txtUsername.Text.Trim(), out temp) && Config.AcceptInternalLogin())
    //        {
    //            if (LoadVerifyUser())
    //            {
    //                SessionManager.SessionSessionID = Guid.NewGuid();
    //                SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //                LoginSession session = LoginSession.Select(this.txtUsername.Text.Trim());
    //                SessionManager.SessionCim = Convert.ToInt32(this.txtUsername.Text.Trim());

    //                if (session == null)
    //                {
    //                    try
    //                    {
    //                        session = new LoginSession(this.txtUsername.Text.Trim(), SessionManager.SessionSessionID);
    //                        session.Insert();
    //                    }
    //                    catch
    //                    {
    //                        SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                        Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                    }
    //                }
    //                else if (session.LoginSessionID != SessionManager.SessionSessionID)
    //                    try
    //                    {
    //                        session.Update();
    //                    }
    //                    catch
    //                    {
    //                        SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                        Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                    }
    //                SessionManager.SessionSite = "INT";// this.ddlSites.SelectedValue;
    //                Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    //            }
    //            else
    //                this.cvLoginError.IsValid = false;
    //        }
    //        else if (Registration.ValidateLogin(this.txtUsername.Text.Trim(), NuComm.Security.Encryption.UTF8.EncryptText(this.txtPassword.Text.Trim())) && Config.AcceptExternalLogin())
    //        {
    //            SessionManager.SessionSessionID = Guid.NewGuid();
    //            SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //            LoginSession session = LoginSession.Select(this.txtUsername.Text.Trim());

    //            if (session == null)
    //            {
    //                try
    //                {
    //                    session = new LoginSession(this.txtUsername.Text.Trim(), SessionManager.SessionSessionID);
    //                    session.Insert();
    //                }
    //                catch (Exception ex)
    //                {
    //                    ErrorLogger.Write(Config.ApplicationName, "login.aspx", "lnkLogin_Click", ex.Message);
    //                    SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                    Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                }
    //            }
    //            else if (session.LoginSessionID != SessionManager.SessionSessionID)
    //                try
    //                {
    //                    session.Update();
    //                }
    //                catch (Exception ex)
    //                {
    //                    ErrorLogger.Write(Config.ApplicationName, "login.aspx", "lnkLogin_Click", ex.Message);
    //                    SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                    Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                }
    //            SessionManager.SessionSite = "EXT";//this.ddlSites.SelectedValue;
    //            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        if (!ex.Message.Contains("Thread was being aborted"))
    //        {
    //            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
    //            service.WriteError(4, "allitems.aspx", "ProcessItem", ex.Message);
    //            SessionManager.SessionMainText = "System has encountered an unknown error. Please contact your examiner/administrator.";
    //            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //        }
    //    }
    //}

    //protected void lnkRegister_Click(object sender, EventArgs e)
    //{
    //    if (string.IsNullOrEmpty(SessionManager.SessionMainText))
    //        Response.Redirect("~/registration.aspx?" + Request.QueryString.ToString());
    //    else if (!string.IsNullOrEmpty(SessionManager.SessionUsername))
    //        Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    //}

    //private bool LoadVerifyUser()
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        NuComm.Connection connection = new NuComm.Connection();
    //        connection.DatabaseServer = Config.Vader2DatabaseServer();// @"Vader2\Callisto";
    //        connection.DatabaseName = Config.Vader2DatabaseName();// "BlackBox";
    //        connection.UserName = Config.Vader2DatabaseUsername();// "us_RAS";
    //        connection.Password = Config.Vader2DatabasePassword();// "";
    //        connection.AddParameter("@EmpID", this.txtUsername.Text.Trim(), SqlDbType.VarChar);
    //        connection.AddParameter("@Password", this.txtPassword.Text.Trim(), SqlDbType.VarChar);
    //        connection.StoredProcedureName = "dbo.pr_RAS_ValidateUser";
    //        if (connection.Fill(ref ds) && (ds.Tables[0].Rows.Count > 0))
    //        {
    //            if (ds.Tables[0].Rows[0][0].ToString() == "1")
    //                return true;
    //        }
    //        connection.DisconnectInstance();
    //        connection = null;
    //        return false;
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
    //        service.WriteError(4, "login.aspx", "LoadVerifyUser", ex.Message);
    //        return false;
    //    }
    //}
    #endregion

    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];

        if (SessionManager.SessionUsername != "")
            Response.Redirect("~/testhub.aspx");
    }

    public IEnumerable<ProviderDetails> GetProviderNames()
    {
        return OpenAuth.AuthenticationClients.GetAll();
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}
