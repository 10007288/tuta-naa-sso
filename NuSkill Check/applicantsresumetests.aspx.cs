using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuSkill.Business;

public partial class resumetests : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataSet taken = TestTaken.SelectPendingCompletion(SessionManager.SessionUsername);
            gvRecentTests.DataSource = taken;
            gvRecentTests.DataBind();
        }
    }

    protected void gvRecentTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Resume")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                if (category != null)
                {
                    SessionManager.SessionTestCategoryID = category.TestCategoryID;
                    Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                    if (questionnaire.Length > 0)
                    {
                        //bool hasPending = false;
                        TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                        if (taken != null && taken.DateEndTaken.ToString().Contains("1/1/0001"))
                        {
                            SessionManager.SessionTestTaken = taken;
                            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                            SessionManager.SessionExamAction = "resumeexam";
                            Response.Redirect("applicantstestdefault.aspx?" + Request.QueryString);
                        }
                        else
                        {
                            lblMessage.Text = "This test has been completed.";
                            mpeMessage.Show();
                        }
                    }
                    else
                    {
                        lblMessage.Text = "This test has no items.";
                        mpeMessage.Show();
                    }

                }
                else
                {
                    lblMessage.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
                    mpeMessage.Show();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "applicantsresumetests.aspx", "gvRecentTests_RowCommand", ex.Message);
            lblMessage.Text = "An error occured.";
            mpeMessage.Show();
        }

    }
}
