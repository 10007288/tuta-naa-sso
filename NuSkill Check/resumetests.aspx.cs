using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class resumetests : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            DataSet taken = TestTaken.SelectPendingCompletion(SessionManager.SessionUsername);
            this.gvRecentTests.DataSource = taken;
            this.gvRecentTests.DataBind();
        }
    }

    protected void gvRecentTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Resume")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                if (category != null)
                {
                    SessionManager.SessionTestCategoryID = category.TestCategoryID;
                    Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                    if (questionnaire.Length > 0)
                    {
                        bool hasPending = false;
                        TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                        if (taken != null && taken.DateEndTaken.ToString().Contains("1/1/0001"))
                        {
                            SessionManager.SessionTestTaken = taken;
                            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                            SessionManager.SessionExamAction = "resumeexam";
                            Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                        }
                        else
                        {
                            this.lblMessage.Text = "This test has been completed.";
                            this.mpeMessage.Show();
                        }
                    }
                    else
                    {
                        this.lblMessage.Text = "This test has no items.";
                        this.mpeMessage.Show();
                    }

                }
                else
                {
                    lblMessage.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
                    this.mpeMessage.Show();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "resumetests.aspx", "gvRecentTests_RowCommand", ex.Message);
            lblMessage.Text = "An error occured.";
            this.mpeMessage.Show();
        }

    }
}
