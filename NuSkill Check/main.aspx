<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site.master"
    AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="main" Title="NuSkill Check v2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Width="100%">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblWelcome" runat="server" Text="Welcome to Transcom University Testing" CssClass="midHeader"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTesthub" runat="server" Text="Click " CssClass="linkbutton" />
                    <asp:LinkButton ID="lnkTestHub" runat="server" Text="here " CssClass="linkbutton" OnClick="lnkTestHub_Click" />
                    <asp:Label ID="lblTestHub2" runat="server" Text="to go to the test hub." CssClass="linkbutton" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
