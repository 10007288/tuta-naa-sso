<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site_applicant.master"
    AutoEventWireup="true" CodeFile="applicantsresumetests.aspx.cs" Inherits="resumetests"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Width="100%">
        <table cellpadding="10" cellspacing="0" style="text-align: left" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblAllAvailableTests" runat="server" Text="Resume Tests" Font-Size="large"
                        Font-Names="Arial" /><br />
                    <hr style="color: Black" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvRecentTests" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                        PageSize="5" Width="100%" EmptyDataText="You have not taken any exams yet." BorderWidth="1"
                        OnRowCommand="gvRecentTests_RowCommand" BorderColor="black" CellPadding="5">
                        <HeaderStyle HorizontalAlign="center" Font-Names="Arial" Font-Size="12px" />
                        <RowStyle Font-Names="Arial" Font-Size="12px" />
                        <Columns>
                            <asp:TemplateField HeaderText="Exam Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                    <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                    <asp:Label ID="lblDateLastSave" runat="server" Text='<%#Bind("LastSave") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblPassed" runat="server" Text='<%#Bind("Passed") %>' Visible="false" />
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("StartDate", "{0:yyyy/MM/dd hh:mm tt}") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("LastSave", "{0:yyyy/MM/dd hh:mm tt}") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblTimeRemaining" runat="server" Text='<%#Bind("TimeRemaining") %>' Visible="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="center" VerticalAlign="middle" Width="25" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkResume" runat="server" Text="Resume" Font-Bold="true" ForeColor="black"
                                        CommandName="Resume" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeMessage" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidMessage" PopupControlID="pnlMessage" CancelControlID="btnMessage" />
    <asp:HiddenField ID="hidMessage" runat="server" />
    <asp:Panel ID="pnlMessage" runat="server" CssClass="modalPopup" HorizontalAlign="center"
        Width="200px">
        <table width="100%" cellpadding="15" cellspacing="0" style="border-style: solid;
            border-color: black;">
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnMessage" runat="server" Text="Return" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
