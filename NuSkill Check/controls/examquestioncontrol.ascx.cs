using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using AjaxControlToolkit;

public partial class controls_examquestioncontrol : System.Web.UI.UserControl
{
    private string rolItems = "rolItems";
    private string rolSet = "rolSet";

    //private List<Interface> RolItems
    //{
    //    get { return (List<Interface>)HttpContext.Current.Session[rolItems]; }
    //    set { HttpContext.Current.Session[rolItems] = value; }
    //}

    private DataSet RolSet
    {
        get { return (DataSet)HttpContext.Current.Session[rolSet]; }
        set { HttpContext.Current.Session[rolSet] = value; }
    }

    private bool IsFromSaveTestResponse
    {
        get { return (bool)this.ViewState["isFromTestResponse"]; }
        set { this.ViewState["isFromTestResponse"] = value; }
    }

    #region Public Accessors?
    public Label LblItemNumber
    {
        get { return this.lblItemNumber; }
    }

    public Label LblItemMessage
    {
        get { return this.LblItemMessage; }
    }

    public Label LblQuestion
    {
        get { return this.lblQuestion; }
    }

    public HtmlTableCell TdQuestion
    {
        get { return this.tdQuestion; }
    }

    public HyperLink HypHyperlink
    {
        get { return this.hypHyperlink; }
    }

    public Image QuestionImg
    {
        get { return this.questionImg; }
    }

    public Panel PnlExamQuestion
    {
        get { return this.pnlExamQuestion; }
    }

    public Panel PnlMultipleChoice
    {
        get { return this.pnlMultipleChoice; }
    }

    public Panel PnlTrueOrFalse
    {
        get { return this.pnlTrueOrFalse; }
    }

    public Panel PnlYesOrNo
    {
        get { return this.pnlYesOrNo; }
    }

    public Panel PnlSequencing
    {
        get { return this.pnlSequencing; }
    }

    public Panel PnlFillInTheBlanks
    {
        get { return this.pnlFillInTheBlanks; }
    }

    public Panel PnlMatchingType
    {
        get { return this.pnlMatchingType; }
    }

    public Panel PnlEssay
    {
        get { return this.pnlEssay; }
    }

    public Panel PnlHotspot
    {
        get { return this.pnlHotspot; }
    }

    public CheckBox ChkChoice1
    {
        get { return this.chkChoice1; }
    }

    public CheckBox ChkChoice2
    {
        get { return this.chkChoice2; }
    }

    public CheckBox ChkChoice3
    {
        get { return this.chkChoice3; }
    }

    public CheckBox ChkChoice4
    {
        get { return this.chkChoice4; }
    }

    public CheckBox ChkChoice5
    {
        get { return this.chkChoice5; }
    }

    public CheckBox ChkChoice6
    {
        get { return this.chkChoice6; }
    }

    public CheckBox ChkChoice7
    {
        get { return this.chkChoice7; }
    }

    public CheckBox ChkChoice8
    {
        get { return this.chkChoice8; }
    }

    public CheckBox ChkChoice9
    {
        get { return this.chkChoice9; }
    }

    public CheckBox ChkChoice10
    {
        get { return this.chkChoice10; }
    }

    public RadioButton RdoTrue
    {
        get { return this.rdoTrue; }
    }

    public RadioButton RdoFalse
    {
        get { return this.rdoFalse; }
    }

    public RadioButton RdoYes
    {
        get { return this.rdoYes; }
    }

    public RadioButton RdoNo
    {
        get { return this.rdoNo; }
    }

    public ReorderList RolSequencing
    {
        get { return this.rolSequencing; }
    }

    public HtmlTableCell ImgCell1
    {
        get { return this.imgCell1; }
    }

    public HtmlTableCell ImgCell2
    {
        get { return this.imgCell2; }
    }

    public HtmlTableCell ImgCell3
    {
        get { return this.imgCell3; }
    }

    public HtmlTableCell ImgCell4
    {
        get { return this.imgCell4; }
    }

    public HtmlTableCell ImgCell5
    {
        get { return this.imgCell5; }
    }

    public HtmlTableCell ImgCell6
    {
        get { return this.imgCell6; }
    }

    public HtmlTableCell ImgCell7
    {
        get { return this.imgCell7; }
    }

    public HtmlTableCell ImgCell8
    {
        get { return this.imgCell8; }
    }

    public HtmlTableCell ImgCell9
    {
        get { return this.imgCell9; }
    }

    public HtmlTableCell ImgCell10
    {
        get { return this.imgCell10; }
    }

    public Image ImgChoice1
    {
        get { return this.imgChoice1; }
    }

    public Image ImgChoice2
    {
        get { return this.imgChoice2; }
    }

    public Image ImgChoice3
    {
        get { return this.imgChoice3; }
    }

    public Image ImgChoice4
    {
        get { return this.imgChoice4; }
    }

    public Image ImgChoice5
    {
        get { return this.imgChoice5; }
    }

    public Image ImgChoice6
    {
        get { return this.imgChoice6; }
    }

    public Image ImgChoice7
    {
        get { return this.imgChoice7; }
    }

    public Image ImgChoice8
    {
        get { return this.imgChoice8; }
    }

    public Image ImgChoice9
    {
        get { return this.imgChoice9; }
    }

    public Image ImgChoice10
    {
        get { return this.imgChoice10; }
    }

    public TextBox TxtEssay
    {
        get { return this.txtEssay; }
    }

    public RadioButtonList DdlAnswer
    {
        get { return this.ddlAnswer; }
    }

    public CustomValidator CvNoAnswer
    {
        get { return this.cvNoAnswer; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public TextBox TxtComments
    {
        get { return this.txtComments; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public RadioButtonList RdoRate
    {
        get { return this.rdoRate; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public DropDownList DdlValues
    {
        get { return this.ddlValues; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public Panel PnlRate
    {
        get { return this.pnlRate; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public Panel PnlComments
    {
        get { return this.pnlComments; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public Panel PnlDropDown
    {
        get { return this.pnlDropDown; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public Panel PnlMultipleAnswer
    {
        get { return this.pnlMultipleAnswer; }
    }

    public Panel PnlCalendar
    {
        get { return this.pnlCalendar; }
    }

    //ADDED BY RAYMARK COSME <11/19/2015>
    public Label LblOther
    {
        get { return this.lblOther; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public TextBox TxtOther
    {
        get { return this.txtOther; }
    }
    //ADDED BY RAYMARK COSME <11/19/2015>
    public CheckBox ChkMA1
    {
        get { return this.chkMA1; }
    }

    public CheckBox ChkMA2
    {
        get { return this.chkMA2; }
    }

    public CheckBox ChkMA3
    {
        get { return this.chkMA3; }
    }

    public CheckBox ChkMA4
    {
        get { return this.chkMA4; }
    }

    public CheckBox ChkMA5
    {
        get { return this.chkMA5; }
    }

    public CheckBox ChkMA6
    {
        get { return this.chkMA6; }
    }

    public CheckBox ChkMA7
    {
        get { return this.chkMA7; }
    }

    public CheckBox ChkMA8
    {
        get { return this.chkMA8; }
    }

    public CheckBox ChkMA9
    {
        get { return this.chkMA9; }
    }

    public CheckBox ChkMA10
    {
        get { return this.chkMA10; }
    }

    public TextBox TxtDate
    {
        get { return this.txtDate; }
    }

    //TODO: GLA 01142014 if answer is incorrect add red border
    public bool AnsweredCorrectly
    {
        set { this.tblQuestion.BorderColor = value ? "" : "red"; }
    }

    public Dictionary<string, object> ReorderObjects
    {
        get
        {
            Dictionary<string, object> reorderObjects = new Dictionary<string, object>();
            reorderObjects.Add("trReorder1", this.reorderRow1);
            reorderObjects.Add("trReorder2", this.reorderRow2);
            reorderObjects.Add("trReorder3", this.reorderRow3);
            reorderObjects.Add("trReorder4", this.reorderRow4);
            reorderObjects.Add("trReorder5", this.reorderRow5);
            reorderObjects.Add("trReorder6", this.reorderRow6);
            reorderObjects.Add("trReorder7", this.reorderRow7);
            reorderObjects.Add("trReorder8", this.reorderRow8);
            reorderObjects.Add("trReorder9", this.reorderRow9);
            reorderObjects.Add("trReorder10", this.reorderRow10);
            reorderObjects.Add("trReorder0", this.reorderRow0);
            reorderObjects.Add("lblReorder1", this.lblReorder1);
            reorderObjects.Add("lblReorder2", this.lblReorder2);
            reorderObjects.Add("lblReorder3", this.lblReorder3);
            reorderObjects.Add("lblReorder4", this.lblReorder4);
            reorderObjects.Add("lblReorder5", this.lblReorder5);
            reorderObjects.Add("lblReorder6", this.lblReorder6);
            reorderObjects.Add("lblReorder7", this.lblReorder7);
            reorderObjects.Add("lblReorder8", this.lblReorder8);
            reorderObjects.Add("lblReorder9", this.lblReorder9);
            reorderObjects.Add("lblReorder10", this.lblReorder10);
            reorderObjects.Add("ddlReorder1", this.ddlReorder1);
            reorderObjects.Add("ddlReorder2", this.ddlReorder2);
            reorderObjects.Add("ddlReorder3", this.ddlReorder3);
            reorderObjects.Add("ddlReorder4", this.ddlReorder4);
            reorderObjects.Add("ddlReorder5", this.ddlReorder5);
            reorderObjects.Add("ddlReorder6", this.ddlReorder6);
            reorderObjects.Add("ddlReorder7", this.ddlReorder7);
            reorderObjects.Add("ddlReorder8", this.ddlReorder8);
            reorderObjects.Add("ddlReorder9", this.ddlReorder9);
            reorderObjects.Add("ddlReorder10", this.ddlReorder10);
            //reorderObjects.Add("txtReorder1", this.txtReorder1);
            //reorderObjects.Add("txtReorder2", this.txtReorder2);
            //reorderObjects.Add("txtReorder3", this.txtReorder3);
            //reorderObjects.Add("txtReorder4", this.txtReorder4);
            //reorderObjects.Add("txtReorder5", this.txtReorder5);
            //reorderObjects.Add("txtReorder6", this.txtReorder6);
            //reorderObjects.Add("txtReorder7", this.txtReorder7);
            //reorderObjects.Add("txtReorder8", this.txtReorder8);
            //reorderObjects.Add("txtReorder9", this.txtReorder9);
            //reorderObjects.Add("txtReorder10", this.txtReorder10);
            return reorderObjects;
        }
    }

    public Dictionary<string, object> Objects
    {
        get
        {
            Dictionary<string, object> matchingObjects = new Dictionary<string, object>();
            matchingObjects.Add("txtCorrectMatch1", this.txtCorrectMatch1);
            matchingObjects.Add("txtCorrectMatch2", this.txtCorrectMatch2);
            matchingObjects.Add("txtCorrectMatch3", this.txtCorrectMatch3);
            matchingObjects.Add("txtCorrectMatch4", this.txtCorrectMatch4);
            matchingObjects.Add("txtCorrectMatch5", this.txtCorrectMatch5);
            matchingObjects.Add("txtCorrectMatch6", this.txtCorrectMatch6);
            matchingObjects.Add("txtCorrectMatch7", this.txtCorrectMatch7);
            matchingObjects.Add("txtCorrectMatch8", this.txtCorrectMatch8);
            matchingObjects.Add("txtCorrectMatch9", this.txtCorrectMatch9);
            matchingObjects.Add("txtCorrectMatch10", this.txtCorrectMatch10);
            //matchingObjects.Add("lblMatchLetter1", this.lblMatchLetter1);
            //matchingObjects.Add("lblMatchLetter2", this.lblMatchLetter2);
            //matchingObjects.Add("lblMatchLetter3", this.lblMatchLetter3);
            //matchingObjects.Add("lblMatchLetter4", this.lblMatchLetter4);
            //matchingObjects.Add("lblMatchLetter5", this.lblMatchLetter5);
            //matchingObjects.Add("lblMatchLetter6", this.lblMatchLetter6);
            //matchingObjects.Add("lblMatchLetter7", this.lblMatchLetter7);
            //matchingObjects.Add("lblMatchLetter8", this.lblMatchLetter8);
            //matchingObjects.Add("lblMatchLetter9", this.lblMatchLetter9);
            //matchingObjects.Add("lblMatchLetter10", this.lblMatchLetter10);
            matchingObjects.Add("lblMatchItem1", this.lblMatchItem1);
            matchingObjects.Add("lblMatchItem2", this.lblMatchItem2);
            matchingObjects.Add("lblMatchItem3", this.lblMatchItem3);
            matchingObjects.Add("lblMatchItem4", this.lblMatchItem4);
            matchingObjects.Add("lblMatchItem5", this.lblMatchItem5);
            matchingObjects.Add("lblMatchItem6", this.lblMatchItem6);
            matchingObjects.Add("lblMatchItem7", this.lblMatchItem7);
            matchingObjects.Add("lblMatchItem8", this.lblMatchItem8);
            matchingObjects.Add("lblMatchItem9", this.lblMatchItem9);
            matchingObjects.Add("lblMatchItem10", this.lblMatchItem10);
            matchingObjects.Add("lblMatchChoice1", this.lblMatchChoice1);
            matchingObjects.Add("lblMatchChoice2", this.lblMatchChoice2);
            matchingObjects.Add("lblMatchChoice3", this.lblMatchChoice3);
            matchingObjects.Add("lblMatchChoice4", this.lblMatchChoice4);
            matchingObjects.Add("lblMatchChoice5", this.lblMatchChoice5);
            matchingObjects.Add("lblMatchChoice6", this.lblMatchChoice6);
            matchingObjects.Add("lblMatchChoice7", this.lblMatchChoice7);
            matchingObjects.Add("lblMatchChoice8", this.lblMatchChoice8);
            matchingObjects.Add("lblMatchChoice9", this.lblMatchChoice9);
            matchingObjects.Add("lblMatchChoice10", this.lblMatchChoice10);
            matchingObjects.Add("trMatching1", this.matching1);
            matchingObjects.Add("trMatching2", this.matching2);
            matchingObjects.Add("trMatching3", this.matching3);
            matchingObjects.Add("trMatching4", this.matching4);
            matchingObjects.Add("trMatching5", this.matching5);
            matchingObjects.Add("trMatching6", this.matching6);
            matchingObjects.Add("trMatching7", this.matching7);
            matchingObjects.Add("trMatching8", this.matching8);
            matchingObjects.Add("trMatching9", this.matching9);
            matchingObjects.Add("trMatching10", this.matching10);
            matchingObjects.Add("tdCorrectMatch1", this.tdCorrectMatch1);
            matchingObjects.Add("tdCorrectMatch2", this.tdCorrectMatch2);
            matchingObjects.Add("tdCorrectMatch3", this.tdCorrectMatch3);
            matchingObjects.Add("tdCorrectMatch4", this.tdCorrectMatch4);
            matchingObjects.Add("tdCorrectMatch5", this.tdCorrectMatch5);
            matchingObjects.Add("tdCorrectMatch6", this.tdCorrectMatch6);
            matchingObjects.Add("tdCorrectMatch7", this.tdCorrectMatch7);
            matchingObjects.Add("tdCorrectMatch8", this.tdCorrectMatch8);
            matchingObjects.Add("tdCorrectMatch9", this.tdCorrectMatch9);
            matchingObjects.Add("tdCorrectMatch10", this.tdCorrectMatch10);
            matchingObjects.Add("tdMatchletter1", this.tdMatchletter1);
            matchingObjects.Add("tdMatchletter2", this.tdMatchletter2);
            matchingObjects.Add("tdMatchletter3", this.tdMatchletter3);
            matchingObjects.Add("tdMatchletter4", this.tdMatchletter4);
            matchingObjects.Add("tdMatchletter5", this.tdMatchletter5);
            matchingObjects.Add("tdMatchletter6", this.tdMatchletter6);
            matchingObjects.Add("tdMatchletter7", this.tdMatchletter7);
            matchingObjects.Add("tdMatchletter8", this.tdMatchletter8);
            matchingObjects.Add("tdMatchletter9", this.tdMatchletter9);
            matchingObjects.Add("tdMatchletter10", this.tdMatchletter10);
            matchingObjects.Add("tdMatchItem1", this.tdMatchItem1);
            matchingObjects.Add("tdMatchItem2", this.tdMatchItem2);
            matchingObjects.Add("tdMatchItem3", this.tdMatchItem3);
            matchingObjects.Add("tdMatchItem4", this.tdMatchItem4);
            matchingObjects.Add("tdMatchItem5", this.tdMatchItem5);
            matchingObjects.Add("tdMatchItem6", this.tdMatchItem6);
            matchingObjects.Add("tdMatchItem7", this.tdMatchItem7);
            matchingObjects.Add("tdMatchItem8", this.tdMatchItem8);
            matchingObjects.Add("tdMatchItem9", this.tdMatchItem9);
            matchingObjects.Add("tdMatchItem10", this.tdMatchItem10);
            matchingObjects.Add("tdMatchChoice1", this.tdMatchChoice1);
            matchingObjects.Add("tdMatchChoice2", this.tdMatchChoice2);
            matchingObjects.Add("tdMatchChoice3", this.tdMatchChoice3);
            matchingObjects.Add("tdMatchChoice4", this.tdMatchChoice4);
            matchingObjects.Add("tdMatchChoice5", this.tdMatchChoice5);
            matchingObjects.Add("tdMatchChoice6", this.tdMatchChoice6);
            matchingObjects.Add("tdMatchChoice7", this.tdMatchChoice7);
            matchingObjects.Add("tdMatchChoice8", this.tdMatchChoice8);
            matchingObjects.Add("tdMatchChoice9", this.tdMatchChoice9);
            matchingObjects.Add("tdMatchChoice10", this.tdMatchChoice10);
            matchingObjects.Add("trCheck1", this.trCheck1);
            matchingObjects.Add("trCheck2", this.trCheck2);
            matchingObjects.Add("trCheck3", this.trCheck3);
            matchingObjects.Add("trCheck4", this.trCheck4);
            matchingObjects.Add("trCheck5", this.trCheck5);
            matchingObjects.Add("trCheck6", this.trCheck6);
            matchingObjects.Add("trCheck7", this.trCheck7);
            matchingObjects.Add("trCheck8", this.trCheck8);
            matchingObjects.Add("trCheck9", this.trCheck9);
            matchingObjects.Add("trCheck10", this.trCheck10);
            matchingObjects.Add("chkChoice1", this.chkChoice1);
            matchingObjects.Add("chkChoice2", this.chkChoice2);
            matchingObjects.Add("chkChoice3", this.chkChoice3);
            matchingObjects.Add("chkChoice4", this.chkChoice4);
            matchingObjects.Add("chkChoice5", this.chkChoice5);
            matchingObjects.Add("chkChoice6", this.chkChoice6);
            matchingObjects.Add("chkChoice7", this.chkChoice7);
            matchingObjects.Add("chkChoice8", this.chkChoice8);
            matchingObjects.Add("chkChoice9", this.chkChoice9);
            matchingObjects.Add("chkChoice10", this.chkChoice10);
            matchingObjects.Add("rdoYes", this.rdoYes);
            matchingObjects.Add("rdoNo", this.rdoNo);
            matchingObjects.Add("tdQuestion", this.tdQuestion);
            return matchingObjects;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (SessionManager.SessionRolItems.Count > 0)
            this.RolSequencing.DataSource = SessionManager.SessionRolItems;
        else
            this.RolSequencing.DataSource = SessionManager.SessionRolInitItems;
        this.RolSequencing.DataBind();
    }

    protected void btnList_Click(object sender, EventArgs e)
    {
        //foreach (Interface face in this.RolItems)
        //{
        //    this.txtEssay.Text += face.ADPEmployeeID + Environment.NewLine;
        //}
    }

    protected void rolSequencing_onItemReorder(object sender, ReorderListItemReorderEventArgs e)
    {
        SessionManager.SessionRolItems = (List<TempQuestionnaire>)this.rolSequencing.DataSource;
    }

    protected void imgClick(object sender, ImageClickEventArgs e)
    {
        ImageButton button = (ImageButton)sender;
        if (button.BorderWidth.Value > 0)
        {
            button.BorderColor = System.Drawing.Color.Empty;
            button.BorderWidth = 0;
        }
        else
        {
            button.BorderColor = System.Drawing.Color.Red;
            button.BorderWidth = 5;
        }
    }
}
