<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userFooterControl.ascx.cs"
    Inherits="controls_userFooterControl" %>
<link href="../themes/style.css" rel="stylesheet" type="text/css" />
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 35px;">
    <tr style="border-right: 0px; border-top: 0px; border-left: 0px;
        color: #000000; border-bottom: 0px;">
        <td align="center" style="background-color: #D0D3DC; color: #001231">
            Copyright � 2013 Transcom North America and Asia
        </td>
    </tr>
</table>
