<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userNavigationControl.ascx.cs"
    Inherits="controls_userNavigationControl" %>
<link href="../themes/style.css" rel="stylesheet" type="text/css" />
<table align="center" width="760px">
    <tr>
        <td colspan="5" align="center" style="background-color: White;">
            <asp:Image ID="imgHome" runat="server" ImageUrl="~/images/TranscomU_Testing_Small.png" />
        </td>
    </tr>
    <tr>
        <td style="height: 19px; width: 25%" align="center">
            <asp:LinkButton ID="lnkTestHub" runat="server" CssClass="lbuttons" Text="Test Hub"
                OnClick="lnkTestHub_Click" />
        </td>
        <!--
        <td style="height: 19px;width:20%" align="center">
            <asp:LinkButton ID="lnkTakeTest" runat="server" CssClass="lbuttons" Text="Take Test" OnClick="lnkTakeTest_Click" />
        </td>
        -->
        <td style="height: 19px; width: 25%" align="center">
            <asp:LinkButton ID="lnkResumeTests" runat="server" CssClass="lbuttons" Text="Resume Test"
                OnClick="lnkResumeTests_Click" />
        </td>
        <td style="height: 19px; width: 25%" align="center">
            <asp:LinkButton ID="lnkViewResults" runat="server" CssClass="lbuttons" Text="Previous Tests"
                OnClick="lnkViewResults_Click" />
        </td>
        <td style="height: 19px; width: 25%" align="center">
            <asp:LinkButton ID="lnkLogout" runat="server" CssClass="lbuttons" Text="Logout" OnClick="lnkLogout_Click" />
        </td>
    </tr>
    <%--<tr style="border-right: 0px; border-top: 0px; font-size: 11px; border-left: 0px;
        color: #000000; border-bottom: 0px; font-family: Tahoma,Verdana,Arial;">
        <td style="border-right: 0px; border-top: 0px; border-left: 0px; border-bottom: 0px;
            text-align: left; height: 13px;">
        </td>
    </tr>--%>
</table>
