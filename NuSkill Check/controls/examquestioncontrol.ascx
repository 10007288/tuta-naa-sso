<%@ Control Language="C#" AutoEventWireup="true" CodeFile="examquestioncontrol.ascx.cs"
    Inherits="controls_examquestioncontrol" %>
<style type="text/css">
    .style1
    {
        width: 117px;
    }
</style>
<asp:Panel ID="pnlExamQuestion" runat="server" Width="100%" HorizontalAlign="center">
    <%--border-color: #4682b4; background-color: White;">--%>
    <table cellpadding="3" cellspacing="3" style="vertical-align: top; 
        background-color: #D8D8D8; text-align: left; border-style: groove; border-width: 4px;"
        width="100%" border="1" runat="server" id="tblQuestion"> 
        <tr>
            <td style="border-style: outset; border-width: 1px; background-color: #C1D6E7;">
                <asp:Label ID="lblItemNumber" runat="server" /><asp:Label ID="lblItemMessage" runat="server"
                    ForeColor="red" />
            </td>
        </tr>
        <tr>
            <td style="border-style: outset; border-width: 1px; background-color: #D3D3D3;" id="tdQuestion"
                runat="server">
                <asp:Label ID="lblQuestion" runat="server" CssClass="testQuestion" Width="100%"></asp:Label>
                <br />
                <asp:HyperLink ID="hypHyperlink" runat="server" Target="_blank" />
                <br />
                <asp:Image runat="server" ID="questionImg" alt="" onerror="this.onerror=null; this.src='images/noimage.png'; this.height='10%'; this.width='100%';" />
            </td>
        </tr>
        <tr>
            <td style="border-style: outset; border-width: 1px; background-color: #FFFFFF;">
                <asp:Panel ID="pnlMultipleChoice" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr id="trCheck1" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice1" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck2" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice2" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck3" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice3" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck4" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice4" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck5" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice5" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck6" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice6" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck7" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice7" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck8" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice8" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck9" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice9" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="trCheck10" runat="server">
                            <td>
                                <asp:CheckBox ID="chkChoice10" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlTrueOrFalse" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td>
                                <asp:RadioButton ID="rdoTrue" runat="server" Text="True" TextAlign="right" GroupName="torf" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rdoFalse" runat="server" Text="False" TextAlign="right" GroupName="torf" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlYesOrNo" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td>
                                <asp:RadioButton ID="rdoYes" runat="server" Text="Yes" TextAlign="right" GroupName="yorn" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rdoNo" runat="server" Text="No" TextAlign="right" GroupName="yorn" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlSequencing" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr id="reorderRow0" runat="server">
                            <td colspan="2">
                                <ajax:ReorderList ID="rolSequencing" runat="server" DragHandleAlignment="left" PostBackOnReorder="false"
                                    OnItemReorder="rolSequencing_onItemReorder" EnableViewState="true">
                                    <ItemTemplate>
                                        <asp:Panel ID="pnlTemplate" runat="server" Height="18px">
                                            <asp:Label ID="lblTemplate" runat="server" Text='<%#Bind("Item") %>' />
                                        </asp:Panel>
                                    </ItemTemplate>
                                    <DragHandleTemplate>
                                        <asp:Panel ID="pnlHandle" runat="server" BackColor="#80A7F0" Width="24px" Height="18px"
                                            BorderWidth="0">
                                            <asp:Image ID="imgArrow" runat="server" ImageUrl="~/images/arrow.JPG" />
                                        </asp:Panel>
                                    </DragHandleTemplate>
                                </ajax:ReorderList>
                                <%--<ajax:FilteredTextBoxExtender ID="ftbReorder1" runat="server" FilterType="numbers" TargetControlID="txtReorder1" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder2" runat="server" FilterType="numbers" TargetControlID="txtReorder2" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder3" runat="server" FilterType="numbers" TargetControlID="txtReorder3" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder4" runat="server" FilterType="numbers" TargetControlID="txtReorder4" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder5" runat="server" FilterType="numbers" TargetControlID="txtReorder5" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder6" runat="server" FilterType="numbers" TargetControlID="txtReorder6" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder7" runat="server" FilterType="numbers" TargetControlID="txtReorder7" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder8" runat="server" FilterType="numbers" TargetControlID="txtReorder8" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder9" runat="server" FilterType="numbers" TargetControlID="txtReorder9" />
                                <ajax:FilteredTextBoxExtender ID="ftbReorder10" runat="server" FilterType="numbers" TargetControlID="txtReorder10" />--%>
                            </td>
                        </tr>
                        <tr id="reorderRow1" runat="server">
                            <td style="width: 30px">
                                <%--<asp:TextBox ID="txtReorder1" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder1" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder1" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow2" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder2" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder2" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder2" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow3" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder3" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder3" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder3" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow4" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder4" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder4" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder4" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow5" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder5" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder5" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder5" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow6" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder6" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder6" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder6" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow7" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder7" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder7" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder7" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow8" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder8" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder8" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder8" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow9" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder9" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder9" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder9" runat="server" Width="400px" />
                            </td>
                        </tr>
                        <tr id="reorderRow10" runat="server">
                            <td>
                                <%--<asp:TextBox ID="txtReorder10" runat="server" Width="30px" MaxLength="2" />--%>
                                <asp:DropDownList ID="ddlReorder10" runat="server" Width="50px" />
                            </td>
                            <td>
                                <asp:Label ID="lblReorder10" runat="server" Width="400px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlFillInTheBlanks" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td align="left">
                                <%--<asp:TextBox ID="txtAnswer" runat="server" Width="400px" />--%>
                                <%--<asp:DropDownList ID="ddlAnswer" runat="server" />--%>
                                <asp:RadioButtonList ID="ddlAnswer" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlMatchingType" runat="server" Width="100%" Visible="false">
                    <%--<table cellpadding="5">
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="Label2" runat="server" Text="Enter the items in the order they will appear in the test in the second column. Enter the list of choices in the right column. Enter the corresponding letter of the correct choice in the left column." />
                                        </td>
                                    </tr>
                                </table>--%>
                    <table cellpadding="5">
                        <tr id="matching1" runat="server">
                            <td id="tdCorrectMatch1" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch1" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch1" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch1" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem1" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem1" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter1" runat="server" valign="top">
                                A:
                            </td>
                            <td id="tdMatchChoice1" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter1" runat="server" Text="A:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice1" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching2" runat="server">
                            <td id="tdCorrectMatch2" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch2" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch2" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch2" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem2" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem2" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter2" runat="server" valign="top">
                                B:
                            </td>
                            <td id="tdMatchChoice2" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter2" runat="server" Text="B:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice2" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching3" runat="server">
                            <td id="tdCorrectMatch3" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch3" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch3" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch3" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem3" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem3" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter3" runat="server" valign="top">
                                C:
                            </td>
                            <td id="tdMatchChoice3" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter3" runat="server" Text="C:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice3" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching4" runat="server">
                            <td id="tdCorrectMatch4" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch4" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch4" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch4" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem4" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem4" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter4" runat="server" valign="top">
                                D:
                            </td>
                            <td id="tdMatchChoice4" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter4" runat="server" Text="D:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice4" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching5" runat="server">
                            <td id="tdCorrectMatch5" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch5" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch5" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch5" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem5" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem5" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter5" runat="server" valign="top">
                                E:
                            </td>
                            <td id="tdMatchChoice5" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter5" runat="server" Text="E:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice5" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching6" runat="server">
                            <td id="tdCorrectMatch6" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch6" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch6" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch6" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem6" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem6" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter6" runat="server" valign="top">
                                F:
                            </td>
                            <td id="tdMatchChoice6" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter6" runat="server" Text="F:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice6" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching7" runat="server">
                            <td id="tdCorrectMatch7" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch7" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch7" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch7" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem7" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem7" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter7" runat="server" valign="top">
                                G:
                            </td>
                            <td id="tdMatchChoice7" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter7" runat="server" Text="G:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice7" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching8" runat="server">
                            <td id="tdCorrectMatch8" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch8" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch8" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch8" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem8" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem8" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter8" runat="server" valign="top">
                                H:
                            </td>
                            <td id="tdMatchChoice8" runat="server">
                                <%--<asp:Label ID="lblMatchLetter8" runat="server" Text="H:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice8" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching9" runat="server">
                            <td id="tdCorrectMatch9" runat="server" valign="top">
                                <asp:TextBox ID="txtCorrectMatch9" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch9" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch9" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem9" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem9" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter9" runat="server" valign="top">
                                I:
                            </td>
                            <td id="tdMatchChoice9" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter9" runat="server" Text="I:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice9" runat="server" />
                            </td>
                        </tr>
                        <tr id="matching10" runat="server" valign="top">
                            <td id="tdCorrectMatch10" runat="server">
                                <asp:TextBox ID="txtCorrectMatch10" runat="server" Width="30px" MaxLength="1" />
                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch10" runat="server" FilterMode="validchars"
                                    ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch10" FilterType="Custom" />
                            </td>
                            <td id="tdMatchItem10" runat="server" valign="top">
                                <asp:Label ID="lblMatchItem10" runat="server" Width="200px" />
                            </td>
                            <td id="tdMatchletter10" runat="server" valign="top">
                                J:
                            </td>
                            <td id="tdMatchChoice10" runat="server" valign="top">
                                <%--<asp:Label ID="lblMatchLetter10" runat="server" Text="J:" Width="20px" />--%>
                                <asp:Label ID="lblMatchChoice10" runat="server"  />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlEssay" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td align="center">
                                <asp:TextBox ID="txtEssay" runat="server" TextMode="MultiLine" Columns="80" Rows="10"
                                    MaxLength="8000" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                <asp:Panel ID="pnlComments" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td align="center">
                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Columns="80" Rows="10"
                                    MaxLength="8000" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                <asp:Panel ID="pnlRate" runat="server" Visible="false" Width="100%">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td align="left">
                                <asp:RadioButtonList ID="rdoRate" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
               <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                <asp:Panel ID="pnlDropDown" runat="server" Visible="false" Width="100%">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlValues" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                <asp:Panel ID="pnlMultipleAnswer" runat="server" Visible="false" Width="100%">
                    <table cellpadding="5" width="100%">
                        <tr id="tr1" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA1" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr2" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA2" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr3" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA3" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr4" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA4" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr5" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA5" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr6" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA6" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr7" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA7" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr8" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA8" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr9" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA9" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr id="tr10" runat="server">
                            <td>
                                <asp:CheckBox ID="chkMA10" runat="server" TextAlign="Right" />
                            </td>
                        </tr>
<%--                        <tr>
                            <td align="left" class="style1">
                                <asp:CheckBoxList ID="chkMA" runat="server">
                                </asp:CheckBoxList>
                            </td>
                        </tr>--%>
                        <tr>
                            <td align="left" class="style1">
                                <asp:Label ID="lblOther" runat="server" Text="If Other, please specify :"></asp:Label>
                                <asp:TextBox ID="txtOther" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlCalendar" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td align="left">
                                <asp:TextBox ID="txtDate" runat="server" /><asp:ImageButton ID="imgCalendarStartDate"
                                    runat="server" ImageUrl="~/images/icon-calendar.gif" />
                                <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtDate"
                                    PopupButtonID="imgCalendarStartDate" Format="yyyy/MM/dd" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlHotspot" runat="server" Width="100%" Visible="false">
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td id="imgCell1" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice1" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell2" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice2" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell3" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice3" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell4" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice4" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell5" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice5" runat="server" OnClick="imgClick" />
                            </td>
                        </tr>
                        <tr>
                            <td id="imgCell6" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice6" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell7" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice7" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell8" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice8" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell9" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice9" runat="server" OnClick="imgClick" />
                            </td>
                            <td id="imgCell10" runat="server" align="center" valign="middle">
                                <asp:ImageButton ID="imgChoice10" runat="server" OnClick="imgClick" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:CustomValidator ID="cvNoAnswer" runat="server" ErrorMessage="You did not provide any answer to this question."
                    Display="static" />
            </td>
        </tr>
    </table>
</asp:Panel>
