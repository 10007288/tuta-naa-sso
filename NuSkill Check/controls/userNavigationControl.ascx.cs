using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class controls_userNavigationControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
        {
            this.lnkLogout.Visible = false;
            this.lnkResumeTests.Visible = false;
            this.lnkTakeTest.Visible = false;
            this.lnkTestHub.Visible = false;
            this.lnkViewResults.Visible = false;
        }
        else
        {
            this.lnkLogout.Visible = true;
            this.lnkResumeTests.Visible = true;
            this.lnkTakeTest.Visible = true;
            this.lnkTestHub.Visible = true;
            this.lnkViewResults.Visible = true;
        }
    }

    protected void lnkTestHub_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();

        FormsAuthentication.SignOut();
        Response.Redirect("~/login.aspx");
    }

    protected void lnkTakeTest_Click(object sender, EventArgs e)
    {
        SessionManager.SessionSubCategory = 0;
        SessionManager.SessionAccountCampaignID = 0;
        Response.Redirect("~/alltests.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkResumeTests_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/resumetests.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkViewResults_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/takentests.aspx?" + Request.QueryString.ToString());
    }
}
