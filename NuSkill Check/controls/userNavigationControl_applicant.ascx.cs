using System;
using System.Web.UI;

public partial class userNavigationControl_applicant : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
        {
            lnkLogout.Visible = false;
            lnkResumeTests.Visible = false;
            lnkTakeTest.Visible = false;
            lnkTestHub.Visible = false;
            lnkViewResults.Visible = false;
        }
        else
        {
            lnkLogout.Visible = true;
            lnkResumeTests.Visible = true;
            lnkTakeTest.Visible = true;
            lnkTestHub.Visible = true;
            lnkViewResults.Visible = true;
        }
    }
    protected void lnkTestHub_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantstesthub.aspx?" + Request.QueryString);
    }
    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("login.aspx?" + Request.QueryString);
    }
    protected void lnkTakeTest_Click(object sender, EventArgs e)
    {
        SessionManager.SessionSubCategory = 0;
        SessionManager.SessionAccountCampaignID = 0;
        Response.Redirect("applicantsalltests.aspx?" + Request.QueryString);
    }
    protected void lnkResumeTests_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantsresumetests.aspx?" + Request.QueryString);
    }
    protected void lnkViewResults_Click(object sender, EventArgs e)
    {
        Response.Redirect("applicantstakentests.aspx?" + Request.QueryString);
    }
}
