using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuSkill.Business;

public partial class applicantsalltests : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("applicantslogin.aspx?" + Request.QueryString);
        if (!IsPostBack)
        {
            try
            {
                //TestCategoryUserHistory[] categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false,
                //                                                                                    true, 0, 220028);

                TestCategoryUserHistory[] categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false,
                                                                                                    true, 0, 220330);

                //TestCategoryUserHistory[] categories;
                //if (SessionManager.SessionAccountCampaignID > 0 || SessionManager.SessionSubCategory > 0)
                //    categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false,
                //                                                              false, true,
                //                                                              SessionManager.SessionAccountCampaignID,
                //                                                              SessionManager.SessionSubCategory);
                //else if (SessionManager.SessionGroupID != 0)
                //    categories = TestCategoryUserHistory.SelectAllTestGroup(SessionManager.SessionUsername, false, false,
                //                                                            SessionManager.SessionGroupID);
                //else
                //    categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false,
                //                                                              false, false, 0,
                //                                                              SessionManager.SessionSubCategory);
                gvAvailableTests.DataSource = categories;
                gvAvailableTests.DataBind();
                foreach (GridViewRow row in gvAvailableTests.Rows)
                {
                    Label lblPassed = row.FindControl("lblPassed") as Label;
                    Label lblTopScore = row.FindControl("lblTopScore") as Label;
                    Image imgPassOrFail = row.FindControl("imgPassOrFail") as Image;
                    if (lblPassed != null && imgPassOrFail != null && lblTopScore != null)
                    {
                        if (lblTopScore.Text == "-1")
                        {
                            lblTopScore.Text = string.Empty;
                            imgPassOrFail.Visible = false;
                        }
                        else if (lblPassed.Text == "True")
                            imgPassOrFail.ImageUrl = "~/images/icon-pass.gif";
                        else if (lblTopScore.Text != "0")
                            imgPassOrFail.ImageUrl = "~/images/icon-fail.gif";
                        else
                        {
                            lblTopScore.Text = string.Empty;
                            imgPassOrFail.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(4, "applicantsalltests.aspx", "Page_Load", ex.Message);
                SessionManager.SessionMainText =
                    "System could not populate the test list. Please contact your examiner/administrator.";
                Response.Redirect("applicantserror.aspx?" + Request.QueryString);
            }
        }
    }

    protected void gvAvailableTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool redirect = false;
        if (e.CommandName == "Take")
        {
            try
            {
                GridViewRow row = (GridViewRow) (((LinkButton) e.CommandSource).NamingContainer);
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                if (category != null)
                {
                    int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
                    if (testCount < category.TestLimit || category.TestLimit == 0)
                    {
                        SessionManager.SessionTestCategoryID = category.TestCategoryID;
                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questionnaire.Length > 0)
                        {
                            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                            redirect = true;
                        }
                        else
                        {
                            lblMessage.Text = "This exam has no items.";
                            mpeMessage.Show();
                        }
                    }
                    else
                    {
                        lblMessage.Text = "You have already used up all tries available for this test.";
                        mpeMessage.Show();
                    }
                }
                else
                {
                    lblMessage.Text =
                        "This exam has been removed from the system or has already expired. Please contact your examiner.";
                    mpeMessage.Show();
                }
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(4, "applicantsalltests.aspx", "gvAvailableTests_RowCommand", ex.Message);
                redirect = false;
                SessionManager.SessionMainText = "An error was encountered while trying to load an exam.";
                Response.Redirect("applicantserror.aspx?" + Request.QueryString);
            }
        }
        if (redirect)
        {
            SessionManager.SessionExamAction = "newexam";
            Response.Redirect("applicantstestdefault.aspx?" + Request.QueryString);
        }
    }
}
