﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true"
    CodeFile="applicantslogin.aspx.cs" Inherits="applicantslogin" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center" DefaultButton="">
            <asp:UpdatePanel ID="udpSub" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlUpdate" runat="server" Height="80px" HorizontalAlign="Center">
                        <asp:UpdateProgress ID="udpProgress" runat="server">
                            <ProgressTemplate>
                                <asp:Panel ID="pnlBuffer" runat="server" Height="80px" HorizontalAlign="Center">
                                    <table align="center">
                                        <tr>
                                            <td align="center" style="height: 80px">
                                                <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" ImageAlign="Middle" />
                                                <br />
                                                <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:Panel>
                    <asp:Panel runat="server" DefaultButton="lnkLogin" ID="pnlMain" CssClass="pnlMain">
                        <asp:Table runat="server" ID="tblLogin" HorizontalAlign="center" Width="350px" defaultbutton="lnkLogin"
                            CssClass="tblLogin">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <table width="100%" align="center">
                                        <tr style="height: 60px;">
                                            <td colspan="2" style="color: #FFF; text-align: center;">
                                                Welcome to Transcom University
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #ffcc00; padding-left: 44px" colspan="2">
                                                <asp:CustomValidator ID="cvLoginError" runat="server" ForeColor="red" ErrorMessage="Invalid login credentials!"
                                                    Display="dynamic" CssClass="validatorStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #ffcc00; padding-left: 44px">
                                                ATS ID
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtATSID" runat="server" />&nbsp;
                                                <asp:CustomValidator ID="cvATSID" runat="server" ErrorMessage="*" CssClass="errorline"
                                                    Display="Dynamic" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #ffcc00; padding-left: 44px">
                                                First Name
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtFirstName" runat="server" />&nbsp;
                                                <asp:CustomValidator ID="cvFirstName" runat="server" ErrorMessage="*" CssClass="errorline"
                                                    Display="Dynamic" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #ffcc00; padding-left: 44px">
                                                Last Name
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtLastName" runat="server" />&nbsp;
                                                <asp:CustomValidator ID="cvLastName" runat="server" ErrorMessage="*" CssClass="errorline"
                                                    Display="Dynamic" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #ffcc00; padding-left: 44px">
                                                Site
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlSites" runat="server" DataTextField="CompanySite" DataValueField="CompanySiteShort"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Text="- select -" Value="" />
                                                    <asp:ListItem Text="Manila 1 - Pasig" Value="Manila 1 - Pasig" />
                                                    <asp:ListItem Text="Manila 2 - EDSA" Value="Manila 2 - EDSA" />
                                                    <asp:ListItem Text="ILOILO" Value="ILOILO" />
                                                    <asp:ListItem Text="BACOLOD" Value="BACOLOD" />
                                                    <asp:ListItem Text="North America" Value="North America" />
                                                </asp:DropDownList>
                                                &nbsp;
                                                <asp:CustomValidator ID="cvLocation" runat="server" ErrorMessage="*" CssClass="errorline"
                                                    Display="Dynamic" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #ffcc00; padding-left: 44px">
                                                <asp:LinkButton ID="lnkRegister" runat="server" Text="Register" CssClass="linkbutton"
                                                    OnClick="lnkRegister_Click" />
                                            </td>
                                            <td align="right" style="padding: 10px 38px 15px 0">
                                                <div id="managecontrol" style="width: 100%">
                                                    <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" OnClick="lnkLogin_Click" />
                                                </div>
                                                <%--<p>
                                                    <asp:Label ID="lblNotify" ForeColor="red" runat="server" />
                                                </p>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:TableCell></asp:TableRow></asp:Table></asp:Panel></ContentTemplate></asp:UpdatePanel></asp:Panel><%--<div runat="server" id="ctrTULinkInternal">
            <div id="footerLink">
                <label>
                    To access this outside the Transcom University Network, please use this address:
                </label>
                <asp:LinkButton runat="server" ID="lbnTUInternal" Text="https://applications.transcom.com/transcomuniversity"
                    PostBackUrl="https://applications.transcom.com/transcomuniversity" />
            </div>
        </div>--%></div></asp:Content>