using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using NuComm.Security.Encryption;
using NuSkill.Business;
using TheLibrary.ErrorLogger;

public partial class hotlink : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            int testCategoryIDValue = 0;
            try
            {
                testCategoryIDValue = Convert.ToInt32(UTF8.DecryptText(Request.QueryString.Get("tid")));
                this.lblExamID.Text = testCategoryIDValue.ToString();
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(4, "hotlink.aspx", "Page_Load", ex.Message);
                this.lblTaking.Text = "The exam you wish to take is invalid.";
                this.pnlMain.Visible = false;
                this.trExamName.Visible = false;
            }
            TestCategory category = TestCategory.Select(testCategoryIDValue, false);
            if (category == null)
            {
                this.lblTaking.Text = "The exam you wish to take has expired.";
                this.pnlMain.Visible = false;
                this.trExamName.Visible = false;
            }
        }
    }

    private DataSet LoadCompanySites()
    {
        try
        {
            DataSet ds = new DataSet();
            int num = 0;
            NuComm.Connection connection = new NuComm.Connection();
            connection.DatabaseServer = Config.Vader2DatabaseServer();
            connection.DatabaseName = "Testing"; //Config.TestingDatabaseName();
            connection.UserName = "us_Survey";//Config.TestingDatabaseUsername();
            connection.Password = "";//Config.TestingDatabasePassword();
            connection.AddParameter("@CIMNumber", 0, SqlDbType.Int);
            connection.StoredProcedureName = "dbo.pr_WeeklyTesting_Lkp_CompanySiteByCIM";
            connection.Fill(ref ds);
            connection.DisconnectInstance();
            connection = null;
            return ds;
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "hotlink.aspx", "LoadCompanySites", ex.Message);
            return null;
        }
    }

    protected void lnkLogin_Click(object sender, EventArgs e)
    {
        try
        {
            int temp = 0;
            if (int.TryParse(this.txtUsername.Text.Trim(), out temp))
            {

                if (LoadVerifyUser())
                {
                    SessionManager.SessionSessionID = Guid.NewGuid();
                    SessionManager.SessionUsername = this.txtUsername.Text.Trim();
                    LoginSession session = LoginSession.Select(this.txtUsername.Text.Trim());
                    SessionManager.SessionCim = Convert.ToInt32(this.txtUsername.Text.Trim());
                    //SessionManager.SessionCompanySite = this.ddlSites.SelectedValue;
                    if (session == null)
                    {
                        try
                        {
                            session = new LoginSession(this.txtUsername.Text.Trim(), SessionManager.SessionSessionID);
                            session.Insert();
                        }
                        catch
                        {
                            SessionManager.SessionMainText = "System could not log you in. Please try again later.";
                            Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
                        }
                    }
                    else if (session.LoginSessionID != SessionManager.SessionSessionID)
                        try
                        {
                            session.Update();
                        }
                        catch
                        {
                            SessionManager.SessionMainText = "System could not log you in. Please try again later.";
                        }
                    SessionManager.SessionSite = "INT";// this.ddlSites.SelectedValue;TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                    this.GoToExam();
                }
                else
                    this.cvLoginError.IsValid = false;
            }
            //if (InternalCredentials.LoginUser(temp, this.txtPassword.Text.Trim()).Result == 0)

            else if (Registration.ValidateLogin(this.txtUsername.Text.Trim(), this.txtPassword.Text.Trim()))
            {
                SessionManager.SessionSessionID = Guid.NewGuid();
                SessionManager.SessionUsername = this.txtUsername.Text.Trim();
                LoginSession session = LoginSession.Select(this.txtUsername.Text.Trim());
                //SessionManager.SessionCompanySite = this.ddlSites.SelectedValue;
                if (session == null)
                {
                    try
                    {
                        session = new LoginSession(this.txtUsername.Text.Trim(), SessionManager.SessionSessionID);
                        session.Insert();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Write(Config.ApplicationName, "hotlink.aspx", "lnkLogin_Click", ex.Message);
                        SessionManager.SessionMainText = "System could not log you in. Please try again later.";
                        Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
                    }
                }
                else if (session.LoginSessionID != SessionManager.SessionSessionID)
                    try
                    {
                        session.Update();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Write(Config.ApplicationName, "hotlink.aspx", "lnkLogin_Click", ex.Message);
                        SessionManager.SessionMainText = "System could not log you in. Please try again later.";
                        Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
                    }
                SessionManager.SessionSite = "EXT";//this.ddlSites.SelectedValue;TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                this.GoToExam();
            }
        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("Thread was being aborted"))
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(4, "hotlink.aspx", "lnkLogin_Click", ex.Message);
                this.lblTaking.Text = "System has encountered an unknown error. Please contact your examiner/administrator.";
                //Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
            }
        }
    }

    private bool LoadVerifyUser()
    {
        try
        {
            DataSet ds = new DataSet();
            int num = 0;
            NuComm.Connection connection = new NuComm.Connection();
            connection.DatabaseServer = Config.Vader2DatabaseServer();// @"Vader2\Callisto";
            connection.DatabaseName = Config.Vader2DatabaseName();// "BlackBox";
            connection.UserName = Config.Vader2DatabaseUsername();// "us_RAS";
            connection.Password = Config.Vader2DatabasePassword();// "";
            connection.AddParameter("@EmpID", this.txtUsername.Text.Trim(), SqlDbType.VarChar);
            connection.AddParameter("@Password", this.txtPassword.Text.Trim(), SqlDbType.VarChar);
            //connection.AddParameter("@CompanyID", 2, SqlDbType.Int);
            connection.StoredProcedureName = "dbo.pr_RAS_ValidateUser";
            if (connection.Fill(ref ds) && (ds.Tables[0].Rows.Count > 0))
            {
                if (ds.Tables[0].Rows[0][0].ToString() == "1")
                    return true;
            }
            connection.DisconnectInstance();
            connection = null;
            return false;
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(4, "hotlink.aspx", "LoadVerifyUser", ex.Message);
            return false;
        }
    }

    protected void GoToExam()
    {
        //ADDED BY RAYMARK COSME <02/13/2016>
        TestCategory category = TestCategory.Select(Convert.ToInt32(this.lblExamID.Text), false);
        
        if (category != null)
        {
            SaveTestResponse[] responses = SaveTestResponse.CheckPendingEssays(SessionManager.SessionUsername, Convert.ToInt32(lblExamID.Text.Trim()));
            if (responses.Length > 0)
            {
                this.lblTaking.Text = "You have taken this test. It is still pending completion/checking.";
                this.pnlMain.Visible = false;
                this.trExamName.Visible = false;
            }
            else
            {
                int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
                int testRetake = TestCategory.checkRetake(category.TestCategoryID);
                if (testCount < category.TestLimit || category.TestLimit == 0)
                {
                    bool hasPassed = false;
                    TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                    foreach (TestTaken test in taken)
                    {
                        if (test.Passed && test.UserID == SessionManager.SessionUsername)
                        {
                            hasPassed = true;
                            break;
                        }
                    }
                    if (hasPassed == true)
                    {
                        if (testRetake == 1)
                        {
                            SessionManager.SessionTestCategoryID = category.TestCategoryID;
                            Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                            if (questionnaire.Length > 0)
                            {
                                SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                                foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                                SessionManager.SessionExamAction = "newexam";
                                SessionManager.SessionExamIsRetake = "retake"; //TODO: GLA 01092014
                                Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                            }
                            else
                            {
                                this.lblTaking.Text = "This exam has no items.";
                            }
                        }
                        else
                        {
                            this.lblTaking.Text = "This exam has no retake. Please contact your examiner.";
                            this.pnlMain.Visible = false;
                            this.trExamName.Visible = false;
                        }
                    }
                    else
                    {
                        SessionManager.SessionTestCategoryID = category.TestCategoryID;
                        Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questionnaire.Length > 0)
                        {
                            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager.SessionQuestionnaire.Add(question);
                            SessionManager.SessionExamAction = "newexam";
                            Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                        }
                    }
                }
                else
                {
                    this.lblTaking.Text = "You have already used up all tries available for this test.";
                }
            }
        }
        else
        {
            lblTaking.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
        }
    }

    //Old Code - GoToExam
    //    {
    //    TestCategory category = TestCategory.Select(Convert.ToInt32(this.lblExamID.Text), false);
    //    if (category != null)
    //    {
    //        SaveTestResponse[] responses = SaveTestResponse.CheckPendingEssays(SessionManager.SessionUsername, Convert.ToInt32(lblExamID.Text.Trim()));
    //        if (responses.Length > 0)
    //        {
    //            this.lblTaking.Text = "You have taken this test. It is still pending completion/checking.";
    //            this.pnlMain.Visible = false;
    //            this.trExamName.Visible = false;
    //        }
    //        else
    //        {
    //            int testCount = TestTaken.Count(SessionManager.SessionUsername, category.TestCategoryID);
    //            if (testCount < category.TestLimit || category.TestLimit == 0)
    //            {
    //                bool hasPassed = false;
    //                TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
    //                foreach (TestTaken test in taken)
    //                {
    //                    if (test.Passed && test.UserID == SessionManager.SessionUsername)
    //                    {
    //                        hasPassed = true;
    //                        break;
    //                    }
    //                }
    //                if (hasPassed == true)
    //                {
    //                    this.lblTaking.Text = "You have already completed and passed this exam.";
    //                    this.pnlMain.Visible = false;
    //                    this.trExamName.Visible = false;
    //                }
    //                else
    //                {
    //                    SessionManager.SessionTestCategoryID = category.TestCategoryID;
    //                    Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
    //                    if (questionnaire.Length > 0)
    //                    {
    //                        SessionManager.SessionQuestionnaire = new List<Questionnaire>();
    //                        foreach (Questionnaire question in questionnaire)
    //                            SessionManager.SessionQuestionnaire.Add(question);
    //                        SessionManager.SessionExamAction = "newexam";
    //                        Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
    //                    }
    //                    else
    //                    {
    //                        this.lblTaking.Text = "This exam has no items.";
    //                    }
    //                }

    //            }
    //            else
    //            {
    //                this.lblTaking.Text = "You have already used up all tries available for this test.";
    //            }
    //        }
    //    }
    //    else
    //    {
    //        lblTaking.Text = "This exam has been removed from the system or has already expired. Please contact your examiner.";
    //    }
    //}
}