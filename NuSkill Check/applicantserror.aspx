<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site_applicant.master"
    AutoEventWireup="true" CodeFile="applicantserror.aspx.cs" Inherits="applicantserror" Title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center">
        <table width="100%" style="height: 400px">
            <tr>
                <td>
                    <asp:Label ID="lblError" runat="server" />
                    <br />
                    <asp:Label ID="lblContact" runat="server" Text="Please contct your examiner/administrator." />
                    <br /><br />
                    <asp:Button ID="btnError" runat="server" Text="Return" OnClick="btnError_Click" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
