<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site_applicant.master"
    AutoEventWireup="true" CodeFile="applicantsregistration_bak.aspx.cs" Inherits="applicantsregistration_bak"
    Title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Width="100%">
        <div align="left" style="padding-left: 10px">
            <asp:LinkButton ID="lnkLogin" runat="server" Text="<< Back to Login page" CssClass="linkbutton"
                OnClick="lnkLogin_Click" />
        </div>
        <table align="center" style="border: 1px solid black; padding: 20px">
            <tr>
                <td align="left" colspan="3">
                    <asp:Label ID="lblRequired" runat="server" Text="*All fields are required." CssClass="warning" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblATSID" runat="server" Text="ATS ID:" />
                </td>
                <td align="left">
                    <asp:TextBox ID="txtATSID" runat="server" />
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvATSID" runat="server" ErrorMessage="ATS ID is required."
                        CssClass="errorline" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblFirstName" runat="server" Text="First Name:" />
                </td>
                <td align="left">
                    <asp:TextBox ID="txtFirstName" runat="server" />
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvFirstName" runat="server" ErrorMessage="First Name is required."
                        CssClass="errorline" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblLastName" runat="server" Text="Last Name:" />
                </td>
                <td align="left">
                    <asp:TextBox ID="txtLastName" runat="server" />
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvLastName" runat="server" ErrorMessage="Last Name is required."
                        CssClass="errorline" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblLocation" runat="server" Text="Site:" />
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlSites" runat="server" DataTextField="CompanySite" DataValueField="CompanySiteShort"
                        AppendDataBoundItems="true" Width="120">
                        <asp:ListItem Text="- select -" Value="" />
                        <asp:ListItem Text="Manila 1 - Pasig" Value="Manila 1 - Pasig" />
                        <asp:ListItem Text="Manila 2 - EDSA" Value="Manila 2 - EDSA" />
                        <asp:ListItem Text="ILOILO" Value="ILOILO" />
                        <asp:ListItem Text="BACOLOD" Value="BACOLOD" />
                    </asp:DropDownList>
                </td>
                <td align="left">
                    <asp:CustomValidator ID="cvLocation" runat="server" ErrorMessage="Site is required."
                        CssClass="errorline" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                </td>
                <td align="left" colspan="2">
                    <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="buttons" OnClick="btnRegister_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeFinished" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidFinished" PopupControlID="pnlFinished" />
    <asp:HiddenField ID="hidFinished" runat="server" />
    <asp:Panel ID="pnlFinished" runat="server" Width="300px" CssClass="modalPopup" HorizontalAlign="center">
        <table width="100%" cellpadding="3">
            <tr>
                <td>
                    <asp:Label ID="lblResult" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="buttons" OnClick="btnReturn_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
