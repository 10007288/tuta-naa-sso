<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_frontend_site.master"
    AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" Title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center">
        <asp:UpdatePanel ID="udpSub" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUpdate" runat="server" Height="150px">
                    <asp:UpdateProgress ID="udpProgress" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="pnlBuffer" runat="server" Height="150px">
                                <table align="center">
                                    <tr>
                                        <td align="center" style="height: 200px">
                                            <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                            <br />
                                            <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:Panel>
                <div align="center">
                    <asp:Panel ID="pnlMain" runat="server" Width="300px" HorizontalAlign="center" CssClass="panelStandard" BorderWidth="0px">
                        <table align="center">
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:CustomValidator ID="cvLoginError" runat="server" ErrorMessage="Invalid Login Credentials."
                                        Display="dynamic" CssClass="validatorStyle" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LoginView ID="LoginView1" runat="server">
                                        <AnonymousTemplate>
                                            <div style="color: #001a33; text-align: center;">
                                                <asp:Label ID="lblWelHdr" runat="server" Text="Label">Welcome to Transcom University Testing! To log in, use your Google account.</asp:Label>
                                            </div>
                                            <asp:Label ID="ModelErrorMessage1" runat="server"></asp:Label>
                                            <div style="text-align: center;">
                                                <asp:ImageButton ID="btnRequestLogin" runat="server" Text="Google Login" 
                                                    OnClick="btnRequestLogin_Click" ImageUrl="~/images/btn_google_signin_dark_normal_web@2x.png" 
                                                    ToolTip="Sign in with your Google Account" style="height: 60px; margin: 20px;" />
                                            </div>
                                        </AnonymousTemplate>
                                    </asp:LoginView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <label>
                                        or</label>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:HyperLink ID="LinkButton1" runat="server" Text="Click here if Applicant" NavigateUrl="~/applicantslogin.aspx"
                                        CssClass="linkbutton" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
