using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class AppRights
    {
        #region Properties
        private int _appRightsID;

        public int AppRightsID
        {
            get { return _appRightsID; }
            set { _appRightsID = value; }
        }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }


        private string _descrption;

        public string Description
        {
            get { return _descrption; }
            set { _descrption = value; }
        }
	
	
        #endregion

        #region Constructors
        public AppRights()
        { }
        #endregion

        #region Methods
        public static AppRights[] SelectAll()
        {
            AppRightsDal dal = new AppRightsDal();
            return Conversion.SetProperties<AppRights>(dal.SelectAll());
        }
        #endregion
    }
}
