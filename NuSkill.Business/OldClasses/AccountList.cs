using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class AccountList
    {

        public AccountList()
        { }

        public AccountList(int accountID, string account)
        {
            this._account = account;
            this._accountID = accountID;
        }

        private int _accountID;

        public int AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }

        private string _account;

        public string Account
        {
            get { return _account; }
            set { _account = value; }
        }

        public static AccountList[] SelectAll(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string>[] dicts = new Dictionary<string,string>[2];
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            dicts[0] = null;
            dicts[1] = dict;
            
            return Conversion.SetProperties<AccountList>(dal.SelectAll(showAll), dicts);
        }

        public static AccountList[] SelectAllVader2(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string>[] dicts = new Dictionary<string, string>[2];
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            dicts[0] = null;
            dicts[1] = dict;

            return Conversion.SetProperties<AccountList>(dal.SelectAllVader2(showAll), dicts);
        }

        public static AccountList[] SelectAccountsVader2(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            return Conversion.SetProperties<AccountList>(dal.SelectAllVader2(showAll), 0);
        }

        public static AccountList[] SelectGenericCampaignsVader2(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            return Conversion.SetProperties<AccountList>(dal.SelectAllVader2(showAll), dict, 1);
        }

        public static AccountList[] SelectAccounts(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            return Conversion.SetProperties<AccountList>(dal.SelectAll(showAll), 0);
        }

        public static AccountList[] SelectGenericCampaigns(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            return Conversion.SetProperties<AccountList>(dal.SelectAll(showAll), dict, 1);
        }
    }
}
