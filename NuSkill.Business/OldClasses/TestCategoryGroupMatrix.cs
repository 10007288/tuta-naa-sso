using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class TestCategoryGroupMatrix
    {
        #region properties
        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private int _testGroupID;

        public int TestGroupID
        {
            get { return _testGroupID; }
            set { _testGroupID = value; }
        }

        #endregion

        #region constructors    
        public TestCategoryGroupMatrix()
        { }

        public TestCategoryGroupMatrix(int testCategoryID, int testGroupID)
        {
            this._testCategoryID = testCategoryID;
            this._testGroupID = testGroupID;
        }
        #endregion

        #region methodss
        public static TestCategoryGroupMatrix[] SelectAll()
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            return Conversion.SetProperties<TestCategoryGroupMatrix>(dal.SelectAll());
        }

        public static TestCategoryGroupMatrix[] SelectByCategory(int testCategoryID)
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            return Conversion.SetProperties<TestCategoryGroupMatrix>(dal.SelectByCategory(testCategoryID));
        }

        public static TestCategoryGroupMatrix[] SelectByGroup(int testGroupID)
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            return Conversion.SetProperties<TestCategoryGroupMatrix>(dal.SelectByGroup(testGroupID));
        }

        public void DeleteSingle()
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            dal.DeleteSingle(this._testCategoryID, this._testGroupID);
        }

        public static void DeleteByCategory(int testCategoryID)
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            dal.DeleteByCategory(testCategoryID);
        }

        public static void DeleteByGroup(int testGroupID)
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            dal.DeleteByGroup(testGroupID);
        }

        public void Insert()
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            dal.Insert(this._testCategoryID, this._testGroupID);
        }

        public static DataSet SelectAllGroupByCategory(int testCategoryID)
        {
            TestCategoryGroupMatrixDAL dal = new TestCategoryGroupMatrixDAL();
            return dal.SelectAllGroupByCategory(testCategoryID);
        }
        #endregion
    }
}
