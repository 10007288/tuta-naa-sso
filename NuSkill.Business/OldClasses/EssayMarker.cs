using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;


namespace NuSkill.Business
{
    public class EssayMarker
    {
        #region properties
        private int _essayMarkerID;

        public int EssayMarkerID
        {
            get { return _essayMarkerID; }
            set { _essayMarkerID = value; }
        }

        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private int _testResponseID;

        public int TestResponseID
        {
            get { return _testResponseID; }
            set { _testResponseID = value; }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private DateTime _dateMarked;

        public DateTime DateMarked
        {
            get { return _dateMarked; }
            set { _dateMarked = value; }
        }

        private string _checkecComments;

        public string CheckerComments
        {
            get { return _checkecComments; }
            set { _checkecComments = value; }
        }
        #endregion

        #region Constructors
        public EssayMarker()
        { }

        public EssayMarker(string userID, int testResponseID, int score, string checkerComments)
        {
            this._userID = userID;
            this._testResponseID = testResponseID;
            this._score = score;
            this._checkecComments = checkerComments;
        }
        #endregion

        #region methods
        public int Insert()
        {
            EssayMarkerDal dal = new EssayMarkerDal();
            return dal.Insert(this._userID, this._testResponseID, this._score, this._checkecComments);
        }

        public static EssayMarker[] SelectByTestTaken(int testTakenID)
        {
            EssayMarkerDal dal = new EssayMarkerDal();
            return Conversion.SetProperties<EssayMarker>(dal.SelectByTestTaken(testTakenID));
        }

        public static EssayMarker SelectByTestResponse(int testResponseID)
        {
            EssayMarkerDal dal = new EssayMarkerDal();
            EssayMarker[] essayMarkers = Conversion.SetProperties<EssayMarker>(dal.SelectByTestResponse(testResponseID));
            return essayMarkers == null || essayMarkers.Length < 1 ? null : essayMarkers[0];
        
        }
        #endregion
    }
}
