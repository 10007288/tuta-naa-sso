using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    public class TestReports
    {
        private int _reportID;

        public int ReportID
        {
            get { return _reportID; }
            set { _reportID = value; }
        }

        private string _displayName;

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private string _reportURL;

        public string ReportURL
        {
            get { return _reportURL; }
            set { _reportURL = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        public static TestReports[] SelectAll()
        {
            TestReportsDal dal = new TestReportsDal();
            return Conversion.SetProperties<TestReports>(dal.SelectAll());
        }
    }
}
