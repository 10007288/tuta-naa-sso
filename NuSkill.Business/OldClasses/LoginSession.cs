using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class LoginSession
    {
        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private Guid _loginSessionID;

        public Guid LoginSessionID
        {
            get { return _loginSessionID; }
            set { _loginSessionID = value; }
        }

        public LoginSession()
        {
        }

        public LoginSession(string userID, Guid loginSessionID)
        {
            this._userID = userID;
            this._loginSessionID = loginSessionID;
        }


        public static LoginSession Select(string userID)
        {
            LoginSessionDAL dal = new LoginSessionDAL();
            LoginSession[] sessions = Conversion.SetProperties<LoginSession>(dal.Select(userID));
            return sessions.Length < 1 || sessions == null ? null : sessions[0];
        }

        public void Insert()
        {
            LoginSessionDAL dal = new LoginSessionDAL();
            dal.Insert(this._userID, this._loginSessionID);
        }

        public void Update()
        {
            LoginSessionDAL dal = new LoginSessionDAL();
            dal.Update(this._userID, this._loginSessionID);
        }

        public static void Delete(string userID)
        {
            LoginSessionDAL dal = new LoginSessionDAL();
            dal.Delete(userID);
        }
    }
}
