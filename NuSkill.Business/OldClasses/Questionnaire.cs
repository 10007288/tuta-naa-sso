using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class Questionnaire
    {
        #region properties
        private int _questionnaireID;

        public int QuestionnaireID
        {
            get { return _questionnaireID; }
            set { _questionnaireID = value; }
        }

        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private string _typeCode;

        public string TypeCode
        {
            get { return _typeCode; }
            set { _typeCode = value; }
        }

        private string _question;

        public string Question
        {
            get { return _question; }
            set { _question = value; }
        }

        private string _choice1;

        public string Choice1
        {
            get { return _choice1; }
            set { _choice1 = value; }
        }

        private string _choice2;

        public string Choice2
        {
            get { return _choice2; }
            set { _choice2 = value; }
        }

        private string _choice3;

        public string Choice3
        {
            get { return _choice3; }
            set { _choice3 = value; }
        }

        private string _choice4;

        public string Choice4
        {
            get { return _choice4; }
            set { _choice4 = value; }
        }

        private string _choice5;

        public string Choice5
        {
            get { return _choice5; }
            set { _choice5 = value; }
        }

        private string _choice6;

        public string Choice6
        {
            get { return _choice6; }
            set { _choice6 = value; }
        }

        private string _choice7;

        public string Choice7
        {
            get { return _choice7; }
            set { _choice7 = value; }
        }

        private string _choice8;

        public string Choice8
        {
            get { return _choice8; }
            set { _choice8 = value; }
        }

        private string _choice9;

        public string Choice9
        {
            get { return _choice9; }
            set { _choice9 = value; }
        }

        private string _choice10;

        public string Choice10
        {
            get { return _choice10; }
            set { _choice10 = value; }
        }

        private string _ans1;

        public string Ans1
        {
            get { return _ans1; }
            set { _ans1 = value; }
        }

        private string _ans2;

        public string Ans2
        {
            get { return _ans2; }
            set { _ans2 = value; }
        }

        private string _ans3;

        public string Ans3
        {
            get { return _ans3; }
            set { _ans3 = value; }
        }

        private string _ans4;

        public string Ans4
        {
            get { return _ans4; }
            set { _ans4 = value; }
        }

        private string _ans5;

        public string Ans5
        {
            get { return _ans5; }
            set { _ans5 = value; }
        }

        private string _ans6;

        public string Ans6
        {
            get { return _ans6; }
            set { _ans6 = value; }
        }

        private string _ans7;

        public string Ans7
        {
            get { return _ans7; }
            set { _ans7 = value; }
        }

        private string _ans8;

        public string Ans8
        {
            get { return _ans8; }
            set { _ans8 = value; }
        }

        private string _ans9;

        public string Ans9
        {
            get { return _ans9; }
            set { _ans9 = value; }
        }

        private string _ans10;

        public string Ans10
        {
            get { return _ans10; }
            set { _ans10 = value; }
        }

        private int _essayMaxScore;

        public int EssayMaxScore
        {
            get { return _essayMaxScore; }
            set { _essayMaxScore = value; }
        }
        
        private string _lastUpdated;

        public string LastUpdated
        {
            get { return _lastUpdated; }
            set { _lastUpdated = value; }
        }

        private string _lastUpdatedBy;

        public string LastUpdatedBy
        {
            get { return _lastUpdatedBy; }
            set { _lastUpdatedBy = value; }
        }


        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        private string _hyperLink;

        public string Hyperlink
        {
            get { return _hyperLink; }
            set { _hyperLink = value; }
        }

        private Byte[] _questionImg;

        public Byte[] QuestionImg
        {
            get { return _questionImg; }
            set { _questionImg = value; }
        }

        #endregion

        #region Constructors
        public Questionnaire()
        {
        }

        public Questionnaire(int questionnaireID, int testCategoryID, string typeCode, string question, string choice1, string choice2,
            string choice3, string choice4, string choice5, string choice6, string choice7, string choice8, string choice9,
            string choice10, string ans1, string ans2, string ans3, string ans4, string ans5, string ans6, string ans7, string ans8,
            string ans9, string ans10, int essayMaxScore, string hyperlink, Byte[] questionImg)
        {
            this._questionnaireID = questionnaireID;
            this._testCategoryID = testCategoryID;
            this._typeCode = typeCode;
            this._question = question;
            this._choice1 = choice1;
            this._choice2 = choice2;
            this._choice3 = choice3;
            this._choice4 = choice4;
            this._choice5 = choice5;
            this._choice6 = choice6;
            this._choice7 = choice7;
            this._choice8 = choice8;
            this._choice9 = choice9;
            this._choice10 = choice10;
            this._ans1 = ans1;
            this._ans2 = ans2;
            this._ans3 = ans3;
            this._ans4 = ans4;
            this._ans5 = ans5;
            this._ans6 = ans6;
            this._ans7 = ans7;
            this._ans8 = ans8;
            this._ans9 = ans9;
            this._ans10 = ans10;
            this._essayMaxScore = essayMaxScore;
            this._hyperLink = hyperlink;
            this._questionImg = questionImg;
        }

        public Questionnaire(string typeCode, string question, string choice1, string choice2,
            string choice3, string choice4, string choice5, string choice6, string choice7, string choice8, string choice9,
            string choice10, string ans1, string ans2, string ans3, string ans4, string ans5, string ans6, string ans7, string ans8,
            string ans9, string ans10, int essayMaxScore, string hyperlink, Byte[] questionImg)
        {
            this._typeCode = typeCode;
            this._question = question;
            this._choice1 = choice1;
            this._choice2 = choice2;
            this._choice3 = choice3;
            this._choice4 = choice4;
            this._choice5 = choice5;
            this._choice6 = choice6;
            this._choice7 = choice7;
            this._choice8 = choice8;
            this._choice9 = choice9;
            this._choice10 = choice10;
            this._ans1 = ans1;
            this._ans2 = ans2;
            this._ans3 = ans3;
            this._ans4 = ans4;
            this._ans5 = ans5;
            this._ans6 = ans6;
            this._ans7 = ans7;
            this._ans8 = ans8;
            this._ans9 = ans9;
            this._ans10 = ans10;
            this._essayMaxScore = essayMaxScore;
            this._hyperLink = hyperlink;
            this._questionImg = questionImg;
        }


        public Questionnaire(string typeCode, string question, string choice1, string choice2,
            string choice3, string choice4, string choice5, string choice6, string choice7, string choice8, string choice9,
            string choice10, string ans1, string ans2, string ans3, string ans4, string ans5, string ans6, string ans7, string ans8,
            string ans9, string ans10, string hyperlink, Byte[] questionImg)
        {
            this._typeCode = typeCode;
            this._question = question;
            this._choice1 = choice1;
            this._choice2 = choice2;
            this._choice3 = choice3;
            this._choice4 = choice4;
            this._choice5 = choice5;
            this._choice6 = choice6;
            this._choice7 = choice7;
            this._choice8 = choice8;
            this._choice9 = choice9;
            this._choice10 = choice10;
            this._ans1 = ans1;
            this._ans2 = ans2;
            this._ans3 = ans3;
            this._ans4 = ans4;
            this._ans5 = ans5;
            this._ans6 = ans6;
            this._ans7 = ans7;
            this._ans8 = ans8;
            this._ans9 = ans9;
            this._ans10 = ans10;
            this._hyperLink = hyperlink;
            this._questionImg = questionImg;
        }

        public Questionnaire(int questionnaireID, int testCategoryID, string typeCode, string question, string choice1, string choice2,
            string choice3, string choice4, string choice5, string choice6, string choice7, string choice8, string choice9,
            string choice10, string ans1, string ans2, string ans3, string ans4, string ans5, string ans6, string ans7, string ans8,
            string ans9, string ans10, string hyperlink, Byte[] questionImg)
        {
            this._questionnaireID = questionnaireID;
            this._testCategoryID = testCategoryID;
            this._typeCode = typeCode;
            this._question = question;
            this._choice1 = choice1;
            this._choice2 = choice2;
            this._choice3 = choice3;
            this._choice4 = choice4;
            this._choice5 = choice5;
            this._choice6 = choice6;
            this._choice7 = choice7;
            this._choice8 = choice8;
            this._choice9 = choice9;
            this._choice10 = choice10;
            this._ans1 = ans1;
            this._ans2 = ans2;
            this._ans3 = ans3;
            this._ans4 = ans4;
            this._ans5 = ans5;
            this._ans6 = ans6;
            this._ans7 = ans7;
            this._ans8 = ans8;
            this._ans9 = ans9;
            this._ans10 = ans10;
            this._hyperLink = hyperlink;
            this._questionImg = questionImg;
        }
        #endregion

        #region methods
        public int Insert()
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            return dal.Insert(this._testCategoryID, this._typeCode, this._question, this._choice1, this._choice2,
                this._choice3, this._choice4, this._choice5, this._choice6, this._choice7, this._choice8, this._choice9, this._choice10,
                this._ans1, this._ans2, this._ans3, this._ans4, this._ans5, this._ans6, this._ans7, this._ans8, this._ans9, this._ans10, this._lastUpdated, this._essayMaxScore, this._hideFromList, this._hyperLink, this._questionImg);
        }

        public void Update()
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            dal.Update(this._questionnaireID, this._testCategoryID, this._typeCode, this._question, this._choice1, this._choice2,
                this._choice3, this._choice4, this._choice5, this._choice6, this._choice7, this._choice8, this._choice9, this._choice10,
                this._ans1, this._ans2, this._ans3, this._ans4, this._ans5, this._ans6, this._ans7, this._ans8, this._ans9, this._ans10, this._essayMaxScore, this._hyperLink, this._questionImg);
        }

        public static Questionnaire Select(int questionnaireID, int testCategoryID)
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            Questionnaire[] questions = Conversion.SetProperties<Questionnaire>(dal.Select(questionnaireID, testCategoryID));
            return questions.Length == 0 || questions == null ? null : questions[0];
        }

        public static Questionnaire Select(int questionnaireID, bool includeHidden)
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            Questionnaire[] questions = Conversion.SetProperties<Questionnaire>(dal.Select(questionnaireID, includeHidden));
            return questions.Length == 0 || questions == null ? null : questions[0];
        }

        public static Questionnaire[] SelectByCategory(int testCategoryID)
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            return Conversion.SetProperties<Questionnaire>(dal.SelectByCategory(testCategoryID));
        }

        public static DataSet SelectSuggested()
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            return dal.SelectSuggested();
        }

        public static DataSet SelectSuggestedUser(string username)
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            return dal.SelectSuggestedUser(username);
        }

        public void Delete()
        {
            QuestionnaireDAL dal = new QuestionnaireDAL();
            dal.Delete(this.QuestionnaireID);
        }
        #endregion
    }
}
