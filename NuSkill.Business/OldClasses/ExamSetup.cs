using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;

namespace NuSkill.Business
{
    [Serializable]
    public class ExamSetup
    {
        #region properties
        private int _examSetupID;

        public int ExamSetupID
        {
            get { return _examSetupID; }
            set { _examSetupID = value; }
        }

        private int _testTakenID;

        public int TestTakenID
        {
            get { return _testTakenID; }
            set { _testTakenID = value; }
        }

        private int _examLimit;

        public int ExamLimit
        {
            get { return _examLimit; }
            set { _examLimit = value; }
        }

        private int _timeLimit;

        public int TimeLimit
        {
            get { return _timeLimit; }
            set { _timeLimit = value; }
        }
        #endregion

        #region Constructors
        public ExamSetup()
        {
        }

        public ExamSetup(int testTakenID, int examLimit, int timeLimit)
        {
            this._testTakenID = testTakenID;
            this._examLimit = examLimit;
            this._timeLimit = timeLimit;
        }
        #endregion

        #region methods
        public void Insert()
        {
            ExamSetupDAL dal = new ExamSetupDAL();
            dal.Insert(this._testTakenID, this._examLimit, this._timeLimit);
        }
        #endregion
    }
}
