using System;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business
{
    [Serializable]
    public class TeamLeaders
    {
        private string _teamName;

        public string TeamName
        {
            get { return _teamName; }
            set { _teamName = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private int _cimNumber;

        public int CIMNumber
        {
            get { return _cimNumber; }
            set { _cimNumber = value; }
        }

        private int _employeeID;

        public int EmployeeID
        {
            get { return _employeeID; }
            set { _employeeID = value; }
        }
	

        public static TeamLeaders[] GetTeamLeaders()
        {
            TeamLeadersDal dal = new TeamLeadersDal();
            return Conversion.SetProperties<TeamLeaders>(dal.GetTeamLeaders());
        }
    }
}
