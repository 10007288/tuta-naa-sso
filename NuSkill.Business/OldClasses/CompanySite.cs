using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class CompanySiteForTests
    {
        private string _companySite;

        public string CompanySite
        {
            get { return _companySite; }
            set { _companySite = value; }
        }

        private string _companySiteShort;

        public string CompanySiteShort
        {
            get { return _companySiteShort; }
            set { _companySiteShort = value; }
        }

        public static CompanySiteForTests[] GetCompanySites(int cimnumber)
        {
            CompanySiteForTestsDal dal = new CompanySiteForTestsDal();
            return Conversion.SetProperties<CompanySiteForTests>(dal.GetCompanySites(cimnumber));
        }

    }
}
