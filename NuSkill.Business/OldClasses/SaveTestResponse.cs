using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class SaveTestResponse
    {
        #region properties
        private int _saveTestResponseID;

        public int SaveTestResponseID
        {
            get { return _saveTestResponseID; }
            set { _saveTestResponseID = value; }
        }

        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private int _testTakenID;

        public int TestTakenID
        {
            get { return _testTakenID; }
            set { _testTakenID = value; }
        }

        private int _questionnaireID;

        public int QuestionnaireID
        {
            get { return _questionnaireID; }
            set { _questionnaireID = value; }
        }

        private string _response1;

        public string Response1
        {
            get { return _response1; }
            set { _response1 = value; }
        }

        private string _response2;

        public string Response2
        {
            get { return _response2; }
            set { _response2 = value; }
        }

        private string _response3;

        public string Response3
        {
            get { return _response3; }
            set { _response3 = value; }
        }

        private string _response4;

        public string Response4
        {
            get { return _response4; }
            set { _response4 = value; }
        }

        private string _response5;

        public string Response5
        {
            get { return _response5; }
            set { _response5 = value; }
        }

        private string _response6;

        public string Response6
        {
            get { return _response6; }
            set { _response6 = value; }
        }

        private string _response7;

        public string Response7
        {
            get { return _response7; }
            set { _response7 = value; }
        }

        private string _response8;

        public string Response8
        {
            get { return _response8; }
            set { _response8 = value; }
        }

        private string _response9;

        public string Response9
        {
            get { return _response9; }
            set { _response9 = value; }
        }

        private string _response10;

        public string Response10
        {
            get { return _response10; }
            set { _response10 = value; }
        }

        private string _essayResponse;

        public string EssayResponse
        {
            get { return _essayResponse; }
            set { _essayResponse = value; }
        }

        #endregion

        #region Constructors
        public SaveTestResponse()
        {
        }

        public SaveTestResponse(string userID, int testTakenID, int questionnaireID, string response1, string response2, string response3,
            string response4, string response5, string response6, string response7, string response8, string response9, string response10, string essayResponse)
        {
            this._userID = userID;
            this._testTakenID = testTakenID;
            this._questionnaireID = questionnaireID;
            this._response1 = response1;
            this._response2 = response2;
            this._response3 = response3;
            this._response4 = response4;
            this._response5 = response5;
            this._response6 = response6;
            this._response7 = response7;
            this._response8 = response8;
            this._response9 = response9;
            this._response10 = response10;
            this._essayResponse = essayResponse;
        }
        #endregion

        #region methods
        public void Insert()
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            dal.Insert(this._userID, this._testTakenID, this._questionnaireID, this._response1, this._response2, this._response3,
                this._response4, this._response5, this._response6, this._response7, this._response8, this._response9, this._response10, this._essayResponse);
        }

        public void UpdateAnswers()
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            dal.UpdateAnswers(this._saveTestResponseID, this._response1, this._response2, this._response3, this._response4, this._response5,
                this._response6, this._response7, this._response8, this._response9, this._response10, this._essayResponse);
        }

        public static SaveTestResponse Retrieve(string userID, int testTakenID, int questionnaireID)
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            SaveTestResponse[] responses = Conversion.SetProperties<SaveTestResponse>(dal.Retrieve(userID, testTakenID, questionnaireID));
            return responses.Length < 1 || responses == null ? null : responses[0];
        }

        public static SaveTestResponse[] CheckPendingEssays(string userID, int testTakenID)
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            return Conversion.SetProperties<SaveTestResponse>(dal.CheckPendingEssays(userID, testTakenID));
        }

        public void Delete()
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            dal.Delete(this._saveTestResponseID);
        }

        public static DataSet GetEssays()
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            return dal.GetEssays();
        }

        public static SaveTestResponse GetByID(int saveTestResponseID)
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            SaveTestResponse[] responses =  Conversion.SetProperties<SaveTestResponse>(dal.GetByID(saveTestResponseID));
            return responses.Length < 1 || responses == null ? null : responses[0];
        }

        public static SaveTestResponse[] GetByTestTaken(int testTakenID)
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            return Conversion.SetProperties<SaveTestResponse>(dal.GetByTestTaken(testTakenID));
        }

        public static DataSet GetMultipleEssays(int testTakenID)
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            return dal.GetMultipleEssays(testTakenID);
        }

        public static DataSet GetPendingExamAnswerStatus(int testTakenID)
        {
            SaveTestResponseDAL dal = new SaveTestResponseDAL();
            return dal.GetPendingExamAnswerStatus(testTakenID);
        }
        #endregion
    }
}
