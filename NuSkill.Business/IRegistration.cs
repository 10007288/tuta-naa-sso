using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IRegistration
    {
        int RegistrationID { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        string PhoneNumber { get; set; }
        string Location { get; set; }
        string AutoUserID { get; set; }
        string AutoPassword { get; set; }
        int CimNumber { get; set; }
        DateTime DateCreated { get; set; }

        void Insert();
        bool CheckExists(string firstName, string lastName, string phoneNumber, string location);
        bool ValidateLogin(string autoUserID, string autoPassword);
        IRegistration[] Search(string searchParam);
        IRegistration Select(string userID);
        IRegistration[] SelectAll();
        int LoginToCim(string username, string password);
        int GetCim(string username);
        void InsertByBatch(int batchID, string prefix, int minSeed, int maxSeed, string suffix);
        IRegistration[] GetExistingByBatch(string prefix, int minseed, int maxseed, string suffix);
        IRegistration[] SelectByBatch(int batchID);
        IRegistration[] SearchByBatch(string username, int batchID);
        int SearchEmployee(int cimNumber);
    }
}
