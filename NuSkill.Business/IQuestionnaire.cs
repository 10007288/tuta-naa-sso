using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface IQuestionnaire
    {
        int QuestionnaireID { get; set; }
        IQuestionType TypeCode { get; set; }
        string Question { get; set; }
        string Hyperlink { get; set; }
        string Choice1 { get; set; }
        string Choice2 { get; set; }
        string Choice3 { get; set; }
        string Choice4 { get; set; }
        string Choice5 { get; set; }
        string Choice6 { get; set; }
        string Choice7 { get; set; }
        string Choice8 { get; set; }
        string Choice9 { get; set; }
        string Choice10 { get; set; }
        string Ans1 { get; set; }
        string Ans2 { get; set; }
        string Ans3 { get; set; }
        string Ans4 { get; set; }
        string Ans5 { get; set; }
        string Ans6 { get; set; }
        string Ans7 { get; set; }
        string Ans8 { get; set; }
        string Ans9 { get; set; }
        string Ans10 { get; set; }
        int EssayMaxScore { get; set; }
        bool HideFromList { get; set; }

        int Insert();
        void Update();
        IQuestionnaire Select(int questionnaireID, int testCategoryID);
        IQuestionnaire Select(int questionnaireID, bool includeHidden);
        IQuestionnaire[] SelectByCategory(int testCategoryID);
        DataSet SelectSuggested();
        DataSet SelectSuggestedUser(string username);
        void Delete();
    }
}
