using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface IUserRights
    {
        int UserRightsID { get; set; }
        string UserID { get; set; }
        string Rights { get; set; }
        int RightsLevel { get; set; }
        bool HideFromList { get; set; }

        IUserRights Select(string username, int rightsLevel);
        IUserRights[] SelectAll();
        IUserRights[] SelectAllByUser(string username);
        void Insert();
        void CopyRights(int copyFrom);
        void DeleteRights();
        void EditRights();
        DataSet GetRoles();
    }
}
