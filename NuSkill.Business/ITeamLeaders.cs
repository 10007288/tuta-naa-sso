using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ITeamLeaders
    {
        string TeamName { get; set; }
        string LastName { get; set; }
        int CIMNumber { get; set; }
        int EmployeeID { get; set; }

        ITeamLeaders[] GetTeamLeaders();
    }
}
