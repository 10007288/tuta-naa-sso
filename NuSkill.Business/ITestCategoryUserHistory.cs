using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ITestCategoryUserHistory
    {
        ITestCategory TestCategory { get; set; }
        string TestName { get; set; }
        int TopScore { get; set; }
        int PassingScore { get; set; }
        bool Passed { get; set; }
        int TriesMade { get; set; }
        int MaxTries { get; set; }

        ITestCategoryUserHistory[] SelectAllUserHistory(string usrID, bool includeElapsed, bool recentOnly, bool isCampaign, int campaignAccountID, int subcategory);
        ITestCategoryUserHistory[] SelectAllTestGroup(string userID, bool includeElapsed, bool recentOnly, int testGroupID);

    }
}
