using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IExamSetup
    {
        int ExamSetupID { get; set; }
        ITestTaken TestTaken { get; set; }
        int ExamLimit { get; set; }
        int TimeLimit { get; set; }
        void Insert();
    }
}
