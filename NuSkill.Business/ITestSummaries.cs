using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface ITestSummaries
    {
        ITestSummary[] GetTestSummary(int year, string site, int campaign);
        IGeneralQuery[] GetGeneralQuery(string site, int campaign, DateTime startDate, DateTime endDate);
        ITestsTakenByLocation[] GetTestsTakenByLocation(int year, string site, int startQuarter);
        ITestsTakenCampaign[] GetTestsTakenCmapaign(int year, int campaign, int startQuarter);
        ITestsTakenTSR[] GetTestsTakenTSR(int year, int cimNumber, int startQuarter);
        IPassedFailed[] GetPassedFailed(int year, bool passed, string site, int campaign, int startQuarter);
        ITeamSummaries[] GetTeamSummaries(int team, DateTime startDate, DateTime endDate, int testCategoryID);
        DataSet GetCompanySites(int cimNumber);
    }

    public interface ITestSummary
    {
        int TestCategoryID { get; set; }
        string TestName { get; set; }
        DateTime StartDate { get; set; }
    }

    public interface IGeneralQuery
    {
        string UserID { get; set; }
        string TestName { get; set; }
        int Score { get; set; }
        bool Passed { get; set; }
        DateTime DateStartTaken { get; set; }
        string Office { get; set; }
        int Campaign { get; set; }
    }

    public interface ITestsTakenByLocation
    {
        string UserID { get; set; }
        string TestName { get; set; }
        int Score { get; set; }
        bool Passed { get; set; }
        DateTime DateStartTaken { get; set; }
        string Office { get; set; }
        int Campaign { get; set; }
    }

    public interface ITestsTakenCampaign
    {
        string UserID { get; set; }
        string TestName { get; set; }
        int Score { get; set; }
        bool Passed { get; set; }
        DateTime DateStartTaken { get; set; }
        string Office { get; set; }
        int Campaign { get; set; }
    }

    public interface ITestsTakenTSR
    {
        string UserID { get; set; }
        string TestName { get; set; }
        int Score { get; set; }
        bool Passed { get; set; }
        DateTime DateStartTaken { get; set; }
        string Office { get; set; }
        int Campaign { get; set; }
    }

    public interface IPassedFailed
    {
        string UserID { get; set; }
        string TestName { get; set; }
        int Score { get; set; }
        bool Passed { get; set; }
        DateTime DateStartTaken { get; set; }
        string Office { get; set; }
        int Campaign { get; set; }
    }

    public interface ITeamSummaries
    {
        int CIM { get; set; }
        string FirstName { get; set; }
        string TestName { get; set; }
        string Passed { get; set; }
        string Score { get; set; }
    }
}
