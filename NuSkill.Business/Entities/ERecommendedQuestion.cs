using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business.Entities
{
    [Serializable]
    public class ERecommendedQuestion : IRecommendedQuestion
    {
        private int _questionnaireID;

        public int QuestionnaireID
        {
            get { return _questionnaireID; }
            set { _questionnaireID = value; }
        }

        private IQuestionnaire _questionnaire;

        public IQuestionnaire Questionnaire
        {
            get { return _questionnaire; }
            set { _questionnaire = value; }
        }


        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        private string _createdBy;

        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private string _creatorComments;

        public string CreatorComments
        {
            get { return _creatorComments; }
            set { _creatorComments = value; }
        }

        private int _recommendedQuestionStatusID;

        public int RecommendedQuestionStatusID
        {
            get { return _recommendedQuestionStatusID; }
            set { _recommendedQuestionStatusID = value; }
        }

        private DateTime _dateApproved;

        public DateTime DateApproved
        {
            get { return _dateApproved; }
            set { _dateApproved = value; }
        }

        private string _approvedBy;

        public string ApprovedBy
        {
            get { return _approvedBy; }
            set { _approvedBy = value; }
        }

        private string _approverComments;

        public string ApproverComments
        {
            get { return _approverComments; }
            set { _approverComments = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        public void Insert()
        {
            RecommendedQuestionDal dal = new RecommendedQuestionDal();
            dal.Insert(this._questionnaireID, this._testCategoryID, this._createdBy, this._creatorComments);
        }

        public IRecommendedQuestion SelectByQuestionnaireID(int questionnaireID)
        {
            RecommendedQuestionDal dal = new RecommendedQuestionDal();
            IRecommendedQuestion[] questions = Conversion.SetProperties<IRecommendedQuestion>(dal.SelectByQuestionnaireID(questionnaireID));
            return questions == null || questions.Length < 1 ? null : questions[0];
        }

        public void Update()
        {
            RecommendedQuestionDal dal = new RecommendedQuestionDal();
            dal.Update(this._questionnaireID, this._recommendedQuestionStatusID, this._approverComments, this._approvedBy);
        }
    }
}
