using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business.Entities
{
    [Serializable]
    internal class EAccountList : IAccountList
    {

        private int _accountID;

        public int AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }

        private string _account;

        public string Account
        {
            get { return _account; }
            set { _account = value; }
        }


        public EAccountList()
        { }

        public EAccountList(int accountID, string account)
        {
            this._account = account;
            this._accountID = accountID;
        }

        public IAccountList[] SelectAll(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string>[] dicts = new Dictionary<string, string>[2];
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            dicts[0] = null;
            dicts[1] = dict;

            return Conversion.SetProperties<IAccountList>(dal.SelectAll(showAll), dicts);
        }

        public IAccountList[] SelectAllVader2(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string>[] dicts = new Dictionary<string, string>[2];
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            dicts[0] = null;
            dicts[1] = dict;

            return Conversion.SetProperties<IAccountList>(dal.SelectAllVader2(showAll), dicts);
        }

        public IAccountList[] SelectAccountsVader2(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            return Conversion.SetProperties<IAccountList>(dal.SelectAllVader2(showAll), 0);
        }

        public IAccountList[] SelectGenericCampaignsVader2(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            return Conversion.SetProperties<IAccountList>(dal.SelectAllVader2(showAll), dict, 1);
        }

        public IAccountList[] SelectAccounts(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            return Conversion.SetProperties<IAccountList>(dal.SelectAll(showAll), 0);
        }

        public IAccountList[] SelectGenericCampaigns(int showAll)
        {
            AccountListDal dal = new AccountListDal();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AccountID", "CampaignID");
            dict.Add("Account", "Campaign");
            return Conversion.SetProperties<IAccountList>(dal.SelectAll(showAll), dict, 1);
        }
    }
}
