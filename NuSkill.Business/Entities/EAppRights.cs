using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business.Entities
{
    [Serializable]
    public class EAppRights : IAppRights
    {
        #region Properties
        private int _appRightsID;

        public int AppRightsID
        {
            get { return _appRightsID; }
            set { _appRightsID = value; }
        }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }


        private string _descrption;

        public string Description
        {
            get { return _descrption; }
            set { _descrption = value; }
        }


        #endregion

        #region Constructors
        public EAppRights()
        { }
        #endregion

        #region Methods
        public IAppRights[] SelectAll()
        {
            AppRightsDal dal = new AppRightsDal();
            return Conversion.SetProperties<IAppRights>(dal.SelectAll());
        }
        #endregion
    }
}
