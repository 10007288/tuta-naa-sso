using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business.Entities
{
    [Serializable]
    internal class TestCampaignAccount : ITestCampaignAccount
    {
        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private int _campaignID;

        public int CampaignID
        {
            get { return _campaignID; }
            set { _campaignID = value; }
        }

        private int _accountID;

        public int AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }

        private bool _isCampaign;

        public bool IsCampaign
        {
            get { return _isCampaign; }
            set { _isCampaign = value; }
        }

        public TestCampaignAccount()
        { }

        public TestCampaignAccount(int testCategoryID)
        {
            this._testCategoryID = testCategoryID;
        }


        public ITestCampaignAccount[] SelectAll()
        {
            TestCampaignAccountDal dal = new TestCampaignAccountDal();
            return Conversion.SetProperties<ITestCampaignAccount>(dal.SelectAll());
        }

        public ITestCampaignAccount Select(int testCategoryID, int campaignAccountID, bool isCampaign)
        {
            TestCampaignAccountDal dal = new TestCampaignAccountDal();
            ITestCampaignAccount[] tcas = Conversion.SetProperties<ITestCampaignAccount>(dal.Select(testCategoryID, campaignAccountID, isCampaign));
            return tcas.Length == 0 || tcas == null ? null : tcas[0];
        }

        public ITestCampaignAccount SelectByTestCategory(int testCategoryID)
        {
            TestCampaignAccountDal dal = new TestCampaignAccountDal();
            ITestCampaignAccount[] tcas = Conversion.SetProperties<ITestCampaignAccount>(dal.SelectByTestCategory(testCategoryID));
            return tcas.Length == 0 || tcas == null ? null : tcas[0];
        }

        public ITestCampaignAccount[] SelectByCampaign(int campaignID)
        {
            TestCampaignAccountDal dal = new TestCampaignAccountDal();
            return Conversion.SetProperties<ITestCampaignAccount>(dal.SelectByCampaign(campaignID));
        }

        public ITestCampaignAccount[] SelectByAccount(int accountID)
        {
            TestCampaignAccountDal dal = new TestCampaignAccountDal();
            return Conversion.SetProperties<ITestCampaignAccount>(dal.SelectByAccount(accountID));
        }

        public void Insert()
        {
            TestCampaignAccountDal dal = new TestCampaignAccountDal();
            if (this._isCampaign)
                dal.Insert(this._testCategoryID, this._campaignID, this._isCampaign);
            else
                dal.Insert(this._testCategoryID, this._accountID, this._isCampaign);
        }

        public void Update()
        {
            TestCampaignAccountDal dal = new TestCampaignAccountDal();
            if (this._isCampaign)
                dal.Update(this._testCategoryID, this._campaignID, this._isCampaign);
            else
                dal.Update(this._testCategoryID, this._accountID, this._isCampaign);
        }
    }
}
