using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class ETestSummaries
    {
        public ITestSummary[] GetTestSummary(int year, string site, int campaign)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return Conversion.SetProperties<ITestSummary>(dal.TestSummary(year, site, campaign));
        }

        public IGeneralQuery[] GetGeneralQuery(string site, int campaign, DateTime startDate, DateTime endDate)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return Conversion.SetProperties<IGeneralQuery>(dal.GeneralQuery(site, campaign, startDate, endDate));
        }

        public ITestsTakenByLocation[] GetTestsTakenByLocation(int year, string site, int startQuarter)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return Conversion.SetProperties<ITestsTakenByLocation>(dal.TestsTakenByLocation(year, site, startQuarter));
        }

        public ITestsTakenCampaign[] GetTestsTakenCmapaign(int year, int campaign, int startQuarter)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return Conversion.SetProperties<ITestsTakenCampaign>(dal.TestsTakenCampaign(year, campaign, startQuarter));
        }

        public ITestsTakenTSR[] GetTestsTakenTSR(int year, int cimNumber, int startQuarter)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return Conversion.SetProperties<ITestsTakenTSR>(dal.TestsTakenTSR(year, cimNumber, startQuarter));
        }

        public IPassedFailed[] GetPassedFailed(int year, bool passed, string site, int campaign, int startQuarter)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return Conversion.SetProperties<IPassedFailed>(dal.PassedFailed(year, passed, site, campaign, startQuarter));
        }

        public ITeamSummaries[] GetTeamSummaries(int team, DateTime startDate, DateTime endDate, int testCategoryID)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return Conversion.SetProperties<ITeamSummaries>(dal.TeamSummaries(team, startDate, endDate, testCategoryID));
        }

        public DataSet GetCompanySites(int cimNumber)
        {
            TestSummariesDal dal = new TestSummariesDal();
            return dal.GetCompanySites(cimNumber);
        }
    }

    [Serializable]
    public class ETestSummary : ITestSummary
    {
        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private DateTime _startDate;

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
    }

    [Serializable]
    public class EGeneralQuery : IGeneralQuery
    {
        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private DateTime _dateStartTaken;

        public DateTime DateStartTaken
        {
            get { return _dateStartTaken; }
            set { _dateStartTaken = value; }
        }

        private string _office;

        public string Office
        {
            get { return _office; }
            set { _office = value; }
        }

        private int _campaign;

        public int Campaign
        {
            get { return _campaign; }
            set { _campaign = value; }
        }
    }

    [Serializable]
    public class ETestsTakenByLocation : ITestsTakenByLocation
    {
        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private DateTime _dateStartTaken;

        public DateTime DateStartTaken
        {
            get { return _dateStartTaken; }
            set { _dateStartTaken = value; }
        }

        private string _office;

        public string Office
        {
            get { return _office; }
            set { _office = value; }
        }

        private int _campaign;

        public int Campaign
        {
            get { return _campaign; }
            set { _campaign = value; }
        }
    }

    [Serializable]
    public class ETestsTakenCampaign : ITestsTakenCampaign
    {
        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private DateTime _dateStartTaken;

        public DateTime DateStartTaken
        {
            get { return _dateStartTaken; }
            set { _dateStartTaken = value; }
        }

        private string _office;

        public string Office
        {
            get { return _office; }
            set { _office = value; }
        }

        private int _campaign;

        public int Campaign
        {
            get { return _campaign; }
            set { _campaign = value; }
        }
    }

    [Serializable]
    public class ETestsTakenTSR : ITestsTakenTSR
    {
        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private DateTime _dateStartTaken;

        public DateTime DateStartTaken
        {
            get { return _dateStartTaken; }
            set { _dateStartTaken = value; }
        }

        private string _office;

        public string Office
        {
            get { return _office; }
            set { _office = value; }
        }

        private int _campaign;

        public int Campaign
        {
            get { return _campaign; }
            set { _campaign = value; }
        }
    }

    [Serializable]
    public class EPassedFailed : IPassedFailed
    {
        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private DateTime _dateStartTaken;

        public DateTime DateStartTaken
        {
            get { return _dateStartTaken; }
            set { _dateStartTaken = value; }
        }

        private string _office;

        public string Office
        {
            get { return _office; }
            set { _office = value; }
        }

        private int _campaign;

        public int Campaign
        {
            get { return _campaign; }
            set { _campaign = value; }
        }
    }

    [Serializable]
    public class ETeamSummaries : ITeamSummaries
    {
        private int _cim;

        public int CIM
        {
            get { return _cim; }
            set { _cim = value; }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private string _passed;

        public string Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private string _score;

        public string Score
        {
            get { return _score; }
            set { _score = value; }
        }
    }
}
