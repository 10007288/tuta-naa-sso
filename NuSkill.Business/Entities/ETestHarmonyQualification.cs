using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    internal class ETestHarmonyQualification : ITestHarmonyQualification
    {
        private int _testQualificationID;

        public int TestQualificationID
        {
            get { return _testQualificationID; }
            set { _testQualificationID = value; }
        }

        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private ITestCategory _testCategory;

        public ITestCategory TestCategory
        {
            get { return _testCategory; }
            set { _testCategory = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private int _harmonyQualificationID;

        public int HarmonyQualificationID
        {
            get { return _harmonyQualificationID; }
            set { _harmonyQualificationID = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        public TestHarmonyQualification[] SelectAll()
        {
            TestHarmonyQualificationDal dal = new TestHarmonyQualificationDal();
            return Conversion.SetProperties<TestHarmonyQualification>(dal.SelectAll());
        }

        public TestHarmonyQualification[] SelectByTestCategory(int testCategoryID)
        {
            TestHarmonyQualificationDal dal = new TestHarmonyQualificationDal();
            return Conversion.SetProperties<TestHarmonyQualification>(dal.SelectByTestCategory(testCategoryID));
        }

        public void Insert()
        {
            TestHarmonyQualificationDal dal = new TestHarmonyQualificationDal();
            dal.Insert(this._testCategoryID, this._harmonyQualificationID);
        }

        public void InsertToVader2(string connectionString, int qualificationID, DateTime dateAchieved, DateTime expiryDate, int instructorCimID, string score, int employeeID, int employeeLoggedIn, int passed, string comments, string licenseNumber)
        {
            TestHarmonyQualificationDal dal = new TestHarmonyQualificationDal(connectionString);
            dal.InsertToVader2(qualificationID, dateAchieved, expiryDate, instructorCimID, score, employeeID, employeeLoggedIn, passed, comments, licenseNumber);
        }

    }
}
