using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    internal class ETestTaken : ITestTaken
    {
        #region properties
        private int _testTakenID;

        public int TestTakenID
        {
            get { return _testTakenID; }
            set { _testTakenID = value; }
        }

        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }
        private ITestCategory _testCategory;

        public ITestCategory TestCategory
        {
            get { return _testCategory; }
            set { _testCategory = value; }
        }


        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _office;

        public string Office
        {
            get { return _office; }
            set { _office = value; }
        }

        private DateTime _dateTaken;

        public DateTime DateStartTaken
        {
            get { return _dateTaken; }
            set { _dateTaken = value; }
        }

        private DateTime _dateEndTaken;

        public DateTime DateEndTaken
        {
            get { return _dateEndTaken; }
            set { _dateEndTaken = value; }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private Nullable<DateTime> _dateLastSave;

        public Nullable<DateTime> DateLastSave
        {
            get { return _dateLastSave == null ? Convert.ToDateTime(null) : _dateLastSave.Value; }
            set { _dateLastSave = value; }
        }

        private int _minutesRemaining;

        public int MinutesRemaining
        {
            get { return _minutesRemaining; }
            set { _minutesRemaining = value; }
        }

        private ISaveTestResponse[] _saveTestResponses;

        public ISaveTestResponse[] SaveTestResponses
        {
            get { return _saveTestResponses; }
            set { _saveTestResponses = value; }
        }

        private ITestResponse[] _testResponses;

        public ITestResponse[] TestResponses
        {
            get { return _testResponses; }
            set { _testResponses = value; }
        }

        #endregion

        #region Constructors
        public ETestTaken()
        {
        }

        public ETestTaken(int testCategoryID, string userID, string office, DateTime dateTaken)
        {
            this._testCategoryID = testCategoryID;
            this._userID = userID;
            this._office = office;
            this._dateTaken = dateTaken;
        }
        #endregion

        #region methods
        public int Insert()
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.Insert(this._testCategoryID, this._userID, this._office, this._dateTaken);
        }

        public ITestTaken Select(int testTakenID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            ITestTaken[] tests = Conversion.SetProperties<ITestTaken>(dal.Select(testTakenID));
            return tests == null || tests.Length < 1 ? null : tests[0];
        }

        public DataSet SelectByUser(string userID, bool recentOnly)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.SelectByUser(userID, recentOnly);
        }

        public DataSet SelectByUserPassed(string userID, bool recentOnly, bool passed)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.SelectByUserPassed(userID, recentOnly, passed);
        }

        public void UpdateTime()
        {
            TestTakenDAL dal = new TestTakenDAL();
            dal.UpdateTime(this._testTakenID, this._dateLastSave.Value, this._minutesRemaining);
        }

        public int Count(string userID, int testCategoryID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.Count(userID, testCategoryID);
        }

        public ITestTaken[] SelectByExam(int testCategoryID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return Conversion.SetProperties<ITestTaken>(dal.SelectByExam(testCategoryID));
        }

        public void Finish()
        {
            TestTakenDAL dal = new TestTakenDAL();
            dal.Finish(this._testTakenID, this._dateEndTaken);
        }

        public void SetPassed(bool passed)
        {
            TestTakenDAL dal = new TestTakenDAL();
            dal.SetPassed(this._testTakenID, passed, this._score);
        }

        public ITestTaken[] SelectPendingRating(string userID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return Conversion.SetProperties<ITestTaken>(dal.SelectPendingRating(userID));
        }

        public DataSet SelectCompleted(string userID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.SelectCompleted(userID);
        }

        public DataSet SelectPendingCompletion(string userID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.SelectPendingCompletion(userID);
        }

        public void UpdateScore(int score, int passingScore)
        {
            TestTakenDAL dal = new TestTakenDAL();
            dal.UpdateScore(this._testTakenID, score, passingScore);
        }

        public DataSet SelectTestTakensWithEssays()
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.GetTestTakensWithEssays();
        }

        public DataSet SelectTestTakensWithEssaysByTest(int testcategoryID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.GetTestTakensWithEssaysByTest(testcategoryID);
        }

        public void Delete()
        {
            TestTakenDAL dal = new TestTakenDAL();
            dal.Delete(this._testTakenID);
        }

        public DataSet SelectCheckedEssays(string userID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.GetCheckedEssays(userID);
        }

        public void UpdateScores(int testCategoryID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            dal.UpdateScores(testCategoryID);
        }

        public DataSet SelectEssaysByUser(string userID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.SelectEssaysByUser(userID);
        }

        public DataSet GetResponsesWithChecker(int testTakenID)
        {
            TestTakenDAL dal = new TestTakenDAL();
            return dal.GetResponsesWithChecker(testTakenID);
        }
        #endregion
    }
}
