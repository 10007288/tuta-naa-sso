using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business.Entities
{
    [Serializable]
    public class EUserBatch : IUserBatch
    {
        private int _batchID;

        public int BatchID
        {
            get { return _batchID; }
            set { _batchID = value; }
        }

        private string _batchName;

        public string BatchName
        {
            get { return _batchName; }
            set { _batchName = value; }
        }

        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        public EUserBatch()
        {
        }

        public IUserBatch[] SelectAll()
        {
            UserBatchDal dal = new UserBatchDal();
            return Conversion.SetProperties<IUserBatch>(dal.SelectAll());
        }

        public int Insert(string batchName)
        {
            UserBatchDal dal = new UserBatchDal();
            return dal.Insert(batchName);
        }

        public IUserBatch Select(string batchName)
        {
            UserBatchDal dal = new UserBatchDal();
            IUserBatch[] batch = Conversion.SetProperties<IUserBatch>(dal.SelectBatchName(batchName));
            return batch == null || batch.Length == 0 ? null : batch[0];
        }
    }
}
