using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ITestCategory
    {
        int TestCategoryID { get;set; }
        string TestName { get;set;}
        DateTime StartDate { get;set;}
        DateTime EndDate { get;set;}
        int NumberOfQuestions { get; set; }
        int PassingGrade { get; set; }
        int TimeLimit { get; set; }
        bool IsEssay { get; set; }
        DateTime DateCreated { get; set; }
        DateTime DateLastUpdated { get; set; }
        string LastUpdated { get; set; }
        string LastUpdatedBy { get; set; }
        bool HideFromList { get; set; }
        int CampaignID { get; set; }
        int AccountID { get; set; }
        bool IsCampaign { get; set; }
        int CourseID { get; set; }
        int TestID { get; set; }
        string Category { get; set; }
        string Subcategory { get; set; }
        string Instructions { get; set; }
        bool HideScores { get; set; }
        bool Retake { get; set;}
        bool RandomizeQuestions { get; set; }
        IQuestionnaire[] Questions { get;set; }
        int Insert();
        ITestCategory Select(int testCategoryID, bool includeElapsed);
        ITestCategory[] SelectByAccountCampaign(int accountCampaignID, int subCategory, bool isCampaign, bool includeElapsed);
        ITestCategory[] Search(string searchparam, bool includeElapsed);
        ITestCategory[] SearchByAccountCampaign(string searchParam, int accountCampaignID, int subCategory, bool isCampaign, bool includeElapsed);
        ITestCategory[] SearchByTestGroup(int testGroupID);
        ITestCategory[] SelectAll(bool includeElapsed, bool recentOnly);
        void DeleteByHide();
        void Update();
        bool TestIsEssay(int testCategoryID);
        DataSet GetTeamSummaryTests();
        ITestCategory[] SelectPendingEssayExams();
    }
}
