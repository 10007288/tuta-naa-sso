using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IPresetRights
    {
        int PresetRightsID { get; set; }
        string RightsName { get; set; }
        string PresetRightsValue { get; set; }

        IPresetRights[] SelectAll();
    }
}
