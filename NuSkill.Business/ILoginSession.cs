using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ILoginSession
    {
        string UserID { get; set; }
        Guid LoginSessionID { get; set; }

        ILoginSession Select(string userID);
        void Insert();
        void Update();
        void Delete(string userID);
    }
}
