using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ITestHarmonyQualification
    {
        int TestQualificationID { get; set; }
        ITestCategory TestCategory { get; set; }
        string TestName { get; set; }
        int HarmonyQualificationID { get; set; }
        bool HideFromList { get; set; }

        TestHarmonyQualification[] SelectAll();
        TestHarmonyQualification[] SelectByTestCategory(int testCategoryID);
        void Insert();
        void InsertToVader2(string connectionString, int qualificationID, DateTime dateArchived, DateTime expiryDate, int instructorCimID, string score, int employeeID, int employeeLoggedIn, int passed, string comments, string licenseNumber);
    }
}
