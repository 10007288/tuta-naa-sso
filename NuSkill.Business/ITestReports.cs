using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ITestReports
    {
        int ReportID { get; set; }
        string DisplayName { get; set; }
        string ReportURL { get; set; }
        bool HideFromList { get; set; }

        ITestReports[] SelectAll();
    }
}
