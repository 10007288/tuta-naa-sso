using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface ITestTaken
    {
        int TestTakenID { get; set; }
        ITestCategory TestCategory { get;set; }
        string UserID { get; set; }
        string Office { get; set; }
        DateTime DateStartTaken { get; set; }
        DateTime DateEndTaken { get; set; }
        int Score { get; set; }
        bool Passed { get; set; }
        Nullable<DateTime> DateLastSave { get; set; }
        int MinutesRemaining { get; set; }
        ISaveTestResponse[] SaveTestResponses { get; set; }
        ITestResponse[] TestResponses { get; set; }

        int Insert();
        ITestTaken Select(int testTakenID);
        DataSet SelectByUser(string userID, bool recentOnly);
        DataSet SelectByUserPassed(string userID, bool recentOnly, bool passed);
        void UpdateTime();
        int Count(string userID, int testCategoryID);
        ITestTaken[] SelectByExam(int testCategoryID);
        void Finish();
        void SetPassed(bool passed);
        ITestTaken[] SelectPendingRating(string userID);
        DataSet SelectCompleted(string userID);
        DataSet SelectPendingCompletion(string userID);
        void UpdateScore(int score, int passingScore);
        DataSet SelectTestTakensWithEssays();
        DataSet SelectTestTakensWithEssaysByTest(int testCategoryID);
        void Delete();
        DataSet SelectCheckedEssays(string userID);
        void UpdateScores(int testCategoryID);
        DataSet SelectEssaysByUser(string userID);
        DataSet GetResponsesWithChecker(int testTakenID);
    }
}
